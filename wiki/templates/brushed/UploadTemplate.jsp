<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>

<%
  WikiContext wikiContext = WikiContext.findContext(pageContext);
  int attCount = wikiContext.getEngine().getAttachmentManager()
                          .listAttachments( wikiContext.getPage() ).size();
  String attTitle = "Attach";
  if( attCount != 0 ) attTitle += " (" + attCount + ")";
%>

<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>
  <title><wiki:Variable var="applicationname" />: Add Attachment</title>
  <wiki:Include page="commonheader.jsp"/>
  <meta name="robots" content="noindex">
</head>

<body>

<div id="wikibody" >

  <wiki:Include page="Header.jsp" />
  
  <wiki:Include page="PageActionsTop.jsp"/>

  <div id="page">
    <wiki:TabbedSection defaultTab="attachments" >
      <wiki:Tab id="pagecontent" title="View" accesskey="v">
        <wiki:Include page="PageTab.jsp"/>
      </wiki:Tab>
      <wiki:PageExists>
      <wiki:Tab id="attachments" title="<%= attTitle %>" accesskey="a">
        <wiki:Include page="AttachmentTab.jsp"/>
      </wiki:Tab>
      <wiki:Tab id="pageinfo" title="Info" accesskey="i">
        <wiki:Include page="InfoTab.jsp"/>
      </wiki:Tab>
      </wiki:PageExists>
    </wiki:TabbedSection> 
  </div>
  
  <wiki:Include page="Favorites.jsp"/> 

  <wiki:Include page="PageActionsBottom.jsp"/>

  <wiki:Include page="Footer.jsp" />

  <div style="clear:both; height:0px;" > </div>

</div>
</body>

</html>

