/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWSDLFileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.FileBindingResolver;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestWSDLFileReader extends TestCase
{
    /**
     * DOCUMENT ME!
     */
    private WSDLFileReader mWSDLFileReader;

    /**
     * DOCUMENT ME!
     */
    private WSDLFileValidator mValidator;

    /**
     * Creates a new TestWSDLFileReader object.
     *
     * @param testName   
     */
    public TestWSDLFileReader(java.lang.String testName)
    {
        super(testName);
    }

    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestWSDLFileReader.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
	/*
        mWSDLFileReader = new WSDLFileReader(new FileBindingResolver());
        System.out.println("testInit");

        String srcroot = System.getProperty("junit.srcroot");

        try
        {
            mValidator =
                new WSDLFileValidator(srcroot
                    + "/binding/file/config/endpoints.wsdl");
            mValidator.validate();
            mWSDLFileReader.init(mValidator.getDocument());
        }
        catch (Exception jbiException)
        {
            jbiException.printStackTrace();
            fail("Cannot Init Config Reader");
        }
	*/
    }

    /**
     * Test of getBean method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testGetBean()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getEndpoint method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testGetEndpoint()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getEndpointCount method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testGetEndpointCount()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getError method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testGetError()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getException method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testGetException()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getWarning method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testGetWarning()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of init method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testInit()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isValid method, of class
     * com.sun.jbi.binding.file.util.WSDLFileReader.
     */
    public void testIsValid()
    {
        // TODO add your test code below by replacing the default call to fail.
    }

    // TODO add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
