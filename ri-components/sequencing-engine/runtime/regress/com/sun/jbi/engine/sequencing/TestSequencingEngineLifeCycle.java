/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSequencingEngineLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import junit.framework.*;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSequencingEngineLifeCycle extends TestCase
{
    /**
     * Creates a new TestSequencingEngineLifeCycle object.
     *
     * @param testName
     */
    public TestSequencingEngineLifeCycle(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestSequencingEngineLifeCycle.class);

        return suite;
    }

    /**
     * Test of getDeploymentMBeanName method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineLifeCycle.
     */
    public void testGetDeploymentMBeanName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getExtensionMBeanName method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineLifeCycle.
     */
    public void testGetExtensionMBeanName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of init method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineLifeCycle.
     */
    public void testInit()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of shutdown method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineLifeCycle.
     */
    public void testShutdown()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of start method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineLifeCycle.
     */
    public void testStart()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of stop method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineLifeCycle.
     */
    public void testStop()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
