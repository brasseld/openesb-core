#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)te00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#create a JBI test installation and start it up
echo Starting framework for tests
. ./regress_defs.ksh

# create NON XML Data Feeder Engine jar

cd $SRCROOT/ri-components/transformation-engine/runtime/bld/test-classes/
cp ./data/csvinput.txt .
cp ./data/te_input.xml .

jar cvf feed.jar *.class *.txt *.xml META-INF/jbi.xml

# Install NON XML Data Feeder Engine
$JBI_ANT -Djbi.install.file=$JV_SRCROOT/ri-components/transformation-engine/runtime/bld/test-classes/feed.jar install-component

echo Started the Test framework
exit 0
