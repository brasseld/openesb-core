/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TransformationEngineLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.util.ServiceManager;
import com.sun.jbi.engine.xslt.util.StringTranslator;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.component.ServiceUnitManager;

import javax.jbi.messaging.DeliveryChannel;
import javax.management.ObjectName;

/**
 * This class implements ComponentLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class TransformationEngineLifeCycle
    implements javax.jbi.component.ComponentLifeCycle, TEResources
{
    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private ComponentContext mContext = null;

    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private DeliveryChannel mChannel = null;

    /**
     * Refernce to logger.
     */
    private Logger mLogger = null;

    /**
     * Refernce to Receiver.
     */
    private Receiver mRcvrProcessor = null;

    /**
     * Refernce to service manager.
     */
    private ServiceManager mServiceManager = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     *    
     */
    private TransformationEngineSUManager mSUManager = null;

    /**
     *    
     */
    private boolean mInitSuccess = false;

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }

    public ServiceUnitManager getSUManager()
    {
        return mSUManager;
    }

    //javax.jbi.component.ComponentLifeCycle methods

    /**
     * Initialize the transformation engine. This performs initialization
     * required by  the transformation engine but does not make it ready to
     * process messages.  This method is called immediately after installation
     * of the Transformation engine. It is also called when the JBI framework
     * is starting up, and any time the transformation engine is being
     * restarted after previously being shut down through a call to
     * shutDown().
     *
     * @param context the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext context)
        throws javax.jbi.JBIException
    {
        if (context == null)
        {
            throw (new javax.jbi.JBIException(mTranslator.getString(
                    TEResources.CONTEXT_IS_NULL), new NullPointerException()));
        }

        mContext = context;
        
        //Set ComponentContext on TransformationEngineContext
        TransformationEngineContext teCtx = TransformationEngineContext.getInstance();
        teCtx.setContext(context);
        
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        
        mLogger = TransformationEngineContext.getInstance().getLogger("");
        
        mServiceManager = ServiceManager.getInstance();
        mServiceManager.setComponentContext(mContext);
    
        mSUManager = new TransformationEngineSUManager();
        mSUManager.setContext(mContext);

        mInitSuccess = true;
        mLogger.info(mTranslator.getString(TEResources.INIT_END));
    }

    /**
     * Shutdown the transformation engine. This performs cleanup before the BPE
     * is terminated. Once this method has been called, init() must be called
     * before the transformation engine can be started again with a call to
     * start().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLogger.info(mTranslator.getString(TEResources.TE_SHUTDOWN_START));
        mContext = null;
        mSUManager = null;
        mServiceManager = null;
        mRcvrProcessor = null;
        mLogger.info(mTranslator.getString(TEResources.TE_SHUTDOWN_END));
    }

    /**
     * Start the transformation engine. This makes the BPE ready to  process
     * messages. This method is called after init() completes when the JBI
     * framework is starting up, and when the transformation engine is being
     * restarted after a previous call to shutDown().  If stop() was called
     * previously but shutDown() was not, start() can be called without a call
     * to init().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         start.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLogger.info(mTranslator.getString(TEResources.ENGINE_STARTED));

        if (!mInitSuccess)
        {
            mLogger.severe(mTranslator.getString(TEResources.INIT_FAILED));

            return;
        }

        mChannel = mContext.getDeliveryChannel();

        if (mChannel == null)
        {
            throw (new javax.jbi.JBIException(mTranslator.getString(
                    TEResources.ENGINE_CHANNEL_IS_NULL),
                new NullPointerException()));
        }

        mRcvrProcessor = new Receiver(mContext);

        mServiceManager.setComponentContext(mContext);
        (new Thread(mRcvrProcessor)).start();

        mLogger.info(mTranslator.getString(TEResources.TE_START_END));
    }

    /**
     * Stop the transformation engine. This makes the BPE stop accepting
     * messages for processing. After a call to this method, start() can be
     * called again without first calling init().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLogger.info(mTranslator.getString(TEResources.TE_STOP_START));

        // To add code to stop all services
        try
        {
            mServiceManager.stopAllServices();

            if (mRcvrProcessor != null)
            {
                mRcvrProcessor.cease();
            }

            if (mChannel != null)
            {
                mChannel.close();
            }
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
        }

        mLogger.info(mTranslator.getString(TEResources.TE_STOP_END));
    }
}
