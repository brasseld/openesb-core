#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)getgf.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#download all the appserver kits into separate dirs.

map_gf_port()
#this maps the DOWNLOAD location to our FORTE_PORT
# current download dirs are:  SunOS SunOS_X86 WINNT Linux Darwin
{
#    echo map_gf_port arg is $1 1>&2

    if [ "$1" = "SunOS" ]; then
        echo solsparc
    elif [ "$1" = "SunOS_X86" ]; then
        echo solx86
    elif [ "$1" = "WINNT" ]; then
        echo nt
    elif [ "$1" = "Linux" ]; then
        echo linux
    elif [ "$1" = "Darwin" ]; then
        echo macosx
    else
        echo "NULL"
    fi
}

##################################### MAIN #####################################

p=`basename $0`

[ "$GF_PLATFORMS" = "" ] && GF_PLATFORMS="SunOS SunOS_X86 WINNT Linux Darwin"
[ "$GF_VERSIONS" = "" ] && GF_VERSIONS="v2ur1-b05"

#GF_DOWNLOADS=http://download.java.net/javaee5/promoted
#GF_DOWNLOADS=http://download.java.net/javaee5/sailfin/trunk/promoted/
GF_DOWNLOADS=http://download.java.net/javaee5/v2ur1/promoted/

if [ ! -r $HOME/.wgetrc ]; then
    bldmsg -p $p -warn creating wget proxy settings in $HOME/.wgetrc - comment out if you are not on swan.
    echo "http_proxy=http://testcache.sfbay:8080/" > $HOME/.wgetrc
    cat $HOME/.wgetrc
fi

for version in $GF_VERSIONS
do
    if [ ! -d $SRCROOT/$version ]; then
        mkdir -p  "$SRCROOT/$version"
        cd "$SRCROOT/$version"
        bldmsg -p $p Working in directory $SRCROOT/$version ...
    fi

    for platform in $GF_PLATFORMS
    do
        forte_port=`map_gf_port $platform`
        platform_name=`echo $platform | tr A-Z a-z`
        installer=glassfish-installer-${version}-${platform_name}.jar
        url="$GF_DOWNLOADS/$platform/$installer"

        mkdir -p downloads/$forte_port
        if [ -r downloads/$forte_port/$installer ]; then
            echo "$url" already downloaded to:
            echo "        downloads/$forte_port/$installer"
        else
            echo "$url"
            echo "        -> downloads/$forte_port/$installer"
            wget --quiet --directory-prefix=downloads/$forte_port $url
        fi
    done
done
