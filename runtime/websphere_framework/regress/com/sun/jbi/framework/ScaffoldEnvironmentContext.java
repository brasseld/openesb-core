/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldEnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.platform.PlatformContext;
import java.util.Properties;

/**
 * This is a test class to provide a simpler framework EnvironmentContext implementation
 * for use by the TestDeployer class. This class extends the EnvironmentContext class 
 * and overrides the get/set*Timeout operations.  
 *
 * @author Sun Microsystems, Inc.
 */
public class ScaffoldEnvironmentContext 
    extends com.sun.jbi.framework.EnvironmentContext
    implements com.sun.jbi.EnvironmentContext
    
{
    private long mComponentTimeout;
    private long mInstallationTimeout;
    private long mDeploymentTimeout;
    private long mServiceUnitTimeout;
    
    /**
     * Constructor.
     * @param initialProperties is the Properties object provided by
     * the AppServer, containing all properties specified in the
     * definition of the JBI lifecycle module.
     * @param asInstallRoot is a string representing the installation
     * root directory of the AppServer.
     * @param asInstanceRoot is a string representing the installation
     * root directory of the AppServer instance.
     * @param mbeanServer is the MBean Server to be used for JMX support.
     * @param provider object representing the JBI provider.
     * @param framework object representing the JBI Framework.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    ScaffoldEnvironmentContext(PlatformContext platform,
                       JBIFramework framework,
                       Properties initialProperties)
        throws javax.jbi.JBIException
    {
        super(platform, framework, initialProperties);
        super.setManagementService(new ScaffoldManagementService());
    }
    
 /**
     * Get the timeout value for component life cycle operations.
     * @return The timeout value in milliseconds for calls to the component's
     * ComponentLifeCycle methods.
     */
    long getComponentTimeout()
    {
        return mComponentTimeout;
    }

    /**
     * Get the timeout value for Service Unit deploy/undeploy operations.
     * @return The timeout value in milliseconds for calls to the component's
     * ServiceUnitManager deploy and undeploy methods.
     */
    long getDeploymentTimeout()
    {
        return mDeploymentTimeout;
    }

    /**
     * Get the timeout value for component install/uninstall operations.
     * @return The timeout value in milliseconds for calls to the component's
     * Bootstrap onInstall and onUninstall methods.
     */
    long getInstallationTimeout()
    {   
        return mInstallationTimeout;
    }

    /**
     * Get the timeout value for Service Unit life cycle operations.
     * @return The timeout value in milliseconds for calls to the component's
     * ServiceUnitManager life cycle methods.
     */
    long getServiceUnitTimeout()
    {
        return mServiceUnitTimeout;
    }
    
   /**
     * Set the component timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setComponentTimeout(long timeout)
    {   
        mComponentTimeout = timeout;
    }

    /**
     * Set the deployment timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setDeploymentTimeout(long timeout)
    {
        mDeploymentTimeout = timeout;
    }

    /**
     * Set the installation timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setInstallationTimeout(long timeout)
    {
        mInstallationTimeout = timeout;
    }

    /**
     * Set the service unit timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setServiceUnitTimeout(long timeout)
    {
        mServiceUnitTimeout = timeout;
    }
}
