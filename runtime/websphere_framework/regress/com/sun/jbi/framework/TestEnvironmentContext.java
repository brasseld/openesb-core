/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.platform.PlatformContext;

/**
 * Tests for the EnvironmentContext class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestEnvironmentContext
    extends junit.framework.TestCase
{
    /**
     * The EnvironmentContext.
     */
    private EnvironmentContext mContext;

    /**
     * AS8 install root directory.
     */
    private String mAS8InstallRoot;

    /**
     * AS8 instance root directory.
     */
    private String mAS8InstanceRoot;

    /**
     * JBI install root directory.
     */
    private String mJbiRoot;

    /**
     * JBI instance root directory.
     */
    private String mJbiInstanceRoot;

    /**
     * MBean server.
     */
    private javax.management.MBeanServer mMBeanServer;

    /**
     * Initial properties.
     */
    private java.util.Properties mProperties;

    /**
     * Constant for package name.
     */
    private static final String PACKAGE_NAME = "com.sun.jbi.framework";

    /**
     * The appserver domain root directory.
     */
    private static final String
        DOMAIN_ROOT = "domains/domain1";

    /**
     * The JBI installation root directory.
     */
    private static final String
        JBI_INSTALL_ROOT = "/jbi";

    /**
     * The JNDI naming prefix
     */
    private static final String
        NAMING_PREFIX = "jbi";

    /**
     * Fake property key.
     */
    private static final String
        PROPERTY_KEY1 = "nerbunge";

    /**
     * Fake property value.
     */
    private static final String
        PROPERTY_VALUE1 = "false";

    /**
     * Fake property key.
     */
    private static final String
        PROPERTY_KEY2 = "kegwheat";

    /**
     * Fake property value.
     */
    private static final String
        PROPERTY_VALUE2 = "4096";

    /**
     * Property not found value.
     */
    private static final String
        PROPERTY_NOT_FOUND = "PROPERTY_NOT_FOUND";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestEnvironmentContext(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the data items need to create
     * the EnvironmentContext in the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        // Create an MBean server

        mMBeanServer = javax.management.MBeanServerFactory.createMBeanServer();

        // Set the AppServer and JBI install roots

        mAS8InstallRoot = System.getProperty("junit.as8base") + "/";
        mAS8InstanceRoot = mAS8InstallRoot + DOMAIN_ROOT;
        mJbiInstanceRoot = mAS8InstanceRoot + JBI_INSTALL_ROOT;
 
        // Create the initial properties

        mProperties = new java.util.Properties();
        
        // Create a new instance of the framework with state = ready
        JBIFramework framework = new JBIFramework();
        framework.setFrameworkReady();
        
        // Create a platform context
        ScaffoldPlatformContext platform = new ScaffoldPlatformContext();
        platform.setInstallRoot(mAS8InstallRoot);
        platform.setInstanceRoot(mAS8InstanceRoot);
       
        // Create and initialize the EnvironmentContext.        
        mContext = new EnvironmentContext(platform, framework, mProperties);

        mContext.setRegistry(new ScaffoldRegistry(mContext));
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =========================  test static methods =============================

    /**
     * Test the destroyInstance method. This also tests the getInstance()
     * method for the failure case when the EnvironmentContext has not been
     * cretaed (or in this case, has been destroyed).
     * @throws Exception if an unexpected error occurs.
     */
    public void testDestroyInstance()
        throws Exception
    {
        try
        {
            mContext.destroyInstance();
            EnvironmentContext.getInstance();
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalStateException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("not yet been created")));
        }
    }

    /**
     * Test the get method for the EnvironmentContext singleton instance.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInstance()
        throws Exception
    {
        assertSame("Failed to get EnvironmentContext instance: ",
                   mContext, EnvironmentContext.getInstance());
    }

// ===================  test framework-internal methods =======================

    /**
     * Test the get method for the ComponentFramework.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentFramework()
        throws Exception
    {
        ComponentFramework cf = mContext.getComponentFramework();
        assertNotNull("Failure on getComponentFramework(): " +
                      "expected a non-null value, got a null",
                      cf);
    }

    /**
     * Test the get method for the ComponentRegistry.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentRegistry()
        throws Exception
    {
        ComponentRegistry cr = mContext.getComponentRegistry();
        assertNotNull("Failure on getComponentRegistry(): " +
                      "expected a non-null value, got a null",
                      cr);
    }

    /**
     * Test the get method for the Logger.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLogger()
        throws Exception
    {
        java.util.logging.Logger log = mContext.getLogger();
        assertNotNull("Failure on getLogger(): " +
                      "expected a non-null value, got a null",
                      log);
        assertEquals("Failure on getLogger(): ",
                     log.getName(), PACKAGE_NAME);
    }

    /**
     * Test the get method for the top-level Logger.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetJbiLogger()
        throws Exception
    {
        java.util.logging.Logger log = mContext.getJbiLogger();
        assertNotNull("Failure on getLogger(): " +
                      "expected a non-null value, got a null",
                      log);
        assertEquals("Failure on getLogger(): ",
                     log.getName(), "com.sun.jbi");
    }

// =========================  test public methods =============================

    /**
     * Test the get method for the AppServer installation root directory.
     * @throws Exception if an unexpected error occurs.
    public void testGetAppServerInstallRoot()
        throws Exception
    {
        assertSame("Failure on getAppServerInstallRoot(): ",
                   mAS8Root, mContext.getAppServerInstallRoot());
    }
     */

    /**
     * Test the get method for the AppServer instance root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetAppServerInstanceRoot()
        throws Exception
    {
        assertSame("Failure on getAppServerInstanceRoot(): ",
                   mAS8InstanceRoot, mContext.getAppServerInstanceRoot());
    }

    /**
     * Test the get method for the ComponentManager.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentManager()
        throws Exception
    {
        com.sun.jbi.ComponentManager cm = mContext.getComponentManager();
        assertNotNull("Failure on getComponentManager(): " +
                      "expected a non-null value, got a null",
                      cm);
    }

    /**
     * Test the get method for the ComponentQuery.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetComponentQuery()
        throws Exception
    {
        com.sun.jbi.ComponentQuery cq = mContext.getComponentQuery();
        assertNotNull("Failure on getComponentQuery(): " +
                      "expected a non-null value, got a null",
                      cq);
    }

    /**
     * Test the get method for the ConnectionManager. This is a pass-through
     * to the NMR so a null value is expected, because the NMR is not active
     * for this test.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetConnectionManager()
        throws Exception
    {
        com.sun.jbi.messaging.ConnectionManager cm = mContext.getConnectionManager();
        assertNull("Failure on getConnectionManager(): " +
                   "expected a null value, got a non-null value",
                   cm);
    }

    /**
     * Test the get method for the default log level.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDefaultLogLevel()
        throws Exception
    {
        java.util.logging.Level ll = mContext.getDefaultLogLevel();
        assertEquals("Failure on getDefaultLogLevel(): ",
                     java.util.logging.Level.INFO, ll);
    }

    /**
     * Test the get method for the initial properties.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetInitialProperties()
        throws Exception
    {
        assertSame("Failure on getInitialProperties(): ",
                   mProperties, mContext.getInitialProperties());
    }

    /**
     * Test the get and set methods for the JBI installation root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetJbiInstallRoot()
        throws Exception
    {
        mContext.setJbiInstallRoot(mJbiRoot);
        assertSame("Failure on set/getJbiInstallRoot(): ",
                   mJbiRoot, mContext.getJbiInstallRoot());
    }

    /**
     * Test the get and set methods for the JBI instance root directory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetJbiInstanceRoot()
        throws Exception
    {
        mContext.setJbiInstanceRoot(mJbiInstanceRoot);
        assertSame("Failure on set/getJbiInstanceRoot(): ",
                   mJbiInstanceRoot, mContext.getJbiInstanceRoot());
    }

    /**
     * Test the get method for the ManagementClass.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetManagementClass()
        throws Exception
    {
        assertNotNull("Failure on getManagementClass(): " +
                      "expected a non-null value, got a null",
                      mContext.getManagementClass(
                          com.sun.jbi.management.MBeanNames.
                              SERVICE_NAME_MESSAGE_SERVICE));
    }

    /**
     * Test the get method for the ManagementMessageFactory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetManagementMessageFactory()
        throws Exception
    {
        com.sun.jbi.management.ManagementMessageFactory mmf =
            mContext.getManagementMessageFactory();
        assertNotNull("Failure on getManagementMessageFactory(): " +
                      "expected a non-null value, got a null",
                      mmf);
    }

    /**
     * Test the get method for the ManagementService.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetManagementService()
        throws Exception
    {
        com.sun.jbi.management.system.ManagementService ms =
            mContext.getManagementService();
        assertNotNull("Failure on getManagementService(): " +
                      "expected a non-null value, got a null",
                      ms);
    }

    /**
     * Test the get method for the MBeanHelper.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetMBeanHelper()
        throws Exception
    {
        com.sun.jbi.management.MBeanHelper mbh = mContext.getMBeanHelper();
        assertNotNull("Failure on getMBeanHelper(): " +
                      "expected a non-null value, got a null",
                      mbh);
    }

    /**
     * Test the get method for the MBeanNames.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetMBeanNames()
        throws Exception
    {
        javax.jbi.management.MBeanNames mbn = mContext.getMBeanNames();
        assertNotNull("Failure on getMBeanNames(): " +
                      "expected a non-null value, got a null",
                      mbn);
    }

    /**
     * Test the get and set methods for the JNDI Naming Context.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetNamingContext()
        throws Exception
    {
        javax.naming.InitialContext nc = new javax.naming.InitialContext();
        mContext.setNamingContext(nc);
        assertSame("Failure on get/setNamingContext(): ",
                   nc, mContext.getNamingContext());
    }

    /**
     * Test the get and set methods for the JNDI Naming prefix.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetNamingPrefix()
        throws Exception
    {
        mContext.setNamingPrefix(NAMING_PREFIX);
        assertSame("Failure on get/setNamingPrefix(): ",
                   NAMING_PREFIX, mContext.getNamingPrefix());
    }

    /**
     * Test the get method for the Normalized Message Service.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetNormalizedMessageService()
        throws Exception
    {
        com.sun.jbi.messaging.MessageService nms =
            mContext.getNormalizedMessageService();
        assertNotNull("Failure on getNormalizedMessageService(): " +
                      "expected a non-null value, got a null",
                      nms);
    }

    /**
     * Test the get method for the notification MBean.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetNotifier()
        throws Exception
    {
        EventNotifierCommon fn = mContext.getNotifier();
        assertNotNull("Failure on getNotifier(): " +
                      "expected a non-null value, got a null",
                      fn);
    }

    /**
     * Test the get method for the  JBI provider type.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetProvider()
        throws Exception
    {
        JBIProvider provider = mContext.getProvider();
        assertEquals("Failure on getProvider(): ",
                     JBIProvider.OTHER, provider);
    }

    /**
     * Test the get method for the ServiceUnitRegistration.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetServiceUnitRegistration()
        throws Exception
    {
        com.sun.jbi.ServiceUnitRegistration sur =
            mContext.getServiceUnitRegistration();
        assertNotNull("Failure on getServiceUnitRegistration(): " +
                      "expected a non-null value, got a null",
                      sur);
    }

    /**
     * Test the get method for the StringTranslator for a package name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringTranslator()
        throws Exception
    {
        com.sun.jbi.StringTranslator st1 =
            mContext.getStringTranslator(PACKAGE_NAME);
        com.sun.jbi.StringTranslator st2 =
            mContext.getStringTranslator(PACKAGE_NAME);
        com.sun.jbi.StringTranslator st3 =
            mContext.getStringTranslator("some.other.package");

        assertNotNull("Failure on getStringTranslator(PACKAGE_NAME): " +
                      "expected a non-null value, got a null",
                      st1);
        assertNotNull("Failure on getStringTranslator(some.other.package): " +
                      "expected a non-null value, got a null",
                      st3);
        assertSame("Received different instances of StringTranslator, " +
                   "should have been the same: ",
                   st1, st2);
        assertNotSame("Received same instances of StringTranslator, " +
                      "should have been different: ",
                      st1, st3);
    }

    /**
     * Test the get method for the StringTranslator for an object.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringTranslatorFor()
        throws Exception
    {
        com.sun.jbi.StringTranslator st1 =
            mContext.getStringTranslatorFor(this);
        com.sun.jbi.StringTranslator st2 =
            mContext.getStringTranslatorFor(this);
        com.sun.jbi.StringTranslator st3 =
            mContext.getStringTranslatorFor(mContext.getManagementService());

        assertNotNull("Failure on getStringTranslatorFor(this): " +
                      "expected a non-null value, got a null",
                      st1);
        assertNotNull("Failure on getStringTranslatorFor(ManagementService): " +
                      "expected a non-null value, got a null",
                      st3);
        assertSame("Received different instances of StringTranslator, " +
                   "should have been the same: ",
                   st1, st2);
        assertNotSame("Received same instances of StringTranslator, " +
                      "should have been different: ",
                      st1, st3);
    }

    /**
     * Test the get method for the Tools Runtime Service.
     * @throws Exception if an unexpected error occurs.
     *
    public void testGetToolsRuntimeService()
        throws Exception
    {
        com.sun.jbi.ui.runtime.ToolsRuntimeService trs =
            mContext.getToolsRuntimeService();
        assertNotNull("Failure on getToolsRuntimeService(): " +
                      "expected a non-null value, got a null",
                      trs);
    }*/

    /**
     * Test the get method for the Transaction Manager with a missing
     * PlatformContext.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetTransactionManagerBad()
        throws Exception
    {
        // Test with a missing PlatformContext. This simulates that no
        // TransactionManager was available.
        
        ScaffoldPlatformContext pc = new ScaffoldPlatformContext();
        pc.setTransactionManager((javax.transaction.TransactionManager)null);
        mContext.setPlatformContext(pc);

        assertNull("Failure on getTransactionManager(): " +
                   "expected a null value, got a non-null",
                   mContext.getTransactionManager());
    }
 	     
    /**
     * Test the get method for the Transaction Manager. 
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetTransactionManagerGood()
        throws Exception
    {
        // Test with a PlatformContext set up.

        PlatformContext pc = new ScaffoldPlatformContext();
        mContext.setPlatformContext(pc);

        javax.transaction.TransactionManager tm =
            mContext.getTransactionManager();
        assertNotNull("Failure on getTransactionManager(): " +
                      "expected a non-null value, got a null",
                      tm);
    }
    

    /**
     * Test the get method for the Version Info.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetVersionInfo()
        throws Exception
    {
        com.sun.jbi.VersionInfo vi =
            mContext.getVersionInfo();
        assertNotNull("Failure on getVersionInfo(): " +
                      "expected a non-null value, got a null",
                      vi);
    }

    /**
     * Test the get method for the WSDL factory.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetWsdlFactory()
        throws Exception
    {
        com.sun.jbi.wsdl2.WsdlFactory wf = mContext.getWsdlFactory();
        assertNotNull("Failure on getWsdlFactory(): " +
                      "expected a non-null value, got a null",
                      wf);
    }
    
    /**
     * Test getting the read only registry DOM
     */
    public void testGetReadOnlyRegistry()
        throws Exception
    {
        mContext.setJbiInstanceRoot(mJbiInstanceRoot);
        org.w3c.dom.Document regDoc = mContext.getReadOnlyRegistry();
        assertNotNull(regDoc);
    }
}
