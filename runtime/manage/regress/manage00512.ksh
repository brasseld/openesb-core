#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00512.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
# manage00512 - test the autodeploy functionality in stand-alone
#               mode using Java SE.
# The glassfish appserver version of this test is manage00502.ksh.

testname=`echo "$0" | sed 's/^.*\///' | sed 's/\..*//'`
echo testname is $testname
. ./regress_defs.ksh

autoInstallDir="$JBISE_HOME/server/jbi/autoinstall"
autoDeployDir="$JBISE_HOME/server/jbi/autodeploy"
statusDir="$autoDeployDir/.autodeploystatus"

rm -rf $autoDeployDir; mkdir -p $autoDeployDir

start_jbise &
startInstanceDelay

###### NOTE:  we are copying components from the main jbi installation.  JBI_DOMAIN_PROPS is not availiable until domain1
######        has been started at least once.

echo "Copying appropriate service assembly into autodeploy directory."
ant -q -emacs -DJBI_DOMAIN_ROOT=$JV_JBISE_HOME/server -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml
cp $JBI_HOME/components/jbicalc/installers/calc.jar $autoInstallDir
autoInstallDelay

echo "After autodeploy ..."
echo "File count: $autoDeployDir"
ls -l $autoDeployDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoDeployed area success files"
ls -l $autoDeployDir | grep _deployed | wc -l
ls -l $autoDeployDir | grep _undeployed | wc -l
echo "autoDeployed area failed files"
ls -l $autoDeployDir | grep _notDeployed | wc -l
ls -l $autoDeployDir | grep _notUndeployed | wc -l
echo "recursive listing"
cd $autoDeployDir
find . -type f -print

echo "After autodeploy ..."
$JBISE_ANT list-service-assemblies

# redeploy
chmod +w $autoDeployDir/*.zip
# replace the deployed .zip file with a different one!
cp $JBI_HOME/components/jbicalc/deployments/emptyAU.zip $autoDeployDir/*.zip
autoDeployDelay

echo "After redeploy ..."
echo "File count: $autoDeployDir"
ls -l $autoDeployDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoDeployed area success files"
ls -l $autoDeployDir | grep _deployed | wc -l
ls -l $autoDeployDir | grep _undeployed | wc -l
echo "autoDeployed area failed files"
ls -l $autoDeployDir | grep _notDeployed | wc -l
ls -l $autoDeployDir | grep _notUndeployed | wc -l
echo "recursive listing"
cd $autoDeployDir
find . -type f -print

echo "After redeploy ..."
$JBISE_ANT list-service-assemblies

# clean up
cd $autoDeployDir
rm -f *.zip
autoDeployDelay

# clean up autoinstall as well
rm -f $autoInstallDir/calc.jar
autoInstallDelay

echo "After autoundeploy ..."
echo "File count: $autoDeployDir"
ls -l $autoDeployDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoDeployed area success files"
ls -l $autoDeployDir | grep _deployed | wc -l
ls -l $autoDeployDir | grep _undeployed | wc -l
echo "autoDeployed area failed files"
ls -l $autoDeployDir | grep _notDeployed | wc -l
ls -l $autoDeployDir | grep _notUndeployed | wc -l
echo "recursive listing"
cd $autoDeployDir
find . -type f -print

echo "After undeploy ..."
$JBISE_ANT list-service-assemblies

# stop the JBI framework
shutdown_jbise
stopInstanceDelay
