/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldEnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentManager;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.JBIProvider;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ServiceUnitRegistration;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.management.MBeanHelper;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.util.EnvironmentAccess;
import com.sun.jbi.wsdl2.WsdlFactory;
import com.sun.jbi.wsdl2.WsdlException;
import com.sun.jbi.management.registry.Registry;


/**
 * Provide local implementation of EnvironmentContext for testing.
 * @author Sun Microsystems, Inc.
 */
public class ScaffoldEnvironmentContext implements ComponentManager,
    ComponentQuery,
    EnvironmentContext,
    StringTranslator

{
    private       String      mJbiInstallRoot;
    private       String      mJbiInstanceRoot;
    private final Logger      mLogger;
    private final MBeanServer mMBeanServer;
    private final MBeanNames  mMBeanNames;
    private final MBeanHelper mMBeanHelper;
    private       Properties  mProps;
    private       String      mAppServerInstallRoot;
    private       String      mAppServerInstanceRoot;
    private       ScaffoldPlatformContext mPlatform;
    private       Registry mRegistry;
    /**
     * Dummy constructor.
     */
    public ScaffoldEnvironmentContext()
    {
        EnvironmentAccess.setContext(this);
        mLogger = Logger.getLogger(this.getClass().getName());
        mPlatform = new ScaffoldPlatformContext();
        mMBeanServer = MBeanServerFactory.createMBeanServer("com.sun.jbi");
        mMBeanNames = new com.sun.jbi.management.support.MBeanNamesImpl("com.sun.jbi", "server");
        mMBeanHelper = new com.sun.jbi.management.support.MBeanHelper(
            mMBeanServer,
            mMBeanNames,
            mLogger
        );
        mProps = new Properties();
    }
    
    
    /**
     * Get the platform-specific context for this implementation.
     * @return The PlatformContext.
     */
    public com.sun.jbi.platform.PlatformContext getPlatformContext()
    {
        return mPlatform;
    }

    /**
     * A scaffolded component manager.
     * @return a scaffolded component manager
     */
    public ComponentManager getComponentManager()
    {
        return (ComponentManager) this;
    }
    
    /**
     * Get a reference to the persisted JBI registry.
     * @return Registry instance
     */
    public com.sun.jbi.registry.Registry getRegistry()
    {
        return mRegistry;
    }
    
    /**
     * Get a read-only reference to the persisted JBI Registry. A DOM registry document 
     * object is returned. 
     * @return the registry document
     */
    public org.w3c.dom.Document getReadOnlyRegistry()
    {
        // return null, if the DOM registry is needed then more stuff goes here
        return null;
    }
    
    /**
     * Set a reference to JBI registry.
     * @param  Registry instance
     */
    public void setRegistry(Registry registry)
    {
        mRegistry = registry;
    }
    

    /**
     * A scaffolded component query
     * @return a scaffolded component query object
     */
    public ComponentQuery getComponentQuery()
    {
        return (ComponentQuery) this;
    }
    
    /**
     * A scaffolded component query
     * @return a scaffolded component query object
     */
    public ComponentQuery getComponentQuery(String targetName)
    {
        return (ComponentQuery) this;
    }

/* methods from EnvironmentContext interface */

    /**
     * Get the component ID of the current component.
     * @return String containing the component ID.
     */
    public String getComponentId()
    {
        return null;
    }

    /**
     * Get the component name of the current component.
     * @return String containing the component name.
     */
    public String getComponentName()
    {
        return null;
    }

    /**
     * Get the installation root directory of the specified Binding Component
     * (BC) or Business Process Engine (BPE).
     * @return String containing the installation root directory path
     */
    public String getComponentRoot()
    {
        return null;
    }

    /**
     * Get the Connection Manager handle.
     * @return The ConnectionManager instance.
     */
    public com.sun.jbi.messaging.ConnectionManager getConnectionManager()
    {
        return null;
    }
    
    /**
     * Indicates whether or not the JBI framework has been fully started.  This
     * method provides clients with a way of determining if the JBI framework
     * started up in passive mode as a result of on-demand initialization.  
     * The 'start' parameter instructs the framework to
     * start completely if it has not already done so.  If the framework has
     * already been started, the request to start again is ignored.
     * @param start requests that the framework start completely before
     *  returning.
     * @return true if the framework is completely started, false otherwise.
     */
    public boolean isFrameworkReady(boolean start)
    {
        return true;
    }

    /**
     * Get the MBeanNames service handle.
     * @return MBeanNames the MBeanNames service
     */
    public MBeanNames getMBeanNames()
    {
        return mMBeanNames;
    }

    /**
     * Get the MBeanHelper service handle.
     * @return MBeanHelper the MBeanHelper service
     */
    public MBeanHelper getMBeanHelper()
    {
        return mMBeanHelper;
    }

    /**
     * Get the MBean server used to register all MBeans in the JBI
     * framework.
     * @return javax.management.MBeanServer the MBean server
     */
    public javax.management.MBeanServer getMBeanServer()
    {
        return mMBeanServer;
    }

    /**
     * Get the JNDI naming context.
     * @return javax.naming.InitialContext the JNDI naming context 
     */ 
    public javax.naming.InitialContext getNamingContext()
    {
        return null;
    }

    /**
     * Get the Event Notifier instance.
     * @return The EventNotifier instance.
     */
    public com.sun.jbi.framework.EventNotifierCommon getNotifier()
    {
        return null;
    }

    /**
     * Gets the AppServer installation root directory.
     * @return String the AppServer install root
     */
    public String getAppServerInstallRoot()
    {
        return mAppServerInstallRoot;
    }
    
    /**
     * Gets the AppServer instance root directory.
     * @return String the AppServer instance root
     */
    public String getAppServerInstanceRoot()
    {
        return mAppServerInstanceRoot;
    }
    
    /**
     * Sets the AppServer install root directory.
     * @param asInstallRoot the AppServer install root
     */
    public void setAppServerInstallRoot(String asInstallRoot)
    {
        mAppServerInstallRoot = asInstallRoot;
    }
    
    /**
     * Sets the AppServer instance root directory.
     * @param asInstanceRoot the AppServer instance root
     */
    public void setAppServerInstanceRoot(String asInstanceRoot)
    {
        mAppServerInstanceRoot = asInstanceRoot;
    }

    /**
     * Gets the initial properties specified in the domain.xml file.
     * @return java.util.Properties is the initial properties
     */
    public java.util.Properties getInitialProperties()
    {
       return mProps;
    }
    
    public void setProperty(String key, String value)
    {
        mProps.setProperty(key, value);
    }

    /**
     * Get the ServiceUnitRegistration handle.
     * @return the Service Unit registration instance.
     */
    public ServiceUnitRegistration getServiceUnitRegistration()
    {
        return null;
    }

    /**
     * Get a StringTranslator for a specific package name.
     * @param packageName - the name of the package for which a StringTranslator
     * is being requested.
     * @return The StringTranslator for the named package.
     */
    public StringTranslator getStringTranslator(String packageName)
    {
        return (StringTranslator) this;
    }

    /**
     * Get a StringTranslator for a specific object.
     * @param object - the object for which a StringTranslator is being
     * requested, using the name of the package containing the object.
     * @return The StringTranslator for the object's package.
     */
    public StringTranslator getStringTranslatorFor(Object object)
    {
        return (StringTranslator) this;
    }

    /**
     * Get the VersionInfo for this runtime.
     * @return The VersionInfo instance.
     */
    public com.sun.jbi.VersionInfo getVersionInfo()
    {
        return (com.sun.jbi.VersionInfo) this;
    }

    /**
     * Get the JBI Init time
     * @return the JBI Init time
     */
    public long getJbiInitTime()
    {
     	return System.currentTimeMillis();
    }

    /**
     * Gets the logger for the framework.
     * @return java.util.Logger is the logger
     */
    public Logger getLogger()
    {
        return Logger.getLogger("com.sun.jbi.management");
    }

    /**
     * Get a handle to the class implementing management for the named
     * JBI schemaorg_apache_xmlbeans.system service.
     * @param aServiceName the service name
     * @return Object is the class implementing manangment.
     */
    public Object getManagementClass(String aServiceName)
    {
        return this;
    }

    /**
     * Get the TransactionManager.
     * @return TransactionManager instance.
     */
    public javax.transaction.TransactionManager getTransactionManager()
    {
        return null;
    }

    /**
     * Sets the main MBean server for use by all components.
     * @param mBeanServer is the MBean server
     */
    public void setMBeanServer(javax.management.MBeanServer mBeanServer)
    {
        System.out.println("In scaffolded setMBeanServer.");
        return;
    }

/* methods from ComponentManager interface */

   /**
    * Get the javax.jbi.component.Component instance.
    * @param name - the unique name of the component being retrieved.
    * @return The instance for the requested component or null if the
    * component is not registered or not active.
    */
    public javax.jbi.component.Component getComponentInstance(String name)
    {
        System.out.println("In scaffolded getComponentInstance.");
        return null;
    }

   /**
    * Get the com.sun.jbi.framework.DeployerMBean instance.
    * @param name - the unique name of the component.
    * @return The instance for the requested component or null if the
    * component is not registered or not active.
    */
    public com.sun.jbi.framework.DeployerMBean getDeployerInstance(String name)
    {
        System.out.println("In scaffolded getDeployerInstance.");
        return null;
    }

   /**
    * Install a Shared Library into the JBI framework.
    * @param id - the unique component ID assigned to the Shared Library.
    * @param name - the unique name of the Shared Library.
    * @param description - the description of the Shared Library.
    * @param componentRoot - the root directory for the Shared Library.
    * @param elements - the list of jar and class files included in this
    * shared namespace, as a List of String objects in the order in which they
    * should appear in the class path. Each list element contains the full
    * path to either a jar file or a directory containing class files.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    public void installSharedLibrary(
        String id,
        String name,
        String description,
        String componentRoot,
        List elements)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded installSharedLibrary.");
        return;
    }

   /**
    * Install a Shared Library into the JBI framework.
    * @param name - the unique component ID assigned to the Shared Library.
    * @param description - the description of the Shared Library.
    * @param componentRoot - the root directory for the Shared Library.
    * @param isSelfFirst - set to true to force the class loader to use a
    * self-first hierarchy, false for parent-first.
    * @param elements - the list of jar and class files included in this
    * shared namespace, as a List of String objects in the order in which they
    * should appear in the class path. Each list element contains the full
    * path to either a jar file or a directory containing class files.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    public void installSharedLibrary(
        String name,
        String description,
        String componentRoot,
        boolean isSelfFirst,
        List elements)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded installSharedLibrary.");
        return;
    }

   /**
    * Load a Component bootstrap into the JBI framework.
    * @param installContext is the InstallationContext
    * @param bootClassName is the name of the bootstrap class for the component
    * @param bootClassPathElements is the list of elements comprising the
    * class path for loading the bootstrap class
    * @param snsIdList is the list of Shared Libraries required by the component
    * @param force set to <code>true</code> to ignore class loading failure.
    * @return javax.management.ObjectName containing the MBean object
    * name and class name.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    public javax.management.ObjectName loadBootstrap(
        InstallationContext installContext,
        String bootClassName,
        List bootClassPathElements,
        List snsIdList,
        boolean force)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded loadBootstrap.");
        return null;
    }

   /**
    * Uninstall a Shared Library from the JBI framework. A Shared
    * Namespace cannot be uninstalled until all dependent components
    * have been uninstalled. If any dependent components are found,
    * the uninstall of the Shared Library is aborted.
    * @param name is the unique name of the shared namespace
    * @throws javax.jbi.JBIException if any error occurs.
    */
    public void uninstallSharedLibrary(String name)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded uninstallSharedLibrary.");
    }

   /**
    * Unload a Component bootstrap from the JBI framework.
    * @param componentName is the name of the component
    * @throws javax.jbi.JBIException if any error occurs.
    */
    public void unloadBootstrap(
        String componentName)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded unloadBootstrap.");
        return;
    }

   /**
    * Cancel a pending component update. This is called when some failure
    * has occurred after <code>validateComponentForUpdate()</code> has already
    * been called but before <code>updateComponent()</code> is called. All
    * this method does is return the component to the <code>SHUTDOWN</code>
    * state and remove the busy indicator from the component entry in the
    * runtime registry cache.
    * @param componentName The name of the component.
    * @throws javax.jbi.JBIException if the component does not exist.
    */
    public void cancelComponentUpdate(
        String componentName)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded cancelComponentUpdate.");
    }

   /**
    * Update an installed component. This is used to replace the runtime
    * jar files of a component with a newer version without requiring the
    * component to be uninstalled (which requires undeployment of all Service
    * Assemblies that have Service Units deployed to the component).
    * @param installContext The installation context.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * String containing the full path to either a jar file or a directory
    * containing class files.
    * @throws javax.jbi.JBIException if the update fails for some reason.
    */
    public void updateComponent(
        com.sun.jbi.component.InstallationContext installContext,
        List bootClassPathElements)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded updateComponent.");
    }

   /**
    * Validate a component for update. This is used to validate that the runtime
    * jar files provided for the update contain valid implementations of the
    * required interfaces and the component classes will load correctly.
    * @param installContext The installation context.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * String containing the full path to either a jar file or a directory
    * containing class files.
    * @throws javax.jbi.JBIException if there is a problem with the updated
    * component.
    */
    public void validateComponentForUpdate(
        com.sun.jbi.component.InstallationContext installContext,
        List bootClassPathElements)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded validateComponentForUpdate.");
    }

    /**
     * Cancel a pending component upgrade. This is called when some failure
     * has occurred after <code>validateComponentForUpgrade()</code> has already
     * been called but before <code>upgradeComponent()</code> is called. All
     * this method does is return the component to the <code>SHUTDOWN</code>
     * state and remove the busy indicator from the component entry in the
     * runtime registry cache.
     * @param componentName The name of the component.
     * @throws javax.jbi.JBIException if the component does not exist.
     */
    public void cancelComponentUpgrade(
        String componentName)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded cancelComponentUpgrade.");
    }

   /**
    * Upgrade an installed component. This is used to upgrade a component to a
    * newer version without requiring the component to be uninstalled (which
    * requires undeployment of all Service Assemblies that have Service Units
    * deployed to the component). There are two ways a component can be updated.
    * If the component does not provide an <code>upgrade()</code> method in its
    * bootstrap class, then the runtime jar files are updated and any changes
    * to the component's installation descriptor in jbi.xml are propagated. If
    * the component provides an <code>upgrade()</code> method, that method is
    * called to give the component the opportunity to upgrade its workspace
    * and all SUs deployed to it to a new version. In this case, it is the
    * responsibility of the component to perform all version verification, and
    * to provide any recovery processing required in the event of a failed
    * upgrade.
    *
    * @param installContext The installation context.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * String containing the full path to either a jar file or a directory
    * containing class files.
    * @throws javax.jbi.JBIException if the update fails for some reason.
    */
    public void upgradeComponent(
        com.sun.jbi.component.InstallationContext installContext,
        String bootClassName,
        List<String> bootClassPathElements,
        List<String> sharedLibraryList)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded upgradeComponent.");
    }

   /**
    * Validate a component for upgrade. This validates that the runtime jar
    * files provided for the upgrade contain valid implementations of the
    * required interfaces and the component classes will load correctly. This
    * also verifies that all Shared Library dependencies are met. It also
    * determines whether or not the component has provided an implementation
    * of the <code>upgrade()</code> method and returns an indicator of that to
    * the caller.
    *
    * @param installContext The installation context.
    * @param bootClassName The name of the bootstrap class for the component.
    * @param bootClassPathElements A list of elements comprising the class
    * path for loading the bootstrap class. Each element in the list is a
    * String containing the full path to either a jar file or a directory
    * containing class files.
    * @param sharedLibraryList A list of String objects specifing the names of
    * the Shared Libraries required by the component, in the order in which
    * they should appear in the class path.
    * @return <code>true</code> if the component provided an <code>upgrade()
    * </code> method, <code>false</code> if not.
    * @throws javax.jbi.JBIException if there is a problem with the component
    * implementation classes or with Shared Library dependencies.
    */
    public boolean validateComponentForUpgrade(
        com.sun.jbi.component.InstallationContext installContext,
        String bootClassName,
        List<String> bootClassPathElements,
        List<String> sharedLibraryList)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded validateComponentForUpgrade.");
        return true;
    }

/* methods from ServiceLifecycle interface */

    /**
     * Initialize a service. This performs any initialization tasks
     * required by the service but does not make the service ready
     * to process requests.
     * @param aContext the JBI environment context created
     * by the JBI framework
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void initService(EnvironmentContext aContext)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded init.");
    }

    /**
     * Start a service. This makes the service ready to process requests.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void startService()
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded start.");
    }

    /**
     * Stop a service. This makes the service stop processing requests.
     * @throws javax.jbi.JBIException if an error occurs
     */
    public void stopService()
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded stop.");
    }

    /**
     * Get the management message factory which enables JBI components
     * to construct status and exception messages.
     * @return An instance of ManagementMessageFactory.
     */
    public com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory()
    {
        System.out.println("In scaffolded getManagementMessageFactory.");
        return null;
    }

/* methods from ComponentQuery interface */

   /**
    * Get a list of component IDs for all registered components of a specified
    * type.
    * @param type - the Component type: ComponentInfo.SHARED_NAME_SPACE,
    * ComponentInfo.BINDING, ComponentInfo.ENGINE, ComponentInfo.ALL, or
    * ComponentInfo.BINDINGS_AND_ENGINES. ComponentInfo.ALL includes all
    * components regardless of their types. ComponentInfo.BINDINGS_AND_ENGINES
    * includes both bindings and engines.
    * @return A List of component IDs ( String ) of all registered components
    * of the requested type. Returns an empty list of no components were found.
    */
    public List getComponentIds(ComponentType type)
    {
        System.out.println("In scaffolded getComponentIds.");
        return null;
    }
 
   /**
    * Get a list of component IDs for all components of a specified type with
    * a specified status.
    * @param type - the Component type: ComponentInfo.SHARED_NAME_SPACE,
    * ComponentInfo.BINDING, ComponentInfo.ENGINE, ComponentInfo.ALL, or
    * ComponentInfo.BINDINGS_AND_ENGINES. ComponentInfo.ALL includes all
    * components regardless of their types. ComponentInfo.BINDINGS_AND_ENGINES
    * includes both bindings and engines.
    * @param status - the Component status: ComponentInfo.LOADED,
    * ComponentInfo.INSTALLED, ComponentInfo.STARTED, or ComponentInfo.STOPPED.
    * @return A List of component IDs ( String ) of all registered components
    * of the requested type with the requested status. Returns an empty list if
    * no components were found.
    */
    public List getComponentIds(ComponentType type, ComponentState status)
    {
        System.out.println("In scaffolded getComponentIds.");
        return null;
    }
 
   /**
    * Get the ComponentInfo for a particular component.
    * @param componentId - the unique ID of the component being retrieved.
    * @return The ComponentInfo for the requested component or null if the
    * component is not registered.
    */
    public ComponentInfo getComponentInfo(String componentId)
    {
        System.out.println("In scaffolded getComponentInfo.");
        return null;
    }

   /**
    * Get a list of component IDs that depend upon a specified Shared Library.
    * @param snsId - the unique ID of the Shared Library.
    * @return A list of the component IDs of all components that depend upon the
    * Shared Library. If none are found, the list is empty.
    */
    public List getDependentComponentIds(String snsId)
    {
        System.out.println("In scaffolded getComponentInfo.");
        return null;
    }
 
   /**
    * Get the ComponentInfo for a particular shared library.
    * @param sharedLibraryName - the unique name of the shared library being
    * retrieved.
    * @return The ComponentInfo for the requested shared library or null if the
    * shared library is not registered.
    */
    public ComponentInfo getSharedLibraryInfo(String sharedLibraryName)
    {
        System.out.println("In scaffolded getSharedLibraryInfo.");
        return null;
    }

   /**
    * Get the current status of a component.
    * @param componentId - the unique component ID.
    * @return The current status of the component: ComponentInfo.INSTALLED,
    * ComponentInfo.LOADED, ComponentInfo.STARTED, or ComponentInfo.STOPPED.
    * For a Shared Library, the status is always ComponentInfo.INSTALLED.
    * @throws javax.jbi.JBIException if the component ID is not registered.
    */
    public ComponentState getStatus(String componentId)
        throws javax.jbi.JBIException
    {
        System.out.println("In scaffolded getStatus.");
        return ComponentState.STOPPED;
    }

/* methods from StringTranslator interface */

   /**
    * Get a localized string using the specified resource key.
    * @param key - the key to the localized string in the resource bundle.
    * @return the localized string.
    */
    public String getString(
        String key)
    {
        return "Scaffolded String is" + key;
    }

   /**
    * Get a localized string using the specified resource key. Handle one
    * message insert.
    * @param key - the key to the localized string in the resource bundle.
    * @param insert1 - the message insert.
    * @return the localized string formatted with the message insert.
    */
    public String getString(
        String key,
        Object insert1)
    {
        return "Scaffolded String is" + key; 
    }

   /**
    * Get a localized string using the specified resource key. Handle two
    * message inserts.
    * @param key - the key to the localized string in the resource bundle.
    * @param insert1 - the first message insert.
    * @param insert2 - the second message insert.
    * @return the localized string formatted with the message inserts.
    */
    public String getString(
        String key,
        Object insert1,
        Object insert2)
    {
        return "Scaffolded String is" + key;
    }

   /**
    * Get a localized string using the specified resource key. Handle three
    * message inserts.
    * @param key - the key to the localized string in the resource bundle.
    * @param insert1 - the first message insert.
    * @param insert2 - the second message insert.
    * @param insert3 - the third message insert.
    * @return the localized string formatted with the message inserts.
    */
    public String getString(
        String key,
        Object insert1,
        Object insert2,
        Object insert3)
    {
        return "Scaffolded String is" + key;
    }

   /**
    * Get a localized string using the specified resource key. Handle four
    * message inserts.
    * @param key - the key to the localized string in the resource bundle.
    * @param insert1 - the first message insert.
    * @param insert2 - the second message insert.
    * @param insert3 - the third message insert.
    * @param insert4 - the fourth message insert.
    * @return the localized string formatted with the message inserts.
    */
    public String getString(
        String key,
        Object insert1,
        Object insert2,
        Object insert3,
        Object insert4)
    {
        return "Scaffolded String is" + key;
    }

   /**
    * Get a localized string using the specified resource key. Handle any
    * number of message inserts.
    * @param key - the key to the localized string in the resource bundle.
    * @param inserts - the array of message inserts.
    * @return the localized string formatted with the message inserts.
    */
    public String getString(
        String key,
        Object[] inserts)
    {
        return "Scaffolded String is" + key;
    }
    
   /**
    * Return a String containing the result of a printStackTrace() on an
    * exception.
    * @param ex the exception for which a stack trace is requested.
    * @return the stack trace in a String.
    */
    public String stackTraceToString(Throwable ex)
    {
        return "";
    }
    
    public JBIProvider getProvider()
    {
        return JBIProvider.OTHER;
    }

    public WsdlFactory getWsdlFactory() throws WsdlException
    {
        return null;
    }

    public String getJbiInstallRoot ()
    {
        return mJbiInstallRoot;
    }
    
    public void setJbiInstallRoot(String jbiRoot)
    {
        mJbiInstallRoot = jbiRoot;
    }
    
    public String getJbiInstanceRoot ()
    {
        return mJbiInstanceRoot;
    }
    
    public void setJbiInstanceRoot(String jbiRoot)
    {
        mJbiInstanceRoot = jbiRoot;
    }
    
    /**
     * This method is used to scaffold EnvironmentContext.isStartOnDeployEnabled()
     * and always returns true
     */
    public boolean isStartOnDeployEnabled()
    {
        return true;
    }
    
    /**
     * This method is used to find out if start-onverify is enabled.
     * When this is enabled components are started automatically when 
     * an application has to be verified for them 
     * This is controlled by the property com.sun.jbi.startOnVerify.
     * By default start-on-verify is enabled. 
     * It is disabled only if com.sun.jbi.startOnVerify=false.
     */
    public boolean isStartOnVerifyEnabled()
    {
        return true;
    }       
}
