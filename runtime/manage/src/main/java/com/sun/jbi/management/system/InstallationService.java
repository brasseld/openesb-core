/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationService.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import com.sun.jbi.ComponentManager;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.management.ComponentInstallationContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.descriptor.SharedLibraryDescriptor;

import com.sun.jbi.management.internal.support.JarFactory;
import com.sun.jbi.management.internal.support.XmlReader;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.support.JbiNameInfo;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.RepositoryException;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.util.FileHelper;

import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.ObjectName;

import javax.jbi.JBIException;

/**
 * The installation service MBean allows administrative tools to manage
 * component and shared library installations.  The tasks supported are:
 * <ul>
 *   <li>Installing (and uninstalling) a shared library</li>
 *   <li>Creating (loading) and destroying (unloading) a component installer
 *       MBean.</li>
 *   <li>Finding an existing component installer MBean.
 * </ul>
 *
 * Installing and uninstalling components is accomplished using
 * {@link InstallerMBean}s, loaded by this MBean.  An individual installer MBean
 * is needed for each component installation / uninstallation.  This is to support 
 * the more complex installation process that some components require.
 *
 * @author JSR208 Expert Group
 */
public class InstallationService extends ModelSystemService
    implements com.sun.jbi.management.InstallationServiceMBean
{
    /**
     * path to the jbi.xml file
     */
    private static final String PATH_TO_JBI_XML_FILE = "META-INF/jbi.xml";
    
    /* Name of install root directory. */
    private static final String INSTALL_ROOT = "install_root";
    
    /** the workspace directory **/
    private static final String WORKSPACE = "workspace";    
    
    /** the dir where component logger settings are stored **/
    private static final String LOGGER_CONFIG = "config";        

    /** the delete me marker in repository **/
    private static final String DELETE_ME = ".DELETE_ME";        
    
    /** the repository service assembly store **/
    private static final String REPOSITORY_SERVICE_ASSEMBLY_STORE = "service-assemblies";
    
    /** the tmp dir in JBI instance root**/
    private static final String TMP_DIR = "tmp";
    
    /** the tmp dir name used to extract the component to call framework validate for upgrade **/
    private static final String UPGRADE_TMP_DIR = "tmp_install_root";
    
    /** underscore used in dir names to save copies of install root during upgrade**/
    private static final String UNDERSCORE = "_";
    
    /** dir name used to save the SUs deployed to a component during component upgrade **/
    private static final String SU_BACKUP = "su";
     
    /**
     * JAR URL Protocol.
     */
    private static final String JAR = "jar";
    
    /**
     * FILE URL Protocol.
     */
    private static final String FILE = "file";

    /**
     * size of array, in bytes, to allocate
     */
    private static final int BYTE_ARRAY_SIZE = 4096;

    /** our immutable name: */
    private final JbiNameInfo mJbiNameInfo =
            new JbiNameInfo("InstallationService");

    /**
     * Management context
     */
    private ManagementContext mContext = null;

    /**
     * Management Message Object
     */
    private BuildManagementMessageImpl mMMImpl = null;    
        
    /**
     * Constructs a InstallationService.
     * @param anEnv is the ManagementContext.
     */
    public InstallationService(ManagementContext anEnv)
        throws javax.jbi.JBIException
    {
        mContext = anEnv;

        /*
         * Local initialization of this service.
         * Local routine is responsible for calling super.initModelSystemService(..).
         */
        initModelSystemService(anEnv.getEnvironmentContext());

        mMMImpl = mContext.getManagementMessageObject();
    }

    /**
     * local model init - called by constructor - create custom mbeans.
     * @param anEnv is the ManagementContext
     */
    protected void initModelSystemService(com.sun.jbi.EnvironmentContext anEnv)
    {
        String loggerName = 
            com.sun.jbi.management.config.LoggerConfigurationFactory.INSTALLATION_LOGGER;
       
        Logger logger = Logger.getLogger(loggerName);
        
        //initialize the super.
        super.initModelSystemService(anEnv, logger, mJbiNameInfo);

        //add InstallationService MBean to START/STOP mbean set:
        mStartMBeans.add(mInstallServiceMBeanName,
            com.sun.jbi.management.InstallationServiceMBean.class, this);
    }


    /**
     * @param aJarFile jar file to be unjared.
     * @param aFileToFind String specifying which file to extract.
     * @return a File (created by CreateTempFile) with that one file.
     * @throws javax.jbi.JBIException if an error occurs
     */
    private File unjar(JarFile aJarFile,
                      String aFileToFind)
        throws javax.jbi.JBIException
    {
        if (null == aJarFile)
        {
            String errMsg = mTranslator.getString(LocalStringKeys.IS_JAR_FILE_NULL);
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("unjar",
                "IS_JAR_FILE_NULL", errMsg);
            throw new javax.jbi.JBIException(jbiTaskStr);
        }
        
        java.util.Enumeration entries = aJarFile.entries();
        byte[] buffer = new byte[BYTE_ARRAY_SIZE];
        while (entries.hasMoreElements())
        {
            JarEntry entry = (JarEntry) entries.nextElement();
            FileOutputStream fos = null;
            InputStream is = null;

            if (!entry.isDirectory() &&
                (entry.getName().equals(aFileToFind)))
            {
                try
                {
                    File retFile = File.createTempFile("jbi", ".xml");
                    fos = new FileOutputStream(retFile);
                    is = aJarFile.getInputStream(entry);
                    int len = is.read(buffer);
                    while (0 < len)
                    {
                        fos.write(buffer, 0, len);
                        len = is.read(buffer);
                    }
                    is.close();
                    is = null;
                    return retFile;
                }
                catch (java.io.IOException ioEx)
                {
                    String errMsg = mTranslator.getString(
                                             LocalStringKeys.IS_IO_EXCEP,
                                             ioEx.getMessage());
                    mLogger.fine(errMsg);
                    String jbiTaskStr = managementMessageExcep("unjar",
                        "IS_IO_EXCEP", errMsg, ioEx);
                    throw new javax.jbi.JBIException(jbiTaskStr);
                }
                finally
                {
                    try
                    {
                        if (null != is)
                        {
                            is.close();
                        }
                        if (null != fos)
                        {
                            fos.close();
                        }
                    }
                    catch (java.io.IOException ioEx)
                    {
                        mLogger.finest(mTranslator.getString(
                                LocalStringKeys.IS_IO_EXCEP, ioEx.getMessage()));
                    }
                }
            }
        }
        return null;
    }

    /**
     * @param aJarFile jar file to be unjared.
     * @param aDirectory File (directory) where to unjar to.
     * @throws javax.jbi.JBIException if an error occurs
     */
    private void unjar(JarFile aJarFile,
                      File aDirectory)
        throws javax.jbi.JBIException
    {
        java.util.Enumeration entries = aJarFile.entries();
        byte[] buffer = new byte[BYTE_ARRAY_SIZE];
        while (entries.hasMoreElements())
        {
            JarEntry entry = (JarEntry) entries.nextElement();
            FileOutputStream fos = null;
            InputStream is = null;

            if (entry.isDirectory())
            {
                File subdir = new File(aDirectory, entry.getName());
                if (!subdir.mkdir())
                {
                    String logMessage = mTranslator.getString(
                        LocalStringKeys.IS_DEBUG_OUTPUT,
                        "Could not create subdirectory " + entry.getName());
                    mLogger.fine(logMessage);
                }
            }
            else
            {
                try
                {
                    File newFile = new File(aDirectory, entry.getName());
                    fos = new FileOutputStream(newFile);
                    is = aJarFile.getInputStream(entry);
                    int len = is.read(buffer);
                    while (0 < len)
                    {
                        fos.write(buffer, 0, len);
                        len = is.read(buffer);
                    }
                    is.close();
                    is = null;
                }
                catch (java.io.IOException ioEx)
                {
                    String errMsg = mTranslator.getString(LocalStringKeys.IS_IO_EXCEP,
                                             ioEx.getMessage());
                    mLogger.fine(errMsg);
                    String jbiTaskStr = managementMessageExcep("unjar",
                        "IS_IO_EXCEP", errMsg, ioEx);
                    throw new javax.jbi.JBIException(jbiTaskStr);
                }
                finally
                {
                    try
                    {
                        if (null != is)
                        {
                            is.close();
                        }
                        if (null != fos)
                        {
                            fos.close();
                        }
                    }
                    catch (java.io.IOException ioEx)
                    {
                        String errMsg = mTranslator.getString(LocalStringKeys.IS_IO_EXCEP,
                                             ioEx.getMessage());
                        mLogger.fine(errMsg);
                    }
                }
            }
        }
    }

/* methods from installationServiceMBean */

    /**
     * Load the installer for a new component for the given component 
     * installation package.
     *
     * @param installZipURL URL locating a ZIP file containing the
     *        JBI Installation package to be installed; must be non-null, 
     *        non-empty, and a legal URL
     * @return the JMX ObjectName of the InstallerMBean loaded from
     *         installZipURL; must be non-null
     */
    public synchronized ObjectName loadNewInstaller(String installZipURL)
    {
        ObjectName      mbName;
        String          compName = null;
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "loadNewInstaller", "(" + installZipURL + ")"));
        
        try
        {  
            URL compURL = convertToProperURL(installZipURL);
            if ( !mEnv.getPlatformContext().isAdminServer() )
            {
                /**
                 * (1). Add the archive to the local registry / repository 
                 */
                //File ar = new File(compURL.getFile());
                File ar = new File(compURL.toURI());
                    
                //File ar = new File(compURL.toString());
                Archive compArchive = new Archive(ar, false);
                getRegistry().getRepository().addArchive(compArchive);
                compName = compArchive.getJbiName();
                getRegistry().getUpdater().addComponent(compArchive.getJbiName(), compArchive.getFileName(), compArchive.getUploadTimestamp());
                String path = compArchive.getPath();
                compURL = convertToProperURL(new File(path).toURL().toString());
                ar.delete();
            }

            mbName = bootStrapComponent(compURL);            
            
            mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_EXITED,
                    "loadNewInstaller", "(" + installZipURL + ") -> " + mbName));

            return mbName;            
        }
        catch (java.net.MalformedURLException URLsEx)
        {
            String errMsg = mTranslator.getString(
                               LocalStringKeys.IS_URL_EXCEP, URLsEx.getMessage());
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("loadNewInstaller",
                "IS_URL_EXCEP", errMsg, URLsEx);

            throw new RuntimeException(jbiTaskStr);
        }
        catch (java.net.URISyntaxException uriEx)
        {
            String errMsg = mTranslator.getString(
                               LocalStringKeys.IS_URL_EXCEP, uriEx.getMessage());
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("loadNewInstaller",
                "IS_URL_EXCEP", errMsg, uriEx);
            
            throw new RuntimeException(jbiTaskStr);
        }
        catch ( java.io.IOException  ioex)
        {
            String errMsg = mTranslator.getString(
                               LocalStringKeys.IS_IO_EXCEP, ioex.getMessage());
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("loadNewInstaller",
                "IS_IO_EXCEP", errMsg, ioex);

            throw new RuntimeException(jbiTaskStr);
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            if ( !mEnv.getPlatformContext().isAdminServer() )
            {
                /**
                 * (1). Remove the archive to the local registry / repository
                 */
                try
                {
                    // Remove from Repos
                    getRegistry().getRepository().removeArchive(ArchiveType.COMPONENT, compName);
                    // Remove from Registry
                    getRegistry().getUpdater().removeComponent("domain", compName);
                }
                catch (Exception exp)
                {
                    mLogger.fine(MessageHelper.getMsgString(exp));
                    throw new RuntimeException(exp.getMessage());
                }
            }
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw new RuntimeException(jbiEx.getMessage());
        }     
    }

    /**
     * Load the InstallerMBean for a previously installed component.
     * <p>
     * The "component name" refers to the 
     * <code>&lt;identification>&lt;name></code> element value from the 
     * component's installation package (see {@link #loadNewInstaller(String)}).
     * 
     * @param componentName the component name identifying the installer to 
     *        load; must be non-null and non-empty
     * @return the JMX ObjectName of the InstallerMBean loaded from an existing
     *         installation context; <code>null</code> if the installer MBean
     *         doesn't exist
     */
    public ObjectName loadInstaller(String componentName)
    {
        return loadInstaller(componentName, false);
    }

    /**
     * Load the InstallerMBean for a previously installed component.
     * <p>
     * The "component name" refers to the 
     * <code>&lt;identification>&lt;name></code> element value from the 
     * component's installation package (see {@link #loadNewInstaller(String)}).
     * 
     * @param componentName the component name identifying the installer to 
     *        load; must be non-null and non-empty
     * @param force set to <code>true</code> when this is being called as part
     *        of a forced uninstall, causing a failure to load the component's
     *        bootstrap class to be ignored.
     * @return the JMX ObjectName of the InstallerMBean loaded from an existing
     *         installation context; <code>null</code> if the installer MBean
     *         doesn't exist
     */
    public synchronized ObjectName loadInstaller(String componentName, boolean force)
    {
        ObjectName mbName = null;
        
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "loadInstaller", "(" + componentName + "," + force + ")"));
        
        try
        {
            // If component is not installed on the instance return null. 
            if ( mContext.getComponentQuery().getComponentInfo(componentName) == null )
            {
                mLogger.warning(mTranslator.getString(LocalStringKeys.IS_COMPONENT_NOT_INSTALLED,
                    componentName));
            }
            else
            {
                mbName = reloadInstaller(componentName, force);
            }
            mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_EXITED,
                        "loadInstaller", "(" + componentName + ") -> " + mbName));
            return mbName;
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw new RuntimeException(jbiEx.getMessage());
        }
    }

    /**
     * Unload an InstallerMBean previously loaded for a component.
     *
     * @param componentName the component name identifying the installer to 
     *        unload; must be non-null and non-empty
     * @param isToBeDeleted <code>true</code> if the component is to be deleted 
     *        as well
     * @return true if the operation was successful, otherwise false
     */
    public synchronized boolean unloadInstaller(String componentName, boolean isToBeDeleted)
    {
        ObjectName  mbName;
        
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "unloadInstaller", "(" + componentName + "," + isToBeDeleted + ")"));

        try
        {
            // unload the loaded component bootstrap
            ComponentManager framework = mContext.getComponentManager();
            mLogger.fine("Unloading bootstrap for component " + componentName);
            framework.unloadBootstrap(componentName);
            
            if (isToBeDeleted && !mEnv.getPlatformContext().isAdminServer())
            {
                // verify that the component has been uninstalled
                if (mContext.getComponentQuery().getComponentInfo(componentName) != null)
                {
                    mLogger.warning(mTranslator.getString(
                            LocalStringKeys.IS_COMPONENT_NOT_UNINSTALLED, componentName));
                    return false;
                }

                getRegistry().getRepository().removeArchive(ArchiveType.COMPONENT, componentName);
                getRegistry().getUpdater().removeComponent("domain", componentName);
            }
        } 
        catch (javax.jbi.JBIException jbiEx)
        {
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw new RuntimeException(jbiEx.getMessage());
        }
        
        
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_EXITED,
                "unloadInstaller", "(" + componentName + "," + isToBeDeleted + ") -> TRUE"));
        
        return true;
    }

    /**
     * Install a shared library installation package.
     * <p>
     * The return value is the unique name for the shared-library, as found
     * in the the value of the installation descriptor's 
     * <code>&lt;identification>&lt;name></code> element.
     * 
     * @param slZipURL URL locating a zip file containing a shared library
     *        installation package; must be non-null, non-empty, and a legal
     *        URL
     * @return the unique name of the shared library loaded from slZipURL; must
     *         be non-null and non-empty
     */
    public synchronized String installSharedLibrary(String slZipURL)
    {
        String slName;
        
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "installSharedLibrary", "(" + slZipURL + ")"));
        
        try
        {
            URL theURL = convertToProperURL(slZipURL);
            
            if ( !mEnv.getPlatformContext().isAdminServer() )
            {
                /**
                 * (1). Add the archive to the local registry / repository 
                 */
                
                File ar = new File(theURL.toURI());
                //File ar = new File(theURL.getFile());
                Archive slArchive = new Archive(ar, false);
                getRegistry().getRepository().addArchive(slArchive);
                getRegistry().getUpdater().addSharedLibrary(slArchive.getJbiName(), 
                    slArchive.getFileName(), slArchive.getUploadTimestamp());
                String path = slArchive.getPath();
                theURL = convertToProperURL(new File(path).toURL().toString());
                ar.delete();
                
            }
            
            // -- At this point the SharedLibrary install root is created
            // -- Just need to install
            slName = installSharedLibraryToFramework(theURL);
            mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_EXITED,
                    "installSharedLibrary", "(" + slZipURL + ") -> " + slName));
            
            return slName;
        }
        catch (java.io.IOException URLsEx)
        {
            String errMsg = mTranslator.getString(
                               LocalStringKeys.IS_URL_EXCEP, URLsEx.getMessage());
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("installSharedLibrary",
                "IS_URL_EXCEP", errMsg, URLsEx);
            throw new RuntimeException(jbiTaskStr);
        }
        catch (java.net.URISyntaxException uriEx)
        {
            String errMsg = mTranslator.getString(
                               LocalStringKeys.IS_URL_EXCEP, uriEx.getMessage());
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("loadNewInstaller",
                "IS_URL_EXCEP", errMsg, uriEx);
            throw new RuntimeException(jbiTaskStr);
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw new RuntimeException(jbiEx.getMessage());
        }
    }


    /**
     * Uninstall a previously installed shared library.
     *
     * @param slName the name of the shared name space to uninstall; must be
     *        non-null and non-empty
     * @return true if the uninstall was successful
     */
    public synchronized boolean uninstallSharedLibrary(String slName)
    {
        Exception   warning         = null;
        
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "uninstallSharedLibrary", "(" + slName + ")"));
        
        try
        {       
            ComponentManager framework = mContext.getComponentManager();
            framework.uninstallSharedLibrary(slName);
        }
        catch (com.sun.jbi.framework.FrameworkWarningException fwEx)
        {
            warning = fwEx;
            // log this mLogger.warning below.
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw new RuntimeException(jbiEx.getMessage());
            // return false;
        }

        if ( !mEnv.getPlatformContext().isAdminServer() )
        {
            /** 
             * If it is not the DAS, then the shared library should be removed from the
             * local registry / repository
             *
             */
            try
            {
                getRegistry().getRepository().removeArchive(ArchiveType.SHARED_LIBRARY, slName);
                getRegistry().getUpdater().removeSharedLibrary("domain", slName);
            }
            catch (Exception ex)
            {
                warning = ex;
            }
        }
        
        if (null != warning)
        {
            String message = warning.getMessage();
            String errMsg = mTranslator.getString(LocalStringKeys.WARNING, message);
            mLogger.fine(errMsg);
            ManagementMessageHolder mmHolder =
                new ManagementMessageHolder("STATUS_MSG");
            mmHolder.setFrameworkLocale ("en_US");
            mmHolder.setTaskName ("uninstallSharedLibrary");
            mmHolder.setTaskResult("SUCCESS");
            mmHolder.setStatusMessageType("WARNING");
            mmHolder.setExceptionMessageType("WARNING");
            if ((message.length() > 11) && (message.startsWith("JBI")))
            {
                mmHolder.setLocMessage (1, message.substring(11));
                mmHolder.setLocToken (1, message.substring(0,9));
            }
            else
            {
                mmHolder.setLocMessage (1, message);
                mmHolder.setLocToken (1, "JBIFW1322");
            }

            String jbiTaskStr = mMMImpl.buildCompleteExceptionMessage (mmHolder);
            // this really should be a return value and NOT a thrown item.
            // but for EA5, uninstallSharedLibrary returns a boolean and not
            // a ManagementMessage.  All the InstallationServiceMBean methods
            // really should RETURN that.
            throw new RuntimeException(jbiTaskStr);
        }
        
        return true;
    }

    
    /*---------------------------------------------------------------------------------*\
     *                      Extended InstallationServiceMBean ops                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Load the installer for a component from the Repository. The component archive from 
     * the repository is uploaded to the target instance(s), loadNewInstaller() in
     * invoked on the remote instance(s). The object name of the Installer MBean 
     * is returned.
     * <br/>
     * If the component is not there in the DAS repository a exception will
     * be thrown. 
     * 
     * @param componentName - name of the registered component.
     * @return the ObjectName of Installer MBean for the registered component
     * @exception javax.jbi.JBIException if the component is not registered.
     */
     public ObjectName loadInstallerFromRepository(String componentName)
        throws javax.jbi.JBIException
     {
         // nop
         return null;
     }
     
    /**
     * Install a shared library from the Repository. This method is not implemented and never
     * called by the facade MBeans.
     * 
     * @param sharedLibraryName - name of the registered shared library.
     * @return the shared library name
     * @exception javax.jbi.JBIException if the shared library is not registered or
     * uninstall fails.
     */
     public String installSharedLibraryFromRepository(String sharedLibraryName)
        throws javax.jbi.JBIException
     {
         // nop
         return sharedLibraryName;
     }
 
     
    /**
     * Uninstall a previously installed shared library. The facade InstallationService
     * never invokes this operation.
     *
     * @param slName the name of the shared name space to uninstall; must be
     *        non-null and non-empty
     * @param keep if true the shared libray is not deleted from the domain. 
     *        If false, the shared library is deleted from the repository 
     *        if after this uninstall it is not installed on any targets.
     * @return true if the uninstall was successful
     */
    public boolean uninstallSharedLibrary(String slName, boolean keep)
    {
        return uninstallSharedLibrary(slName);
    }

   
   /**
    * Upgrade a component. This is used to perform an upgrade of the runtime
    * files of a component without requiring undeployment of Service Assemblies
    * with Service Units deployed to the component.
    * @param componentName The name of the component.
    * @param installZipURL The URL to the component archive.
    * @return a status management message that contains component upgrade result 
    * @throws JBIException if there is a problem with the upgrade.
    */
    public String upgradeComponent(String componentName, String installZipPath)
    throws javax.jbi.JBIException
    {
        doUpgradeComponent(componentName, installZipPath);
        return new MessageBuilder(mTranslator).buildFrameworkMessage(
                    "upgradeComponent",
                     MessageBuilder.TaskResult.SUCCESS); 
    }
    
   /**
    * Set a component's upgrade number.
    * This method is used by synchronization to set the component upgrade number
    * @param componentName The name of the component.
    * @upgradeNumber the upgradeNumber.
    * @throws JBIException if there is a problem with the upgrade.
    */
    public void upgradeComponent(String componentName, long upgradeNumber)
    throws javax.jbi.JBIException
    {
        setComponentUpgradeNumber(componentName, upgradeNumber);      
    }  
    
//Private methods

    /**
     * Reload the InstallerMBean for a previously installed component.
     *
     * @param aComponentId the component name identifying the installer to load.
     * @param force set to <code>true</code> if this is a forced uninstall.
     * @return - the JMX ObjectName of the InstallerMBean loaded from an existing
     * installation context.
     * @throws javax.jbi.JBIException if an error occurs
     */
    private ObjectName reloadInstaller(String aComponentId, boolean force)
        throws javax.jbi.JBIException
    {
        mLogger.fine(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "reloadInstaller", "(" + aComponentId + "," + force + ")"));
        
        MBeanNames      jbiname = mContext.getMBeanNames();
        AdminService    myAS    = mContext.getAdminServiceHandle();
        XmlReader       jbiXml;
        ObjectName      returnName = null;

        String componentRoot = getInstallRoot(ArchiveType.COMPONENT, aComponentId);
        
        if (componentRoot != null)
        {
            mLogger.finest(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                    "reloadInstaller: root = " + componentRoot));

            if (myAS.isEngine(aComponentId))
            {
                returnName = jbiname.getEngineMBeanName(aComponentId, "Installer");
                if (mContext.getMBeanServer().isRegistered(returnName))
                {
                    mLogger.finest(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                            "reloadInstaller: found existing engine InstallationMBean = " + 
                            returnName));
                    return returnName;
                }
            }
            else if (myAS.isBinding(aComponentId))
            {
                returnName = jbiname.getBindingMBeanName(aComponentId, "Installer");
                if (mContext.getMBeanServer().isRegistered(returnName))
                {
                    mLogger.finest(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                        "reloadInstaller: found existing engine InstallationMBean = " +
                        returnName));
                    return returnName;
                }
            }

            jbiXml = getJbiXml(new File(componentRoot, PATH_TO_JBI_XML_FILE), false);

            ComponentManager framework = mContext.getComponentManager();
            try
            {
                ComponentInstallationContext cic =
                    jbiXml.getInstallationContext();
                cic.setIsInstall(false);
                cic.setInstallRoot(componentRoot);
                mLogger.fine("Loading the bootstrap for component " + aComponentId);
                returnName = framework.loadBootstrap (cic,
                    jbiXml.getBootstrapClassName(),
                    jbiXml.getBootstrapClassPath(),
                    jbiXml.getSharedLibraryIds(),
                    force);
            }
            catch (com.sun.jbi.framework.FrameworkWarningException fwEx)
            {
                // just log the mLogger.warning, but do not return it to the caller.
                mLogger.warning(MessageHelper.getMsgString(fwEx));
            }
            catch (javax.jbi.JBIException jbiEx)
            {
                String errMsg = mTranslator.getString(LocalStringKeys.IS_FRAMEWORK_UNINSTALL);
                String jbiTaskStr = managementMessageExcep("install",
                    "IS_FRAMEWORK_UNINSTALL", jbiEx.getMessage(), jbiEx);
                throw new javax.jbi.JBIException(jbiTaskStr);
            }
        }
        return returnName;
    }

    /**
     * Eliminate spaces (-> "%20") to handle odd cases like installation
     * directories containing a space.
     * 
     * @param installJarURI - String containing URL to convert
     * @return - a URL fixing spaces contained within.
     * @throws java.net.MalformedURLException if an error occurs
     */
    private URL convertToProperURL(String installJarURI)
        throws java.net.MalformedURLException
    {
        // Encode the #
        installJarURI = installJarURI.replaceAll("#", "%23");
        
        // fix problems commonly occuring with 'file:' URLs on Windows
        if (installJarURI.startsWith("file://"))
        {
            return new File(installJarURI.substring(7)).toURL();
        }

        if (installJarURI.startsWith("file:" + File.separator))
        {
            return new File(installJarURI.substring(5)).toURL();
        }

        // next case: Windows only, skip over the /
        if (installJarURI.startsWith("file:/"))
        {
            return new File(installJarURI.substring(6)).toURL();
        }

        if (installJarURI.startsWith("file:"))
        {
            return new File(installJarURI.substring(5)).toURL();
        }

        // last ditch effort to handle URLs with non-encoded spaces
        try
        {
            return new java.net.URI(installJarURI.replaceAll(" ", "%20")).toURL();
        }
        catch (java.net.URISyntaxException e)
        {
            throw new java.net.MalformedURLException (e.getMessage());
        }
    }

    /**
     * Utility routine to insert a path prefix into a list of class paths
     * @param aPrefix - String containing the install root
     * @param aClassPath - List of class paths
     * @return List of class paths prefixed with the specified prefix
     */
    private List insertPathPrefix(String aPrefix, List aClassPath)
    {
        java.util.Vector retList = new java.util.Vector();
        for (int j = 0; j < aClassPath.size(); ++j)
        {
            String item = aPrefix + "/" + (String) aClassPath.get(j);
            retList.add(item);
            String logMessage = mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                "insertPathPrefix: adding " + item);
            mLogger.finest(logMessage);
            if ("true".equalsIgnoreCase( System.getProperty(
                            "com.sun.jbi.config.management.allowAbsolutePaths")))
            {
                retList.add((String) aClassPath.get(j));
                logMessage = mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                    "insertPathPrefix: also adding " + aClassPath.get(j));
                mLogger.finest(logMessage);
            }
        }
        return retList;
    }

    /**
     * Utility routine to remove a directory or file.
     * @return 0 for success.
     * @param aFileOrDir file/directory to be removed.
     */
    private int removeIt(String aFileOrDir)
    {
        int st = 0;
        java.io.File f = new java.io.File(aFileOrDir);
        if (f.exists() && !f.delete())
        {
            f.deleteOnExit();
            st++;
        }

        return st;
    }

    /**
     * Utility routine to recursively remove a directory.
     * @return 0 if directory is successfully removed (recursively).
     * @param aDir directory name.
     */
    private int removeDirectory(String aDir)
    {
        int st = 0;
        String filePath;
        java.io.File d = new java.io.File(aDir);
        if (!d.exists())
        {
            return st;
        }

        if (!d.isDirectory())
        {
            return removeIt(aDir);
        }

        String[] filesToBeRemoved = d.list();

        for (int i = 0; i < filesToBeRemoved.length; i++)
        {
            filePath = aDir + "/" + filesToBeRemoved[i];
            java.io.File f = new java.io.File(filePath);
            if (f.isDirectory())
            {
                st += removeDirectory(filePath);
            }
            else
            {
                st += removeIt(filePath);
            }
        }
        st += removeIt(aDir);
        return st;
    }

    /**
     * Utility routine to move an unable-to-delete directory
     * into a "Trash" directory.
     * @param aDir directory name.
     */
    private void moveToTrash(String aDir)
    {
        String logMessage = mTranslator.getString(
                               LocalStringKeys.METHOD_ENTERED,
                               "moveToTrash",
                               "(" + aDir + ")");
        mLogger.fine(logMessage);

        String jbiRoot = this.getJbiInstallRoot();

        if ((null == jbiRoot) || ("".equals(jbiRoot)))
        {
            logMessage = mTranslator.getString(LocalStringKeys.IS_NO_TRASH);
            mLogger.warning(logMessage);
            return;
        }

        jbiRoot += "/Trash";
        int dirNum = 1;
        String trashRootPath = jbiRoot + "/" + dirNum;
        File trashRoot = new File(trashRootPath);
        while (trashRoot.exists())
        {
            dirNum++;
            trashRootPath = jbiRoot + "/" + dirNum;
            trashRoot = new File(trashRootPath);
        }
        logMessage = mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                        "Creating Trash directory as " + trashRootPath);
        mLogger.fine(logMessage);
        if (!trashRoot.mkdirs())
        {
            logMessage = mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                        "Cannot create directory " + trashRootPath);
            mLogger.warning(logMessage);
            return;
        }
        logMessage = mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                        "Now about to rename " + aDir + " to " + trashRootPath + ".");
        mLogger.fine(logMessage);
        File oldRoot = new File(aDir);
        if (!oldRoot.renameTo(trashRoot))
        {
            logMessage = mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                            "Could not rename " + aDir + " to " + trashRootPath + ".");
            mLogger.warning(logMessage);
        }
    }

    /**
     * Return the InstallerMBean for a component.
     *
     * @param aComponentId - the component UUID identifying the component.
     * @return - the JMX ObjectName of the InstallerMBean for that component.
     */
    private ObjectName installerMBeanName(String aComponentId)
    {
        mLogger.fine(mTranslator.getString(LocalStringKeys.METHOD_ENTERED,
                "installerMBeanName", "(" + aComponentId + ")"));
        
        ObjectName      mbName;
        AdminService    admin;
        ComponentInfo   compInfo;
        
        admin    =  mContext.getAdminServiceHandle();
        compInfo = mContext.getComponentQuery().getComponentInfo(aComponentId);
        
        if (compInfo == null)
        {
            // framework does not know about this component
            mbName = null;
        }
        else if (compInfo.getComponentType() == ComponentType.ENGINE)
        {
            mbName = mContext.getMBeanNames().getEngineMBeanName(aComponentId, "Installer");
        }
        else if (compInfo.getComponentType() == ComponentType.BINDING)
        {
            mbName = mContext.getMBeanNames().getBindingMBeanName(aComponentId, "Installer");
        }
        else
        {
            mLogger.warning(mTranslator.getString(
                    LocalStringKeys.IS_COMPONENT_NO_INSTALL_MBEAN, aComponentId));
            mbName = null;
        }

        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_EXITED,
                "installerMBeanName", "(" + aComponentId + ") -> " + mbName));
        
        return mbName;
    }

    /**
     * Return the install root prefix.
     *
     * @return - the base component root directory.
     */
    private String getInstallRootPrefix()
    {
        String jbiRoot = mContext.getJbiInstallRoot();

        if ((null == jbiRoot) || ("".equals(jbiRoot)))
        {
            jbiRoot = System.getProperty("junit.srcroot") +
                      "/install/lassen";
        }
        return jbiRoot;
    }

    /**
     * Obtain the JBI install root, either from the environment
     * context, or from a property.
     * @return - the component root prefix.
     */
    private String getJbiInstallRoot()
    {
        String jbiRoot = mContext.getJbiInstallRoot();

        mLogger.finest(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                "getJbiInstallRoot: jbiRoot = " + jbiRoot));
        
        if ((null == jbiRoot) || ("".equals(jbiRoot)))
        {
            jbiRoot = System.getProperty("junit.srcroot") +
                      "/install/lassen";
        }
        jbiRoot = jbiRoot.replace('\\', '/');
        mLogger.finer(mTranslator.getString(LocalStringKeys.METHOD_EXITED,
                "getJbiInstallRoot", "() -> " + jbiRoot));
        
        return jbiRoot;
    }

    /**
     * Close the given (open) JarFile, ignoring IOExceptions
     * @param aJarFile - an open Jar file
     */
    void closeJarFile(JarFile aJarFile)
    {
        try
        {
            aJarFile.close();
        }
        catch (java.io.IOException ioEx)
        {
            String errMsg = mTranslator.getString(LocalStringKeys.IS_IO_EXCEP,
                                 ioEx.getMessage());
            mLogger.finest(errMsg);
        }
    }

    /**
     * Helper method to create a ManagementMessage exception
     * @param methodName - the method generating the exception
     * @param token - the message key for the exception
     * @param message - the message itself
     */
    private String managementMessageExcep(String methodName,
        String token,
        String message)
    {
        ManagementMessageHolder mmHolder =
            new ManagementMessageHolder("EXCEPTION_MSG");
        mmHolder.setFrameworkLocale ("en_US");
        mmHolder.setTaskName (methodName);
        mmHolder.setTaskResult ("FAILED");
        if ((message.length() > 11) && (message.startsWith("JBI")))
        {
            mmHolder.setLocMessage (1, message.substring(11));
            mmHolder.setLocToken (1, message.substring(0,9));
        }
        else
        {
            mmHolder.setLocMessage (1, message);
            mmHolder.setLocToken (1, token);
        }
        mmHolder.setExceptionMessageType ("ERROR");
        mmHolder.setFrameworkLocale ("en_US");
        String jbiTaskStr = mMMImpl.buildCompleteExceptionMessage (mmHolder);
        return jbiTaskStr;
    }

    /**
     * Helper method to create a ManagementMessage exception
     * @param methodName - the method generating the exception
     * @param token - the message key for the exception
     * @param excep - the exception itself (to wrapper)
     */
    private String managementMessageExcep(String methodName,
        String token,
        String message,
        Throwable excep)
    {
        ManagementMessageHolder mmHolder =
            new ManagementMessageHolder("EXCEPTION_MSG");
        mmHolder.setFrameworkLocale ("en_US");
        mmHolder.setTaskName (methodName);
        mmHolder.setTaskResult ("FAILED");
        mmHolder.setExceptionObject (excep);
        if ((message.length() > 11) && (message.startsWith("JBI")))
        {
            mmHolder.setLocMessage (1, message.substring(11));
            mmHolder.setLocToken (1, message.substring(0,9));
        }
        else
        {
            mmHolder.setLocMessage (1, message);
            mmHolder.setLocToken (1, token);
        }
        mmHolder.setExceptionMessageType ("ERROR");
        mmHolder.setFrameworkLocale ("en_US");
        String jbiTaskStr = mMMImpl.buildCompleteExceptionMessage (mmHolder);
        return jbiTaskStr;
    }
    
    /** Returns an XmlReader representation of the jbi.xml descriptor in
     *  the specified JAR.  If an error occurs, the JAR is closed before 
     *  throwing an exception.
     */
    private XmlReader getJbiXml(JarFile jar)
        throws javax.jbi.JBIException
    {
        File jbiFile;
                
        try
        {
            jbiFile = unjar(jar, PATH_TO_JBI_XML_FILE);
            return getJbiXml(jbiFile, true);
        }
        catch(javax.jbi.JBIException jbiEx)
        {
            closeJarFile(jar);
            throw jbiEx;
        }
    }
    
    /** Returns an XmlReader representation of the jbi.xml descriptor in
     *  the specified JAR.  If an error occurs, the JAR is closed before 
     *  throwing an exception.
     */
    private XmlReader getJbiXml(File jbiFile, boolean validate)
        throws javax.jbi.JBIException
    {
        XmlReader jbiXml;
        if (null == jbiFile)
        {
            String errMsg = mTranslator.getString(LocalStringKeys.IS_NO_JBI_XML_IN_JAR);
            mLogger.fine(errMsg);
            String jbiTaskStr = managementMessageExcep("install",
                "IS_NO_JBI_XML_IN_JAR", errMsg);
            throw new javax.jbi.JBIException(jbiTaskStr);
        }
        
        try
        {
            jbiXml = new XmlReader();        
            jbiXml.setEnvironmentContext(mContext.getEnvironmentContext());
            
            jbiXml.loadAndParse(jbiFile.getAbsolutePath(), true);
            
            return jbiXml;
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            throw(jbiEx);
        }
        catch (Exception e)
        {
            mLogger.fine(MessageHelper.getMsgString(e));
            String errMsg = mTranslator.getString(
                                LocalStringKeys.IS_VALIDATION_EXCEP, e.getMessage());
            String jbiTaskStr = managementMessageExcep("install",
                "IS_VALIDATION_EXCEP", errMsg, e);
            throw new javax.jbi.JBIException(jbiTaskStr);
        }
    }

    
    /*---------------------------------------------------------------------------------*\
     *                           Private Helpers                                       *
    \*---------------------------------------------------------------------------------*/
        
    /**
     * @param alURL - path to the shared library archive
     */
    String installSharedLibraryToFramework(URL aComponentURL)
        throws javax.jbi.JBIException
    {
        Archive         installArchive;
        String          slName;
        
        
        mLogger.fine(mTranslator.getString(
                LocalStringKeys.METHOD_ENTERED, 
                "install", "(" + aComponentURL + ")"));

        installArchive = getRepositoryArchive(aComponentURL);
        
        slName = installArchive.getJbiName();
        
        String slInstallRoot = getInstallRoot(ArchiveType.SHARED_LIBRARY, slName);
        
        mLogger.finer( mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                        "install Root = " + slInstallRoot));

        mLogger.finer(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT, 
            "Installing shared library to framework"));
        ComponentManager framework = mContext.getComponentManager();
        
        SharedLibraryDescriptor descr = 
                new SharedLibraryDescriptor(installArchive.getJbiXml(false));
        
        try
        {
            framework.installSharedLibrary(
                    slName, "The description is not persisted, should be removed.", slInstallRoot, 
                    descr.isSharedLibraryClassLoaderSelfFirst(), 
                    insertPathPrefix(slInstallRoot, descr.getSharedLibraryClassPathElements()));

            mLogger.finest(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                "installed shared library = " + slName));
                
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            String errMsg = mTranslator.getString(LocalStringKeys.IS_FRAMEWORK_INSTALL);
            String jbiTaskStr = managementMessageExcep("install",
                "IS_FRAMEWORK_INSTALL", jbiEx.getMessage(), jbiEx);
            throw new javax.jbi.JBIException(jbiTaskStr);
        }
        
        return slName;
    }
    
    
    /**
     * Create the Component Installation Context and load the bootstrap.
     * 
     * @param aComponentURL the component .jar specification
     * @param isSharedLib - true iff the component is a shared library.
     * @return the ID of the component/library
     * @throws javax.jbi.JBIException if an error occurs
     */
    ObjectName bootStrapComponent(URL aComponentURL)
        throws javax.jbi.JBIException
    {   
        Archive         installArchive;
        String          compName;
        ObjectName      mbName;
        
        mLogger.fine(mTranslator.getString(
                LocalStringKeys.METHOD_ENTERED, 
                "bootStrapComponent", "(" + aComponentURL + ")"));
        
        installArchive = getRepositoryArchive(aComponentURL);
        
        compName = installArchive.getJbiName();
        
        // -- Create installation Context
        String compInstallRoot = getInstallRoot(ArchiveType.COMPONENT, compName);
        mLogger.finer( mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                        "install Root = " + compInstallRoot));

        mLogger.finer(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT, "Loading the Component Bootstrap"));
        ComponentManager framework = mContext.getComponentManager();
        try
        {
            // -- Create the Component Workspace
            String workspaceRoot = compInstallRoot + File.separator + "workspace";
            new File(workspaceRoot).mkdirs();   
            
            ComponentDescriptor descr = 
                    new ComponentDescriptor(installArchive.getJbiXml(false));
            
            // -- Component Installation Context
            ComponentInstallationContext cic = getComponentInstallationContext(descr,
                compInstallRoot);
            cic.setIsInstall(true);
            cic.setInstallRoot(compInstallRoot);
            cic.setWorkspaceRoot(workspaceRoot);
            
            // -- Load Bootstrap
            mbName = framework.loadBootstrap (
                    cic, descr.getBootstrapClassName(),
                    insertPathPrefix(compInstallRoot, descr.getBootstrapClassPathElements()), 
                    descr.getSharedLibraryIds(), false);
            
            mLogger.finest(mTranslator.getString(LocalStringKeys.IS_DEBUG_OUTPUT,
                "loadBootstrap MBean = " + mbName));

        }
        catch (javax.jbi.JBIException jbiEx)
        {
            String errMsg = mTranslator.getString(LocalStringKeys.IS_FRAMEWORK_INSTALL);
            String jbiTaskStr = managementMessageExcep("loadNewInstaller",
                "IS_FRAMEWORK_INSTALL", jbiEx.getMessage(), jbiEx);
            throw new javax.jbi.JBIException(jbiTaskStr);
        }
        
        return mbName;
    }
    
    /**
     * @return the shared library root
     */
    private String getInstallRoot(ArchiveType type, String name)
        throws javax.jbi.JBIException
    {        
        Archive archive = null;
        String archivePath;
        String installRoot = null;
         
         
        archive =  getRegistry().getRepository().getArchive(type, name);
        
        if ( archive != null )
        {
            archivePath = archive.getPath();
            installRoot = new File(archivePath).getParent() +
                    File.separator + INSTALL_ROOT;
        }

        mLogger.finest("Install Root for component " + name + " is " + installRoot);
        return installRoot;
    }
            
    private Archive getRepositoryArchive(URL aURL)
        throws javax.jbi.JBIException
    {
        try
        {
            return new Archive(new File(aURL.toURI()), false);
        }
        catch (Exception jbiEx)
        {
            String errMsg = mTranslator.getString(LocalStringKeys.IS_FRAMEWORK_INSTALL);
            String jbiTaskStr = managementMessageExcep("getRepositoryArchive",
                "IS_FRAMEWORK_INSTALL", jbiEx.getMessage(), jbiEx);
            throw new javax.jbi.JBIException(jbiTaskStr);
        }
    }
    
    private ComponentInstallationContext getComponentInstallationContext(
        ComponentDescriptor descr,
        String installRoot)
        throws javax.jbi.JBIException
    {
        
        String componentClassName = descr.getComponentClassName();
        String componentName = descr.getName();
        
        boolean isBinding = (descr.getComponentType() == com.sun.jbi.ComponentType.BINDING);
        org.w3c.dom.DocumentFragment docFrag = null;
        
        try
        {
            //docFrag = getExtensionData(jbi);
            docFrag = getExtensionData(installRoot);
        }
        catch ( Exception ex )
        {
            throw new javax.jbi.JBIException(ex);
        }
        
        ComponentInstallationContext ic = new ComponentInstallationContext(componentName,
            (isBinding ? InstallationContext.BINDING : InstallationContext.ENGINE),
            componentClassName,
            descr.getComponentClassPathElements(),
            docFrag);
        ic.setDescription(descr.getDescription());
        if ( descr.isBootstrapClassLoaderSelfFirst() )
        {
            ic.setBootstrapClassLoaderSelfFirst();
        }
        if ( descr.isComponentClassLoaderSelfFirst() )
        {
            ic.setComponentClassLoaderSelfFirst();
        }
        return ic;
    }
      
    
    /**
     * Trim the whitespaces from all the elements in the list.
     */
    private List<String> trim(List<String> list)
    {
        List<String> trimmedList = new java.util.ArrayList<String>(list.size());
        for ( String str : list )
        {
            trimmedList.add(str.trim());
        }
        return trimmedList;
    }
    
    /**
     * Get the extension data from the Components jbi.xml and return the
     * data in a DocumentFragment
     */
    private org.w3c.dom.DocumentFragment getExtensionData(String componentRoot)
        throws Exception
    {
        /**
        com.sun.jbi.management.util.ComponentType compType = jbi.getComponent();
        Marshaller m = sJC.createMarshaller();
        
        javax.xml.parsers.DocumentBuilderFactory 
            dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
        org.w3c.dom.DocumentFragment 
            docFrag = db.newDocument().createDocumentFragment();

        List<Object> anyData = compType.getAnyOrAny();
        
        for ( Object extObj : anyData )
        {
            m.marshal(extObj, docFrag);
        }
        
        return docFrag;
         */
        
        // have to do it this way there is a bug in JAXB getAnyOrAny 
        // does not work correctly
        XmlReader  
            jbiXml = getJbiXml(new File(componentRoot, PATH_TO_JBI_XML_FILE), false);
        ComponentInstallationContext cic =
                jbiXml.getInstallationContext();
        
        return cic.getInstallationDescriptorExtension();
    }
    
    /** 
     * This method is used to extract a given jar in the specified dir.
     * @param parentDir the dir to extract the jar in
     * @param archiveZip the jar file
     * @throws IOException if there are problems in extracting the jar
     */
    private void extractJar(String parentDir, File archiveZip)
    throws IOException
    {

        File parent = new File(parentDir);
        parent.mkdir();
        JarFactory jarHelper = new JarFactory(parent.getAbsolutePath());
        jarHelper.unJar(archiveZip);
        mLogger.finer("Extracted " + archiveZip.getName() + " in " + parentDir);            

    }
    
    /**
     * This method is used to validate the component for upgrade.
     * This method creates a temporary install root for the component and 
     * calls framework's validate using the temporary install root.
     * @param componentName the componentName
     * @param installZipURL the path to the component file given for upgrade
     * @return true if the component involved implements upgrade SPI
     * @throws JBIException if the component is not validated for upgrade
     */
    private boolean validateComponentForUpgrade(
            String componentName, 
            String installZipURL)
    throws JBIException
    {
        boolean isUpgradeImplemented = false;
        String tempInstallRoot = 
                mEnv.getJbiInstanceRoot() + File.separator +
                TMP_DIR + File.separator + componentName + UPGRADE_TMP_DIR + 
                FileHelper.getTimestamp();        
        try
        {
            File updateFile = new File(installZipURL);                        

            extractJar(tempInstallRoot, updateFile);
            Archive updateArchive = 
                    new Archive(updateFile, true);
            
            ComponentDescriptor descriptor = 
                    new ComponentDescriptor(updateArchive.getJbiXml(false));
            ComponentInstallationContext installationContext = 
                    getComponentInstallationContext(descriptor, tempInstallRoot);
            installationContext.setIsInstall(false);
            installationContext.setInstallRoot(tempInstallRoot);
            
            isUpgradeImplemented =
            mEnv.getComponentManager().validateComponentForUpgrade(
                    installationContext, 
                    descriptor.getBootstrapClassName(),
                    insertPathPrefix(
                            tempInstallRoot, 
                            descriptor.getBootstrapClassPathElements()),
                    descriptor.getSharedLibraryIds());
            
            mLogger.fine("Framework has validated component archive " + installZipURL);        

            File tempInstallRootFile = new File(tempInstallRoot);
            FileHelper.cleanDirectory(tempInstallRootFile);
            tempInstallRootFile.delete();
        }
        catch (IOException ioEx)
        {
            if (tempInstallRoot != null)
            {
                File tempInstallRootFile = new File(tempInstallRoot);
                if (tempInstallRootFile.exists())
                {
                    FileHelper.cleanDirectory(tempInstallRootFile);
                    tempInstallRootFile.delete();
                }
            }
            throw new JBIException(ioEx.toString());
        }  
        return isUpgradeImplemented;
    }
    
    /**
     * This method is used to backup existing install root for a component
     * @param installRoot the installRoot 
     * @param componentName the component name
     * @return String the backupDir where the installroot was saved
     * @throws IOException if there are issues in creating a backup of the install root
     * @throws RegistryException if information about the component could not be read
     * from registry
     */
    private String backupInstallRoot(String installRoot, String componentName)
    throws IOException, RegistryException
    {
        String installRootBkup = 
            mEnv.getJbiInstanceRoot() + File.separator + 
            TMP_DIR + File.separator + 
            componentName + UNDERSCORE + 
            getRegistry().getGenericQuery().getComponentUpgradeNumber(componentName) 
            + UNDERSCORE + FileHelper.getTimestamp();                  
        FileHelper.copy(installRoot, installRootBkup); 
        mLogger.finer("Install root has been saved in " + installRootBkup);
        return installRootBkup;        
    }
    
    /**
     * This method is used to upgrade a component by calling framework's 
     * upgrade component
     * @param componentName the component name
     * @throws JBIException if the component could not be upgraded
     */
    private void invokeFrameworkUpgradeComponent(String componentName)
    throws JBIException
    {
        Archive archive = 
                getRegistry().getRepository().getArchive(
                    ArchiveType.COMPONENT, 
                    componentName);
        String compInstallRoot = getInstallRoot(ArchiveType.COMPONENT, componentName);              
        ComponentManager framework = mContext.getComponentManager();
        ComponentDescriptor descr = new ComponentDescriptor(archive.getJbiXml(false));

        ComponentInstallationContext installationContext = 
                getComponentInstallationContext(
                        descr,
                        compInstallRoot);
        installationContext.setIsInstall(true);
        installationContext.setInstallRoot(compInstallRoot);
        installationContext.setWorkspaceRoot(compInstallRoot + File.separator + WORKSPACE);        
        
        framework.upgradeComponent (
                    installationContext, 
                    descr.getBootstrapClassName(),
                    insertPathPrefix(
                            compInstallRoot, 
                            descr.getBootstrapClassPathElements()),
                    descr.getSharedLibraryIds());        
        mLogger.finer("framework successfully upgraded the component " + componentName);        
    }
    
    /**
     * This method is used to copy the workspace of a component from the backup 
     * dir to the newly created install root
     * @param backupDir the dir where the component install root was saved
     * @param componentName component name
     * @param componentRoot the install root of the component
     * @throws IOException if the workspace dir could not be recreated
     */
    private void copyWorkspaceFromBackup(
            String backupDir, 
            String componentName, 
            String componentRoot)
    throws IOException
    {
        /*
         * backupDir is <instanceRoot>/tmp/sun-http-binding_backup
         * installRoot is <instanceRoot>/components/sun-http-binding
         * we need to copy backupDir/intall_root/workspace to
         * components/sun-http-binding/install_root/workspace 
         */
        String backupWorkspace = 
                backupDir + File.separator + 
                INSTALL_ROOT + File.separator +
                WORKSPACE;
        String workspace = 
                componentRoot + File.separator + 
                INSTALL_ROOT + File.separator +                
                WORKSPACE;
        FileHelper.copy(backupWorkspace, workspace);
        mLogger.fine("Recreated component workspace from " + backupWorkspace + " to " + workspace);
          
    }
    
    /**
     * This method is used to copy logger settings of a omponent from the backup 
     * dir to the newly created install root
     * @param backupDir the dir where the component install root was saved
     * @param componentName component name
     * @param componentRoot the install root of the component
     * @throws IOException if the workspace dir could not be recreated
     */
    private void copyLoggerSettingsFromBackup(
            String backupDir, 
            String componentName, 
            String componentRoot)
    throws IOException
    {
        /*
         * backupDir is <instanceRoot>/tmp/sun-http-binding_backup
         * installRoot is <instanceRoot>/components/sun-http-binding
         * we need to copy backupDir/intall_root/workspace to
         * components/sun-http-binding/install_root/workspace 
         */
        String backupConfig = 
                backupDir + File.separator + 
                LOGGER_CONFIG;
        String config = 
                componentRoot + File.separator + 
                LOGGER_CONFIG;
        boolean result = FileHelper.copy(backupConfig, config);
        if (result)
        {
            mLogger.fine("Recreated component logger settings from " + 
                    backupConfig + " to " + config);
        } 
        else
        {
            if (new File(backupConfig).exists())
            {
                mLogger.warning(mTranslator.getString(
                    LocalStringKeys.IS_COMPONENT_UPGRADE_LOGGER_NOT_RESTORED, 
                    componentName));
            }
        }
          
    }
        
    
    /**
     * This method is used to restore a component's install root from a backup
     * copy. This method is used in cases where there were issues in component
     * upgrade and we need to restore the component install root
     * @param componentName the component name
     * @param backupDir the dir where the component install root was saved
     * @param componentRoot the install root for the component
     */
    private void restoreInstallRoot(
            String componentName,
            String backupComponentRoot, 
            String componentRoot)
    {
        /*
         * backupDir is <instanceRoot>/tmp/sun-http-binding_backup
         * installRoot is <instanceRoot>/components/sun-http-binding
         * we need to copy backupDir to <instanceRoot>/components
         */         
        try
        {
            if (getRegistry().getRepository().archiveExists(
                    ArchiveType.COMPONENT, 
                    componentName))
            {

                getRegistry().getRepository().removeArchive(
                    ArchiveType.COMPONENT, 
                    componentName);                 
            }
            //remove the marker
            File marker = new File(componentRoot, DELETE_ME);
            if (marker != null && marker.exists())
            {
                marker.delete();
            }    
            
            FileHelper.copy(
                    backupComponentRoot, 
                    componentRoot);
    
            File backupDir = new File(backupComponentRoot);
            FileHelper.cleanDirectory(backupDir);
            backupDir.delete();
            mLogger.fine("Restored component install root from " + backupComponentRoot);
        }
        catch (Exception ex)
        {
            mLogger.warning(mTranslator.getString(
                    LocalStringKeys.IS_COMPONENT_UPGRADE_INSTALL_ROOT_RESTORE_FAILED) 
                    + ex.getMessage());
        }
 
    }
    
   /**
    * Actually upgrade a component. This is used to perform an upgrade of the runtime
    * files of a component without requiring undeployment of Service Assemblies
    * with Service Units deployed to the component.
    * @param componentName The name of the component.
    * @param installZipPath The Path to the component archive.
    * @throws JBIException if there is a problem with the upgrade.
    */
    
    private void doUpgradeComponent(String componentName, String installZipPath)
    throws javax.jbi.JBIException
    {
        String installZipURL; 
        boolean isUpgradeImplemented;
        
        //Step 1: Call framework to validate. Upon failure call cancelUpgrade
        try 
        {
            installZipURL = new File(convertToProperURL(installZipPath).toURI()).getAbsolutePath();    
            isUpgradeImplemented = 
                    validateComponentForUpgrade(componentName, installZipURL);
            
        }
        catch (JBIException jbiEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw jbiEx;
        }
        catch (MalformedURLException urlEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            mLogger.fine(urlEx.toString());
            throw new JBIException(urlEx);
        }   
        catch (java.net.URISyntaxException uriEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            mLogger.log(Level.FINE, uriEx.toString(), uriEx);
            throw new JBIException(uriEx);
        }
        
        //Step 2: finished validation. call framework to actually upgrade
        try 
        {
            if (mEnv.getPlatformContext().isAdminServer() )
            {
                upgradeComponentInAdminServer(
                        componentName, 
                        isUpgradeImplemented);                
            } 
            else
            {
                Registry registry = getRegistry();
                synchronized(registry)
                {
                    upgradeComponentInInstance(
                        componentName, 
                        installZipURL,
                        isUpgradeImplemented);
                }
            }
            mLogger.info(mTranslator.getString(
                LocalStringKeys.IS_COMPONENT_UPGRADE_SUCCESSFUL, 
                componentName));                   
            
        }
        catch (JBIException jbiEx)
        {
            mLogger.warning(mTranslator.getString(
                LocalStringKeys.IS_COMPONENT_UPGRADE_FAILED, 
                componentName));                        
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw jbiEx;
        }
    }
    
    /**
     * This method is used to upgrade a component in an admin server instance.
     * The component install root is updated in the facade installation service.
     * This method invokes the framework upgrade component (after saving SU roots
     * if needed).
     * @param componentName the component name.
     * @param isUpgradeImplemented true if the component involved implements upgrade SPI.
     * @throws JBIException if there are problems in updating the component.
     */
    private void upgradeComponentInAdminServer(
            String componentName, 
            boolean isUpgradeImplemented)
    throws JBIException
    {
        
        //in DAS component install root has been updated by facade
        
        String suBackupDir = null;
        //1. backup SUs if needed
        try 
        {
            if (isUpgradeImplemented)
            {
                suBackupDir = backupSUsDeployedOnComponent(componentName);
            }
        }
        catch (ManagementException mEx)
        {
            throw new JBIException(mEx.getMessage());
        }
        
        //2. call framework to upgrade component
        try
        {
            invokeFrameworkUpgradeComponent(componentName);     
        } 
        catch (JBIException jbiEx)
        {
            try
            {
                if (isUpgradeImplemented)
                {
                    restoreSUsDeployedOnComponent(componentName, suBackupDir);
                }
            }
            catch (ManagementException mEx)
            {
                mLogger.fine(MessageHelper.getMsgString(mEx));
            }
            //we do not restore component install root for 'server' because it
            //will affect the domain repository
            mLogger.finer("Component update failed in server, install root not restored");
            throw jbiEx;
        }
    }
       
    /**
     * This method is used to upgrade a component in a non admin server instance
     * This method takes a back up of the existing install root and replaces it
     * using the given archive. With the updated install root the upgradeComponent
     * method in framework is invoked. If there are failures the install root is
     * restored. If the upgrade was successful the upgrade-number is incremented
     * @param componentName the component name
     * @param installZipURL the archive path
     * @param isUpgradeImplemented true if the component implements upgrade SPI
     * @throws JBIException if there are problems in updating the component 
     */
    private void upgradeComponentInInstance(
            String componentName, 
            String installZipURL,
            boolean isUpgradeImplemented)
    throws JBIException
    {
        //1. backup existing install root
        mLogger.finer("Entered upgradeComponentInInstance with file " + installZipURL);        
        String backupDir;
        String suBackupDir = null;
        String componentRoot;
        String existingArchiveName = null;
        Archive upgradeArchive = null;
        try 
        {
            existingArchiveName = 
                    getRegistry().getGenericQuery().getComponentFileName(componentName);
            
            //componentRoot will be <instanceRoot>/jbi/components/sun-http-binding                  
            componentRoot = new File(
                     getInstallRoot(
                         ArchiveType.COMPONENT, 
                         componentName)).getParent();

            
            backupDir = backupInstallRoot(componentRoot, componentName);        
            mLogger.finer("Successfully saved install root in " + backupDir);
        }
        catch (IOException ioEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            mLogger.fine(MessageHelper.getMsgString(ioEx));
            throw new JBIException(
                    mTranslator.getString(
                        LocalStringKeys.IS_COMPONENT_UPGRADE_INSTALL_ROOT_BACKUP_FAILED, componentName) +
                        ioEx.getMessage());
        }
        catch (RegistryException regEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            mLogger.fine(MessageHelper.getMsgString(regEx));
            throw new JBIException(
                    mTranslator.getString(
                        LocalStringKeys.IS_COMPONENT_UPGRADE_INSTALL_ROOT_BACKUP_FAILED, componentName) +
                        regEx.getMessage());
            
        }

        //2. remove existing install root
        try 
        {
            getRegistry().getRepository().removeArchive(ArchiveType.COMPONENT, componentName);
            mLogger.finer("Successfully removed install root from repository");
        }
        catch (RepositoryException repEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            restoreInstallRoot(componentName, backupDir, componentRoot);
            mLogger.fine(MessageHelper.getMsgString(repEx));
            throw new JBIException(
                    mTranslator.getString(
                        LocalStringKeys.IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_DELETED, componentName) +
                        repEx.getMessage());
        }

        //3. verify if install root is really gone
        if (new File(componentRoot).exists())
        {
            mLogger.finer("Install root was not completely deleted from repository");            
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            restoreInstallRoot(componentName, backupDir, componentRoot);
            String message = mTranslator.getString(
                LocalStringKeys.IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_DELETED, componentName);
            mLogger.fine(message);            
            throw new JBIException(message);
        }  

        //4. create new install root and bring over the old workspace
        try
        {
            upgradeArchive = 
                    getRegistry().getRepository().addArchive(ArchiveType.COMPONENT, installZipURL);
            mLogger.finer("Successfully added new archive in repository" );   
            copyWorkspaceFromBackup(backupDir, componentName, componentRoot);
            mLogger.finer("Successfully recreated component workspace" );         
            copyLoggerSettingsFromBackup(backupDir, componentName, componentRoot);
            mLogger.finer("Successfully recreated logger settings" );                 
        }
        catch (IOException ioEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            restoreInstallRoot(componentName, backupDir, componentRoot);
            mLogger.fine(MessageHelper.getMsgString(ioEx));
            throw new JBIException(
                    mTranslator.getString(
                        LocalStringKeys.IS_COMPONENT_UPGRADE_INSTALL_ROOT_NOT_CREATED) +
                        ioEx.getMessage());            
        }
        catch (RepositoryException repEx)
        {
            mEnv.getComponentManager().cancelComponentUpgrade(componentName);
            restoreInstallRoot(componentName, backupDir, componentRoot);
            mLogger.fine(MessageHelper.getMsgString(repEx));
            throw new JBIException(
                    mTranslator.getString(
                        LocalStringKeys.IS_COMPONENT_UPGRADE_WORKSPACE_NOT_RESTORED, componentName) +
                        repEx.getMessage());            
        }
        
        //5. backup the SUs if upgrade SPI is implemented by component
        try
        {
            if (isUpgradeImplemented)
            {
                suBackupDir = backupSUsDeployedOnComponent(componentName);
            }        
        } 
        catch (ManagementException mEx)
        {
            restoreInstallRoot(componentName, backupDir, componentRoot);
            mLogger.fine(MessageHelper.getMsgString(mEx));
            throw new JBIException(mEx);
        }
       
        //6. now call framework to upgrade the component
        try
        {
            invokeFrameworkUpgradeComponent(componentName);
            mLogger.finer("Successfully upgradedthe component in framework");            
            incrementComponentUpgradeNumber(componentName);
        }
        catch (JBIException jbiEx)
        {
            //cleanup the new contents before restore
            restoreInstallRoot(componentName, backupDir, componentRoot);
            
            try
            {
                if (isUpgradeImplemented)
                {
                    restoreSUsDeployedOnComponent(componentName, suBackupDir);
                }
            }
            catch (ManagementException mEx)
            {
               mLogger.fine(MessageHelper.getMsgString(mEx));
            }
            mLogger.fine(MessageHelper.getMsgString(jbiEx));
            throw jbiEx;
        }

         //7.if archive names are different modify registry
        modifyArchiveNameInRegistry(
                componentName,
                existingArchiveName,
                upgradeArchive.getFileName());

        //8.the descriptors will be different, remove old one from cache 
        ((com.sun.jbi.management.registry.xml.GenericQueryImpl)
        getRegistry().getGenericQuery()).removeComponentFromCache(componentName);     
        
        
        //we want to preserve the backup copy in instances too.
        /*File backup = new File(backupDir);
        FileHelper.cleanDirectory(backup);       
        backup.delete();   */
   
    } 
    

    /**
     * This method is used to increment the upgrade number for a component by one
     * @param componentName the component name
     * @throws RegistryException if the upgrade number could not be incremented
     */
    private void incrementComponentUpgradeNumber(String componentName)
    throws RegistryException
    {
            BigInteger currNumber = 
                    getRegistry().getGenericQuery().getComponentUpgradeNumber(componentName);
            getRegistry().getUpdater().setComponentUpgradeNumber(
                    componentName,
                    currNumber.add(BigInteger.ONE));
        mLogger.fine("Incremented upgrade number for component" + componentName);
    }
    
   /**
     * This method is used to set the upgrade number for a component
     * @param componentName the component name
     * @param upgradeNumber the upgrade number to be set
     * @throws RegistryException if the upgrade number could not be incremented
     */
    private void setComponentUpgradeNumber(String componentName, long upgradeNumber)
    throws RegistryException
    {
         getRegistry().getUpdater().setComponentUpgradeNumber(
                    componentName,BigInteger.valueOf(upgradeNumber));    
        mLogger.fine("Set upgrade number for component" + componentName);
    } 
    
    /**
     * This method is used to backup the SUs deployed to a component.
     * @param componentName the component name
     * @return String the dir where the SUs are saved
     * @throws ManagementException if there are issues in copying the SU roots
     */
    private String backupSUsDeployedOnComponent(String componentName)
    throws ManagementException
    {
        String backupRoot = null;
        try 
        {
            backupRoot = 
                    mEnv.getJbiInstanceRoot() + File.separator + 
                    TMP_DIR + File.separator + 
                    componentName + UNDERSCORE + 
                    getRegistry().getGenericQuery().getComponentUpgradeNumber(componentName) 
                    + UNDERSCORE + SU_BACKUP + UNDERSCORE + FileHelper.getTimestamp();

            ComponentInfo compInfo = getComponentQuery().getComponentInfo(componentName);
            if ( compInfo != null )
            {
                List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();

                for ( ServiceUnitInfo suInfo : suList)
                {
                    String backupDir = backupRoot + File.separator + 
                                suInfo.getServiceAssemblyName() + File.separator +
                                suInfo.getName();

                    String saRoot = 
                            mEnv.getJbiInstanceRoot() + File.separator + 
                            REPOSITORY_SERVICE_ASSEMBLY_STORE + File.separator + 
                            suInfo.getServiceAssemblyName() + File.separator +                        
                            suInfo.getName();
                    mLogger.fine("Saving a copy of SU root from " + saRoot + " at " + backupDir);         
                    FileHelper.copy(saRoot, backupDir);                 
                }
            }        
        } 
        catch (IOException ex)
        {
            throw new ManagementException(ex);
        }      
        catch (RegistryException regEx)
        {
            throw new ManagementException(regEx);
        }
        return backupRoot;
    }
    
    /**
     * This method is used to restore the SU roots from a backup copy.
     * This method is called if the component SPI encountered issues in 
     * upgrade and we want to restore everything to previous state
     * @param componentName the component name
     * @param backupDir the dir where the backup is stored
     * @throws ManagementException if the SU roots cannot be restored
     */
    private void restoreSUsDeployedOnComponent(String componentName, String backupDir)
    throws ManagementException
    {
        try 
        {
            File saList[] = new File(backupDir).listFiles();
            for(int i=0; i<saList.length; i++)
            {
                File suList[] = saList[i].listFiles();
                for (int j=0; j<suList.length; j++)
                {
                    String suBackupDir = 
                            backupDir + File.separator +
                            saList[i].getName() + File.separator +
                            suList[j].getName();
                    
                    String suRoot =
                           mEnv.getJbiInstanceRoot() + File.separator + 
                           REPOSITORY_SERVICE_ASSEMBLY_STORE + File.separator + 
                           saList[i].getName() + File.separator +
                           suList[j].getName();
                    if (FileHelper.copy(suBackupDir, suRoot))
                    {
                        mLogger.fine("Successfully restored SU root from " + suBackupDir + " to " + suRoot);                                                 
                    }
                    else
                    {
                        mLogger.fine("Could not restore SU root in "+ suRoot + " from " + suBackupDir);                                                 
                    }
                }
            }
        } 
        catch(Exception ioEx)
        {
            throw new ManagementException(ioEx.getMessage());
        }
    }
    
    
    /**
     * This method is used to modify the name of the component archive in 
     * registry. This is needed to address the cases where the name of the
     * archive given for upgrade is different from the current
     * one.
     * @param componentName the component name
     * @param exisitingName the archive name that is in registry
     * @param newName the new archive name given for upgrade
     * @param installZipURL the URL for achive provided for upgrade
     */
    private void modifyArchiveNameInRegistry(
            String componentName,
            String existingName,
            String newName)
    throws RegistryException, ManagementException
    {
        if (!existingName.equals(newName))
        {
            getRegistry().getUpdater().setComponentFileName(componentName, newName);
        }
    }    
}
