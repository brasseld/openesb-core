/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.io.Reader;
import java.util.List;
import javax.jbi.management.DeploymentException;

/**
 * This is the interface for Deployment Context, which is an
 * in-memory representation of JBI Assembly Unit jar file contents.
 *
 * @author Sun Microsystems, Inc.
 */
public interface DeploymentContext
{
    /**
     * Returns name of the Service Assembly(SA), 
     * whose meta-data this context represents.  
     * 
     * @return name of the Service Assembly(SA)
     */
    String getSAName() throws DeploymentException;

    /**
     * Returns a list of SU names that are part of the
     * current Service Assembly(SA). 
     * 
     * @return List of SU names that are part of the SA
     */
    List getSUList();

    /**
     * Returns the name of the component that is the target
     * of the given Service Unit.
     * 
     * @param aSUName name of the Service Unit
     * @return name of the Component
     */
    String getTargetComponentName(String aSUName);

    /**
     * Returns the jar file corresponding to the SU name. This
     * is the jar file that will be deployed to the given component.
     * 
     * @param aSUName name of the Service Unit
     * @return SU jar file 
     */
    Reader getArtifactsJar(String aSUName);
    
    /**
     * Returns the deployment descriptor(jbi.xml) of the current Service Assembly(SA). 
     * 
     * @return deployment descriptor of the Service Assembly(SA)
     */
    String getDeploymentDescriptor();
}
