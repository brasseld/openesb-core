/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.util.Properties;
import java.util.Enumeration;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.MBeanHelper;


/**
 * This is the Management Service of the JBI Framework, which provides
 * the JMX MBean server and related items.
 *
 * @author Sun Microsystems, Inc.
 */
public class ManagementService
    implements com.sun.jbi.ServiceLifecycle
{
   /**
    * Handle to the logger
    */
    private java.util.logging.Logger mLog;

   /**
    * copy of framework EnvironmentContext	
    */
    private EnvironmentContext mEnv;

   /**
    * Handle to the Management Context
    */
    private ManagementContext mMgmtContext;

   /**
    * Handle to the main MBean server
    */
    private MBeanServer mMBeanServer;

   /**
    * Handle to the MBeanHelper class, for creating & registering mbeans:
    */
    private MBeanHelper mMBeanHelper;
    
    /**
     * Handle to StringTranslator for message translation
     */
     private StringTranslator mTranslator;
    
   /**
    * Initialize the Management Service.
    * @param envContext the JBI environment context created
    * by the JBI framework
    * @throws javax.jbi.JBIException if an error occurs
    */
    public void initService(EnvironmentContext envContext)
        throws javax.jbi.JBIException
    {
        mEnv = envContext;  //save a copy of framework EnvironmentContext	
        Properties jbiprops = mEnv.getInitialProperties();
        
        mTranslator = mEnv.getStringTranslator("com.sun.jbi.management");

        //initialize any missing properties to default values:
        initJbiProperties(jbiprops);

        // copy initial (static) jbi properties to global schemaorg_apache_xmlbeans.system properties:
        exportJbiProperties(jbiprops);

        //create management context object:
        mMgmtContext = new ManagementContext(envContext);

        //note - all variables we need from framework env context are wrapped by
        //the management context.  So use the management context to retrieve values:
        mMBeanServer = mMgmtContext.getMBeanServer();
        mMBeanHelper = mMgmtContext.getMBeanHelper();

        mLog = mMgmtContext.getLogger();
        mLog.fine("ManagementService: initializing");        
        
        if ( !mMBeanHelper.createSystemServiceLoggerMBean("ManagementService", mLog) )
        {
            String errMsg = mTranslator.getString(LocalStringKeys.MNGMTSVC_CANNOT_CREATE_LOGGERMBEAN);
            mLog.severe(errMsg);
        }        

        //Create DeploymentService:
        if (!createDeploymentService(mMgmtContext))
        {
            throw new javax.jbi.JBIException(
                "cannot create JBI Framework DeploymentService.");
        }

        //Create InstallationService:
        if (!createInstallationService(mMgmtContext))
        {
            throw new javax.jbi.JBIException(
                "cannot create JBI Framework InstallationService.");
        }

        //Create LoggingService:
        if (!createLoggingService(mMgmtContext))
        {
            throw new javax.jbi.JBIException(
                "cannot create JBI Framework LoggingService.");
        }

        //Create AdminService LAST, as it is the entry point:
        if (!createAdminService(mMgmtContext))
        {
            throw new javax.jbi.JBIException(
                "cannot create JBI Framework AdminService.");
        }
        
        try
        {
            MBeanNames mbn = mMgmtContext.getMBeanNames();
            ObjectName adminLC = mbn.getSystemServiceMBeanName(
                mbn.SERVICE_NAME_ADMIN_SERVICE, mbn.CONTROL_TYPE_LIFECYCLE);

            mLog.fine(
                "ManagementService: Starting admin service lifecycle, "
                + adminLC.toString());

            // start up the admin service via its lifecycle:
            mMBeanServer.invoke(adminLC, "start", new Object[0], new String[0]);

            //now tell the admin service to start the management layer:
            ObjectName adminSVC = mbn.getSystemServiceMBeanName(
                mbn.SERVICE_NAME_ADMIN_SERVICE, mbn.CONTROL_TYPE_ADMIN_SERVICE);
            mLog.fine(
                "ManagementService: Starting Management Services, "
                + adminSVC.toString());
            mMBeanServer.invoke(adminSVC, "startManagementServices", new Object[0], new String[0]);
        }
        catch (Exception e)
        {
            // No need to log this, the framework logs it
            throw new javax.jbi.JBIException(e);
        }

        mLog.fine("ManagementService: initialization complete");
    }

    /**
     * Start the Management service.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void startService()
        throws javax.jbi.JBIException
    {
        mLog.fine("ManagementService: starting");
        mLog.fine("ManagementService: startup complete");
    }

    /**
     * Stop the Management service.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stopService()
        throws javax.jbi.JBIException
    {
        mLog.fine("ManagementService: shutting down");
        
        try 
        { 
            MBeanNames mbn = mMgmtContext.getMBeanNames();
            ObjectName adminSVC = mbn.getSystemServiceMBeanName(
                mbn.SERVICE_NAME_ADMIN_SERVICE, mbn.CONTROL_TYPE_ADMIN_SERVICE);

            mLog.fine(
                "ManagementService: shutdown Management layer, "
                + adminSVC.toString());

            //shutdown the management layer:
            mMBeanServer.invoke( adminSVC, "shutdownManagementServices", new Object[0], new String[0]);
        }
        catch (Exception e)
        {
            mLog.warning(e.toString());
            throw new javax.jbi.JBIException(e);
        }

        //unregister the mbean for my logger:
        if ( !mMBeanHelper.destroySystemServiceLoggerMBean("ManagementService", mLog) )
        {
            mLog.fine("ManagementService: cannot destroy my LoggerMBean!");
        }

        mLog.fine("ManagementService: shutdown complete");
    }
    
    /**
     * This is called by the framework whenever a component changes state, so
     * that the deployment service can be notified to update any affected
     * service connections.
     * @param componentName name of a component that has changed state
     */
    public void updateComponentState(String componentName)
    {
        // -- Notify Deployment Service of component change
        DeploymentService depSvc = mMgmtContext.getDeploymentServiceHandle();
        depSvc.updateServiceAssemblyConnections(componentName);
    }
    
    /**
     * Helper to get ManagementContext from an Environment Context in SunASJBIFramework.
     */
    public ManagementContext getManagementContext()
    {
        return (mMgmtContext);
    }
    
    /**
     * initialize any missing properties to default values:
     * @param jbiprops is the property object we are initializing.
     */
    private void initJbiProperties(Properties jbiprops)
    {
        /** Add domain root values iff this is the admin server */
        if (mEnv.getPlatformContext().isAdminServer())
        {
            /** NOTE : this code references a platform-specific property :
             *  com.sun.aas.domainName.  It is only used in our 
             *  regression environment, so this is not a huge deal, but we need
             *  to find a more portable way to surface this property.
             */
            
            //re-export some key appserver properties:
            jbiprops.setProperty("com.sun.jbi.domain.root",
                mEnv.getPlatformContext().getInstanceRoot() );

            String domainName = System.getProperty("com.sun.aas.domainName");
            if (domainName != null)
            {
                jbiprops.setProperty("com.sun.jbi.domain.name", domainName);
            }
        }

    }

    /**
     * Export the jbi inital properties as schemaorg_apache_xmlbeans.system properties, so we can
     * use them to generate configuration scripts.
     * @param jbiprops holds the properties we are going to export.
     */
    private void exportJbiProperties(Properties jbiprops)
    {
        //for each property in jbiprops...
        for ( Enumeration e = jbiprops.propertyNames(); e.hasMoreElements();)
        {
            String key = (String) e.nextElement();
            String value = jbiprops.getProperty(key );

            //Now, copy to schemaorg_apache_xmlbeans.system:
            System.setProperty(key, value);
        }
    }


//////////
// Methods to create each schemaorg_apache_xmlbeans.system service
//////////

    /**
     * Create, initialize,  and register the AdminService
     * lifecycle MBean with the JBI Framework MBEANSERVER.
     *
     * Save handle to new service.
     *
     * @param gv is the management context.
     * @return false if we cannot.
     */
    private static boolean createAdminService(ManagementContext gv)
    {
        String thisService = "AdminService";

        //create object:
        AdminService svc = new AdminService(gv);

        //save handle to AdminService:
        gv.setAdminServiceHandle(svc);

        //bootstrap the service:
        //NOTE:  this happen at START time, not INIT time:
        svc.bootstrap();

        return true;
    }

    /**
     * Create, initialize,  and register the ConfigurationService
     * lifecycle MBean with the JBI Framework MBEANSERVER.
     *
     * Save handle to new service.
     *
     * @param gv is the management context.
     * @return false if we cannot.
     *
    private static boolean createConfigurationService(ManagementContext gv)
    {
        String thisService = "ConfigurationService";

        //create object:
        ConfigurationService svc = new ConfigurationService();

        //save handle to ConfigurationService:
        gv.setConfigurationServiceHandle(svc);

        //bootstrap the service:
        //NOTE:  this happen at START time, not INIT time:
        svc.bootstrap();

        return true;
    }*/

    /**
     * Create, initialize,  and register the DeploymentService
     * lifecycle MBean with the JBI Framework MBEANSERVER.
     *
     * Save handle to new service.
     *
     * @param gv is the management context.
     * @return false if we cannot.
     */
    private static boolean createDeploymentService(ManagementContext gv)
        throws javax.jbi.JBIException
    {
        String thisService = "DeploymentService";

        //create object:
        DeploymentService svc = new DeploymentService(gv);

        //save handle to DeploymentService:
        gv.setDeploymentServiceHandle(svc);

        //bootstrap the service:
        //NOTE:  this happen at START time, not INIT time:
        svc.bootstrap();

        return true;
    }

    /**
     * Create, initialize,  and register the InstallationService
     * lifecycle MBean with the JBI Framework MBEANSERVER.
     *
     * Save handle to new service.
     *
     * @param gv is the management context.
     * @return false if we cannot.
     */
    private static boolean createInstallationService(ManagementContext gv)
        throws javax.jbi.JBIException
    {
        String thisService = "InstallationService";

        //create object:
        InstallationService svc = new InstallationService(gv);

        //save handle to InstallationService:
        gv.setInstallationServiceHandle(svc);

        //bootstrap the service:
        //NOTE:  this happen at START time, not INIT time:
        svc.bootstrap();

        return true;
    }

    /**
     * Create, initialize,  and register the LoggingService
     * lifecycle MBean with the JBI Framework MBEANSERVER.
     *
     * Save handle to new service.
     *
     * @param gv is the management context.
     * @return false if we cannot.
     */
    private static boolean createLoggingService(ManagementContext gv)
    {
        String thisService = "LoggingService";

        //create object:
        LoggingService svc = new LoggingService(gv);

        //save handle to LoggingService:
        gv.setLoggingServiceHandle(svc);

        //bootstrap the service:
        //NOTE:  this happen at START time, not INIT time:
        svc.bootstrap();

        return true;
    }
}
