/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ControllerMBean.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.common;

import javax.management.ObjectName;

/**
 * The ControllerMBean is a container MBEAN for the other MBEANS registered in
 * a JBI Managed Component.  It is used to collect a component MBEANS in order
 * to present a Component View to higher level management agents.  It also serves
 * as a repository for controls that operate on the component as a whole, for example,
 * about command, help command, etc.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ControllerMBean
{
    /**
     * Return current version and other info about this component.
     * @return info String
     */
    String getComponentInfo();

    /**
     * Return help text about this component.
     * @return help text about this component.
     */
    String getHelp();

    /**
     * Return the JMX ObjectName for this component's DeployerMBean or null.
     * @return the JMX ObjectName for this component's DeployerMBean or null.
     */
    ObjectName getDeployerMBean();

    /**
     * Return the JMX ObjectName for this component's LifeCycleMBean
     * or null.
     * @return the JMX ObjectName for this component's LifeCycleMBean
     * or null.
     */
    ObjectName getLifeCycleMBean();

    /**
     * Return the ObjectName for LoggerMBean named <CODE>aLoggerName</CODE>,
     * or null, if the named logger does not exist in this component.
     * @return ObjectName of LoggerMBean.
     */
    ObjectName getLoggerMBeanByName(String aLoggerName);

    /**
     * Return the ObjectName of the default LoggerMBean for this component.
     * @return ObjectName of default LoggerMBean or null.
     */
    ObjectName getDefaultLoggerMBean();

    /**
     * Return the ObjectNames for all of the LoggerMBean's for this component.
     * @return array of ObjectName, possibly of zero length.
     */
    ObjectName[] getLoggerMBeans();

    /**
     * Return the componentName
     * @return the componentName
     */
    String getcomponentName();
}
