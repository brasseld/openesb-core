/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Util.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Util.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 17, 2004, 2:26 PM
 */

package com.sun.jbi.internal.security;

import java.util.Properties;
import java.util.HashMap;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class Util
{
    /**
     * @param packageName is the pkgname.
     * @return  a String Translator. 
     */
    public static com.sun.jbi.StringTranslator getStringTranslator (String packageName)
    {
        Class clazz;
        java.lang.reflect.Constructor ctor;
        try
        {
            clazz = Class.forName("com.sun.jbi.framework.StringTranslator");
            ctor = clazz.getDeclaredConstructors()[0];
            ctor.setAccessible(true);
            return (com.sun.jbi.StringTranslator)
                ctor.newInstance(new Object[] {packageName, null});
                    //this.getClass().getClassLoader()});
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }    
        
    }
    
}
