/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LifeCycleImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.test.binding1.rt;

import java.io.File;

import java.util.logging.Logger;
import java.util.Vector;

import javax.jbi.JBIException;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.MBeanNames;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;

import javax.xml.soap.SOAPMessage;
import javax.xml.soap.MessageFactory;

import com.sun.jbi.binding.security.SecurityHandler;
import com.sun.jbi.binding.security.Endpoint;
import com.sun.jbi.management.ManagementMessageBuilder;




/**
 * This is an implementation of a Binding life cycle which starts a
 * thread.  The thread is the main implementation of the binding.
 *
 * @author Sun Microsystems, Inc.
 */
public class LifeCycleImpl 
    implements Component, ComponentLifeCycle
{
    
    /**
     * Logger instance
     */
    private Logger mLog = Logger.getLogger("com.sun.jbi.internal.security.test.binding1");
    /**
     *  Actual binding implementation
     */
    /**
     * Local copy of the component ID
     */
    private String mComponentId;
    /**
     * fix it
     */    
    private ComponentContext mContext;
    /**
     * fix it
     */    
    private ManagementMessageBuilder mMgmtMsgBuilder;
    
    /**
     * fix it
     */    
    private ServiceUnitMgrImpl mDeployer;
    
    /**
     * The Security Handler.
     */
    private SecurityHandler mSecHndlr;
    
    /**
     * Service name.
     */
    private String mServiceName = "TestService";
    
    /**
     * TNS.
     */
    private String mTns = "http://www.sun.com";
    
    /**
     * Provider Endpoint Prefix
     */
    private String mPEPrefix = "ProviderEndpoint";
    
    /**
     * Consumer Endpoint Prefix
     */
    private String mCEPrefix = "ConsumerEndpoint";
    
    /**
     * Deployment Folder
     */
    private String mDeplDir = "deployment";
    
    /**
     * Message Dir.
     */
    private String mMsgDir = "messages";
    
    /**
     * Deployed Endpoint's registry
     */
    private Vector mDeplRegistry = new Vector();
    
    
    
    /**
     * Get the required life cycle control implementation for this component.
     * @return the life cycle control implementation for this component.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Get the Service Unit manager for this component. If this component
     * does not support deployments, return <code>null</code>.
     * @return the Service Unit manager for this component, or <code>null</code>
     * if there is none.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return mDeployer;
    }

    /**
     * Resolve descriptor details for the specified reference, which is for a
     * service provided by this component.
     * @param ref the endpoint reference to be resolved.
     * @return the description for the specified reference.
     */
    public org.w3c.dom.Document getServiceDescription(ServiceEndpoint ref)
    {
        return null;
    }

    /**
     * Initialize the Binding Component.
     * @param context the JBI binding environment context created
     * by the JBI framework
     * @throws JBIException if an error occurs
     */
    public void init(ComponentContext context) throws JBIException
    {
        
        if ( context == null  )
        {
            throw new JBIException("Null argument received for " +
            "ComponentContext");
        }
        
        this.mLog = Logger.getLogger("com.sun.jbi.internal.security.test.binding1");
        
        this.mContext = context;
        this.mComponentId = context.getComponentName();
        
        try
        {
            this.mMgmtMsgBuilder =
                ((com.sun.jbi.component.ComponentContext) mContext).
                getManagementMessageFactory().newBuildManagementMessage();
        }
        catch (Exception e)
        {
            throw new JBIException("Unable to create Mmgt Msg Builder", e);
        }
        
        createDeployer();
        
        mLog.info("Binding " + mComponentId + " initialized");
        
    }
    
    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there
     * is none, return null.
     * @return ObjectName the JMX object name of the additional MBean or null
     * if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }
    
    /**
     * Start the Binding Component.
     * @throws JBIException if an error occurs
     */
    public void start() throws JBIException
    {
        
        mLog.info("Binding " + mComponentId + " started");
        
        mSecHndlr = 
            ( (com.sun.jbi.component.ComponentContext) mContext).getSecurityHandler();
        
        try
        {

            SecurityHelper secHelper = new SecurityHelper();

            // -- Load the JCE Provider
            secHelper.loadProvider(); 

            registerDeployments();

            simulateRequestResponse();

            unregisterDeployments();

            secHelper.unloadProvider(); 
        }
        catch (Exception ex )
        {
            throw new JBIException(ex.getMessage(), ex);
        }
        
        mLog.info("Binding " + mComponentId + " going to sleep .. zz.zzz");
         
        try
        {
            Thread.sleep(30000);
        }
        catch (Exception ex)
        {
            
        }
        
        mLog.info("Binding " + mComponentId + " woke up .. G' Morning");
    }
    
    /**
     * Stop the Binding Component.
     * @throws JBIException if an error occurs
     */
    public void stop() throws JBIException
    {
        mLog.info("Binding " + mComponentId + " stopped");
    }
    
    /**
     * Shut down the Binding Component.
     * @throws JBIException if an error occurs
     */
    public void shutDown() throws JBIException
    {
        mLog.info("Binding " + mComponentId + " shut down");
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public Logger getLogger()
    {
        return this.mLog;
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public String getId()
    {
        return this.mComponentId;
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public ComponentContext getContext()
    {
        return this.mContext;
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public ManagementMessageBuilder getMgmtMsgBuilder()
    {
        return this.mMgmtMsgBuilder;
    }
    
    /**
     * fix it
     * @throws JBIException fix it
     */    
    public void createDeployer() throws JBIException
    {
        try
        {
            if ( this.mDeployer == null )
            {
                this.mDeployer = new ServiceUnitMgrImpl(this);
            }
        } catch (Exception e)
        {
            throw new JBIException("Security Test Binding1 Deployer Creation Failed",e);
        }
    }
    
    /**
     * This method registers the Endpoint deployments. This test binding bypasses the
     * deployment contract, to simplify the test.
     */
    private void registerDeployments()
        throws Exception
    {
        int i = 1;
        boolean providerDone = false;
        
        do
        {
            String deplFile = 
                mContext.getInstallRoot() + File.separator + mDeplDir 
                + File.separator + mPEPrefix + i + ".xml";
            if (  new File(deplFile).exists()  )
            {
                Endpoint ep = new EndpointImpl(mPEPrefix + i, mServiceName, mTns, 
                       mComponentId, javax.jbi.messaging.MessageExchange.Role.PROVIDER, 
                       deplFile );
                mSecHndlr.getDeploymentListener().addDeployment(ep);
                mDeplRegistry.add(ep);
            }
            else
            {
                providerDone = true;
                System.out.println("Deployment File : " + deplFile + " does not exist.");
            }
            i++;
        } while (!providerDone);
        
        
        int j = 1;
        boolean consumerDone = false;
        
        do
        {
            String deplFile = 
                mContext.getInstallRoot() + File.separator + mDeplDir 
                + File.separator + mCEPrefix + j + ".xml";
            if ( new File(deplFile).exists() ) 
            {
                Endpoint ep = new EndpointImpl(mCEPrefix + j, mServiceName, mTns, 
                       mComponentId,javax.jbi.messaging.MessageExchange.Role.CONSUMER, 
                       deplFile );
                mSecHndlr.getDeploymentListener().addDeployment(ep);
                mDeplRegistry.add(ep);
            }
            else
            {
                consumerDone = true;
                System.out.println("Deployment File : " + deplFile + " does not exist.");
            }
            j++;
        } while (!consumerDone);
       
    }
    
    /**
     * This method registers the Endpoint deployments. This test binding bypasses the
     * deployment contract, to simplify the test.
     */
    private void unregisterDeployments()
    {
        java.util.Iterator itr = mDeplRegistry.iterator ();
        
        while ( itr.hasNext() )
        {
            mSecHndlr.getDeploymentListener().removeDeployment((Endpoint) itr.next());
        }
        
    }
    
    /**
     * Simulate a request / response
     */
    private void simulateRequestResponse()
        throws Exception
    {
        int i = 1;
        boolean msgTestDone = false;
        
        do
        {
        // -- Read the sample soap message
         String msg = mContext.getInstallRoot() + File.separator + mMsgDir 
                + File.separator + "soapmsg0" + i + ".xml" ;
         File msgFile = new File( msg );
         
         if (  msgFile.exists()  )
         {
             SOAPMessage soapMsg = readSoapMsg(new java.io.FileInputStream(msgFile));
             Endpoint provider = new EndpointImpl(mPEPrefix + i, mServiceName, mTns, 
                       mComponentId, javax.jbi.messaging.MessageExchange.Role.PROVIDER, 
                       null );
             Endpoint consumer = new EndpointImpl(mCEPrefix + i, mServiceName, mTns, 
                       mComponentId, javax.jbi.messaging.MessageExchange.Role.CONSUMER, 
                       null );
             
             // -- Secure Request
             /** The Provider Implementation does not support the dynamic case
             javax.security.auth.Subject subject = new javax.security.auth.Subject();
             subject.getPrivateCredentials().add(
                 new com.sun.jbi.binding.security.PasswordCredential("nikita", "atikin"));
             */
             javax.security.auth.Subject subject = null;
             SOAPMessageContext msgCtx = new SOAPMessageContext(soapMsg);
             mSecHndlr.getInterceptor().processOutgoingMessage(
                provider, "default", msgCtx, subject);
             
             System.out.println("Secure Message :");
             ( (SOAPMessage) msgCtx.getMessage()).writeTo(System.out);
             
             // -- Validate Request
             subject = null;
             mSecHndlr.getInterceptor().processIncomingMessage(
                consumer, "default", msgCtx, subject);
             
             // -- Secure Response
             subject = null;
             mSecHndlr.getInterceptor().processOutgoingMessage(
                 consumer, "default", msgCtx, subject);
             
             System.out.println("Secure Message :");
             ( (SOAPMessage) msgCtx.getMessage()).writeTo(System.out);
             
             // -- Validate Response
             subject = null;
             mSecHndlr.getInterceptor().processIncomingMessage(
                 provider, "default", msgCtx, subject);
             
             ( (SOAPMessage) msgCtx.getMessage()).writeTo(System.out);
         }
         else
         {
             System.out.println("Message File : " + msg + "does not exist.");
             msgTestDone = true;
         }
         i++;
        }
        while ( !msgTestDone );
        
    }
    
    /**
     * Create a SOAP Message from the message stream.
     */
    private SOAPMessage readSoapMsg(java.io.InputStream istr)
        throws Exception
    {
        javax.xml.soap.MimeHeaders header = new javax.xml.soap.MimeHeaders();
        header.addHeader("CONTENT-TYPE", "text/xml");
        return MessageFactory.newInstance().createMessage(
            header , istr);
    }    
    
    /**
     * Resolves the specified endpoint reference into a service endpoint. This
     * is called by the component when it has an endpoint reference that it
     * needs to resolve into a service endpoint.
     * @param endpointReference the endpoint reference as an XML fragment.
     * @return the service endpoint corresponding to the specified endpoint
     * reference, or <code>null</code> if the reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment endpointReference)
    {
        return null;
    }

    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
}
