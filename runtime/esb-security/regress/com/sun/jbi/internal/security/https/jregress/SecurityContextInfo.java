/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityContextInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityContextInfo.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 25, 2004, 1:11 PM
 */

package com.sun.jbi.internal.security.https.jregress;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityContextInfo
{
    // private String config = null;
    private String mSrcroot = null;
    
    // -- Server ( Server ) Settings 
    public static String[] serverKeyAliases = new String[] {"s1as"};
    public static String[] serverKeyPasswords = new String[] {"changeit"};

    public static String   serverTrustStoreURL = null;
    public static String   serverTrustStorePassword = "changeit";
    public static String   serverTrustStoreType = "JKS";
    
    public static String   serverKeyStoreURL = null;
    public static String   serverKeyStorePassword = "changeit";
    public static String   serverKeyStoreType = "JKS";
    
    // -- Client Settings
    public static String[] clientKeyAliases = new String[] {"xws-security-client"};
    public static String[] clientKeyPasswords = new String[] {"changeit"};

    public static String   clientTrustStoreURL = null;
    public static String   clientTrustStorePassword = "changeit";
    public static String   clientTrustStoreType = "JKS";
    
    public static String   clientKeyStoreURL = null;
    public static String   clientKeyStorePassword = "changeit";
    public static String   clientKeyStoreType = "JKS";

    public SecurityContextInfo(String keystoreBase)
    {   
        serverTrustStoreURL = keystoreBase + "/server-truststore.jks";
        serverKeyStoreURL = keystoreBase + "/server-keystore.jks";
        
        clientTrustStoreURL = keystoreBase + "/client-truststore.jks";
        clientKeyStoreURL = keystoreBase + "/client-keystore.jks";
        
    }
    
    // -- Accessors 
    
    // -- Server Key store / Trust Store information
    /**
     * @return the Server KeyStore URL
     */
    public String getServerKeyStoreURL()
    {
        return serverKeyStoreURL;
    }
    
    /**
     * @return the Server KeyStore Passowrd
     */
    public String getServerKeyStorePassword()
    {
        return serverKeyStorePassword;
    }
    
    /**
     * @return the Server KeyStore Type
     */
    public String getServerKeyStoreType()
    {
        return serverKeyStoreType;
    }
    
    /**
     * @return the Server TrustStore URL
     */
    public String getServerTrustStoreURL()
    {
        return serverTrustStoreURL;
    }
    
    /**
     * @return the Server TrustStore Passowrd
     */
    public String getServerTrustStorePassword()
    {
        return serverTrustStorePassword;
    }
    
    /**
     * @return the Server TrustStore Type
     */
    public String getServerTrustStoreType()
    {
        return serverTrustStoreType;
    }
    /**
     * @return Server Key Aliases
     */
    public String[] getServerKeyAliases()
    {
        return serverKeyAliases;
    }
    
    /**
     * @return Server Key Passwords
     */
    public String[] getServerKeyPasswords()
    {
        return serverKeyPasswords;
    }
    
    // -- Client Key store / Trust Store information
    /**
     * @return the Client KeyStore URL
     */
    public String getClientKeyStoreURL()
    {
        return clientKeyStoreURL;
    }
    
    /**
     * @return the Client KeyStore Passowrd
     */
    public String getClientKeyStorePassword()
    {
        return clientKeyStorePassword;
    }
    
    /**
     * @return the Client Key Store Type
     */
    public String getClientKeyStoreType()
    {
        return clientKeyStoreType;
    }
    
    /**
     * @return the Client Trust URL
     */
    public String getClientTrustStoreURL()
    {
        return clientTrustStoreURL;
    }
    
    /**
     * @return the Client Trust Passowrd
     */
    public String getClientTrustStorePassword()
    {
        return clientTrustStorePassword;
    }
    
    /**
     * @return the Client Trust Store Type
     */
    public String getClientTrustStoreType()
    {
        return clientTrustStoreType;
    }
    
    /**
     * @return Client Key Aliases
     */
    public String[] getClientKeyAliases()
    {
        return clientKeyAliases;
    }
    
    /**
     * @return Server Key Passwords
     */
    public String[] getClientKeyPasswords()
    {
        return clientKeyPasswords;
    }
}
