/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JmxObjectNames.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.mbeans;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;


/** 
 * This class provides the JBI MBean ObjectNames.
 * @author Sun Microsystems, Inc.
 */
public final class JmxObjectNames
{
    
    /** JMX Domain name for the jbi jmx server. */
    public static final String JMX_JBI_DOMAIN = "com.sun.jbi";
    
    /** JBI MBean Object type. */
    public static final String CONTROL_TYPE_KEY = "ControlType";
    
    /** JBI MBean Object type value. */
    public static final String CONTROL_TYPE_VALUE = "Configuration";
    
    /** JBI MBean Object type. */
    public static final String COMPONENT_TYPE_KEY = "ComponentType";
    /** JBI MBean Object type value. */
    public static final String SYSTEM_COMPONENT_TYPE_VALUE = "System";
    
    /** JBI Service Names. */
    /** JBI MBean Object type name.*/
    public static final String SERVICE_NAME_KEY = "ServiceName";
    /** Service Names.*/
    public static final String SECURITY_SERVICE = "SecurityService";

    /** jbi jmx domain. */
    private static String sJbiJmxDomain = null;
    
    /** jbi admin object name. */
    private static ObjectName sSecSvcConfigMBeanObjectName = null;
    
    /**
     * gets the jmx domain name.
     * @return domain name
     */
    public static String getJbiJmxDomain()
    {
        if ( sJbiJmxDomain == null )
        {
            sJbiJmxDomain = System.getProperty("jbi.jmx.domain", JMX_JBI_DOMAIN);
        }
        return sJbiJmxDomain;
    }
    
    /** 
     * @return the ObjectName for the Security Service Configuration Mbean.
     * @throws MalformedObjectNameException if the object name is not formatted
     * according to jmx object name
     */
    public static ObjectName getSecurityServiceConfigMBeanObjectName()
        throws MalformedObjectNameException
    {
        if ( sSecSvcConfigMBeanObjectName == null )
        {
            String mbeanRegisteredName = getJbiJmxDomain() + ":" +
                SERVICE_NAME_KEY + "=" + SECURITY_SERVICE + "," +
                CONTROL_TYPE_KEY + "=" + CONTROL_TYPE_VALUE + "," +
                COMPONENT_TYPE_KEY + "=" + SYSTEM_COMPONENT_TYPE_VALUE;
            sSecSvcConfigMBeanObjectName = new ObjectName(mbeanRegisteredName);
        }
        return sSecSvcConfigMBeanObjectName;   
    }
    
}
