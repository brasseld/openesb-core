/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AppSrvKeyStoreManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  KeyStoreManager.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 24, 2004, 12:35 AM
 */

package com.sun.jbi.internal.security.keymgt;

import com.sun.jbi.StringTranslator;

import java.security.cert.CertStore;
import java.security.KeyStore;
import java.util.Properties;
import java.util.logging.Logger;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class AppSrvKeyStoreManager 
    implements com.sun.jbi.internal.security.KeyStoreManager
{
    
    /** KeyStoreManager Name. */
    private String mName;
    
    /** Type Value { JavaStandard, SJSAS }. */
    private String mType;
    
     
    /** The StringTranslator. */
   // private StringTranslator mTranslator;
    
    /** The App Serv CallbackHandler. */
    private CallbackHandler mAppSrvCBHandler;
       
    /** Constructor. */
    public AppSrvKeyStoreManager ()
    {
        mAppSrvCBHandler = null;
        Logger mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
    }
    
        
    /**
     * Get the name of the KeyStoreManager.
     * @return the name of KeyStoreManager
     */
    public String getName()
    {
        return mName;
    }
    
    /**
     * Set the User KeyStoreManager.
     * @param name is the name of the KeyStoreManager
     */
    public void setName(String name)
    {
        mName = name;
    }
    
    /**
     * Get the Type String. 
     * In Shasta 1.0 we support "JavaStandard" which encompasses : JKS, PKCS12, JCEKS
     * and "SJSAS" which implies the Application Server environment.
     *
     * @return manager type
     */
    public String getType()
    {
        return mType;
    }
    
    /**
     * Set the type string.
     *
     * @param type is the type.
     */
    public void setType(String type)
    {
        mType = type;
    }
    
    /**
     * Set the StringTranslator on the Plugin. The StringTranslator
     * is created for the package the plugin belongs to.
     *
     * @param translator is the StringTranslator to use.
     */
    public void setStringTranslator(StringTranslator translator)
    {
        // mTranslator = translator;
    }
    
    /**
     * @return a KeyStore instance. This method returns null, since we do not have
     * access to App Serv keystore.
     */
    public KeyStore getKeyStore()
    {
        return null;
    }
    
    /**
     * @return a TrustStore instance. This method returns null, use the 
     * TrustStoreCallback instead.
     */
    public KeyStore getTrustStore()
    {
        return null;
    }
    
    
    /**
     * @return a CertificateStore instance, this method returns null, use 
     * CertStoreCallback
     */
    public CertStore getCertStore()
    {
       return null;
    }
    
    /**
     * Reload the Key / Trust Stores.
     *
     * @throws IllegalStateException if the information cannot be reloaded
     */
    public void reload()
        throws IllegalStateException
    {
        // initialize(mProps);
    }
    /**
     * Initialize this KeyStore Service.
     *
     * @param initProperties are the Properties specified for the Service
     * @throws IllegalStateException if initialization fails.
     */
    public void initialize (Properties initProperties) throws IllegalStateException
    {
        try
        {
            mAppSrvCBHandler = 
                com.sun.jbi.internal.security.AppSrvEnvironment.getCallbackHandler();
        }
        catch (Exception ex)
        {
            throw new IllegalStateException(ex.toString());
        }
    }
    
    
    /**
     * The implementation on the CallbackHandlerInterface. This class supports
     * CertStoreCallback, PrivateKeyCallback, SecretKeyCallback & TrustStoreCallback
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws java.io.IOException - if an input or output error occurs. 
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks 
     * parameter.
     */
    public void handle(Callback[] callbacks)
        throws java.io.IOException, UnsupportedCallbackException
    {
        mAppSrvCBHandler.handle(callbacks);
    }
    
    /**
     * @return the password required for accessing the KeyStore
     */
    public String getKeyStorePassword ()
    {
        return null;
    }
    
    /**
     * @return the password required for accessing the Trust Store
     */
    public String geyTrustStorePassword ()
    {
        return null;
    }
    
}
