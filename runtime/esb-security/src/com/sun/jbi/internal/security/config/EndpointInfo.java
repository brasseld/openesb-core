/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.config;

/**
 * Endpoint interface provides a means to define the three attributes that
 * uniquely identify an endpoint.
 *
 * @author Sun Microsystems, Inc.
 */
public interface EndpointInfo
{
    
    /**
     * Get the Local name of the endpoint.
     *
     * @return the Local Name of the Endpoint
     */
    String getName();
    
    /**
     * Get the Local Name of the parent Service that contains this Endpoint.
     *
     * @return the local name of the Parenter Service
     */
    String getServiceName();
     
   /**
    * Get the targetNamespace URI for the Endpoint.
    * @return the targetNamespace URI for the Endpoint
    */
    java.net.URI getTargetNamespace();
    
    /**
     *
     * Get the Component Id associated with the Endpoint.
     *
     * @return the Component Id
     * @deprecated
     */
    String getComponentId();
}
