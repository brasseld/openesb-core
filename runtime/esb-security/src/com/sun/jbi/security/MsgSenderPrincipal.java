/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MsgSenderPrincipal.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  MsgSenderPrincipal.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 14, 2005, 11:59 AM
 */

package com.sun.jbi.security;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class MsgSenderPrincipal
    implements java.security.Principal, java.io.Serializable
{
    
    /** The Priincipal name. */
    private String mName;
    
    /** 
     * Creates a new instance of MsgSenderPrincipal.
     *
     * @param name is the Name of the principal.
     */
    public MsgSenderPrincipal (String name)
    {
        mName = name;
    }
    
    /**
     * Returns the name of the Principal.
     *
     * @return the name of the principal.
     */
    public String getName ()
    {
        return mName;
    }
    
    /**
     * Return a hash code for this Principal.
     *
     * The hash code is calculated via: getName().hashCode()
     *
     * @return a hash code for this Principal.
     */
    public int hashCode ()
    {
        return getName().hashCode ();
    }
    
    /**
     * Compares the specified Object with this X500Principal for equality.
     * Specifically, this method returns true if the Object o is an X500Principal
     * and if the respective canonical string representations (obtained via the
     * getName(X500Principal.CANONICAL) method) of this object and o are equal.
     *
     * @param o object to compare with
     * @return true if the specified Object is equal to this X500Principal,
     * false otherwise
     */
    public boolean equals (Object o)
    {
        if ( o == null )
        {
            return false;
        }
        
        if ( o instanceof MsgSenderPrincipal)
        {
            return getName().equals(
                ( (MsgSenderPrincipal) o).getName());
        }
        
        return false;
    }
    
}
