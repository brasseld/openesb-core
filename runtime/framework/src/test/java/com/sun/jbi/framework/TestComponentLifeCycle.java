/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentLifeCycle.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.ComponentInstallationContext;

import java.util.ArrayList;

/**
 * Tests for the ComponentLifeCycle class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentLifeCycle
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Current SRCROOT path.
     */
    private String mSrcroot;

    /**
     * Instance of the EnvironmentSetup helper class.
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of the EnvironmentContext created by EnvironmentSetup.
     */
    private EnvironmentContext mEnvironment;

    /**
     * Instance of the ComponentFramework created by EnvironmentSetup.
     */
    private ComponentFramework mCompFW;

    /**
     * Instance of the ComponentRegistry creatd by EnvironmentSetup.
     */
    private ComponentRegistry mCompReg;

    /**
     * Component instance created during setup.
     */
    private Component mComponent;

    /**
     * ComponentInstallationContext instance created during setup.
     */
    private ComponentInstallationContext mInstallContext;

    /**
     * ComponentLifeCycle instance created during setup.
     */
    private ComponentLifeCycle mLifeCycle;

    /**
     * Result holder for threads.
     */
    private Throwable mResults[];

    /**
     * Thread counter.
     */
    private int mThreadCount;

    /**
     * Object for notification of thread completion.
     */
    private Boolean mWaitNotify;

    /**
     * Constant for start operation.
     */
    private static final int START = 1;

    /**
     * Constant for stop operation.
     */
    private static final int STOP = 2;

    /**
     * Constant for shutDown operation.
     */
    private static final int SHUTDOWN = 3;

    /**
     * Constant for shutDown operation with force set.
     */
    private static final int SHUTDOWNFORCE = 4;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentLifeCycle(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentLifeCycle instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mSrcroot = Utils.getSourceRoot() + "/";

        // Create and initialize the EnvironmentContext. Create, initialize,
        // and start up the Component Registry and Framework.

        mSetup = new EnvironmentSetup();
        mEnvironment = mSetup.getEnvironmentContext();
        mSetup.startup(true, true);
        mCompReg = mEnvironment.getComponentRegistry();
        mCompFW = mEnvironment.getComponentFramework();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        mSetup.shutdown(true, true);
        System.err.println("***** END of test " + mTestName);
        super.tearDown();
    }

    /**
     * Install the component for the test using the supplied component name.
     * @param name the component name for this component.
     * @throws Exception when set up fails for any reason.
     */
    private void install(String name)
        throws Exception
    {
        // Create component class path lists

        ArrayList bootstrapClassPath = new ArrayList();
        bootstrapClassPath.add(mSrcroot + Constants.BC_BOOTSTRAP_CLASS_PATH);
        ArrayList componentClassPath = new ArrayList();
        componentClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH);

        // Create installation context.

        mInstallContext =
            new ComponentInstallationContext(
                name,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                componentClassPath,
                null);
        mInstallContext.setInstallRoot(mSrcroot);
        mInstallContext.setIsInstall(true);

        // Install the component.

        mCompFW.loadBootstrap(
            mInstallContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            bootstrapClassPath,
            null, false);
        mCompFW.installComponent(mInstallContext);

        // Get the Component instance and create a ComponentLifeCycle instance.

        mComponent = mCompReg.getComponent(name);
        mLifeCycle = new ComponentLifeCycle(mComponent, mCompFW);
    }

// =============================  test methods ================================

    /**
     * Test the get method for the current state.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetCurrentState()
        throws Exception
    {
        install(Constants.BC_NAME);
        assertEquals("Failure getting current state: ",
                     mLifeCycle.SHUTDOWN, mLifeCycle.getCurrentState());
    }

    /**
     * Test the get method for the DeployerMBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDeploymentMBeanName()
        throws Exception
    {
        install(Constants.BC_NAME);
        assertNotNull("Failure getting DeploymentMBeanName: ",
                      mLifeCycle.getDeploymentMBeanName());
    }

    /**
     * Test the get method for the ExtensionMBean name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetExtensionMBeanName()
        throws Exception
    {
        install(Constants.BC_NAME);
        assertNull("Failure getting ExtensionMBeanName: ",
                   mLifeCycle.getExtensionMBeanName());
    }

    /**
     * Tests the start method with good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testStartGood()
        throws Exception
    {
        install(Constants.BC_NAME);
        mLifeCycle.start();
        assertTrue("start failed: component state is " +
                   mComponent.getStatusAsString(), 
                   mComponent.isStarted());
    }

    /**
     * Tests the stop method with good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testStopGood()
        throws Exception
    {
        install(Constants.BC_NAME);
        mLifeCycle.start();
        mLifeCycle.stop();
        assertTrue("stop failed: component state is " +
                   mComponent.getStatusAsString(), 
                   mComponent.isStopped());
    }

    /**
     * Tests the shutDown method with good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownGood()
        throws Exception
    {
        install(Constants.BC_NAME);
        mLifeCycle.start();
        mLifeCycle.stop();
        mLifeCycle.shutDown();
        assertTrue("shutDown failed: component state is " +
                   mComponent.getStatusAsString(), 
                   mComponent.isInstalled());
    }

    /**
     * Tests the shutDownForce method with good result.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownForceGood()
        throws Exception
    {
        install(Constants.BC_NAME_BAD_SHUTDOWN);
        mLifeCycle.start();
        mLifeCycle.stop();
        mLifeCycle.shutDown(true);
        assertTrue("shutDownForce failed: component state is " +
                   mComponent.getStatusAsString(), 
                   mComponent.isInstalled());
    }

    /**
     * Tests the start method's multiple operation safeguards.
     * @throws Exception if an unexpected error occurs.
     */

    public void testStartBusy()
        throws Exception
    {
        install(Constants.BC_NAME_SLOW_START);
 
        mWaitNotify = new Boolean(false);
        mResults = new Throwable[2];

        mThreadCount = 0;
        Thread t1 = new Thread(new TestThread(START, 0));
        Thread t2 = new Thread(new TestThread(START, 1));

        t1.start();
        t2.start();

        synchronized(mWaitNotify)
        {
            mWaitNotify.wait();
        }
       
        assertTrue("start failed: component state is " +
            mComponent.getStatusAsString(), 
            mComponent.isStarted());

        if ( null == mResults[0] )  // thread t1 finished first
        {
            assertNotNull("start 2 succeeded, should have failed",
                mResults[1]);
            assertTrue("start 2 incorrect exception received: " +
                mResults[1].toString(),
                (-1 < mResults[1].getMessage().indexOf("JBIFW2011")));
        }
        else  // thread t2 finished first
        {
            assertNull("start 2 failed, should have succeeded: exception is " +
                mResults[1], mResults[1]);
            assertTrue("start 1 incorrect exception received: " +
                mResults[0].toString(),
                (-1 < mResults[0].getMessage().indexOf("JBIFW2011")));
        }
    }

    /**
     * Tests the stop method's multiple operation safeguards.
     * @throws Exception if an unexpected error occurs.
     */

    public void testStopBusy()
        throws Exception
    {
        install(Constants.BC_NAME_SLOW_STOP);
        mLifeCycle.start();
 
        mWaitNotify = new Boolean(false);
        mResults = new Throwable[2];

        mThreadCount = 0;
        Thread t1 = new Thread(new TestThread(STOP, 0));
        Thread t2 = new Thread(new TestThread(STOP, 1));

        t1.start();
        t2.start();

        synchronized(mWaitNotify)
        {
            mWaitNotify.wait();
        }
       
        assertTrue("start failed: component state is " +
            mComponent.getStatusAsString(), 
            mComponent.isStopped());

        if ( null == mResults[0] )  // thread t1 finished first
        {
            assertNotNull("stop 2 succeeded, should have failed",
                mResults[1]);
            assertTrue("stop 2 incorrect exception received: " +
                mResults[1].toString(),
                (-1 < mResults[1].getMessage().indexOf("JBIFW2011")));
        }
        else  // thread t2 finished first
        {
            assertNull("stop 2 failed, should have succeeded: exception is " +
                mResults[1], mResults[1]);
            assertTrue("stop 1 incorrect exception received: " +
                mResults[0].toString(),
                (-1 < mResults[0].getMessage().indexOf("JBIFW2011")));
        }
    }

    /**
     * Tests the shutDown method's multiple operation safeguards.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownBusy()
        throws Exception
    {
        install(Constants.BC_NAME_SLOW_SHUTDOWN);
        mLifeCycle.start();
 
        mWaitNotify = new Boolean(false);
        mResults = new Throwable[2];

        mThreadCount = 0;
        Thread t1 = new Thread(new TestThread(SHUTDOWN, 0));
        Thread t2 = new Thread(new TestThread(SHUTDOWN, 1));

        t1.start();
        t2.start();

        synchronized(mWaitNotify)
        {
            mWaitNotify.wait();
        }
       
        assertTrue("shutDown failed: component state is " +
            mComponent.getStatusAsString(), 
            mComponent.isInstalled());

        if ( null == mResults[0] )  // thread t1 finished first
        {
            assertNotNull("shutDown 2 succeeded, should have failed",
                mResults[1]);
            assertTrue("shutDown 2 incorrect exception received: " +
                mResults[1].toString(),
                (-1 < mResults[1].getMessage().indexOf("JBIFW2011")));
        }
        else  // thread t2 finished first
        {
            assertNull("shutDown 2 failed, should have succeeded: exception is " +
                mResults[1], mResults[1]);
            assertTrue("shutDown 1 incorrect exception received: " +
                mResults[0].toString(),
                (-1 < mResults[0].getMessage().indexOf("JBIFW2011")));
        }
    }

    /**
     * Tests the shutDown method's multiple operation safeguards with force.
     * @throws Exception if an unexpected error occurs.
     */

    public void testShutDownForceBusy()
        throws Exception
    {
        install(Constants.BC_NAME_SLOW_SHUTDOWN);
        mLifeCycle.start();
 
        mWaitNotify = new Boolean(false);
        mResults = new Throwable[2];

        mThreadCount = 0;
        Thread t1 = new Thread(new TestThread(SHUTDOWNFORCE, 0));
        Thread t2 = new Thread(new TestThread(SHUTDOWNFORCE, 1));

        t1.start();
        t2.start();

        synchronized(mWaitNotify)
        {
            mWaitNotify.wait();
        }
       
        assertTrue("shutDown failed: component state is " +
            mComponent.getStatusAsString(), 
            mComponent.isInstalled());
        assertNull("shutDown 1 failed, exception is " +
                mResults[0], mResults[0]);
        assertNull("shutDown 2 failed, exception is " +
                mResults[1], mResults[1]);
    }

    /**
     * Class to run on a separate thread to perform an operation.
     */
    class TestThread implements Runnable
    {
        /**
         * Operation to perform.
         */
        int mOperation;

        /**
         * Sequence number of thread.
         */
        int mSequence;

        /**
         * Constructor.
         * @param operation the operation to perform.
         */
        public TestThread(int operation, int sequence)
        {
            mOperation = operation;
            mSequence = sequence;
            ++mThreadCount;
        }

        /**
         * Separate thread to perform an operation on the component to test
         * the "busy" flag implementation.
         */
        public final void run()
        {
            try
            {
                switch (mOperation)
                {
                    case START:
                        mLifeCycle.start();
                        break;
                    case STOP:
                        mLifeCycle.stop();
                        break;
                    case SHUTDOWN:
                        mLifeCycle.shutDown();
                        break;
                    case SHUTDOWNFORCE:
                        mLifeCycle.shutDown(true);
                        break;
                }
                mResults[mSequence] = null;
            }
            catch ( Throwable ex )
            {
                mResults[mSequence] = ex;
                ex.printStackTrace();
            }
            synchronized ( mWaitNotify )
            {
                if ( --mThreadCount ==  0 )
                {
                    mWaitNotify.notifyAll();
                }
            }
        }
    }
}
