/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FastBinding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package restart;

import javax.xml.namespace.QName;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.jbi.messaging.InOnly;

/**
 * This binding is used to test framework restart processing.  The start()
 * method of ServiceUnitManager looks for an endpoint that should have been
 * activated by SlowEngine.  This should not result in an endpoint resolution 
 * errors since all service units should init() before start() is called.
 *
 * @author Sun Microsystems, Inc.
 */
public class FastBinding extends BaseComponent
{
    /** Create a new instance of FastBinding */
    public FastBinding()
    {
        mComponentName = "test-fast-binding";
    }
    
    /**
     * Start the deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    @Override
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info("FastBinding is looking for an endpoint for service " + 
                serviceUnitName);
        
        ServiceEndpoint endpoint = mContext.getEndpoint(
                new QName(serviceUnitName), serviceUnitName);
        
        if (endpoint == null)
        {
            throw new javax.jbi.management.DeploymentException(createErrorResult(
                    "start", "Failed to locate endpoint for service " + serviceUnitName));
        }
        
        // Try to send an exchange (exposes broken service connections)
        try
        {
            InOnly inOnly = mContext.getDeliveryChannel().createExchangeFactory().createInOnlyExchange();
            inOnly.setEndpoint(endpoint);
            mContext.getDeliveryChannel().send(inOnly);
        }
        catch (javax.jbi.messaging.MessagingException msgEx)
        {            
            throw new javax.jbi.management.DeploymentException(createErrorResult(
                    "start", msgEx.toString()));
        }
    }    
}
