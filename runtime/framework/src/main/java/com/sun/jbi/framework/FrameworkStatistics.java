/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.monitoring.FrameworkStatisticsMBean;
import java.util.Date;

/**
 * This class implements the MBean for collection of statistics for the
 * framework. All statistics are since the last JBI startup; they are all
 * reset when JBI is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public class FrameworkStatistics
    implements FrameworkStatisticsMBean
{
    /**
     * Time of the last JBI Framework startup.
     */
    private Date mLastRestartTime;

    /**
     * Amount of time in milliseconds taken for the last JBI Framework startup.
     */
    private long mStartupTime;

    /**
     * Number of component registry additions since the last JBI startup.
     */
    private long mRegistryAdds;

    /**
     * Number of component registry deletions since the last JBI startup.
     */
    private long mRegistryDeletes;

    //
    // Methods defined in the FrameworkStatisticsMBean interface.
    //

    /**
     * Get the time/date of the last JBI Framework restart.
     * @return The time/date of the last restart of the JBI Framework.
     */
    public Date getLastRestartTime()
    {
        return mLastRestartTime;
    }

    /**
     * Get the time in milliseconds used by the last restart of the JBI
     * framework.
     * @return The total startup time in milliseconds.
     */
    public long getStartupTime()
    {
        return mStartupTime;
    }

    /**
     * Get the count of component registry additions.
     * @return The total number of entries added to the component registry
     * since the last JBI startup.
     */
    public long getRegistryAdds()
    {
        return mRegistryAdds;
    }

    /**
     * Get the count of component registry deletions.
     * @return The total number of entries deleted from the component registry
     * since the last JBI startup.
     */
    public long getRegistryDeletes()
    {
        return mRegistryDeletes;
    }

    //
    // Instrumentation methods local to the framework. Note that the methods
    // which operate on fields not guaranteed to be thread-safe for updates
    // are synchronized to prevent data corruption.
    //

    /**
     * Set the time/date of the last JBI Framework restart.
     * @param lastRestartTime The time/date of the last JBI Framework restart.
     */
    synchronized void setLastRestartTime(Date lastRestartTime)
    {
        mLastRestartTime = lastRestartTime;
    }

    /**
     * Set the time used for the last JBI Framework restart.
     * @param startupTime The number of milliseconds used by the last JBI
     * Framework startup.
     */
    synchronized void setStartupTime(long startupTime)
    {
        mStartupTime = startupTime;
    }

    /**
     * Increment the count of component registry additions.
     */
    synchronized void incrementRegistryAdds()
    {
        ++mRegistryAdds;
    }

    /**
     * Increment the count of component registry deletions.
     */
    synchronized void incrementRegistryDeletes()
    {
        ++mRegistryDeletes;
    }

    /**
     * Reset component registry statistics.
     */
    synchronized void resetRegistryStatistics()
    {
        mRegistryAdds = 0;
        mRegistryDeletes = 0;
    }

}
