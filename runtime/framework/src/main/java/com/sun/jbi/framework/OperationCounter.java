/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OperationCounter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * This class provides a counter that is used with a set of threaded operations.
 * It provides methods for incrementing and decrementing the counter. This
 * class is used when a thread is creating a number of instances of a class
 * extending the <CODE>Operation</CODE> abstract class to run on separate
 * threads, optionally under a time limit. The creating thread creates an
 * instance of this class, and passes that instance to the constructor for the
 * <CODE>Operation</CODE> subclass instances as it creates them. The <CODE>
 * Operation</CODE> constructor calls the <CODE>incrememt</CODE> method on this
 * class, automatically keeping a count of the number of threaded operations
 * that have been created. All of the operations are then started on separate
 * threads, and the creating thread calls the <CODE>wait</CODE> method on the
 * instance of this class, optionally with the desired timeout value. As the
 * <CODE>Operation</CODE> instances complete their processing, their <CODE>
 * run</CODE> methods call the <CODE>decrement</CODE> method on the instance
 * of this class. When the last operation completes, the <CODE>decrement</CODE>
 * method sets the counter to zero, and calls <CODE>notify</CODE> to end the
 * wait on the original thread. If the wait times out, the <CODE>Operation
 * </CODE> instances can be checked to determine which one(s) did not complete.
 *
 * @author Sun Microsystems, Inc.
 */
class OperationCounter
{
    /**
     * Counter of outstanding operations.
     */
    private int mCounter;

    /**
     * Increment the counter.
     *
     * @return The new value of the counter.
     */
    synchronized int increment()
    {
        return ++mCounter;
    }

    /**
     * Decrement the counter. If it reaches zero, notify all waiting threads.
     *
     * @return The new value of the counter.
     */
    synchronized int decrement()
    {
        if ( --mCounter == 0 )
        {
            notifyAll();
        }
        return mCounter;
    }

    /**
     * Get the the counter value.
     *
     * @return The value of the counter.
     */
    synchronized int getValue()
    {
        return mCounter;
    }

}
