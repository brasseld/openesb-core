/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EnvironmentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.config.ConfigurationBuilder;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.xml.UpdaterImpl;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.util.EnvironmentAccess;
import com.sun.jbi.wsdl2.WsdlException;
import com.sun.jbi.wsdl2.WsdlFactory;

import java.io.File;

import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.naming.InitialContext;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * This context contains data needed by all components and services running in
 * the JBI environment.
 *
 * @author Sun Microsystems, Inc.
 */
public class EnvironmentContext
    implements com.sun.jbi.EnvironmentContext
{
    /**
     * Constant for error message when getInstance() called out of sequence.
     */
    private static final String NOT_YET_CREATED =
        "The Environment Context has not yet been created.";

    /**
     * Constant for the JBI default log level property value.
     */
    private static final String JBI_DEFAULT_LOG_LEVEL_VALUE =
        "INFO";

    /**
     * Constant for the JBI default log level property key.
     */
    private static final String JBI_DEFAULT_LOG_LEVEL_KEY =
        "com.sun.jbi.defaultLogLevel";

    /**
     * Constant for the JBI instance name property key.
     */
    private static final String JBI_INSTANCE_NAME_KEY =
        "com.sun.jbi.framework.JBIFramework.JbiName";

    /**
     * Constant for the JBI root directory name under the AppServer install
     * root.
     */
    private static final String JBI_ROOT = "jbi";

    /**
     * Constant for the JMX domain name for all JBI MBeans.
     */
    private static final String JMX_DOMAIN_NAME = "com.sun.jbi";

    /**
     * Constant for the configuration directory path.
     */
    private static final String CONFIG_PATH =
        "/system/private/config";

    /**
     * Constant for property not found.
     */
    private static final String PROPERTY_NOT_FOUND =
        "PROPERTY_NOT_FOUND";
    
    /**
     * Static reference to instance of this context.
     */
    private static EnvironmentContext sEnvironmentContext;

    /**
     * Handle to the Event Notifier MBean instance.
     */
    private EventNotifier mNotifier;

    /**
     * Handle to platform-specific context.
     */
    private PlatformContext mPlatformContext;

    /**
     * AppServer installation root directory path.
     */
    private String mAppServerInstallRoot;

    /**
     * AppServer instance root directory path.
     */
    private String mAppServerInstanceRoot;

    /**
     * Handle to the Component Framework.
     */
    private ComponentFramework mComponentFramework;

    /**
     * Handle to the Component Registry.
     */
    private ComponentRegistry mComponentRegistry;
    
    /**
     * Handle to the JBI Registry
     */
    private com.sun.jbi.management.registry.Registry mRegistry;

    /**
     * JBI default log level.
     */
    private Level mDefaultLogLevel;

    /**
     * Handle to the framework statistics.
     */
    private FrameworkStatistics mFrameworkStatistics;

    /**
     * Handle to the Properties from the lifecycle module definition.
     */
    private Properties mInitialProperties;

    /**
     * JBI installation root directory path.
     */
    private String mJbiInstallRoot;

    /**
     * JBI instance root directory path.
     */
    private String mJbiInstanceRoot;

    /**
     * Handle to the top-level Logger for the runtime.
     */
    private Logger mJbiLog;

    /**
     * Handle to the Logger for the framework.
     */
    private Logger mLog;

    /**
     * Global JBI logger MBean name; null if none was created.
     */
    private static ObjectName sJbiLogMBeanName;

    /**
     * Framework logger MBean name; null if none was created.
     */
    private static ObjectName sLogMBeanName;

    /**
     * Framework notification MBean name; null if none was created.
     */
    private static ObjectName sNotifierMBeanName;

    /**
     * Framework statistics MBean name; null if none was created.
     */
    private static ObjectName sStatsMBeanName;

    /**
     * Handle to the Management Service.
     */
    private com.sun.jbi.management.system.ManagementService mManagementService;
    
    /**
     * Handle to the Configuration Service.
     */
    private com.sun.jbi.management.system.ConfigurationService mConfigService;

    /**
     * Handle to the ManagementMessageFactory.
     */
    private com.sun.jbi.management.ManagementMessageFactory mMFactory;

    /**
     * Handle to the MBeanHelper helper class.
     */
    private com.sun.jbi.management.MBeanHelper mMBeanHelper;

    /**
     * Handle to the MBeanNames helper class.
     */
    private com.sun.jbi.management.MBeanNames mMBeanNames;

    /**
     * Handle to the VersionInfo class.
     */
    private com.sun.jbi.VersionInfo mVersionInfo;

    /**
     * Handle to the MBean server.
     */
    private MBeanServer mMBeanServer;

    /**
     * Handle to the AppServer's JNDI naming context.
     */
    private InitialContext mNamingContext;

    /**
     * Handle to the Normalized Message Service.
     */
    private com.sun.jbi.messaging.MessageService mNormalizedMessageService;

    /**
     * Table of handles to StringTranslators.
     */
    private HashMap mStringTranslators;

    /**
     * Handle to the Management Runtime Service.
     */
    private com.sun.jbi.management.facade.ManagementRuntimeService mMgmtRuntimeService;

    /**
     * StringTranslator for the framework.
     */
    private StringTranslator mTranslator;

    /**
     * TransactionManager for framework users.
     */
    private javax.transaction.TransactionManager mTransactionManager;

    /**
     * Flag to indicate TransactionManager is unavailable.
     */
    private boolean mTransactionManagerUnavailable;

    
    /**
     * JBI provider type.
     */
    private JBIProvider mProvider;
    
    /** 
     * Naming prefix.
     */
    private String mNamingPrefix;

    /** 
     * JBI Framework reference.
     */
    private JBIFramework mFramework;

    /**
     * The Registry DOM Document
     */
    private org.w3c.dom.Document mRegistryDocument;
   
    /**
     * JBI Framework initialization timestamp in milliseconds.
     */
    private long mJbiInitTime;
 
    /**
     * Constructor.
     * @param platform the platform context for the AppServer.
     * @param framework the representing the JBI Framework instance.
     * @param initialProperties is the Properties object provided by
     * the AppServer, containing all properties specified in the
     * definition of the JBI lifecycle module.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    EnvironmentContext(PlatformContext platform,
                       JBIFramework framework,
                       Properties initialProperties)
        throws javax.jbi.JBIException
    {

        // Set the static instance references
        
        sEnvironmentContext = this;
        EnvironmentAccess.setContext(this);

        // Save AppServer and JBI information
        mFramework = framework;
        mProvider = platform.getProvider();
        mPlatformContext = platform;
        mInitialProperties = initialProperties;

        // Initialize the table of StringTranslators and get a translator
        // for framework messages

        String pkgName = this.getClass().getPackage().getName();
        mStringTranslators = new HashMap();
        mTranslator = new StringTranslator(pkgName, null);
        mStringTranslators.put(pkgName, mTranslator);

        // Create the framework logger and set its log level

        createLogger(pkgName);

        // Initialize the AppServer and JBI installation and instance root
        // directory paths with the canonical paths and replace backslashes
        // backslashes with forward slashes.

        mAppServerInstallRoot = platform.getInstallRoot();
        mAppServerInstanceRoot = platform.getInstanceRoot();
        
        // Use com.sun.jbi.home if it's provided, otherwise default to 
        // platform install root + /jbi

        if (initialProperties.getProperty("com.sun.jbi.home") != null)
        {
            mJbiInstallRoot = initialProperties.getProperty("com.sun.jbi.home");
        }
        else
        {
            mJbiInstallRoot = mAppServerInstallRoot + File.separator + JBI_ROOT;
        }
        
        mJbiInstanceRoot = mAppServerInstanceRoot + File.separator + JBI_ROOT;
        File dummyFile = new File(mJbiInstanceRoot);
        try
        {
            mJbiInstanceRoot = dummyFile.getCanonicalPath();
        }
        catch (java.io.IOException ioEx)
        {
            ; // Ignore - just fall through
        }
        mJbiInstanceRoot = mJbiInstanceRoot.replace('\\', '/');
        
        // Set platform instance root and install root
        System.setProperty(PlatformContext.INSTALL_ROOT_TOKEN, 
                mAppServerInstallRoot);
        System.setProperty(PlatformContext.INSTANCE_ROOT_TOKEN, 
                mAppServerInstanceRoot);

        // Save MBean server and create MBean helpers and framework MBeans.
        mMBeanServer = platform.getMBeanServer();
        mMBeanNames = new com.sun.jbi.management.support.MBeanNamesImpl(
            JMX_DOMAIN_NAME, platform.getInstanceName());

        mMBeanHelper = new com.sun.jbi.management.support.MBeanHelper(
            mMBeanServer, mMBeanNames, mLog);

        mFrameworkStatistics = new FrameworkStatistics();
        mNotifier = new EventNotifier(this);

        createMBeans();

        // Create version info instance (version info is compiled in)

        mVersionInfo =
            (com.sun.jbi.VersionInfo) new com.sun.jbi.management.VersionInfo();

        if (mLog.isLoggable(Level.FINE))
        {
            // Log JBI information
            mLog.fine("JBI instance root set to " + mJbiInstanceRoot);
            if ( null != mInitialProperties )
            {
                mLog.fine("Initial properties are:");
                java.io.ByteArrayOutputStream bos =
                    new java.io.ByteArrayOutputStream();
                mInitialProperties.list(new java.io.PrintWriter(bos));
                mLog.fine(mTranslator.getString(
                    LocalStringKeys.EC_INITIAL_PROPERTIES_LIST, bos));
            }
        }
 
        // Create instances of all services
        mComponentFramework = new ComponentFramework();
        mComponentRegistry = new ComponentRegistry();
        mConfigService = 
            new com.sun.jbi.management.system.ConfigurationService(); 
        mManagementService =
            new com.sun.jbi.management.system.ManagementService();
        mMFactory =
            new com.sun.jbi.management.system.ManagementMessageFactoryImpl();
        mNormalizedMessageService =
            new com.sun.jbi.messaging.MessageService();
        mMgmtRuntimeService =
            new com.sun.jbi.management.facade.ManagementRuntimeService();
    }

    /**
     * Destroy the instance of this context created by the constructor.
     */
    void destroyInstance()
    {
        if ( null != sJbiLogMBeanName )
        {
            unregisterMBean(sJbiLogMBeanName);
        }
        if ( null != sLogMBeanName )
        {
            unregisterMBean(sLogMBeanName);
        }
        if ( null != sNotifierMBeanName )
        {
            unregisterMBean(sNotifierMBeanName);
        }
        if ( null != sStatsMBeanName )
        {
            unregisterMBean(sStatsMBeanName);
        }
        sEnvironmentContext = null;
    }

    /**
     * Get the instance of this context created by the constructor.
     * @return instance of the EnvironmentContext created by the constructor.
     */
    public static EnvironmentContext getInstance()
    {
        if ( null == sEnvironmentContext )
        {
            throw new java.lang.IllegalStateException(NOT_YET_CREATED);
        }
        return sEnvironmentContext;
    }

//*****************************************************************************
//***                                                                       ***
//*** Framework-internal accessor methods.                                  ***
//***                                                                       ***
//*****************************************************************************

    /**
     * Get the Component Framework service handle.
     * @return The ComponentFramework instance.
     */
    ComponentFramework getComponentFramework()
    {
        return mComponentFramework;
    }

    /**
     * Get the Component Registry service handle.
     * @return The ComponentRegistry instance.
     */
    ComponentRegistry getComponentRegistry()
    {
        return mComponentRegistry;
    }
    
    /**
     * Get the framework statistics handle.
     * @return The FrameworkStatistics instance.
     */
    FrameworkStatistics getFrameworkStatistics()
    {
        return mFrameworkStatistics;
    }

    /**
     * Get the top-level logger for the runtime.
     * @return The Logger instance.
     */
    Logger getJbiLogger()
    {
        return mJbiLog;
    }

    /**
     * Get the logger for the framework.
     * @return The Logger instance.
     */
    Logger getLogger()
    {
        return mLog;
    }

    /**
     * Get the timeout value for component life cycle operations.
     * @return The timeout value in milliseconds for calls to the component's
     * ComponentLifeCycle methods.
     */
    long getComponentTimeout()
    {
        long componentTimeout = 0;
        Object value = getConfigurationAttribute(ConfigurationCategory.Installation, 
            com.sun.jbi.management.config.InstallationConfigurationFactory.COMPONENT_TIMEOUT);
        if ( value != null )
        {
            componentTimeout = (Integer) value;
        }
        return componentTimeout;
    }

    /**
     * Get the timeout value for Service Unit deploy/undeploy operations.
     * @return The timeout value in milliseconds for calls to the component's
     * ServiceUnitManager deploy and undeploy methods.
     */
    long getDeploymentTimeout()
    {
     
        long deploymentTimeout = 0;
        
        Object value = getConfigurationAttribute(ConfigurationCategory.Deployment, 
            com.sun.jbi.management.config.DeploymentConfigurationFactory.DEPLOYMENT_TIMEOUT);
        
        if ( value != null )
        {
            deploymentTimeout = (Integer) value;
        }
        
        return deploymentTimeout;
    }

    /**
     * Get the timeout value for component install/uninstall operations.
     * @return The timeout value in milliseconds for calls to the component's
     * Bootstrap onInstall and onUninstall methods.
     */
    long getInstallationTimeout()
    {
        long installationTimeout = 0;
        
        Object value = getConfigurationAttribute(ConfigurationCategory.Installation, 
            com.sun.jbi.management.config.InstallationConfigurationFactory.INSTALLATION_TIMEOUT);
        
        if ( value != null )
        {
            installationTimeout = (Integer) value;
        }
        
        return installationTimeout;
    }

    /**
     * Get the timeout value for Service Unit life cycle operations.
     * @return The timeout value in milliseconds for calls to the component's
     * ServiceUnitManager life cycle methods.
     */
    long getServiceUnitTimeout()
    {

        long serviceUnitTimeout = 0;
        
        Object value = getConfigurationAttribute(ConfigurationCategory.Deployment, 
            com.sun.jbi.management.config.DeploymentConfigurationFactory.SERVICE_UNIT_TIMEOUT);
        
        if ( value != null )
        {
            serviceUnitTimeout = (Integer) value;
        }
        
        return serviceUnitTimeout;
    }

//*****************************************************************************
//***                                                                       ***
//*** Framework-internal mutator methods.                                   ***
//***                                                                       ***
//*****************************************************************************

    /**
     * Set the installation root directory of the AppServer.
     * @param installRoot String containing the installation root directory
     * of the AppServer.
    void setAppServerInstallRoot(String installRoot)
    {
        mAppServerInstallRoot = installRoot;
    }
     */

    /**
     * Set the instance root directory of the AppServer.
     * @param instanceRoot String containing the instance root directory
     * of the AppServer.
     */
    void setAppServerInstanceRoot(String instanceRoot)
    {
        mAppServerInstanceRoot = instanceRoot;
    }

    /**
     * Set the component timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setComponentTimeout(long timeout)
    {   
        setConfigurationAttribute(ConfigurationCategory.Installation, 
            com.sun.jbi.management.config.InstallationConfigurationFactory.COMPONENT_TIMEOUT,
            new Long(timeout));
    }

    /**
     * Set the deployment timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setDeploymentTimeout(long timeout)
    {
        setConfigurationAttribute(ConfigurationCategory.Deployment, 
            com.sun.jbi.management.config.DeploymentConfigurationFactory.DEPLOYMENT_TIMEOUT,
            new Long(timeout));
    }

    /**
     * Set the installation timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setInstallationTimeout(long timeout)
    {
        setConfigurationAttribute(ConfigurationCategory.Installation, 
            com.sun.jbi.management.config.InstallationConfigurationFactory.INSTALLATION_TIMEOUT,
            new Long(timeout));
    }

    /**
     * Set the service unit timeout value (used for junit testing only).
     * @param timeout the timeout value in milliseconds.
     */
    void setServiceUnitTimeout(long timeout)
    {
        setConfigurationAttribute(ConfigurationCategory.Deployment, 
            com.sun.jbi.management.config.DeploymentConfigurationFactory.SERVICE_UNIT_TIMEOUT,
            new Long(timeout));
    }

    /**
     * Set the installation root directory of the JBI framework.
     * @param installRoot String containing the installation root directory
     * of the JBI framework
     */
    void setJbiInstallRoot(String installRoot)
    {
        mJbiInstallRoot = installRoot;
    }

    /**
     * Set the instance root directory of the JBI framework.
     * @param instanceRoot String containing the instance root directory
     * of the JBI framework
     */
    void setJbiInstanceRoot(String instanceRoot)
    {
        mJbiInstanceRoot = instanceRoot;
    }

    /**
     * Set the JNDI naming context.
     * @param namingContext the JNDI naming context from the AppServer.
     */
    void setManagementService(com.sun.jbi.management.system.ManagementService ms)
    {
        mManagementService = ms;
    }

    /**
     * Set the JNDI naming context.
     * @param namingContext the JNDI naming context from the AppServer.
     */
    void setNamingContext(InitialContext namingContext)
    {
        mNamingContext = namingContext;
    }

    /**
     * Set the JNDI naming prefix.
     * @param prefix naming prefix for binding JNDI names.
     */
    void setNamingPrefix(String prefix)
    {
        mNamingPrefix = prefix;
    }
    
    /**
     * Set the platform context implementation.
     * @param platform platform context impl
     */
    void setPlatformContext(PlatformContext platform)
    {
        mPlatformContext = platform;
    }

    /**
     * Set the JBI Framework initialization timestamp in milliseconds.
     * @param initialization timestamp in milliseconds.
     */
    void setJbiInitTime(long jbiInitTime)
    {
     	mJbiInitTime = jbiInitTime;
    }

    
//*****************************************************************************
//***                                                                       ***
//*** Public accessor methods.                                              ***
//***                                                                       ***
//*****************************************************************************

    /**
     * Get the installation root directory of the AppServer.
     * @return String containing the installation root directory path
     */
    public String getAppServerInstallRoot()
    {
        return mAppServerInstallRoot;
    }

    /**
     * Get the instance root directory of the AppServer instance.
     * @return String containing the instance root directory path
     */
    public String getAppServerInstanceRoot()
    {
        return mAppServerInstanceRoot;
    }

    /**
     * Get the Component Manager handle.
     * @return The ComponentManager handle, which is a thin interface to the
     * ComponentFramework service.
     */
    public com.sun.jbi.ComponentManager getComponentManager()
    {
        return (com.sun.jbi.ComponentManager) mComponentFramework;
    }

    /**
     * Get the Component Query handle. This returns the ComponentQuery with this
     * instances target.
     *
     * @return the ComponentQuery for the current server instance. This ComponentQuery
     * provides information on components and shared libraries installed to the
     * server instance.
     */
    public com.sun.jbi.ComponentQuery getComponentQuery()
    {   
        try
        {
            return ((Registry) getRegistry()).getComponentQuery();
        }
        catch (com.sun.jbi.management.registry.RegistryException rex)
        {
            mLog.warning(rex.getMessage());
            return null;
        }
    }

    /**
     * Get the ComponentQuery for a specified target
     * @return The ComponentQuery instance.
     * @param targetName - either "domain" or a valid server / cluster name
     */
    public com.sun.jbi.ComponentQuery getComponentQuery(String targetName)
    {
        try
        {
            return ((Registry) getRegistry()).getComponentQuery(targetName);
        }
        catch (com.sun.jbi.management.registry.RegistryException rex)
        {
            mLog.warning(rex.getMessage());
            return null;
        }
    }
    
    /**
     * Get the PlatformContext implementation.
     * @return The PlatformContext instance.
     */
    public PlatformContext getPlatformContext()
    {
        return mPlatformContext;
    }

    /**
     * Get a reference to the persisted JBI registry. 
     * 
     * The registry is created if not already available and the result is cached for
     * later callers.
     *
     * The framework will transition from lazy to normal if the registry is requested
     * (or if the Framework is explicitly enabled.) After creating the registry we will
     * make sure the Framework is ready before continuing.
     *
     * Note: JBIFramework.frameReady() will indirectly call getRegistry() and getRegistry()
     * will directly call JBIFramework.frameworkRead(). The recursion is broken by carefully
     * setting the internal framework ready flag. 
     * 
     * See: JBIFramework.readyFramework() and JBIFramework.prepare()
     *
     * @return Registry instance
     */
    public com.sun.jbi.registry.Registry getRegistry()
    {
        boolean     checkFramework = false;

        synchronized (this)
        {
            if (mRegistry == null)
            {            
                getJustRegistry();
                checkFramework = true;
           }
        }
        if (checkFramework)
        {
            mFramework.frameworkReady();
        }
        return mRegistry;
    }
    
    /**
     * This is used by the ComponentRegistry Service to bypass the deferred initialization checks.
     */
    public com.sun.jbi.registry.Registry getJustRegistry()
    {
        if (mRegistry == null)
        {
            java.util.Properties props = new java.util.Properties();
            props.setProperty(
                com.sun.jbi.management.registry.Registry.REGISTRY_FOLDER_PROPERTY,
                getJbiInstanceRoot() + java.io.File.separator + "config" );

            // Create an instance of management context
            com.sun.jbi.management.system.ManagementContext mgmtCtx = 
                    new com.sun.jbi.management.system.ManagementContext(this);

            try
            {
                mRegistry =  com.sun.jbi.management.registry.RegistryBuilder.buildRegistry( 
                    new com.sun.jbi.management.registry.RegistrySpecImpl(
                        com.sun.jbi.management.registry.RegistryType.XML, props, mgmtCtx));
                ((UpdaterImpl)mRegistry.getUpdater()).correctTimestamps();
            }
            catch (com.sun.jbi.management.registry.RegistryException rEx)
            {
                // Registry initialization has failed.
                String errMsg = mTranslator.getString(
                    LocalStringKeys.EC_JBI_REGISTRY_INIT_FAILED,
                    rEx.getMessage());
                mLog.warning(rEx.getMessage());
                throw new RuntimeException(errMsg, rEx);
            }
            mLog.fine("JBIEnvironmentContext: Create the registry...");
        }
        
        return (mRegistry);
    }
    
    /**
     * This method is for junit tests to use a Scaffold registry.
     */
    void setRegistry(Registry registry)
    {
        mRegistry = registry;
    }
    
    /**
     * Get a read-only reference to the persisted JBI Registry. A DOM registry document 
     * object is returned. If the registry file is missing or cannot be read then a null 
     * value is returned.
     *
     * @return the registry document
     */
    public org.w3c.dom.Document getReadOnlyRegistry()
    {
        if ( mRegistryDocument == null )
        {
            try
            {
                // The registry file
                File registryFile = new File( getJbiInstanceRoot() + "/config/jbi-registry.xml");

                if (registryFile.canRead())
                {        
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    mRegistryDocument = db.parse(registryFile);
                }
            }
            catch (Exception ex)
            {
                mLog.warning(ex.getMessage());
            }
        }
        return mRegistryDocument;
    }
    
    
    /**
     * Indicates whether or not the JBI framework has been fully started.  This
     * method provides clients with a way of determining if the JBI framework
     * started up in passive mode as a result of on-demand initialization.  
     * The 'start' parameter instructs the framework to
     * start completely if it has not already done so.  If the framework has
     * already been started, the request to start again is ignored.
     * @param start requests that the framework start completely before
     *  returning.
     * @return true if the framework is completely started, false otherwise.
     */
    public boolean isFrameworkReady(boolean start)
    {
        if (start)
        {
            mFramework.frameworkReady();
        }
        
        return mFramework.isFrameworkReady();
    }

    /**
     * Get the Connection Manager handle.
     * @return The ConnectionManager instance.
     */
    public com.sun.jbi.messaging.ConnectionManager getConnectionManager()
    {
        return mNormalizedMessageService.getConnectionManager();
    }

    /**
     * Get the default log level specified at startup.
     * @return The default log level as a Level instance.
     */
    public java.util.logging.Level getDefaultLogLevel()
    {
        return mDefaultLogLevel;
    }

    /**
     * Get the initial properties provided by the AppServer from the
     * properties specified in the lifecycle module definition in the
     * domain.xml file.
     * @return The initial properties from the AppServer.
     */
    public java.util.Properties getInitialProperties()
    {
        return mInitialProperties;
    }

   
    /**
     * Get the installation root directory of the JBI schemaorg_apache_xmlbeans.system.
     * @return The JBI installation root directory path.
     */
    public String getJbiInstallRoot()
    {
        return mJbiInstallRoot;
    }

    /**
     * Get the instance root directory of the JBI schemaorg_apache_xmlbeans.system.
     * @return The JBI instance root directory path.
     */
    public String getJbiInstanceRoot()
    {
        return mJbiInstanceRoot;
    }

    /**
     * Get a handle to the class implementing management for the named
     * JBI schemaorg_apache_xmlbeans.system service.
     * @param aServiceName - the name of the JBI schemaorg_apache_xmlbeans.system service.
     * @return The class implementing management for the schemaorg_apache_xmlbeans.system service.
     */
    public Object getManagementClass(String aServiceName)
    {
        if (aServiceName.equals(mMBeanNames.SERVICE_NAME_MESSAGE_SERVICE))
        {
            return (Object) mNormalizedMessageService;
        }

        return null;
    }

    /**
     * Get the management message factory which enables JBI components
     * to construct status and exception messages.
     * @return The ManagementMessageFactory instance.
     */
    public com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory()
    {
        return mMFactory; 
    }

    /**
     * Get the Management Service handle.
     * @return The ManagementService instance.
     */
    public com.sun.jbi.management.system.ManagementService getManagementService()
    {
        return mManagementService;
    }
    
    /**
     * Get the Configuration Service handle.
     * @return The ConfigurationService instance.
     */
    public com.sun.jbi.management.system.ConfigurationService getConfigurationService()
    {
        return mConfigService;
    }
 
    /**
     * Get the MBean helper service which enables JBI components to request
     * that the Management Service create and register MBeans on their behalf.
     * @return The MBeanHelper instance.
     */
    public com.sun.jbi.management.MBeanHelper getMBeanHelper()
    {
        return mMBeanHelper;
    }

    /**
     * Get the MBean naming service which enables JBI components to construct
     * MBean names that follow the JBI conventions.
     * @return The MBeanNames instance.
     */
    public com.sun.jbi.management.MBeanNames getMBeanNames()
    {
        return mMBeanNames;
    }
    
    /**
     * Get the MBean server with which all MBeans are registered.
     * @return javax.management.MBeanServer the main MBean server
     */
    public javax.management.MBeanServer getMBeanServer()
    {
        return mMBeanServer;
    }

    /**
     * Get the JNDI naming context currently in effect.
     * @return javax.naming.InitialContext the JNDI naming context
     */
    public javax.naming.InitialContext getNamingContext()
    {
        return mNamingContext;
    }

    /** Gets the naming prefix.
     * @return naming prefix
     */
    public String getNamingPrefix()
    {
        return mNamingPrefix;
    }   
    
    /**
     * Get the Normalized Message Service handle.
     * @return The NormalizedMessageService instance.
     */
    public com.sun.jbi.messaging.MessageService getNormalizedMessageService()
    {
        return mNormalizedMessageService;
    }

    /**
     * Get the Event Notifier for emitting event notifications from the runtime.
     * @return The EventNotifier instance.
     */
    public EventNotifierCommon getNotifier()
    {
        return (EventNotifierCommon) mNotifier;
    }

    /**
     * Returns the Provider of JBI.
     * @return provider
     */
    public JBIProvider getProvider()
    {
        return mProvider;        
    }
    
    /**
     * Get the Service Unit registration handle.
     * @return The ServiceUnitRegistration handle, which is a thin interface
     * to the ComponentRegistry service.
     */
    public com.sun.jbi.ServiceUnitRegistration getServiceUnitRegistration()
    {
        return (com.sun.jbi.ServiceUnitRegistration) mComponentRegistry;
    }

    /**
     * Get the StringTranslator for a specified package name.
     * @param packageName - the name of the package containing the resource
     * bundle to be used by this StringTranslator.
     * @return The StringTranslator instance.
     */
    public com.sun.jbi.StringTranslator getStringTranslator(String packageName)
    {
        StringTranslator translator =
            (StringTranslator) mStringTranslators.get(packageName);

        if ( null == translator )
        {
            translator = new StringTranslator(packageName, null);
            mStringTranslators.put(packageName, translator);
            mLog.finer("StringTranslator created for package " + packageName);
        }

        return (com.sun.jbi.StringTranslator) translator;
    }

    /**
     * Get the StringTranslator for a specified package name using the class
     * loader for a specified component ID.
     * @param packageName - the name of the package containing the resource
     * bundle to be used by this StringTranslator.
     * @param componentId - the component ID for which the classloader is to
     * be used.
     * @return The StringTranslator instance.
     */
    public com.sun.jbi.StringTranslator getStringTranslator(
        String packageName, String componentId)
    {
        ClassLoader cl = null;
        StringTranslator translator =
            (StringTranslator) mStringTranslators.get(packageName);

        if ( null == translator )
        {
            // Try to get the classloader for the specified component ID. If
            // this fails, log an error, and use the default classloader for
            // attempting to load the resource bundle. The StringTranslator
            // constructor will log further diagnostics.

            try
            {
                cl = ClassLoaderFactory.getInstance().
                    getComponentClassLoader(componentId);
            }
            catch ( javax.jbi.JBIException ex )
            {
                mLog.severe(mTranslator.getString(
                    LocalStringKeys.EC_STRING_TRANSLATOR_CLASSLOADER_NOT_FOUND,
                    componentId, packageName, ex.getMessage()));
            }
            translator = new StringTranslator(packageName, cl);
            mStringTranslators.put(packageName, translator);
        }

        return (com.sun.jbi.StringTranslator) translator;
    }

    /**
     * Get the StringTranslator for a specified object.
     * @param object - an object in the package that contains the resource
     * bundle to be used for this StringTranslator.
     * @return The StringTranslator instance.
     */
    public com.sun.jbi.StringTranslator getStringTranslatorFor(Object object)
    {
        return getStringTranslator(object.getClass().getPackage().getName());
    }

    /**
     * Get the Management Runtime Service handle.
     * @return The ManagementRuntimeService instance.
     */
    public com.sun.jbi.management.facade.ManagementRuntimeService 
    getMgmtRuntimeService()
    {
        return mMgmtRuntimeService;
    }
    
    /**
     * Get a TransactionManager from the AppServer.
     * @return A TransactionManager instance.
     */
    public javax.transaction.TransactionManager getTransactionManager()
    {
        if ( null == mTransactionManager && !mTransactionManagerUnavailable )
        {
            try
            {
                mTransactionManager = mPlatformContext.getTransactionManager();
            }
            catch ( Exception ex )
            {
                mTransactionManagerUnavailable = true;
                mLog.severe(mTranslator.getString(
                    LocalStringKeys.EC_TRANSACTION_MANAGER_NOT_FOUND,
                    ex.getClass().getName()));
                mLog.severe(mTranslator.stackTraceToString(ex));
            }
        }
        return mTransactionManager;
    }

    /**
     * Get the VersionInfo for this runtime.
     * @return The VersionInfo instance.
     */
    public com.sun.jbi.VersionInfo getVersionInfo()
    {
        return mVersionInfo;
    }

    /**
     * Get a copy of the WSDL factory. This needs to be done before
     * any reading, writing, or manipulation of WSDL documents can
     * be performed using the WSDL API.
     *
     * @return An instance of the WSDL factory.
     * @exception WsdlException If the factory cannot be instantiated.
     */
    public WsdlFactory getWsdlFactory() throws WsdlException
    {
      return com.sun.jbi.wsdl2.impl.WsdlFactory.newInstance();
    }

    /**
     * Get the JBI Framework initialization timestamp in milliseconds. 
     * @return the time, in milliseconds, when the JBI Framework was initialized.
     */
    public long getJbiInitTime()
    {
     	return mJbiInitTime;
    }
    

//*****************************************************************************
//***                                                                       ***
//*** Private methods                                                       ***
//***                                                                       ***
//*****************************************************************************

    /**
     * Create framework logger and initialize its log level.
     * @param pkgName the name of the framework package to be used as the
     * logger name.
     */
    private void createLogger(String pkgName)
    {
        // Create the top-level JBI logger which is used primarily to provide
        // a logger from which other loggers can inherit a default log level. 

        mJbiLog = Logger.getLogger("com.sun.jbi");

        // Create the logger for the framework. This logger will inherit the
        // level from the top-level logger.

        mLog = Logger.getLogger(pkgName);

        // If no log level is set for the top-level logger, see if a level
        // was provided through the initial properties.

        if ( null == mJbiLog.getLevel() )
        {
            // Validate the log level specified in the initial properties, if
            // available, and save it for access by the getDefaultLogLevel()
            // method. If the value is invalid or missing, use the hard-coded
            // default.
            String logLevelString = null;

            if ( null != mInitialProperties )
            {
                logLevelString = 
                    mInitialProperties.getProperty(JBI_DEFAULT_LOG_LEVEL_KEY,
                        JBI_DEFAULT_LOG_LEVEL_VALUE);
            }
            else
            {
                logLevelString = JBI_DEFAULT_LOG_LEVEL_VALUE;
            }
            try
            {
                mDefaultLogLevel = Level.parse(logLevelString);
            }
            catch (Exception e)
            {
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.EC_INVALID_LOG_LEVEL, logLevelString,
                    Level.INFO.getName()));
                mDefaultLogLevel = Level.INFO;
            }
            mJbiLog.setLevel(mDefaultLogLevel);
        }
        else
        {
            mDefaultLogLevel = mJbiLog.getLevel();
        }
        System.setProperty(JBI_DEFAULT_LOG_LEVEL_KEY, mDefaultLogLevel.getName());
    }

    /**
     * Create framework MBeans.
     */
    private void createMBeans()
    {
        // Create the framework logger MBean for the global JBI top-level
        // logger.

        try
        {
            com.sun.jbi.management.support.DefaultLoggerMBeanImpl logMBean =
                new com.sun.jbi.management.support.DefaultLoggerMBeanImpl(
                    mJbiLog, "JBI default");
            sJbiLogMBeanName = registerMBean(logMBean,
                com.sun.jbi.management.common.LoggerMBean.class,
                MBeanNames.CONTROL_TYPE_LOGGER,
                MBeanNames.SERVICE_NAME_JBI, mJbiLog);
        }
        catch ( Exception ex )
        {
            mLog.severe(ex.getMessage());
            mLog.warning(mTranslator.getString(
                LocalStringKeys.EC_NO_LOGGER_MBEAN));
        }

 
        // Create the event notifier MBean
        try
        {
            sNotifierMBeanName = registerMBean(mNotifier,
                EventNotifierMBean.class,
                MBeanNames.CONTROL_TYPE_NOTIFICATION);
            mNotifier.instanceStarting();
        }
        catch ( javax.jbi.JBIException ex )
        {
            mLog.severe(ex.getMessage());
            mLog.warning(mTranslator.getString(
                LocalStringKeys.EC_NO_NOTIFICATION_MBEAN));
        }

        // Create the framework Statistics MBean
        try
        {
            sStatsMBeanName = registerMBean(mFrameworkStatistics,
                com.sun.jbi.monitoring.FrameworkStatisticsMBean.class,
                MBeanNames.CONTROL_TYPE_STATISTICS);
        }
        catch ( javax.jbi.JBIException ex )
        {
            mLog.severe(ex.getMessage());
            mLog.warning(mTranslator.getString(
                LocalStringKeys.EC_NO_STATISTICS_MBEAN));
        }

    }
    
    /**
     * Create Logger MBean
     */
    public void createLoggerMBeans()
    {
                
        /**
         * The logger for the Framework is the SystemServiceLoggerMBeanImpl
         * instance, this instance listens to changes in the log level from
         * the facade/instance Logger Configuration MBean. 
         */
        try
        {
            com.sun.jbi.management.support.SystemServiceLoggerMBeanImpl logMBean =
                new com.sun.jbi.management.support.SystemServiceLoggerMBeanImpl(
                    mLog, "Framework");
            sLogMBeanName = registerMBean(logMBean,
                com.sun.jbi.management.common.LoggerMBean.class,
                MBeanNames.CONTROL_TYPE_LOGGER,
                MBeanNames.SERVICE_NAME_FRAMEWORK, mLog);
        }
        catch ( Exception ex )
        {
            mLog.severe(ex.getMessage());
            mLog.warning(mTranslator.getString(
                LocalStringKeys.EC_NO_LOGGER_MBEAN));
        }
    }

    /**
     * Create and register an MBean with the main MBean Server if it is
     * available.
     * @param instance the MBean implementation instance.
     * @param interfaceClass the interface implemented by the instance.
     * @param mbeanType the type of MBean to be registered. The value of this
     * is set using constants from the MBeanNames class.
     * @return the MBean object name of the created MBean or null if no MBean
     * server is available.
     * @throws javax.jbi.JBIException if the MBean creation or registration
     * fails.
     */
    private ObjectName registerMBean(
        Object instance,
        Class interfaceClass,
        String mbeanType)
        throws javax.jbi.JBIException
    {
        return registerMBean(instance, interfaceClass, mbeanType, null, null);
    }

    /**
     * Create and register an MBean with the main MBean Server if it is
     * available.
     * @param instance the MBean implementation instance.
     * @param interfaceClass the interface implemented by the instance.
     * @param mbeanType the type of MBean to be registered. The value of this
     * is set using constants from the MBeanNames class.
     * @param service the service name if a logger MBean is being registered,
     * or null if not. This is to allow the top-level JBI logger and the
     * framework logger to be registered separately.
     * @param logger the Logger if a logger MBean is being registered, or null
     * if not.
     * @return the MBean object name of the created MBean or null if no MBean
     * server is available.
     * @throws javax.jbi.JBIException if the MBean creation or registration
     * fails.
     */
    private ObjectName registerMBean(
        Object instance,
        Class interfaceClass,
        String mbeanType,
        String service,
        Logger logger)
        throws javax.jbi.JBIException
    {
        // If there is no MBean Server available, give up.

        if ( null == getMBeanServer() )
        {
            return null;
        }

        mLog.finer("Registering framework " + mbeanType + " MBean");

        // Create the MBean.

        StandardMBean mbean = null;
        try
        {
            /**
             * If the instance is a StandardMBean register the instance,
             * JMX introspection will expose the Management Interfaces.
             */
            if ( instance instanceof StandardMBean )
            {
                mbean = (StandardMBean) instance;
            }
            else
            {
                mbean = new StandardMBean(instance, interfaceClass);
            }
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            String exName = ncEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.EC_MBEAN_CREATION_FAILED,
                exName), ncEx);
        }

        // Create the MBean ObjectName using the MBean control type. Note
        // the special treatment for logger MBeans.

        ObjectName mbeanName = null;
        if ( mMBeanNames.CONTROL_TYPE_LOGGER == mbeanType )
        {
            mbeanName = mMBeanNames.getSystemServiceLoggerMBeanName(
                service, logger);
        }
        else
        {
            mbeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_FRAMEWORK, mbeanType);
        }
        if ( null == mbeanName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.EC_MBEAN_NAME_CREATION_FAILED,
                mMBeanNames.SERVICE_NAME_FRAMEWORK, mbeanType);
            throw new javax.jbi.JBIException(msg);
        }

        // Register the MBean. If it is already registered, unregister it
        // first, as it is an old registration with a stale instance reference.

        try
        {
            if ( getMBeanServer().isRegistered(mbeanName) )
            {
                try
                {
                    getMBeanServer().unregisterMBean(mbeanName);
                }
                catch ( javax.management.InstanceNotFoundException infEx )
                {
                    ; // Just ignore this error
                }
            }
            getMBeanServer().registerMBean(mbean, mbeanName);
        }
        catch ( javax.management.InstanceAlreadyExistsException iaeEx )
        {
            String exName = iaeEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.EC_MBEAN_REGISTRATION_FAILED,
                exName), iaeEx);
        }
        catch ( javax.management.MBeanRegistrationException mbrEx )
        {
            String exName = mbrEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.EC_MBEAN_REGISTRATION_FAILED,
                exName), mbrEx);
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            String exName = ncEx.getClass().getName();
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.EC_MBEAN_REGISTRATION_FAILED,
                exName), ncEx);
        }
        return mbeanName;
    }

    /**
     * Unregister an MBean with the main MBean Server if it is available.
     * @param mbeanName the MBean ObjectName.
     */
    private void unregisterMBean(ObjectName mbeanName)
    {
        // If there is no MBean Server available, give up.

        if ( null == getMBeanServer() )
        {
            return;
        }

        mLog.finer("Unregistering framework MBean: " + mbeanName);

        // Unregister the MBean. If it is already registered, unregister it
        // first, as it is an old registration with a stale instance reference.

        if ( getMBeanServer().isRegistered(mbeanName) )
        {
            try
            {
                getMBeanServer().unregisterMBean(mbeanName);
            }
            catch ( javax.management.InstanceNotFoundException infEx )
            {
                ; // Just ignore this error
            }
            catch ( javax.management.MBeanRegistrationException mbrEx )
            {
                String exName = mbrEx.getClass().getName();
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.EC_MBEAN_UNREGISTRATION_FAILED,
                    mbeanName, exName));
            }
        }
    }
    
    /**
     * This method is used to find out if start-on-deploy is enabled.
     * When this is enabled components are started automatically when 
     * there is deployment for them. 
     * 
     * By default start-on-deploy is enabled. 
     *  
     * @return true if startOnDeploy is enabled.
     */
    public boolean isStartOnDeployEnabled()
    {
        
        boolean startOnDeploy = true;
        
        Object value = getConfigurationAttribute(ConfigurationCategory.Deployment, 
            com.sun.jbi.management.config.DeploymentConfigurationFactory.START_ON_DEPLOY);
        
        if ( value != null )
        {
            startOnDeploy = (Boolean) value;
        }
        
        return startOnDeploy;
    }
    
    /**
     * This method is used to find out if startOnVerify is enabled.
     * When this is enabled components are started automatically when 
     * an application has to be verified for them. 
     * 
     * By default startOnVerify is enabled. 
     *  
     * @return true if startOnVerify is enabled.
     */
    public boolean isStartOnVerifyEnabled()
    {
        
        boolean startOnVerify = true;
        
        Object value = getConfigurationAttribute(
            ConfigurationCategory.Deployment, 
            com.sun.jbi.management.config.DeploymentConfigurationFactory.START_ON_VERIFY);
        
        if ( value != null )
        {
            startOnVerify = (Boolean) value;
        }
        
        return startOnVerify;
    }
        
    
    /**
     * Get the value of a configuration attribute
     *
     * @param category - configuration category the attribute is defined in
     * @param attrName - name of the attribute
     * @return the value of the requested configuration attribute
     */
    private Object getConfigurationAttribute(ConfigurationCategory category, String attrName)
    {
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(category.toString());
        
        ObjectName configMBeanName = mMBeanNames.
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    ConfigurationBuilder.getControlType(svcType));
        
        Object attrValue = null;
        try
        {
            if ( mMBeanServer.isRegistered(configMBeanName) )
            {
                attrValue = mMBeanServer.getAttribute(configMBeanName, attrName);
            }
        }
        catch ( javax.management.JMException jmex)
        {
            mLog.warning(jmex.getMessage());
        }
        return attrValue;
    }
    
    /**
     * Set the value of a configuration attribute.
     *
     * @param category - configuration category the attribute is defined in
     * @param attrName - name of the attribute
     * @param attrValue - value of the attribute
     */
    private void setConfigurationAttribute(ConfigurationCategory category, String attrName,
        Object attrValue)
    {
        
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(category.toString());
        
        ObjectName configMBeanName = getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    ConfigurationBuilder.getControlType(svcType));
      
        try
        {
            javax.management.Attribute 
                attribute = new javax.management.Attribute(attrName, attrValue);
            mMBeanServer.setAttribute(configMBeanName, attribute);
        }
        catch ( javax.management.JMException jmex)
        {
            mLog.warning(jmex.getMessage());
        }
    }
    
}
