/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLogger.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.logging.Level;


/**
 * This class implements the ComponentLoggerMBean for a Component (BC or SE).
 * This MBean acts as an agent between the JMX management service and the
 * UI runtime to provide operations for displaying and modifying the logger
 * levels for a component.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentLogger
    implements ComponentLoggerMBean
{
    /**
     * Component instance handle.
     */
    private Component mComponent;

    /**
     * Logger used for local logging.
     */
    private Logger mLog;

    /**
     * Logger info table. This has an entry for each logger registered by the
     * component. Each entry is an instance of LoggerInfo.
     * This table is a TreeMap so that the loggers are kept in alphabetical
     * order by name.
     */
    private TreeMap <String, LoggerInfo> mLoggerInfo;

    /**
     * Logger name prefix for loggers that need a prefix added.
     */
    private String mLoggerNamePrefix;

    /**
     * Logger name for top-level logger.
     */
    private String mLoggerNameTop;

    /**
     * Logger level properties for this component. This is perstisted as a
     * separate file in the component's install root.
     */
    private Properties mLoggerSettings;
 
    /**
     * Local handle to StringTranslator.
     */
    private StringTranslator mTranslator;

    /**
     * The name of the directory where the component logger level properties
     * file is written. NOTE: This package-accessible to allow the unit test
     * to use it.
     */
    static final String CONFIG_DIRECTORY = "/config/";

    /**
     * The name of the file containing the component logger level properties.
     * NOTE: This package-accessible to allow the unit test to use it.
     */
    static final String LOGGER_SETTINGS = "loggerSettings.properties";

    /**
     * The value for a component logger level property for inheriting from the
     * parent logger. NOTE: This package-accessible to allow the unit test
     * to use it.
     */
    static final String LOGGER_DEFAULT = "DEFAULT";

    /**
     * The suffix for the logger display name property stored in the logger
     * properties file. NOTE: This package-accessible to allow the unit test
     * to use it.
     */
    static final String DISPLAY_NAME = ".displayName";

    /**
     * XML tag for the component logger section in the installation descriptor
     * extension.
     */
    static final String TAG_LOGGING_SECTION = "logging:Logging";
    //static final String TAG_LOGGING_SECTION = "Logging";

    /**
     * XML tag for the root attribute of the component logger section in the
     * installation descriptor extension.
     */
    static final String TAG_LOGGING_ROOT = "root";

    /**
     * XML tag for a component logger definition in the component logger section
     * of the installation descriptor extension.
     */
    static final String TAG_LOGGER_DEFINITION = "logging:logger";
    //static final String TAG_LOGGER_DEFINITION = "logger";

    /**
     * XML tag for the displayName attribute of a component logger definition in
     * the component logger section of the installation descriptor extension.
     */
    static final String TAG_DISPLAY_NAME = "displayName";

    /**
     * XML tag for the addPrefix attribute of a component logger definition in
     * the component logger section of the installation descriptor extension.
     */
    static final String TAG_ADD_PREFIX = "addPrefix";

    /**
     * XML namespace URI for the component logger section of the installation
     * descriptor extension.
     */
    static final String URI_LOGGING_SECTION = "http://www.sun.com/jbi/descriptor/logging";

    /**
     * Constructor. This loads any logger levels that were previously persisted,
     * and creates the top-level logger for the component, setting its parent
     * to the top-level JBI logger and its level to the persisted level if 
     * found.
     * @param component is the Component instance for this component.
     */
    ComponentLogger(Component component)
    {
        mComponent = component;
        EnvironmentContext env = EnvironmentContext.getInstance();
        mLog = (Logger) env.getLogger();
        mLoggerInfo = new TreeMap();
        setLoggerNamePrefix(component.getName());
        mLoggerSettings = new Properties();
        mTranslator = (StringTranslator) env.getStringTranslatorFor(this);

        // Load any persisted logger levels from the properties file that
        // was written by a previous execution of the runtime. Then get any
        // pre-defined logger information from the component's installation
        // descriptor.

        loadLoggerSettings();
        getLoggerDefs();

        // Create the top-level logger for the component and initialize its
        // level. Add the logger to the logger info table.

        mLoggerNameTop = getLoggerNamePrefix();
        int dot = mLoggerNameTop.lastIndexOf(".");
        if ( -1 < dot )
        {
            mLoggerNameTop = mLoggerNameTop.substring(0, dot);
        }
        Logger logger = Logger.getLogger(mLoggerNameTop);
        logger.setParent(env.getJbiLogger());
        logger.setLevel(getSavedLevel(mLoggerNameTop));
        addLogger(logger, mComponent.getName());
    }

//
// Methods defined in the ComponentLoggerMBean interface
//

    /**
     * Get the localized display name of the specified logger. If none is
     * available, return a default name set to the last level of the logger
     * name.
     *
     * @param logName the name of the logger.
     * @return String representing the localized display name.
     */
    public String getDisplayName(String logName)
    {
        if ( null == logName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "logName"));
        }

        String displayName = null;

        // Look for an entry in the logger info table, and if it has no name
        // set, look for an entry in the logger settings properties.

        LoggerInfo info = mLoggerInfo.get(logName);
        if ( null != info )
        {
            String dn = info.getDisplayName();
            if ( null == dn )
            {
                dn = mLoggerSettings.getProperty(logName+DISPLAY_NAME);
                if ( null != dn )
                {
                    displayName = dn;
                    info.setDisplayName(dn);
                }
            }
            else
            {
                displayName = dn;
            }
        }

        // If the display name is still not set, create a default from the
        // last level of the logger name

        if ( null == displayName )
        {
            int lastDot = logName.lastIndexOf(".");
            if ( -1 < lastDot )
            {
                displayName = logName.substring(lastDot+1);
            }
            else
            {
                displayName = logName;
            }
        }

        return displayName;
    }

    /**
     * Get the log level of the specified logger. If the level is not set,
     * search the parent logger chain until a logger is found with a level set.
     * In the case where this method is called before the component has been
     * started, the logger may not exist. In this case, retrieve the logger
     * level property and use its value, or if there is no logger level property
     * set, use the top-level component logger's level. The top-level component
     * logger always exists because it is created by the constructor of this
     * class.
     *
     * @param logName the name of the logger.
     * @return String representing log level or null if no level is available.
     * @throws javax.jbi.JBIException if the logger is not known.
     */
    public String getLevel(String logName)
        throws javax.jbi.JBIException
    {
        if ( null == logName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "logName"));
        }

        LoggerInfo info = mLoggerInfo.get(logName);

        // If the logger name has not been registered, give up.

        if ( null == info )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.COMP_LOGGER_NOT_FOUND, logName));
        }

        // The logger name has been registered. If the logger has not been
        // created, use its saved level from the logger level properties,
        // if available. If no level has been saved for this logger, get
        // the level of the top-level component logger, which always exists.

        Logger log = info.getLogger();
        if ( null == log )
        {
            String lvl = mLoggerSettings.getProperty(logName);
            if ( null == lvl || lvl.equals(LOGGER_DEFAULT) )
            {
                lvl = getLevel(mLoggerNameTop);
            }
            return Level.parse(lvl).getLocalizedName();
        }

        // The logger has been created. Get its level, and if it is not set,
        // run the parent logger chain until a logger is found with a level
        // set. If no logger is found with a level set, return null. Note
        // that this should never happen, as all loggers' parent chains
        // eventually lead to the main JDK logger, which always has a
        // level set.

        Level level = log.getLevel();
        while ( null == level )
        {
            log = log.getParent();
            if ( null != log )
            {
                level = log.getLevel();
            }
            else
            {
                break;
            }
        }
        if ( null != level )
        {
            return level.getLocalizedName();
        }
        else
        {
            return null;
        }
    }

    /**
     * Get the names of all loggers controlled by this MBean.
     *
     * @return an array of logger names as String objects.
     */
    public String[] getLoggerNames()
    {
        String[] s = new String[mLoggerInfo.size()];
        mLoggerInfo.keySet().toArray(s);
        return s;
    }

    /**
     * Set the log level of the specified logger to ALL.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setAll(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.ALL);
    }

    /**
     * Set the log level of the specified logger to CONFIG.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setConfig(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.CONFIG);
    }

    /**
     * Set the log level of the specified logger to the default, which is
     * null to inherit the parent logger's level.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setDefault(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, null);
    }

    /**
     * Set the log level of the specified logger to FINE.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setFine(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.FINE);
    }

    /**
     * Set the log level of the specified logger to FINER.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setFiner(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.FINER);
    }

    /**
     * Set the log level of the specified logger to FINEST.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setFinest(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.FINEST);
    }

    /**
     * Set the log level of the specified logger to INFO.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setInfo(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.INFO);
    }

    /**
     * Set the log level of the specified logger to OFF.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setOff(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.OFF);
    }

    /**
     * Set the log level of the specified logger to SEVERE.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setSevere(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.SEVERE);
    }

    /**
     * Set the log level of the specified logger to WARNING.
     *
     * @param logName the logger name.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    public int setWarning(String logName)
        throws javax.jbi.JBIException
    {
        return setLevel(logName, Level.WARNING);
    }

//
// Methods not in the ComponentLoggerMBean interface. These are used only
// in the framework.
//

    /**
     * Add an entry to the logger info table. Each entry contains the logger
     * name as the key and the LoggerInfo instance as the value. If an entry
     * already exists for this logger name, but its logger reference has not
     * been set, set it now. Do the same for the display name.
     *
     * @param logger The Logger instance.
     * @param displayName The logger's display name.
     * @return <code>true</code> if either the logger instance was not already
     * set in the existing LoggerInfo, or there was no existing LoggerInfo for
     * this logger. Returns <code>false</code> if there was an existing
     * LoggerInfo and its logger instance was already set. Note that the return
     * value is used only for unit testing.
     */
    boolean addLogger(Logger logger, String displayName)
    {
        if ( null == logger )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "logger"));
        }
        if ( null == displayName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "displayName"));
        }

        boolean added = false;
        String loggerName = logger.getName();
        LoggerInfo info = null;

        // Add a new LoggerInfo to the logger info table if one does not already
        // exist for this logger. Otherwise, retrieve the existing LoggerInfo
        // and update it with the logger reference and display name if not
        // already set. This means that if the display name was pre-defined
        // at component installation time, that name is retained and not 
        // replaced with the default display name provided by the caller.

        if ( mLoggerInfo.containsKey(loggerName) )
        {
            info = mLoggerInfo.get(loggerName);
            if ( null == info.getLogger() )
            {
                info.setLogger(logger);
                added = true;
            }
            if ( null == info.getDisplayName() )
            {
                info.setDisplayName(displayName);
            }
        }
        else
        {
            info = new LoggerInfo(logger, displayName);
            mLoggerInfo.put(loggerName, info);
            added = true;
        }
        if ( added )
        {
            logger.setLevel(getSavedLevel(loggerName));
        }
        return added;
    }

    /**
     * Add predefined logger information to the logger info table. This creates
     * a new LoggerInfo object using the provided values, and adds it to the
     * logger info table. It also adds a property for the logger to the logger
     * settings, with the value set to the default logger level. The logger
     * reference is not set, as the logger has not yet been created.
     * 
     * Note that this method is called only during installation or upgrade of
     * a component.
     *
     * @param loggerName the name of the logger to be added.
     * @param displayName the display name of the logger to be added; if this
     * is null the last level of the logger name is used.
     * @param addPrefix set to <code>true</code> if the prefix should be added
     * to this logger name, or <code>false</code> if the logger name should
     * be kept as is.
     */
    void addLoggerInfo(String loggerName, String displayName, boolean addPrefix)
    {
        // If the prefix is to be added to this logger name, and it doesn't
        // already start with the prefix, add the prefix now
        if ( addPrefix )
        {
            if ( null != mLoggerNamePrefix )
            {
                if ( !loggerName.startsWith(mLoggerNamePrefix) )
                {
                    loggerName = mLoggerNamePrefix + loggerName;
                }
            }
        }

        // If the display name is not provided, create a default from the
        // last level of the logger name
        if ( null == displayName )
        {
            int lastDot = loggerName.lastIndexOf(".");
            if ( -1 < lastDot )
            {
                displayName = loggerName.substring(lastDot+1);
            }
            else
            {
                displayName = loggerName;
            }
        }

        if ( !mLoggerInfo.containsKey(loggerName) )
        {
            // Create a LoggerInfo for the logger and add it to the logger
            // info table.
            LoggerInfo info = new LoggerInfo(displayName);
            mLoggerInfo.put(loggerName, info);
        }
        else
        {
            // Update the existing LoggerInfo for this logger.
            LoggerInfo info = mLoggerInfo.get(loggerName);
            info.setDisplayName(displayName);
        }

        // Set the logger level property in the logger settings.
        if ( null == mLoggerSettings.getProperty(loggerName) )
        {
            mLoggerSettings.setProperty(loggerName, LOGGER_DEFAULT);
            mLoggerSettings.setProperty(loggerName + DISPLAY_NAME, displayName);
        }
    }

    /**
     * Get the pre-defined logger names provided in the installation descriptor
     * extension for a component and add them to the logger MBean for that
     * component. This is done at component install or upgrade time, and in
     * the case of pre-installed schemaorg_apache_xmlbeans.system components, during the first time
     * this MBean is created. After that, the logger names remain in the
     * persisted list of logger settings for the component. Note that during
     * an upgrade, any definition changes are merged into the existing persisted
     * settings to that an upgrade will not lose any previously saved logger
     * level settings.
     */
    void getLoggerDefs()
    {
        getLoggerDefs(false);
    }
    
    /**
     * Get the pre-defined logger names provided in the installation descriptor
     * extension for a component and add them to the logger MBean for that
     * component. This is done at component install or upgrade time, and in
     * the case of pre-installed schemaorg_apache_xmlbeans.system components, during the first time
     * this MBean is created. After that, the logger names remain in the
     * persisted list of logger settings for the component. Note that during
     * an upgrade, any definition changes are merged into the existing persisted
     * settings to that an upgrade will not lose any previously saved logger
     * level settings.
     * @param upgradeFlag indicating whether it is called from upgrade operation or not
     */
    void getLoggerDefs(boolean upgradeFlag)
    {
        // First get the installation descriptor. If there is none, there is
        // nothing to do.
        com.sun.jbi.management.descriptor.ComponentDescriptor cd =
                           mComponent.getInstallationDescriptorModel(upgradeFlag);
        
        if ( null != cd )
        {
            // Get the logging section from the installation descriptor. If
            // there is none, there is nothing to do.
            org.w3c.dom.Element logging = cd.getComponentLoggingXml();
            if ( null != logging )
            {
                try
                {
                    // Get the logger root if any is specified
                    String prefix = XmlUtil.getAttribute(
                        (org.w3c.dom.Node) logging, TAG_LOGGING_ROOT);
                    if ( 0 < prefix.length() )
                    {
                        setLoggerNamePrefix(prefix);
                    }

                    // Now get a list of logger definitions and process them
                    org.w3c.dom.NodeList logDefs = XmlUtil.getElements(
                        (org.w3c.dom.Node) logging, TAG_LOGGER_DEFINITION, false);
                    if ( null != logDefs )
                    {
                        for ( int l = 0; l < logDefs.getLength(); l++ )
                        {
                            org.w3c.dom.Node logDef = logDefs.item(l);
                            String name = XmlUtil.getStringValue(logDef);
                            String displayName = XmlUtil.getAttribute(logDef,
                                TAG_DISPLAY_NAME);
                            String addPrefixStr = XmlUtil.getAttribute(logDef,
                                TAG_ADD_PREFIX);
                            boolean addPrefix = true;
                            if ( 0 < addPrefixStr.length() )
                            {
                                addPrefix = Boolean.parseBoolean(addPrefixStr);
                            }
                            if ( 0 == displayName.length() )
                            {
                                displayName = null;
                            }
                            addLoggerInfo(name, displayName, addPrefix);
                        }
                    }
                }
                catch ( javax.jbi.JBIException ex )
                {
                    mLog.warning(ex.getMessage());
                }
            // Save all the logger settings that have been processed
            saveLoggerSettings();
            }
        }
    }

    /**
     * Get the logger name prefix for this component.
     * @return the logger name prefix.
     */
    String getLoggerNamePrefix()
    {
        return mLoggerNamePrefix;
    }

    /**
     * Get the saved logger level for a logger. This is used for restoring
     * persisted logger levels that were set explicitly.
     *
     * @param loggerName The logger name.
     * @return The logger level, or null if none was persisted.
     */
    Level getSavedLevel(String loggerName)
    {
        if ( null == loggerName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "loggerName"));
        }
        String level = mLoggerSettings.getProperty(loggerName);
        if ( null != level )
        {
            if ( level.equals(LOGGER_DEFAULT) )
            {
                return null;
            }
            return Level.parse(level);
        }
        else
        {
            return null;
        }
    }

    /**
     * Load logger level setting properties from the persisted file.
     */
    void loadLoggerSettings()
    {
        FileInputStream inStream = null;
        File configFile = new File(getConfigDirName() + LOGGER_SETTINGS);
        try
        {
            inStream = new FileInputStream(configFile);
            mLoggerSettings.load(inStream);
        }
        catch ( java.io.FileNotFoundException fnfEx )
        {
            ; // This just means no logger settings were persisted
        }
        catch ( java.io.IOException ioEx )
        {
            mLog.log(Level.WARNING, mTranslator.getString(
                LocalStringKeys.COMP_LOGGER_SETTINGS_LOAD_FAILED,
                mComponent.getComponentTypeAsString(),
                mComponent.getName(),
                configFile.getAbsolutePath()),
                ioEx);
        }
        finally
        {
            if ( null != inStream )
            {
                try
                {
                    inStream.close();
                }
                catch ( java.io.IOException ioEx )
                {
                    mLog.log(Level.WARNING, mTranslator.getString(
                        LocalStringKeys.COMP_LOGGER_SETTINGS_CLOSE_FAILED,
                        mComponent.getComponentTypeAsString(),
                        mComponent.getName(),
                        configFile.getAbsolutePath()),
                        ioEx);
                }
            }
        }
    }

    /**
     * Save logger level setting properties to a persisted file.
     */
    synchronized void saveLoggerSettings()
    {
        File configDir = new File(getConfigDirName());
        if ( !configDir.exists() )
        {
            if ( !configDir.mkdir() )
            {
                mLog.warning(mTranslator.getString(
                    LocalStringKeys.COMP_LOGGER_SETTINGS_CREATE_FAILED,
                    configDir.getAbsolutePath(),
                    mComponent.getComponentTypeAsString(),
                    mComponent.getName()));
                return;
            }
        }

        File configFile = new File(getConfigDirName() + LOGGER_SETTINGS);
        FileOutputStream outStream = null;
        try
        {
            outStream = new FileOutputStream(configFile);
            mLoggerSettings.store(outStream, "Logger levels for " +
                mComponent.getComponentTypeAsString() + " " +
                mComponent.getName());
        }
        catch ( java.io.FileNotFoundException fnfEx )
        {
            mLog.log(Level.WARNING, mTranslator.getString(
                LocalStringKeys.COMP_LOGGER_SETTINGS_SAVE_FAILED,
                mComponent.getComponentTypeAsString(),
                mComponent.getName(),
                configFile.getAbsolutePath()),
                fnfEx);
        }
        catch ( java.io.IOException ioEx )
        {
            mLog.log(Level.WARNING, mTranslator.getString(
                LocalStringKeys.COMP_LOGGER_SETTINGS_SAVE_FAILED,
                mComponent.getComponentTypeAsString(),
                mComponent.getName(),
                configFile.getAbsolutePath()),
                ioEx);
        }
        finally
        {
            if ( null != outStream )
            {
                try
                {
                    outStream.close();
                }
                catch ( java.io.IOException ioEx )
                {
                    mLog.log(Level.WARNING, mTranslator.getString(
                        LocalStringKeys.COMP_LOGGER_SETTINGS_CLOSE_FAILED,
                        mComponent.getComponentTypeAsString(),
                        mComponent.getName(),
                        configFile.getAbsolutePath()),
                        ioEx);
                }
            }
        }

    }

    /**
     * Return an indication of whether or not a logger is registered.
     *
     * @return <code>true</code> if the logger is registered, or
     * <code>false</code> if not.
     */
    boolean isLoggerRegistered(String name)
    {
        if ( mLoggerInfo.containsKey(name) )
        {
            LoggerInfo info = mLoggerInfo.get(name);
            if ( null != info.getLogger() )
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Set the logger name prefix for this component.
     * @param prefix the logger name prefix to be set.
     */
    void setLoggerNamePrefix(String prefix)
    {
        if ( prefix.endsWith(".") )
        {
            mLoggerNamePrefix = prefix;
        }
        else
        {
            mLoggerNamePrefix = prefix + ".";
        }
    }

//
// Private methods
//

    /**
     * Get the config directory name for this component.
     *
     * @return The config directory name.
     */
    private String getConfigDirName()
    {
        File installRoot = new File(mComponent.getInstallRoot());
        return installRoot.getParent() + CONFIG_DIRECTORY;
    }

    /**
     * Set the log level of the specified logger to the specified level. This 
     * also persists the logger settings for the component so that if a restart
     * occurs, the logger settings are retained.
     *
     * @param logName the logger name.
     * @param logLevel the log level.
     * @return 0 if operation is successful.
     * @throws javax.jbi.JBIException if the logger is not found.
     */
    int setLevel(String logName, Level logLevel)
        throws javax.jbi.JBIException
    {
        if ( null == logName )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "logName"));
        }

        LoggerInfo info = mLoggerInfo.get(logName);
        if ( null != info )
        {
            Logger log = info.getLogger();
            // This is here to support setting the levels of loggers that
            // are not obtained through ComponentContext.getLogger()
            if ( null == log )
            {
                log = Logger.getLogger(logName);
                info.setLogger(log);
            }
            log.setLevel(logLevel);
            mLog.fine("Logger " + logName + " set to " +
                (logLevel == null ? "null" : logLevel.getLocalizedName()));

            if ( null == logLevel )
            {
                mLoggerSettings.setProperty(logName, LOGGER_DEFAULT);
            }
            else
            {
                mLoggerSettings.setProperty(logName,
                    logLevel.getLocalizedName());
            }
            saveLoggerSettings();
            return 0;
        }
        else
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.COMP_LOGGER_NOT_FOUND, logName));
        }
    }

    /**
     * This private class holds information about a single component logger.
     */
    private class LoggerInfo
    {
         /**
          * Logger instance.
          */
         Logger mLogger;

         /**
          * Display name.
          */
         String mDisplayName;

         /**
          * Constructor.
          *
          * @param logger The Logger instance.
          * @param displayName The display name for the logger.
          */
         LoggerInfo(Logger logger, String displayName)
         {
             mLogger = logger;
             mDisplayName = displayName;
         }

         /**
          * Alternate constructor used for pre-defined logger names.
          *
          * @param displayName The display name for the logger.
          */
         LoggerInfo(String displayName)
         {
             mLogger = null;
             mDisplayName = displayName;
         }

         /**
          * Get the Logger instance for this logger.
          * @return The java.util.logging.Logger instance.
          */
         Logger getLogger()
         {
             return mLogger;
         }

         /**
          * Get the Logger name for this logger.
          * @return The name of the logger.
          */
         String getLoggerName()
         {
             return mLogger.getName();
         }

         /**
          * Get the display name for this logger.
          * @return The display name for the logger.
          */
         String getDisplayName()
         {
             return mDisplayName;
         }

         /**
          * Set the Logger instance for this logger.
          * @param logger the java.util.logging.Logger instance.
          */
         void setLogger(Logger logger)
         {
             mLogger = logger;
         }

         /**
          * Set the display name for this logger.
          * @param displayName the display name to be set.
          */
         void setDisplayName(String displayName)
         {
             mDisplayName = displayName;
         }
    }
}
