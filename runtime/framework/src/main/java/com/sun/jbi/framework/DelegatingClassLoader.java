/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DelegatingClassLoader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.ArrayList; 
import java.util.Iterator; 
import java.util.logging.Logger;

/**
 * Implementation of a Component's delegating classloader.
 * There is one instance of this classloader per component.
 * This classloader walks through a chain of shared classloaders
 * to try and load a class. It returns as soon as the first classloader
 * in the chain is able to find the correct classloader.
 * 
 * @author Sun Microsystems, Inc.
 */
public final class DelegatingClassLoader extends java.security.SecureClassLoader
{
    /**
     *  Reference to list of classloaders for this component's Shared
     *  Libraries.
     */
    private ArrayList mSharedClassLoaders;

    /**
     *  StringTranslator used to translate messages.
     */
    private StringTranslator mTranslator;

    /**
     *  handle to parent class loader
     */
    private ClassLoader mParentLoader;

    /**
     *  handle to schemaorg_apache_xmlbeans.system logger
     */
    private Logger mLogger;

    /**
     * Constructor.
     * @param  parent the parent classloader to this DelegatingClassloader
     */
    public DelegatingClassLoader(ClassLoader parent)
    {
        super ( parent );

        ClassLoader systemCL = getSystemClassLoader();
        mParentLoader = (parent != null) ? parent : systemCL;
        mSharedClassLoaders = new ArrayList();

        // get logger , string translator from the env
        EnvironmentContext eCtx = EnvironmentContext.getInstance();
        mTranslator = (StringTranslator) eCtx.getStringTranslatorFor(this);
        //mLogger = (Logger)eCtx.getLogger();
        //mLogger = Logger.getLogger(this.getClass().getPackage().getName());
    }

    /**
     * Adds a Shared Library classloader to this instance of the DCL.
     * @param sharedLoader The instance of shared classloader to add.
     * @throws IllegalArgumentException If the shared classloader passed in
     * is null.
     */
    public void addSharedClassLoader( ClassLoader sharedLoader )
        throws IllegalArgumentException
    {
        if ( null == sharedLoader )
        {
           throw new IllegalArgumentException(mTranslator.getString(
               LocalStringKeys.NULL_ARGUMENT, "sharedLoader"));
        }
        mSharedClassLoaders.add( sharedLoader );
    }


    /**
     * Overloaded loadClass method  that works similarly to the 
     * <code>loadClass(String, boolean)</code> method with <code>
     * false</code> asthe second argument.
     *
     * @param  name The name of class to load.
     * @return The loaded class.
     * @throws ClassNotFoundException If the class cannot be loaded  by this
     * classloader.
     */ 
    public Class loadClass(String name)
        throws ClassNotFoundException
    {
        return loadClass (name, false);
    }

    /**
     * Overloaded loadClass method .
     * The purpose of overloading loadClass() here is
     * not to implement the Shared Class "selfFirst" logic as it may seem. 
     * That happens inside the Shared Classloaders themselves when invoked
     * inside findClass() of this class .
     * The purpose of this method is to prevent shared classes such as XML
     * libraries that may be found in the Appserver class path from being
     * inadvertently loaded ahead of time by the DelegatingClassLoader rather
     * than the class loaders for the Shared Libraries that contain the XML 
     * library . This can happen because findClass() inside 
     * DelegatingClassLoader is called after its parent (app server) class 
     * loader has been consulted . DelegatingClassLoader is not a real class
     * loader - just a proxy for the shared class loaders and this method
     * ensures that the behaviour is as such . By finally delegating to the 
     * parent app server class loader , this method ensures that class 
     * loading failures do not occur for components that do not have
     * shared libraries and hence shared class loaders.
     *
     * @see java.lang.ClassLoader#loadClass(String, boolean) 
     * @see java.lang.ClassLoader#findClass(String) 
     *
     *
     * The sequence of looking for a class is as follows -
     * <ul>
     * <li>call findLoadedClass(String) to check if the class has
     * already been loaded. 
     * <li>call findClass() to try loading the class locally, if it 
     * succeeds , hand back the class 
     * <li> finally hand control to parent and let the normal delegation 
     * class loading continue.
     * <li> if class was not found in above sequence, throw a 
     * ClassNotFoundException 
     * <li> resolveClass is called at each stage before returning the 
     * loaded class 
     * </ul>
     *
     * @param  name The name of class to load.
     * @param  resolve whether or not to resolve the class definition
     * @return The loaded class.
     * @throws ClassNotFoundException If the class cannot be loaded  by this
     * classloader.
     */ 

    protected Class loadClass( String name, boolean resolve)
        throws ClassNotFoundException
    {
        Class class2Load = null;

        //mLogger.finest("Delegating ClassLoader - loadClass: " + name);

        // 1. call findLoadedClass, return if found
        //
        class2Load = findLoadedClass( name );
  
        if ( class2Load != null)
        {
            if (resolve) 
            {
                resolveClass( class2Load );
            }

            //mLogger.finest("Class " + name + " will be loaded from cache");

            return class2Load;
        }

        // loading it locally first
        // this means "search the shared classloaders first
        try
        {
            class2Load = findClass( name );
            if (class2Load != null)
            {
                if (resolve) 
                {
                    resolveClass( class2Load );
                }

                //mLogger.finest("Class " + name + " will be loaded using " +
                //    this.toString()));
                return class2Load;
            }
        }
        catch (ClassNotFoundException cnfe)
        {
            ; // ignore
        }

        // send it to the parent classloader
        //
        try
        {
            class2Load = mParentLoader.loadClass( name );
            if (resolve) 
            {
                resolveClass( class2Load );
            }
            //mLogger.finest("Class " + name + " will be loaded using " +
            //    mParentLoader.toString()));
            return class2Load; 
        }
        catch (ClassNotFoundException cnfe)
        {
            ; // ignore 
        }

        //  all efforts are in vain - class was not found 
        throw new ClassNotFoundException(name);
    }

    /**
     * Overloaded findClass method to walk through the shared classloader chain.
     * @param  name The name of class to load.
     * @return The loaded class.
     * @throws ClassNotFoundException If the class cannot be loaded  by this
     * classloader.
     */ 
    public Class findClass( String name)
        throws ClassNotFoundException
    {
        boolean found = false;
        Class sharedClass = null;

        for (Iterator cItr = mSharedClassLoaders.iterator(); cItr.hasNext();)
        {
            try
            {
                ClassLoader nextSharedCL = (ClassLoader) cItr.next();
                sharedClass = nextSharedCL.loadClass( name );

                // if we reach here, we've loaded the class
                // and can stop looking further.
                found = true;
                break;
            }
            catch ( ClassNotFoundException cnfe )
            {
                continue;
            }
        }

        // if we reach here with found=false that means
        // that the requested class was not in the shared classpath
        if (!found)
        {
            throw new ClassNotFoundException( name );
        }

        return sharedClass;
    }
}
