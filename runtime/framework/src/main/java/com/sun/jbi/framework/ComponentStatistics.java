/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.monitoring.ComponentStatisticsBase;
import com.sun.jbi.monitoring.ComponentStatisticsMBean;
import com.sun.jbi.monitoring.StatisticsBase;

import java.util.Date;
import javax.management.openmbean.CompositeData;

/**
 * This class implements the MBean for collection of statistics for a single
 * installed component. All statistics are since the last JBI startup; they
 * are all reset when JBI is restarted.
 *
 * Please note the 
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentStatistics
    implements ComponentStatisticsBase, ComponentStatisticsMBean
{
    /**
     * Instance of StatisticsBase for manipulating the object tree.
     */
    private StatisticsBase mStatisticsBase;

    /**
     * Time the component was last successfully started.
     */
    private Date mLastRestartTime;

    /**
     * Count of calls to the life cycle init() method.
     */
    private int mInitRequests;

    /**
     * Count of calls to the life cycle start() method.
     */
    private int mStartRequests;

    /**
     * Count of calls to the life cycle stop() method.
     */
    private int mStopRequests;

    /**
     * Count of calls to the life cycle shutDown() method.
     */
    private int mShutDownRequests;

    /**
     * Count of calls to life cycle methods that resulted in exceptions.
     */
    private int mFailedRequests;

    /**
     * Count of calls to life cycle methods that timed out.
     */
    private int mTimedOutRequests;

    /**
     * Number of Service Units currently deployed.
     */
    private short mDeployedSUs;

    /**
     * Count of calls to the ServiceUnitManager deploy() method.
     */
    private int mDeploySURequests;

    /**
     * Count of calls to the ServiceUnitManager init() method.
     */
    private int mInitSURequests;

    /**
     * Count of calls to the ServiceUnitManager start() method.
     */
    private int mStartSURequests;

    /**
     * Count of calls to the ServiceUnitManager stop() method.
     */
    private int mStopSURequests;

    /**
     * Count of calls to the ServiceUnitManager shutDown() method.
     */
    private int mShutDownSURequests;

    /**
     * Count of calls to the ServiceUnitManager undeploy() method.
     */
    private int mUndeploySURequests;

    /**
     * Count of calls to ServiceUnitManager methods that resulted in exceptions.
     */
    private int mFailedSURequests;

    /**
     * Count of calls to ServiceUnitManager methods that timed out.
     */
    private int mTimedOutSURequests;

    /**
     * Count of current number of services or endpoints registered with NMR.
     */
    private short mRegisteredServicesOrEndpoints;

    /**
     * Constant used to compute percentages.
     */
    private static final int ONE_HUNDRED = 100;

    /**
     * Constant used to compute rates.
     */
    private static final int MILLISECONDS_PER_HOUR = 3600000;


    /**
     * Constructor to create the StatisticsBase and MessagingStatistics
     * instances.
     * @param key the component name used as the key for the StatisticsBase.
     */
    public ComponentStatistics(String key)
    {
        mStatisticsBase =
            new com.sun.jbi.util.monitoring.StatisticsBaseImpl(key);
    }

    //
    // Methods defined in StatisticsMBean must delegate to StatisticsBase.
    //

    /**
     * Test whether or not statistics collection is enabled.
     * @return true if statistics collection is enabled, false if not.
     */
    public boolean isEnabled()
    {
        return mStatisticsBase.isEnabled();
    }

    /**
     * Disable statistics collection. This method causes collection for this
     * object and all its child objects to be disabled.
     */
    public void setDisabled()
    {
        mStatisticsBase.setDisabled();
    }

    /**
     * Enable statistics collection. This method causes collection for this
     * object and all its child objects to be enabled.
     */
    public void setEnabled()
    {
        mStatisticsBase.setEnabled();
    }

    //
    // Methods defined in the ComponentStatisticsBase interface. These
    // methods can be called from outside the framework, and are used by
    // the NMR instrumentation code.
    //

    /**
     * Get the StatisticsBase instance.
     * @return the StatisticsBase instance for this object.
     */
    public StatisticsBase getStatisticsBase()
    {
        return mStatisticsBase;
    }

    /**
     * Decrement the current number of services or endpoints registered with
     * the NMR.
     */
    public void decrementRegisteredServicesOrEndpoints()
    {
        --mRegisteredServicesOrEndpoints;
    }

    /**
     * Increment the current number of services or endpoints registered with
     * the NMR.
     */
    public void incrementRegisteredServicesOrEndpoints()
    {
        ++mRegisteredServicesOrEndpoints;
    }

    //
    // Methods defined in the ComponentStatisticsMBean interface. These
    // methods provide all the MBean attributes visible to JMX clients.
    //

    /**
     * Get the time that this component was last started.
     * @return The time of the last successful start() call.
     */
    public Date getLastRestartTime()
    {
        return mLastRestartTime;
    }

    /**
     * Get the count of calls to the life cycle init() method.
     * @return The total number of calls to init().
     */
    public int getInitRequests()
    {
        return mInitRequests;
    }

    /**
     * Get the count of calls to the life cycle start() method.
     * @return The total number of calls to start().
     */
    public int getStartRequests()
    {
        return mStartRequests;
    }

    /**
     * Get the count of calls to the life cycle stop() method.
     * @return The total number of calls to stop().
     */
    public int getStopRequests()
    {
        return mStopRequests;
    }

    /**
     * Get the count of calls to the life cycle shutDown() method.
     * @return The total number of calls to shutDown().
     */
    public int getShutDownRequests()
    {
        return mShutDownRequests;
    }

    /**
     * Get the count of failed calls to life cycle methods (excluding timeouts).
     * @return The total number of failed calls.
     */
    public int getFailedRequests()
    {
        return mFailedRequests;
    }

    /**
     * Get the count of timed out calls to life cycle methods.
     * @return The total number of timed out calls.
     */
    public int getTimedOutRequests()
    {
        return mTimedOutRequests;
    }

    /**
     * Get the current number of Service Units deployed.
     * @return The current number of deployed SUs.
     */
    public short getDeployedSUs()
    {
        return mDeployedSUs;
    }

    /**
     * Get the count of calls to the ServiceUnitManager deploy() method.
     * @return The total number of calls to deploy().
     */
    public int getDeploySURequests()
    {
        return mDeploySURequests;
    }

    /**
     * Get the count of calls to the ServiceUnitManager undeploy() method.
     * @return The total number of calls to undeploy().
     */
    public int getUndeploySURequests()
    {
        return mUndeploySURequests;
    }

    /**
     * Get the count of calls to the ServiceUnitManager init() method.
     * @return The total number of calls to init().
     */
    public int getInitSURequests()
    {
        return mInitSURequests;
    }

    /**
     * Get the count of calls to the ServiceUnitManager start() method.
     * @return The total number of calls to start().
     */
    public int getStartSURequests()
    {
        return mStartSURequests;
    }

    /**
     * Get the count of calls to the ServiceUnitManager stop() method.
     * @return The total number of calls to stop().
     */
    public int getStopSURequests()
    {
        return mStopSURequests;
    }

    /**
     * Get the count of calls to the ServiceUnitManager shutDown() method.
     * @return The total number of calls to shutDown().
     */
    public int getShutDownSURequests()
    {
        return mShutDownSURequests;
    }

    /**
     * Get the count of failed calls to ServiceUnitManager methods (excluding
     * timeouts).
     * @return The total number of failed calls.
     */
    public int getFailedSURequests()
    {
        return mFailedSURequests;
    }

    /**
     * Get the count of timed out calls to ServiceUnitManager methods.
     * @return The total number of timed out calls.
     */
    public int getTimedOutSURequests()
    {
        return mTimedOutSURequests;
    }

    /**
     * Get the current number of services (for an SE) or endpoint (for a BC)
     * registered with the NMR.
     * @return The total number of registered services or endpoints.
     */
    public short getRegisteredServicesOrEndpoints()
    {
        return mRegisteredServicesOrEndpoints;
    }

    //
    // Methods used only within the framework code to update framework-
    // provided statistics.
    //

    /**
     * Set the time that this component was last started.
     * @param startTime The time of the last successful start() call.
     */
    void setLastRestartTime(Date startTime)
    {
        mLastRestartTime = startTime;
    }

    /**
     * Increment the count of calls to the life cycle init() method.
     */
    void incrementInitRequests()
    {
        ++mInitRequests;
    }

    /**
     * Increment the count of calls to the life cycle start() method.
     */
    void incrementStartRequests()
    {
        ++mStartRequests;
    }

    /**
     * Increment the count of calls to the life cycle stop() method.
     */
    void incrementStopRequests()
    {
        ++mStopRequests;
    }

    /**
     * Increment the count of calls to the life cycle shutDown() method.
     */
    void incrementShutDownRequests()
    {
        ++mShutDownRequests;
    }

    /**
     * Increment the count of failed calls to life cycle methods (excluding
     * timeouts).
     */
    void incrementFailedRequests()
    {
        ++mFailedRequests;
    }

    /**
     * Increment the count of timed out calls to life cycle methods.
     */
    void incrementTimedOutRequests()
    {
        ++mTimedOutRequests;
    }

    /**
     * Decrement the current number of Service Units deployed.
     */
    void decrementDeployedSUs()
    {
        --mDeployedSUs;
    }

    /**
     * Increment the current number of Service Units deployed.
     */
    void incrementDeployedSUs()
    {
        ++mDeployedSUs;
    }

    /**
     * Increment the count of calls to the ServiceUnitManager deploy() method.
     */
    void incrementDeploySURequests()
    {
        ++mDeploySURequests;
    }

    /**
     * Increment the count of calls to the ServiceUnitManager undeploy() method.
     */
    void incrementUndeploySURequests()
    {
        ++mUndeploySURequests;
    }

    /**
     * Increment the count of calls to the ServiceUnitManager init() method.
     */
    void incrementInitSURequests()
    {
        ++mInitSURequests;
    }

    /**
     * Increment the count of calls to the ServiceUnitManager start() method.
     */
    void incrementStartSURequests()
    {
        ++mStartSURequests;
    }

    /**
     * Increment the count of calls to the ServiceUnitManager stop() method.
     */
    void incrementStopSURequests()
    {
        ++mStopSURequests;
    }

    /**
     * Increment the count of calls to the ServiceUnitManager shutDown() method.
     */
    void incrementShutDownSURequests()
    {
        ++mShutDownSURequests;
    }

    /**
     * Increment the count of failed calls to ServiceUnitManager methods
     * (excluding timeouts).
     */
    void incrementFailedSURequests()
    {
        ++mFailedSURequests;
    }

    /**
     * Increment the count of timed out calls to ServiceUnitManager methods.
     */
    void incrementTimedOutSURequests()
    {
        ++mTimedOutSURequests;
    }

    /**
     * Reset all statistics.
     */
    void resetAllStatistics()
    {
        resetFrameworkStatistics();
    }

    /**
     * Reset framework statistics. Note that the last restart time of the
     * component is not reset, this is always preserved until the next restart
     * of the component.
     */
    void resetFrameworkStatistics()
    {
        mInitRequests = 0;
        mStartRequests = 0;
        mStopRequests = 0;
        mShutDownRequests = 0;
        mFailedRequests = 0;
        mTimedOutRequests = 0;
        mDeployedSUs = 0;
        mDeploySURequests = 0;
        mInitSURequests = 0;
        mStartSURequests = 0;
        mStopSURequests = 0;
        mShutDownSURequests = 0;
        mUndeploySURequests = 0;
        mFailedSURequests = 0;
        mTimedOutSURequests = 0;
        mRegisteredServicesOrEndpoints = 0;
    }

}
