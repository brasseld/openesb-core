/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Deployer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.registry.Registry;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.ServiceUnitManager;

/**
 * This class implements the DeployerMBean for a component (BC or SE). This
 * MBean acts as an agent between the Deployment Service, the Component
 * Registry, and the component's Service Unit Manager to coordinate the
 * registration and unregistration of Service Units, the maintenance of the
 * states of Service Units, and the control of the Service Unit life cycle.
 * Methods in this class interact directly with the Component Registry and
 * the component's Service Unit Manager implementation.
 *
 * Direct calls to the component's Service Unit Manager are protected by a
 * timeout, so that ill-behaved components cannot hang the schemaorg_apache_xmlbeans.system. The
 * <CODE>deploy</CODE>, <CODE>undeploy</CODE>, <CODE>init</CODE>,
 * <CODE>start</CODE>, <CODE>stop</CODE>, and <CODE>shutDown</CODE> methods
 * are all called under timeout protection.
 *
 * @author Sun Microsystems, Inc.
 */
public class Deployer implements DeployerMBean
{
    /**
     * The Component instance representing this component.
     */
    private Component mComponent;

    /**
     * The ComponentRegistry handle.
     */
    private ComponentRegistry mComponentRegistry;

    /**
     * The ComponentStatistics instance representing this component.
     */
    private ComponentStatistics mComponentStatistics;

    /**
     * The EnvironmentContext handle.
     */
    private EnvironmentContext mEnv;

    /**
     * The Logger instance for logging.
     */
    private Logger mLog;

    /**
     * The ServiceUnitManager instance for this component.
     */
    private ServiceUnitManager mSuMgr;

    /**
     * The StringTranslator for constructing messages.
     */
    private StringTranslator mTranslator;

    /**
     * Constructor.
     * @param component is the instance representing the component with which
     * this deployer is associated.
     */
    Deployer(Component component)
    {
        mComponent = component;
        mComponentStatistics = component.getStatisticsInstance();
        mEnv = EnvironmentContext.getInstance();
        mComponentRegistry = mEnv.getComponentRegistry();
        mLog = mEnv.getLogger();
        mSuMgr = null;
        mTranslator = mEnv.getStringTranslatorFor(this);

    }

//
// Public methods from the com.sun.jbi.framework.DeployerMBean interface
//

    /**
     * Deploy a Service Unit to the component.
     *
     * @param serviceUnitName the unique name of the Service Unit being
     * deployed.
     * @param serviceUnitRootPath the full path to the deployment descriptor
     * for this Service Unit.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not successfully deployed.
     * @return A status message.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("deploy", msg));
        }

        if ( null == serviceUnitRootPath )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitRootPath");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("deploy", msg));
        }

        if ( mComponent.isServiceUnitRegistered(serviceUnitName) )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_ALREADY_DEPLOYED,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("deploy", msg));
        }

        // Disallow operation if component is not started
        checkComponentState(serviceUnitName, "deploy");

        if ( statisticsEnabled() )
        {
            mComponentStatistics.incrementDeploySURequests();
        }
        mLog.finer("Calling deploy() for Service Unit " + serviceUnitName +
            " on " + mComponent.getComponentTypeAsString() + " " +
            mComponent.getName());
        String status = performOperation(ServiceUnitOperation.DEPLOY,
            mEnv.getDeploymentTimeout(), "deploy",
            serviceUnitName, serviceUnitRootPath);
        if ( statisticsEnabled() )
        {
            mComponentStatistics.incrementDeployedSUs();
        }
        return status;
    }

    /**
     * Returns a list of the names all Service Units currently deployed to
     * this component.
     *
     * @return An array of Strings containing the names of the Service Units.
     * The array is empty if no Service Units are deployed.
     */
    public String[] getDeployments()
    {
        List sus = mComponent.getServiceUnitList();
        int suCount = sus.size();
        String[] names = new String[suCount];
        if ( 0 < suCount )
        {
            for ( int i = 0; i < suCount; i++ )
            {
                names[i] = ((ServiceUnit) sus.get(i)).getName();
            }
        }
        return names;
    }

    /**
     * Returns the current state of a Service Unit.
     * @param serviceUnitName the unique name of the Service Unit.
     * @return the current state of the Service Unit as one of the values in
     * ServiceUnitState, or ServiceUnitState.UNKNOWN if the Service Unit is
     * not found.
     */
    public ServiceUnitState getServiceUnitState(String serviceUnitName)
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("getServiceUnitState", msg));
        }
        ServiceUnit su = mComponent.getServiceUnit(serviceUnitName);
        if ( null != su )
        {
            return su.getState();
        }
        else
        {
            return ServiceUnitState.UNKNOWN;
        }
    }

    /**
     * Returns a boolean value indicating whether the specified Service Unit
     * is currently deployed.
     * @param serviceUnitName the unique name of the Service Unit.
     * @return true if the Service Unit is deployed, false if not.
     */
    public boolean isDeployed(String serviceUnitName)
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("isDeployed", msg));
        }

        return mComponent.isServiceUnitRegistered(serviceUnitName);
    }

    /**
     * Initialize a Service Unit. This is the first phase of a two-phase
     * start, where the component must prepare to receive service requests
     * related to the Service Unit. 
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the absolute path to the directory, which
     * has the extracted service unit contents.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state, or if the component is not
     * started.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("init", msg));
        }

        if ( null == serviceUnitRootPath )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitRootPath");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("init", msg));
        }
        
        ServiceUnit su = mComponent.getServiceUnit(serviceUnitName);
        if ( null == su )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_NOT_FOUND,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("init", msg));
        }

        // Disallow operation if component is not started
        checkComponentState(serviceUnitName, "init");

        if ( su.isStarted() )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_CANNOT_INIT_STARTED,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("init", msg));
        }

        try
        {
            su.setDesiredState(ServiceUnitState.STOPPED);
            if ( su.isShutdown() )
            {
                if ( statisticsEnabled() )
                {
                    mComponentStatistics.incrementInitSURequests();
                }
                mLog.finer("Calling init() for Service Unit " +
                    serviceUnitName + " on " +
                    mComponent.getComponentTypeAsString() + " " +
                    mComponent.getName());
                performOperation(ServiceUnitOperation.INIT,
                                 mEnv.getServiceUnitTimeout(), "init",
                                 serviceUnitName, serviceUnitRootPath);
                su.setStopped();
            }
        }
        finally
        {
            persistState(su, "init");
        }
    }

    /**
     * Shut down a Service Unit. This causes the Service Unit to return to the
     * state it was in after <code>deploy()</code> and before <code>init()</code>.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state, or if the component is
     * not started.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        shutDown(serviceUnitName, false);
    }

    /**
     * Shut down a Service Unit. This causes the Service Unit to return to the
     * state it was in after <code>deploy()</code> and before <code>init()</code>.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @param force set to true to force the shutdown regardless of state.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state, or if the component is
     * not started.
     */
    public void shutDown(String serviceUnitName, boolean force)
        throws javax.jbi.management.DeploymentException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("shutDown", msg));
        }

        ServiceUnit su = mComponent.getServiceUnit(serviceUnitName);
        if ( null == su )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_NOT_FOUND,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("shutDown", msg));
        }

        // Disallow operation if component is not started
 	checkComponentState(serviceUnitName, "shutDown");

        if ( su.isStarted() && !force )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_CANNOT_SHUT_DOWN_NOT_STOPPED,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("shutDown", msg));
        }

        su.setDesiredState(ServiceUnitState.SHUTDOWN);

        // If the SU is started, this must be a forced shutdown. Attempt
        // to stop the SU just for completeness. Set the SU to stopped
        // state regardless of the outcome.

        if ( su.isStarted() )
        {
            if ( statisticsEnabled() )
            {
                mComponentStatistics.incrementStopSURequests();
            }
            try
            {
                mLog.finer("Calling stop() for Service Unit " +
                    serviceUnitName + " on " +
                    mComponent.getComponentTypeAsString() + " " +
                    mComponent.getName());
                performOperation(ServiceUnitOperation.STOP,
                    mEnv.getServiceUnitTimeout(), "stop",
                    serviceUnitName, null);
            }
            catch ( javax.jbi.management.DeploymentException dEx )
            {
                // ignore this exception
            }
            finally
            {
                su.setStopped();
            }
        }

        if ( su.isStopped() )
        {
            if ( statisticsEnabled() )
            {
                mComponentStatistics.incrementShutDownSURequests();
            }
            try
            {
                mLog.finer("Calling shutDown() for Service Unit " +
                    serviceUnitName + " on " +
                    mComponent.getComponentTypeAsString() + " " +
                    mComponent.getName());
                performOperation(ServiceUnitOperation.SHUTDOWN,
                                 mEnv.getServiceUnitTimeout(), "shutDown",
                                 serviceUnitName, null);
                su.setShutdown();
                mEnv.getNotifier().emitServiceUnitNotification(
                    EventNotifier.EventType.ShutDown,
                    serviceUnitName, mComponent.getName(),
                    su.getServiceAssemblyName(),
                    mTranslator.getString(LocalStringKeys.SUF_SHUT_DOWN,
                        serviceUnitName, mComponent.getName(),
                        su.getServiceAssemblyName()));
            }
            catch ( javax.jbi.management.DeploymentException dEx )
            {
                if ( !force )
                {
                    throw dEx;
                }
                else
                {
                    su.setShutdown();
                }
            }
            finally
            {
                persistState(su, "shutDown");
            }
        }
    }

    /**
     * Start a Service Unit. This is the second phase of a two-phase start,
     * where the component can now initiate service requests related to the
     * Service Unit.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state, or if the component is
     * not started.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("start", msg));
        }

        ServiceUnit su = mComponent.getServiceUnit(serviceUnitName);
        if ( null == su )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_NOT_FOUND,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("start", msg));
        }

        // Disallow operation if component is not started
        checkComponentState(serviceUnitName, "start");

        if ( su.isShutdown() )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_CANNOT_START_NOT_INITIALIZED,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("start", msg));
        }

        try
        {
            su.setDesiredState(ServiceUnitState.STARTED);
            if ( su.isStopped() )
            {
                if ( statisticsEnabled() )
                {
                    mComponentStatistics.incrementStartSURequests();
                }
                mLog.finer("Calling start() for Service Unit " +
                    serviceUnitName + " on " +
                    mComponent.getComponentTypeAsString() + " " +
                    mComponent.getName());
                performOperation(ServiceUnitOperation.START,
                                 mEnv.getServiceUnitTimeout(), "start",
                                 serviceUnitName, null);
                su.setStarted();
                mEnv.getNotifier().emitServiceUnitNotification(
                    EventNotifier.EventType.Started,
                    serviceUnitName, mComponent.getName(),
                    su.getServiceAssemblyName(),
                    mTranslator.getString(LocalStringKeys.SUF_STARTED,
                        serviceUnitName, mComponent.getName(),
                        su.getServiceAssemblyName()));
            }
        }
        finally
        {
            persistState(su, "start");
        }
    }

    /**
     * Stop a Service Unit. This causes the component to cease generating
     * service requests related to the Service Unit. It returns the Service Unit
     * to a state equivalent to after <code>init()</code> was called.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state, or if the component is
     * not started.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("stop", msg));
        }

        ServiceUnit su = mComponent.getServiceUnit(serviceUnitName);
        if ( null == su )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_NOT_FOUND,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("stop", msg));
        }

        // Disallow operation if component is not started
        checkComponentState(serviceUnitName, "stop");

        if ( su.isShutdown() )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_CANNOT_STOP_SHUT_DOWN,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("stop", msg));
        }

        try
        {
            su.setDesiredState(ServiceUnitState.STOPPED);
            if ( su.isStarted() )
            {
                if ( statisticsEnabled() )
                {
                    mComponentStatistics.incrementStopSURequests();
                }
                mLog.finer("Calling stop() for Service Unit " +
                    serviceUnitName + " on " +
                    mComponent.getComponentTypeAsString() + " " +
                    mComponent.getName());
                performOperation(ServiceUnitOperation.STOP,
                                 mEnv.getServiceUnitTimeout(), "stop",
                                 serviceUnitName, null);
                su.setStopped();
                mEnv.getNotifier().emitServiceUnitNotification(
                    EventNotifier.EventType.Stopped,
                    serviceUnitName, mComponent.getName(),
                    su.getServiceAssemblyName(),
                    mTranslator.getString(LocalStringKeys.SUF_STOPPED,
                        serviceUnitName, mComponent.getName(),
                        su.getServiceAssemblyName()));
            }
        }
        finally
        {
            persistState(su, "stop");
        }
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the unique name of the Service Unit being
     * undeployed.
     * @param serviceUnitRootPath the full path to the deployment descriptor
     * for this Service Unit.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not successfully undeployed.
     * @return A status message.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        return undeploy(serviceUnitName, serviceUnitRootPath, false);
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the unique name of the Service Unit being
     * undeployed.
     * @param serviceUnitRootPath the full path to the deployment descriptor
     * for this Service Unit.
     * @param force set to true to force the undeploy regardless of any
     * conditions.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not successfully undeployed.
     * @return A status message.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath,
        boolean force)
        throws javax.jbi.management.DeploymentException
    {
        if ( null == serviceUnitName )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitName");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("undeploy", msg));
        }

        if ( null == serviceUnitRootPath )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "serviceUnitRootPath");
            mLog.severe(msg);
            throw new java.lang.IllegalArgumentException(
                buildMessage("undeploy", msg));
        }

        // Disallow operation if component is not started and force is not
        // specified
        if ( !force )
        {
            checkComponentState(serviceUnitName, "undeploy");
        }

        ServiceUnit su = mComponent.getServiceUnit(serviceUnitName);
        if ( null == su )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_NOT_FOUND,
                serviceUnitName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("undeploy", msg));
        }

        if ( !su.isShutdown() )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_CANNOT_UNDEPLOY_NOT_SHUT_DOWN,
                serviceUnitName, su.getStateAsString());
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage("undeploy", msg));
        }

        String status = "";

        if ( !force )
        {
            // Normal undeploy
            if ( statisticsEnabled() )
            {
                mComponentStatistics.incrementUndeploySURequests();
            }
            mLog.finer("Calling undeploy() for Service Unit " +
                serviceUnitName + " on " +
                mComponent.getComponentTypeAsString() + " " +
                mComponent.getName());
            status = performOperation(ServiceUnitOperation.UNDEPLOY,
                mEnv.getDeploymentTimeout(), "undeploy",
                serviceUnitName, serviceUnitRootPath);
            if ( statisticsEnabled() )
            {
                mComponentStatistics.decrementDeployedSUs();
            }
        }
        else
        {
            // Forced undeploy - must complete even if component is not started
            if ( mComponent.isStarted() )
            {
                if ( statisticsEnabled() )
                {
                    mComponentStatistics.incrementUndeploySURequests();
                }
                try
                {
                    mLog.finer("Calling undeploy() for Service Unit " +
                        su.getName() + " on " +
                        mComponent.getComponentTypeAsString() + " " +
                        mComponent.getName());
                    performOperation(ServiceUnitOperation.UNDEPLOY,
                        mEnv.getDeploymentTimeout(), "undeploy",
                        serviceUnitName, serviceUnitRootPath);
                }
                catch( javax.jbi.management.DeploymentException dEx )
                {
                    // Ignore the exception
                }
            }

            // Always decrement the number of deployed SUs and return a SUCCESS
            // message

            status = buildSuccessMessage("undeploy");
            if ( statisticsEnabled() )
            {
                mComponentStatistics.decrementDeployedSUs();
            }
        }
        return status;
    }

//
// Package-only methods
//

    /**
     * Set the ServiceUnitManager reference for this instance.
     * @param suManager the instance of javax.jbi.component.ServiceUnitManager
     * returned by the by the component's javax.jbi.component.Component.
     * getServiceUnitManager() method.
     */
    void setServiceUnitManager(ServiceUnitManager suManager)
        throws javax.jbi.management.DeploymentException
    {
        mSuMgr = suManager;
    }

//
// Private methods
//

    /**
     * Construct a message compliant with the defined schema for messages.
     * The assumption is that any message in the exception is a JBI message
     * with a 9-character message ID, with or without a colon separator. The
     * message ID is extracted and provided in the loc-token element, and
     * the message text is extracted and provided in the loc-message element.
     * @param task the failing task.
     * @param msg the message text.
     * @return an XML string containing the message in the correct format.
     */
    private String buildMessage(String task, String msg)
    {
        int end = msg.indexOf(":");
        if ( 0 < end )
        {
            end = msg.indexOf(" ");
        }
        StringBuffer returnMsg = new StringBuffer();
        returnMsg.append(
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        returnMsg.append(
            "<component-task-result>");
        returnMsg.append(
            "<component-name>");
        returnMsg.append(mComponent.getName());
        returnMsg.append(
            "</component-name>");
        returnMsg.append(
            "<component-task-result-details>");
        returnMsg.append(
            "<task-result-details>");
        returnMsg.append(
            "<task-id>");
        returnMsg.append(task);
        returnMsg.append(
            "</task-id>");
        returnMsg.append(
            "<task-result>FAILED</task-result>");
        returnMsg.append(
            "<message-type>ERROR</message-type>");
        returnMsg.append(
            "<task-status-msg>");
        returnMsg.append(
            "<msg-loc-info>");
        returnMsg.append(
            "<loc-token>");
        returnMsg.append(msg.substring(0, end - 1).trim());
        returnMsg.append(
            "</loc-token>");
        returnMsg.append(
            "<loc-message>");
        returnMsg.append(msg.substring(end + 1).trim());
        returnMsg.append(
            "</loc-message>");
        returnMsg.append(
            "</msg-loc-info>");
        returnMsg.append(
            "</task-status-msg>");
        returnMsg.append(
            "</task-result-details>");
        returnMsg.append(
            "</component-task-result-details>");
        returnMsg.append(
            "</component-task-result>");

        return returnMsg.toString();
    }

    /**
     * Construct a success message compliant with the defined schema for
     * messages.
     * @param task the successful task.
     * @return an XML string containing the message in the correct format.
     */
    private String buildSuccessMessage(String task)
    {
        StringBuffer returnMsg = new StringBuffer();
        returnMsg.append(
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        returnMsg.append(
            "<component-task-result>");
        returnMsg.append(
            "<component-name>");
        returnMsg.append(mComponent.getName());
        returnMsg.append(
            "</component-name>");
        returnMsg.append(
            "<component-task-result-details>");
        returnMsg.append(
            "<task-result-details>");
        returnMsg.append(
            "<task-id>");
        returnMsg.append(task);
        returnMsg.append(
            "</task-id>");
        returnMsg.append(
            "<task-result>SUCCESS</task-result>");
        returnMsg.append(
            "</task-result-details>");
        returnMsg.append(
            "</component-task-result-details>");
        returnMsg.append(
            "</component-task-result>");

        return returnMsg.toString();
    }

    /**
     * Check to make sure the component is in started state.
     * @param suName the name of the SU being operated upon.
     * @param oper the name of the operation being requested.
     * @throws javax.jbi.management.DeploymentException if the component is
     * not in started state.
     */
    private void checkComponentState(String suName, String oper)
        throws javax.jbi.management.DeploymentException
    {
        if ( !mComponent.isStarted() )
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_COMP_NOT_STARTED,
                oper, suName);
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage(oper, msg));
        }
    }

    /**
     * Perform the specified operation on the Service Unit and wait for it to
     * complete or time out.
     * @param operation the operation to be performed.
     * @param timeout the timeout interval for the operation.
     * @param task the task being executed.
     * @param suName the name of the Service Unit.
     * @param suRoot the root directory path of the Service Unit or null if
     * not required for the operation.
     * @return the result string from the operation or null if none present.
     * @throws javax.jbi.management.DeploymentException if any runtime error
     * or timeout occurs.
     */
    private String performOperation(int operation, long timeout, String task,
                                    String suName, String suRoot)
        throws javax.jbi.management.DeploymentException
    {
        String msg;
        Object args[] = {suName, suRoot};
        OperationCounter oc = new OperationCounter();

        if ( null == mSuMgr )
        {
            msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_OPERATION_NOT_AVAILABLE,
                    task, suName);
            throw new javax.jbi.management.DeploymentException(
                buildMessage(task, msg));
        }

        ServiceUnitOperation suOper =
            new ServiceUnitOperation(oc, mComponent.getName(), mSuMgr,
                operation, args);

        new Thread(suOper, suName).start();
        synchronized ( oc )
        {
            try
            {
                if ( 0 < oc.getValue() )
                {
                    oc.wait(timeout);
                }
            }
            catch ( java.lang.InterruptedException iEx )
            {
                suOper.getThread().interrupt();
                msg = mTranslator.getString(
                    LocalStringKeys.DMB_SU_OPERATION_INTERRUPTED,
                    task, suName);
                mLog.warning(msg);
                throw new javax.jbi.management.DeploymentException(
                    buildMessage(task, msg));
            }
        }
        if ( suOper.completed() )
        {
            Throwable ex = suOper.getException();
            if ( null != ex )
            {
                if ( statisticsEnabled() )
                {
                    mComponentStatistics.incrementFailedSURequests();
                }
                if ( ex instanceof javax.jbi.management.DeploymentException )
                {
                    throw (javax.jbi.management.DeploymentException) ex;
                }
                else
                {
                    String exMsg = ex.getMessage();
                    if ( null == exMsg )
                    {
                        msg = mTranslator.getString(
                            LocalStringKeys.DMB_SU_OPERATION_EXCEPTION_NO_MSG,
                            ex.getClass().getName(), task, suName);
                    }
                    else
                    {
                        msg = mTranslator.getString(
                            LocalStringKeys.DMB_SU_OPERATION_EXCEPTION,
                            ex.getClass().getName(), task, suName, exMsg);
                    }
                    mLog.log(Level.WARNING, msg, ex);
                    throw new javax.jbi.management.DeploymentException(
                        buildMessage(task, msg));
                }
            }
        }
        else
        {
            suOper.getThread().interrupt();
            if ( statisticsEnabled() )
            {
                mComponentStatistics.incrementTimedOutSURequests();
            }
            msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_OPERATION_TIMEOUT,
                task, suName, new Long(timeout));
            mLog.warning(msg);
            throw new javax.jbi.management.DeploymentException(
                buildMessage(task, msg));
        }
        return (String) suOper.getReturnValue();
    }

    /**
     * Force the state of an SU to be persisted after a change.
     * @param su  the Service Unit being processed.
     * @param task the task being executed.
     * @deprecated
     */
    private void persistState(ServiceUnit su, String task)
    {
        try
        {
            com.sun.jbi.management.registry.Updater 
                updater = ((Registry) mEnv.getRegistry()).getUpdater();
            updater.setServiceUnitState(su.getDesiredState(), 
                mComponent.getName(), su.getName());
        }
        catch(com.sun.jbi.management.registry.RegistryException rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.DMB_SU_STATE_PERSIST_FAILURE,
                su.getName(), task, rex.getMessage());
            mLog.warning(msg);
        }
    }

    /**
     * Return true if statistics are enabled for the component, false if not.
     * @return true if statistics enabled, false if not.
     */
    private boolean statisticsEnabled()
    {
        if ( null != mComponentStatistics )
        {
            if ( mComponentStatistics.isEnabled() )
            {
                return true;
            }
        }
        else
        {
            mComponentStatistics = mComponent.getStatisticsInstance();
            if ( null != mComponentStatistics )
            {
                if ( mComponentStatistics.isEnabled() )
                {
                    return true;
                }
            }
        }
        return false;
    }
}
