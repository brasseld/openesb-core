/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventManagementTargetStateInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.impl;

public class EventManagementTargetStateInfo {

    
    private String mTargetName;
    private boolean mIsLastStateUp;
    private boolean mIsUp;
    private boolean mHasNotificationRegisteration;
    private EventManagementControllerMBean mEventManagementControllerMBean;
    
    public EventManagementTargetStateInfo(String targetName, boolean isUp,
            boolean hasNotificationRegisteration) {
        mTargetName = targetName;
        mIsUp = isUp;
        mHasNotificationRegisteration = hasNotificationRegisteration;
    }
    
    public String getTargetName() {
        return mTargetName;
    }

    public boolean isUp() {
        return mIsUp;
    }
    
    public void setIsUp(boolean aIsUp) {
        if(aIsUp != mIsLastStateUp) {
            mIsLastStateUp =  mIsUp;
        }
        mIsUp = aIsUp;
        if(!mIsUp) {
            // the target is down will need to rereg.
            mHasNotificationRegisteration = false;
        }
    }

    public boolean hasNotificationRegisteration() {
        return mHasNotificationRegisteration;
    }
    

    public void setHasNotificationRegisteration(
            boolean hasNotificationRegisteration) {
        mHasNotificationRegisteration = hasNotificationRegisteration;
    }

    public boolean isLastStateUp() {
        return mIsLastStateUp;
    }
    
}
