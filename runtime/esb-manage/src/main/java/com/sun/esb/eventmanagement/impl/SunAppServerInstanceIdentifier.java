package com.sun.esb.eventmanagement.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import com.sun.appserv.management.DomainRoot;
import com.sun.appserv.management.client.ProxyFactory;
import com.sun.appserv.management.config.ConfigConfig;
import com.sun.appserv.management.config.HTTPListenerConfig;
import com.sun.appserv.management.config.HTTPServiceConfig;
import com.sun.esb.eventmanagement.api.InstanceIdentifier;
import com.sun.jbi.platform.PlatformContext;

public class SunAppServerInstanceIdentifier implements InstanceIdentifier {
    
    private static final Logger mLogger;
    private static boolean isDebugEnabled;
    static {
        mLogger = Logger.getLogger(EventManagementControllerMBean.class.getName());
         isDebugEnabled = mLogger.isLoggable(Level.FINEST);
     
     }
    private final static String CONNECTORS_QUERY = "com.sun.appserv:type=Connector,*";
    private final static String CONNECTOR_PARTIAL_OBJECT_NAME = 
        "com.sun.appserv:type=Connector,address=0.0.0.0,port=";
    private final static String SELECTOR_QUERY = "com.sun.appserv:type=Selector,*";
    private PlatformContext mPlatformContext;
    private MBeanServerConnection mMBeanServer;
    private final static String SERVER_HTTP_LISTENER1 = "com.sun.appserv:" +
    		"type=http-listener,id=http-listener-1," +
    		"config=server-config,category=config";
    private final static String SERVER_ADMIN_LISTENER1 = " amx:X-ConfigConfig=server-config," +
    		"j2eeType=X-HTTPListenerConfig,name=admin-listener,X-HTTPServiceConfig=na";
    HTTPListenerConfig mHTTPListenerConfig;
    
    public SunAppServerInstanceIdentifier(PlatformContext aPlatformContext) {
        mPlatformContext = aPlatformContext;
        mMBeanServer = mPlatformContext.getMBeanServer();
        getAdminListenerConfig();
    }
    
    public String getInstanceUniqueName() {
        return mPlatformContext.getInstanceName();
    }

    public String getInstanceUniquePort() {
        ObjectName on = null;
        
      try {
            on = new ObjectName(CONNECTORS_QUERY);
            Set<ObjectName> connectorsSet = mMBeanServer.queryNames(on, null);
            for (ObjectName connector : connectorsSet) {
                String address = connector.getKeyProperty("address");
                if("0.0.0.0".equals(address)) {
                    String portNumber = connector.getKeyProperty("port");
                    return portNumber;
                }
            }
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbeans we query is part it. 
            if(isDebugEnabled) {
                mLogger.log(Level.FINEST, "Exception ", e);       
            }

        } 
        return "";
    }
    
    public String getInstanceHttpPort() {
        String httpPortNumber = "";
        try {
            List<String> httpPortList = new ArrayList<String>();
            ObjectName on = new ObjectName(SELECTOR_QUERY);
            Set<ObjectName> selectorSet = mMBeanServer.queryNames(on, null);
            for (ObjectName selctor : selectorSet) {
                String name = selctor.getKeyProperty("name");
                if(name.startsWith("http")) {
                    httpPortList.add(name.substring(4));
                }
            }
            for (String  portNumber : httpPortList) {
                ObjectName connectorObjectName = new ObjectName(CONNECTOR_PARTIAL_OBJECT_NAME+portNumber);
                String[] signs = new String[] {"java.lang.String"};
                Object[] params = new Object[] {"secure"};
                String isSecure = (String)mMBeanServer.invoke(connectorObjectName,"getProperty",params,signs);
                if(!"true".equalsIgnoreCase(isSecure)) {
                    httpPortNumber = portNumber;
                    break;
                }
               
            }
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbeans we query are part it. 
            if(isDebugEnabled) {
                e.printStackTrace();
            }
        } 
        return httpPortNumber;
    }

    public String getHttpPort() {
        String httpPortNumber ="8080";
        try {
            ObjectName on = new ObjectName(SERVER_HTTP_LISTENER1);
            httpPortNumber = (String)mMBeanServer.getAttribute(on, "port");
        } catch (Exception e) {
            // should not fail since we running on glassfish & the mbean we query is part it. 
            if(isDebugEnabled) {
                e.printStackTrace();
            }
        } 
        return httpPortNumber;
    }

    public String getDomainAdminPort() {
    
        String httpPortNumber =null;
        boolean isDAS = mPlatformContext.isAdminServer();
        if(!isDAS) {
            return "";
        }
        httpPortNumber =  mHTTPListenerConfig.getPort();
        return httpPortNumber;
    }
    
    public Boolean isSecureAdminPort() {
        Boolean isSecure = false;
        boolean isDAS = mPlatformContext.isAdminServer();
        if(!isDAS) {
            return isSecure;
        }
        
        isSecure = mHTTPListenerConfig.getSecurityEnabled();
        return isSecure;
       
    }
    
    private void getAdminListenerConfig() {
        boolean isDAS = mPlatformContext.isAdminServer();
        if(!isDAS) {
            return;
        }
        DomainRoot domain = 
            ProxyFactory.getInstance(mMBeanServer).getDomainRoot();
    
        Map<String, ConfigConfig> serverConfigMap = 
            domain.getDomainConfig().getConfigConfigMap();
        ConfigConfig config = serverConfigMap.get("server-config");
        HTTPServiceConfig httpService = config.getHTTPServiceConfig();
        Map<String,HTTPListenerConfig> listenerMap    = httpService.getHTTPListenerConfigMap();
        mHTTPListenerConfig =(HTTPListenerConfig)listenerMap.get( "admin-listener");
       
    }
    
}
