/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;

import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.system.ManagementException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.jbi.JBIException;
import javax.jbi.management.LifeCycleMBean;

import javax.management.ObjectName;

/**
 * ComponentLifeCycleMBean defines the standard life cycle controls for
 * JBI Installable Components.
 * <ul>
 *   <li>Initialize the component, preparing it to receive service requests.</li>
 *   <li>Start the component, allowing it to initiate service requests.</li>
 *   <li>Stop the component from initiating any more service requests.</li>
 *   <li>Shut down the component, returning it to the uninitialized state
 *       where it cannot receive service requests.</li>
 *   <li>Query the JMX object name of the extension MBean for the component.</li>
 * </ul>
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentLifeCycle 
    extends Facade
    implements com.sun.jbi.management.ComponentLifeCycleMBean
{
    
    private String                     mComponentName;
    private ComponentType              mComponentType;
    private Map<String, ObjectName>    mInstanceComponentLCMBeans;
        
    public ComponentLifeCycle(EnvironmentContext ctx, String target, 
        String compName, ComponentType compType, Map<String, ObjectName> compLCMBeans)
        throws ManagementException
    {
        super(ctx, target);
        
        mComponentName                  = compName;
        mComponentType                  = compType;
        mInstanceComponentLCMBeans      = compLCMBeans;
    }
        
    /**
     * Start the component.
     * 
     * @exception javax.jbi.JBIException if the item fails to start.
     */
    public void start() 
        throws JBIException
    {
        try
        {
            if ( !mTarget.equals(DOMAIN))
            {
                if ( mPlatform.isStandaloneServer(mTarget) )
                {
                    startComponentOnInstance(mTarget);
                }
                else
                {
                    startComponentOnCluster(mTarget);
                }
                setDesiredState(mComponentName, ComponentState.STARTED, mTarget);
            }
            else
            {
                throwNotSupportedManagementException("start");
            }
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }

    /**
     * Stop the item. This suspends current messaging activities.
     * 
     * @exception javax.jbi.JBIException if the item fails to stop.
     */
    public void stop() 
        throws JBIException
    {
        try
        {
            if ( !mTarget.equals(DOMAIN))
            {
                if ( mPlatform.isStandaloneServer(mTarget) )
                {
                    stopComponentOnInstance(mTarget);
                }
                else
                {
                    stopComponentOnCluster(mTarget);
                }
                setDesiredState(mComponentName, ComponentState.STOPPED, mTarget);
            }
            else
            {
                throwNotSupportedManagementException("stop");
            }
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }

    /**
     * Shut down the item. This releases resources and returns the item
     * to an uninitialized state.
     *
     * @exception javax.jbi.JBIException if the item fails to shut down.
     */
    public void shutDown() 
            throws JBIException
    {
        shutDown(false);
    }

    /**
     * Get the current state of the managed compononent on the target.
     * 
     * @return the current state of this managed component (must be one of the 
     *         string constants defined by this interface)
     */
    public String getCurrentState()
    {
        if ( !mTarget.equals(DOMAIN))
        {
            return getCurrentStateOnTarget();
        }
        else
        {
            return getCurrentStateOnAllTargets();
        }
    }

    
    /**
     * Get the JMX ObjectName for the life cycle extension MBean for this 
     * component. If there is none, return <code>null</code>.
     * Note that this MBean may serve as a container for multiple MBeans,
     * as required by the component implementation.
     * 
     * @return ObjectName the JMX object name of the additional MBean 
     *         or <code>null</code> if there is no additional MBean.
     * @exception javax.jbi.JBIException if there is a failure getting component
     *            information for the component to which this life cycle 
     *            applies.
     */
    public ObjectName getExtensionMBeanName() 
        throws JBIException
    {
        try
        {
            // TODO : There should be a facade Extension MBean
            if ( !mTarget.equals(DOMAIN))
            {
                return getExtensionMBeanNameForTarget();
            }
            else
            {
                try
                {
                    return new ObjectName("com.sun.jbi:Target=domain");
                }
                catch(Exception ex)
                {

                    String msg = mMsgBuilder.buildExceptionMessage("getExtensionMBeanName", ex);
                    throw new JBIException(msg);
                }
            }
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                        Extended ComponentLifeCycleMBean ops                      *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Shut down this component. 
     *
     * @param force set to true indicates that the component should be shutdown 
     * forcefully; that is, place the component in the "Shutdown" state regardless of the 
     * result of calling its LifeCycle shutDown method.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    public void shutDown(boolean force) throws javax.jbi.JBIException
    {
        try
        {
            if ( !mTarget.equals(DOMAIN))
            {
                if ( mPlatform.isStandaloneServer(mTarget) )
                {
                    shutDownComponentOnInstance(mTarget, force);
                }
                else
                {
                   shutdownComponentOnCluster(mTarget, force);
                }
                setDesiredState(mComponentName, ComponentState.SHUTDOWN, mTarget);
            }   
            else
            {
                throwNotSupportedManagementException("shutDown");
            }
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                        Private Helpers                                           *
    \*----------------------------------------------------------------------------------*/
     
    /**
     * Update the DAS Registry : set the desired state for the component
     *
     * @param compName - component Name
     * @param state - ComponentState
     * @param target - target name
     */
    private void setDesiredState(String compName, ComponentState state, String target )
        throws ManagementException
    {
        try
        {
            getUpdater().setComponentState(target, state, compName);
        }
        catch (com.sun.jbi.management.registry.RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("setDesiredState", rex);
            throw new ManagementException(errMsg);
        }
    }
    
    /**
     * Get Current state of the Component on the target
     */
    private String getCurrentStateOnTarget()
    {
        String state = LifeCycleMBean.UNKNOWN;
        mLog.finer(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_GETTING_COMPONENT_STATE_ON_TARGET, 
            mComponentName, mTarget));
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
              state = getCurrentStateOnInstance(mTarget);   
            }
            else
            {
                state = getCurrentStateOnCluster(mTarget);   
            }
        }
        catch ( ManagementException mex)
        {
            throw new RuntimeException( new JBIException(mex.getMessage()));
        }
        return state;
    }
    
    /**
     * Get Current state of the Component on the instance
     */
    private String getCurrentStateOnInstance(String instanceName)
        throws ManagementException
    {
        return getComponentStateOnInstance(mComponentName, instanceName);
    }
    
    /**
     * Get the state of the Component on the cluster
     */
    private String getCurrentStateOnCluster(String clusterName)
        throws ManagementException
    {
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        List<ComponentState> states = new ArrayList<ComponentState>();
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    String state = getCurrentStateOnInstance(instance);
                    states.add(ComponentState.valueOfLifeCycleState(state));
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    continue;
                }
            }

            try
            {
                if ( exceptionMap.size() > 0 )
                {
                    handleClusteredInstanceFailures("getCurrentState", 
                        exceptionMap,     
                        instances.size(), 
                        LocalStringKeys.JBI_ADMIN_FAILED_START_COMPONENT_ON_INSTANCE);
                }
            }
            catch (ManagementException mex)
            {
                throw new RuntimeException(mex.getMessage());
            }
        }
        
        mLog.finer(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_COMPONENT_STATES_ON_CLUSTER,
            mComponentName, clusterName, states.toString()));
        ComponentState effState = ComponentState.computeEffectiveState(states);
        return ComponentState.getLifeCycleState(effState);
    }
    
    /**
     * Get the Extension MBean for a Component on a Instance
     */
    private ObjectName getExtensionMBeanNameForTarget()
        throws ManagementException
    {
        ObjectName objName = null;
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            objName = getExtensionMBeanNameForInstance(mTarget);
        }
        else
        {
            // cluster case is non-trivial tbd
        }
        return objName;
    }
    
    /**
     * Get the Extension MBean for a Component on a Instance
     */
    private ObjectName getExtensionMBeanNameForInstance(String instanceName)
        throws ManagementException
    {
            ObjectName extMBean = null;
            if ( isInstanceRunning(instanceName) )
            {
                // Invoke op on the remote ComponentLifeCycleMBean
                String[] sign   = new String[0];
                Object[] params = new Object[0];

                return (ObjectName) invokeRemoteOperation(
                    getComponentLifeCycleMBeanName(mComponentName, mComponentType, instanceName), 
                        "getExtensionMBeanName", params, sign,
                        instanceName);
            }
            else
            {
                String[] params = new String[]{instanceName};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_INSTANCE_DOWN, params);
                String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage("getExtensionMBeanName",
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), 
                    params,
                    mMsgBuilder.getMessageToken(errMsg));
                throw new ManagementException(jbiTaskMsg);
            }
    }
    
    /**
     * Shut down the component on a instance.
     *
     * @param instanceName - name of the instance to which the command is targeted
     * @param force - if set to true, the state change is to be forceful. 
     * @throws ManagementException
     */
    private void shutDownComponentOnInstance(String instanceName, boolean force)
        throws ManagementException
    {
            mLog.finer(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CHANGING_COMPONENT_STATE_ON_INSTANCE,
                "shutDown", mComponentName, instanceName));
            if ( isInstanceRunning(instanceName) )
            {
                String[] sign;
                Object[] params;
                if ( force )
                {
                    sign   = new String[]{"boolean"};
                    params = new Object[]{new Boolean(force)};
                }
                else
                {
                    sign   = new String[0];
                    params = new Object[0];
                }
                invokeRemoteVoidOperation(getComponentLifeCycleMBeanName(mComponentName, mComponentType, instanceName), 
                    "shutDown", params, sign, instanceName);
            }
    }
    
    /**
     * Start the component on an instance
     *
     * @param instanceName - name of the instance to which the command is targeted
     * @throws ManagementException
     */
    private void startComponentOnInstance(String instanceName)
        throws ManagementException
    {
            mLog.finer(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CHANGING_COMPONENT_STATE_ON_INSTANCE,
                "start", mComponentName, instanceName));
            if ( isInstanceRunning(instanceName) )
            {
                String[] sign   = new String[0];
                Object[] params = new Object[0];
                invokeRemoteVoidOperation(getComponentLifeCycleMBeanName(mComponentName, mComponentType, instanceName), 
                    "start", params, sign, instanceName);
            }
    }
    
    /**
     * Stop the component on an instance.
     *
     * @param instanceName - name of the instance to which the command is targeted
     * @throws ManagementException
     */
    private void stopComponentOnInstance(String instanceName)
        throws ManagementException
    {
            mLog.finer(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CHANGING_COMPONENT_STATE_ON_INSTANCE,
                "stop", mComponentName, instanceName));
            if ( isInstanceRunning(instanceName) )
            {
                String[] sign   = new String[0];
                Object[] params = new Object[0];
                invokeRemoteVoidOperation(getComponentLifeCycleMBeanName(mComponentName, mComponentType, instanceName), 
                    "stop", params, sign, instanceName);
            }
    }
    
    /**
     * Start the component on each and every instance on the cluster, if the operation
     * succeeds on even a single instance it is considered a success.
     */
    private void startComponentOnCluster(String clusterName)
        throws javax.jbi.JBIException
    {
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    startComponentOnInstance(instance);
                }
                catch(JBIException jex)
                {
                    exceptionMap.put(instance, jex);
                    continue;
                }
            }
            
            if ( exceptionMap.size() > 0 )
            {
                try
                {
                    handleClusteredInstanceFailures("start", 
                        exceptionMap,     
                        instances.size(), 
                        LocalStringKeys.JBI_ADMIN_FAILED_START_COMPONENT_ON_INSTANCE);

                } 
                catch (ManagementException mex)
                {
                    throw new RuntimeException(mex.getMessage());
                }
            }
            
        }
    }
    
    /**
     * Stop the component on each and every instance on the cluster. If it is a partial
     * success i.e. one or more exceptions, get the state of the component in the cluster 
     * if the state is not stopped, then an exception with the composite message is thrown.
     */
    private void stopComponentOnCluster(String clusterName)
        throws javax.jbi.JBIException
    {
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    stopComponentOnInstance(instance);
                }
                catch(JBIException jex)
                {
                    exceptionMap.put(instance, jex);
                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                String currState = getCurrentState();
                if ( ComponentState.STOPPED != ComponentState.valueOfLifeCycleState(currState) )
                {
                    try
                    {
                        handleClusteredInstanceFailures("stop", 
                            exceptionMap, 
                            // -- this makes sure an exception is thrown
                            exceptionMap.size(), 
                            LocalStringKeys.JBI_ADMIN_FAILED_STOP_COMPONENT_ON_INSTANCE);
                    } 
                    catch (ManagementException mex)
                    {
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
        }
    }
    
    /**
     * Shutdown the component on each and every instance on the cluster. If it is a partial
     * success i.e. one or more exceptions, get the state of the component in the cluster 
     * if the state is not shutdown, then an exception with the composite message is thrown.
     *
     * @param force - if set to true the component is to be forcibly shutdown on all
     * instances in the cluster.
     */
    private void shutdownComponentOnCluster(String clusterName, boolean force)
        throws javax.jbi.JBIException
    {
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    shutDownComponentOnInstance(instance, force);
                }
                catch(JBIException jex)
                {
                    exceptionMap.put(instance, jex);
                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                String currState = getCurrentState();
                if ( ComponentState.SHUTDOWN != ComponentState.valueOfLifeCycleState(currState) )
                {
                    try
                    {
                        handleClusteredInstanceFailures("shutDown", 
                            exceptionMap, 
                            // -- this makes sure an exception is thrown
                            exceptionMap.size(), 
                            LocalStringKeys.JBI_ADMIN_FAILED_SHUTDOWN_COMPONENT_ON_INSTANCE);
                    } 
                    catch (ManagementException mex)
                    {
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
        }
    }
    
    /**
     * @return the Component LifeCycle MBean for the instance. 
     *
     * First get the ComponentLifeCycleMBean name from the instanceComponentLCMBeans map,
     * If not found in the map and the instance is running, the instance started after
     * the DAS started ( i.e. after this target MBean was registered ), it would have
     * synchronized and the component is now installed on the instance, so can build the
     * ComponentLifeCycleMBean for the instance.
     * 
     * @deprecated 
     */
    private ObjectName getComponentLifeCycleMBean(String instanceName)
    {
        ObjectName compLCMBean = mInstanceComponentLCMBeans.get(instanceName);
        
        /**
         * The ComponentLifeCycle MBean is null for an instance if the instance
         * was not up when the component was installed. 
         *
         * If the instance is running during a LC operation, it would have 
         * synchronized and the component is installed on the instance. So we
         * can build the Component Life Cycle MBean name assuming this.
         */
        boolean isInstanceRunning = false;
        try
        {
            isInstanceRunning = isInstanceRunning(instanceName);
        }
        catch ( Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
        }
        if ( compLCMBean == null && isInstanceRunning )
        {
            MBeanNames mbnNames = mMgtCtx.getMBeanNames(instanceName);
            if ( ComponentType.BINDING == mComponentType )
            {
                compLCMBean = mbnNames.getBindingMBeanName(mComponentName, 
                    MBeanNames.CONTROL_TYPE_LIFECYCLE);
            }
            else
            {
                 compLCMBean = mbnNames.getEngineMBeanName(mComponentName, 
                        MBeanNames.CONTROL_TYPE_LIFECYCLE);   
            }
        }
        return compLCMBean;
    }
    
    /**
     * The Component Lifecycle MBean ObjectName can be null for an instance, this occurs when 
     * an instance is down when install is invoked on the cluster target. 
     * In this case the operation cannot be performed on the instance.
     */
    private boolean canPerformOperationOnInstanceCheck(String instanceName, String taskId)
    {
        boolean canDo = true;
        ObjectName compLC;
        
        try
        {
            compLC= getComponentLifeCycleMBeanName(mComponentName,
            mComponentType, instanceName);
        }
        catch ( ManagementException mex)
        {
            compLC = null;
        }
        
        if ( compLC == null )
        {
            String warning = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_CANNOT_PERFORM_COMPONENT_LIFECYCLE_OP,
                taskId, mComponentName, instanceName);
            
            canDo = false;
        }
        
        return canDo;
    }
    
    /**
     * This operation gets the state of the component on all targets in the domain
     * and returns the most advanced state of the component.
     * 
     * @return the most advanced LifeCycleMBean state string
     */
    private String getCurrentStateOnAllTargets()
    {
        List<String> targets = null;
        
        try
        {
            targets = getAllTargets();
        }
        catch(com.sun.jbi.management.system.ManagementException mex)
        {
            throw new RuntimeException(mex.getMessage());            
        }  
       List<ComponentState> states = new ArrayList<ComponentState>();
       for( String target : targets )
       {
           try
           {
               ObjectName facadeCompLCMBean = null;
               if ( mComponentType == ComponentType.BINDING )
               {
                    facadeCompLCMBean = mMBeanNames.getBindingMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.ComponentLifeCycle, 
                        target);
               }
               else
               {
                    facadeCompLCMBean = mMBeanNames.getEngineMBeanName(
                        mComponentName, 
                        MBeanNames.ComponentServiceType.ComponentLifeCycle, 
                        target);
               }
               
               String state = (String) mMBeanSvr.getAttribute(
                       facadeCompLCMBean, "CurrentState");
               states.add(ComponentState.valueOfLifeCycleState(state));
               mLog.finest("The state of component " + mComponentName + " on target " 
                + target + " is "  + state);
               
           }
           catch ( Exception ex )
           {
               states.add(ComponentState.UNKNOWN);
               continue;
           }
       }
       
       
       mLog.finest("The state of component  " + mComponentName + " on targets " 
                + targets.toString() + " is " + states);
       ComponentState effState = ComponentState.computeEffectiveState(states);
       return ComponentState.getLifeCycleState(effState);
    }
}
