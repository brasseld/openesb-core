#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01200.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01200 - System Components Test

echo testname is jbiadmin01200
. ./regress_defs.ksh

#create sa needed for deployment test
ant -q  -lib "$REGRESS_CLASSPATH" -f jbiadmin01200.xml 1>&2

echo list, start, stop test for server
asadmin  list-jbi-shared-libraries --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT 2>&1
asadmin  list-jbi-binding-components  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT 2>&1
asadmin  list-jbi-service-engines --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT 2>&1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_JBI_HOME/components/sun-http-binding/httpbc.jar 2>&1

echo list, start, stop test for instance1
asadmin  list-jbi-shared-libraries --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT 2>&1
asadmin  list-jbi-binding-components  --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT 2>&1
asadmin  list-jbi-service-engines --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT 2>&1
asadmin start-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin stop-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin start-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
asadmin stop-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1

echo deploy test on server
asadmin  list-jbi-binding-components  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate=started 2>&1
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/schemaorg_apache_xmlbeans.system-comps-sa.jar 2>&1
asadmin  list-jbi-binding-components  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate=started 2>&1

echo deploy test on instance1
asadmin  list-jbi-binding-components  --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate=started 2>&1
asadmin deploy-jbi-service-assembly --target=instance1  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/schemaorg_apache_xmlbeans.system-comps-sa.jar 2>&1
asadmin  list-jbi-binding-components  --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate=started 2>&1

echo undeploy, uninstall test on instance1
asadmin undeploy-jbi-service-assembly --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT schemaorg_apache_xmlbeans.system-comps-sa 2>&1
asadmin stop-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin shut-down-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin uninstall-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1

#test install from domain. doing this from asant because asadmin is broken
echo testing install from domain
$JBI_ANT_NEG -Djbi.target=instance1 -Djbi.component.name=sun-http-binding install-component

#start, stop, uninstall the component that was installed from domain
asadmin start-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin stop-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin shut-down-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin uninstall-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin uninstall-jbi-component --target=sync --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin uninstall-jbi-component --target=CAS-cluster1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1


echo undeploy, uninstall test on server
asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT schemaorg_apache_xmlbeans.system-comps-sa 2>&1
#asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 2>&1
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-http-binding 2>&1


echo reinstall test on server
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_JBI_HOME/components/sun-http-binding/httpbc.jar 2>&1

echo reinstall test on instance1
asadmin install-jbi-component --target="instance1" --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_JBI_HOME/components/sun-http-binding/httpbc.jar 2>&1
