/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationServiceMBeanImpl.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.configuration;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.Descriptor;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.modelmbean.ModelMBeanAttributeInfo;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.base.services.AbstractListStateServiceMBeansImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.TargetType;
import com.sun.esb.management.common.data.ApplicationVerificationReport;
import com.sun.esb.management.common.data.helper.ApplicationVerificationReportWriter;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.Util;
import com.sun.jbi.ui.runtime.verifier.JBIApplicationVerifier;
import com.sun.jbi.util.Constants;

/**
 * Defines operations for common configuration services across the JBI Runtime,
 * component containers, configuring logger levels, etc.
 * 
 * @author graj
 */
public class ConfigurationServiceMBeanImpl extends
        AbstractListStateServiceMBeansImpl implements ConfigurationService,
        Serializable, Constants {
    
    static final long serialVersionUID = -1L;
    
    /**
     * Constructor - Constructs a new instance of ConfigurationServiceMBeanImpl
     * 
     * @param anEnvContext
     */
    public ConfigurationServiceMBeanImpl(EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with configurable
     * attributes
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean has configuration 
     *              attributes
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isComponentConfigSupported(String componentName, String targetName)  
        throws ManagementRemoteException
     {
        Boolean isComponentConfigSupported = false;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("isComponentConfigSupported(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                isComponentConfigSupported = (Boolean) this.invokeMBeanOperation(
                        configFacadeMBean, "isComponentConfigSupported",
                        new Object[] {}, new String[] {});
                
                logDebug("isComponentConfigSupported(): result = "
                        + isComponentConfigSupported);
                
            } catch (ManagementRemoteException jbiRE) {
                logDebug("isComponentConfigSupported caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("isComponentConfigSupported  caught non-ManagementRemoteException:");
                logDebug(ex);
                
                throw new ManagementRemoteException(ex);
            }
        } else {
            
            // TODO
            // Component installed and no MBean - return false
            // Component not installed - 
            // -- Component Configuration Facade MBean not found -- error
            // component not installed on target or does not have a config MBean.
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return isComponentConfigSupported;         
     }
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported(String componentName, String targetName)  
        throws ManagementRemoteException
     {
        Boolean isAppConfigSupported = false;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("isAppConfigSupported(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                isAppConfigSupported = (Boolean) this.invokeMBeanOperation(
                        configFacadeMBean, "isAppConfigSupported",
                        new Object[] {}, new String[] {});
                
                logDebug("isAppConfigSupported(): result = "
                        + isAppConfigSupported);
                
            } catch (ManagementRemoteException jbiRE) {
                logDebug("isAppConfigSupported caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("isAppConfigSupported  caught non-ManagementRemoteException:");
                logDebug(ex);
                
                throw new ManagementRemoteException(ex);
            }
        } else {
            
            // TODO
            // Component installed and no MBean - return false
            // Component not installed - 
            // -- Component Configuration Facade MBean not found -- error
            // component not installed on target or does not have a config MBean.
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return isAppConfigSupported;
     }
     
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported(String componentName, String targetName)  
        throws ManagementRemoteException
     {
        Boolean isAppVarsSupported = false;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("isAppVarsSupported(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                isAppVarsSupported = (Boolean) this.invokeMBeanOperation(
                        configFacadeMBean, "isAppVarsSupported",
                        new Object[] {}, new String[] {});
                
                logDebug("isAppVarsSupported(): result = "
                        + isAppVarsSupported);
                
            } catch (ManagementRemoteException jbiRE) {
                logDebug("isAppVarsSupported caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("isAppVarsSupported  caught non-ManagementRemoteException:");
                logDebug(ex);
                
                throw new ManagementRemoteException(ex);
            }
        } else {
            
            // TODO
            // Component installed and no MBean - return false
            // Component not installed - 
            // -- Component Configuration Facade MBean not found -- error
            // component not installed on target or does not have a config MBean.
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return isAppVarsSupported;
     }
     
    /**
     * Get the CompositeType definition for the components application configuration 
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the CompositeType for the components application configuration.
     */
     public CompositeType queryApplicationConfigurationType
         (String componentName, String targetName)
             throws ManagementRemoteException
     {
        
        domainTargetCheck(targetName);
        
        CompositeType appCfgType = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("queryApplicationConfigurationType(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                appCfgType = (CompositeType) this.invokeMBeanOperation(
                        configFacadeMBean, "queryApplicationConfigurationType",
                        new Object[] {}, new String[] {});
                
                return appCfgType;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("queryApplicationConfigurationType caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("queryApplicationConfigurationType  caught non-ManagementRemoteException:");
                logDebug(ex);
                
                throw new ManagementRemoteException(ex);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    
    /**
     * Add a named application configuration to a component installed on a given
     * target.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            application configuration name
     * @param config
     *            application configuration represented as a set of properties.
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#addApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String, java.util.Properties)
     */
    public String addApplicationConfiguration(String componentName,
            String targetName, String name, Properties config)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("addApplicationConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "addApplicationConfiguration",
                        new Object[] { name, config }, new String[] {
                                "java.lang.String", "java.util.Properties" });
                
                logDebug("addApplicationConfiguration(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("addApplicationConfiguration caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("addApplicationConfiguration  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.add.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * Add application variables to a component installed on a given target. If
     * even a variable from the set is already set on the component, this
     * operation fails.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariables -
     *            set of application variables to add. The values of the
     *            application variables have the application variable type and
     *            value and the format is "[type]value"
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not added the management message has a
     *         ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#addApplicationVariables(java.lang.String,
     *      java.lang.String, java.util.Properties)
     */
    @SuppressWarnings("unchecked")
    public String addApplicationVariables(String componentName,
            String targetName, Properties appVariables)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("addApplicationVariables(" + componentName + "," + targetName
                + "): configMBean = " + configFacadeMBean);
        
        Map<String/* appVarName */, String /* JBI MgmtMsg result */> results = 
                new HashMap<String/* appVarName */, String /** JBI MgmtMsg result*/>();
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            
            componentStartedOnTargetCheck(componentName, targetName);
            
            // For each application variable invoke addApplicationVariable() on
            // the facade MBean. If the operation is not successful, get the
            // instance details : the error task status msg and the exception
            // info.
            // 
            // Build a composite message which has all the details re. which
            // attribute failed to be added and why ( in case of cluster need
            // the instance details )
            Set appVarNames = appVariables.keySet();
            
            for (Iterator itr = appVarNames.iterator(); itr.hasNext();) {
                String name = (String) itr.next();
                String typeAndValueStr = (String) appVariables.get(name);
                String result = null;
                try {
                    CompositeData appVarCD = componentConfigurationHelper
                            .createApplicationVariableComposite(name,
                                    typeAndValueStr);
                    result = (String) this
                            .invokeMBeanOperation(
                                    configFacadeMBean,
                                    "addApplicationVariable",
                                    new Object[] { name, appVarCD },
                                    new String[] { "java.lang.String",
                                            "javax.management.openmbean.CompositeData" });
                    
                    logDebug("addApplicationVariable(): result = " + result);
                } catch (ManagementRemoteException jbiRE) {
                    // exception msg is a management message already; just keep
                    // it as it is
                    logDebug("caught ManagementRemoteException:");
                    logDebug(jbiRE);
                    result = jbiRE.getMessage();
                    
                } catch (Exception ex) {
                    logDebug("caught non-ManagementRemoteException:");
                    logDebug(ex);
                    result = ex.getMessage();
                }
                results.put(name, result);
            }
            
        } else {
            // -- Component Configuration Facade MBean not found -- error component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        
        JBIManagementMessage msg = JBIManagementMessage
                .createJBIManagementMessage("addApplicationVariables", results,
                        false);
        return com.sun.jbi.ui.common.JBIResultXmlBuilder.getInstance()
                .createJbiResultXml(msg);
    }
    
    /**
     * Delete a named application configuration in a component installed on a
     * given target.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            name of application configuration to be deleted
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#deleteApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String deleteApplicationConfiguration(String componentName,
            String targetName, String name) throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("deleteApplicationConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "deleteApplicationConfiguration",
                        new Object[] { name },
                        new String[] { "java.lang.String" });
                
                logDebug("deleteApplicationConfiguration(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("deleteApplicationConfiguration caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("deleteApplicationConfiguration  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.delete.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * Delete application variables from a component installed on a given
     * target. If even a variable from the set has not been added to the
     * component, this operation fails.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariableNames -
     *            names of application variables to delete.
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not deleted the management message has a
     *         ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#deleteApplicationVariables(java.lang.String,
     *      java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public String deleteApplicationVariables(String componentName,
            String targetName, String[] appVariableNames)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("deleteApplicationVariables(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        Map<String/* appVarName */, String /* JBI MgmtMsg result */> results = new HashMap();
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            
            componentStartedOnTargetCheck(componentName, targetName);
            
            // For each application variable invoke setApplicationVariable() on
            // the
            // facade MBean. If the operation is not successful, get the
            // instance
            // details : the error task status msg and the exception info.
            // 
            // Build a composite message which has all the details re. which
            // attribute failed to be set and why ( in case of cluster need the
            // instance details )
            if(appVariableNames != null) {
                for (String appVarName : appVariableNames) {
                    String result = null;
                    try {
                        result = (String) this.invokeMBeanOperation(
                                configFacadeMBean, "deleteApplicationVariable",
                                new Object[] { appVarName },
                                new String[] { "java.lang.String" });
                        
                        logDebug("deleteApplicationVariable(): result = " + result);
                    } catch (ManagementRemoteException jbiRE) {
                        // exception msg is a management message already; just keep
                        // it
                        // as it is
                        logDebug("caught ManagementRemoteException:");
                        logDebug(jbiRE);
                        result = jbiRE.getMessage();
                        
                    } catch (Exception ex) {
                        logDebug("caught non-ManagementRemoteException:");
                        logDebug(ex);
                        result = ex.getMessage();
                    }
                    results.put(appVarName, result);
                }
            }
            
        } else {
            // -- Component Configuration Facade MBean not found -- error component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        JBIManagementMessage msg = JBIManagementMessage
                .createJBIManagementMessage("deleteApplicationVariables",
                        results, true);
        return com.sun.jbi.ui.common.JBIResultXmlBuilder.getInstance()
                .createJbiResultXml(msg);
    }
    
    /**
     * This method is used to export the application variables and application
     * configuration objects used by the given application in the specified
     * target.
     * 
     * @param applicationName
     *            the name of the application
     * @param targetName
     *            the target whose configuration has to be exported
     * @param configDir
     *            the dir to store the configurations
     * @returns String the id for the zip file with exported configurations
     * 
     * @throws ManagementRemoteException
     *             if the application configuration could not be exported
     * 
     * Note: param configDir is used between ant/cli and common client client.
     * The return value is used between common client server and common client
     * client.
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#exportApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String exportApplicationConfiguration(String applicationName,
            String targetName, String configDir)
            throws ManagementRemoteException {
        String result = null;
        JBIApplicationVerifier verifier = 
                new JBIApplicationVerifier(
                this.environmentContext);
        try {
            domainTargetCheck(targetName);
            result = verifier.exportApplicationConfiguration(
                    applicationName, 
                    targetName,
                    configDir);
        } catch (Exception ManagementRemoteException) {
            throw new ManagementRemoteException(ManagementRemoteException);
        }
        return result;

    }
    
    /**
     * Get a specific named configuration. If the named configuration does not
     * exist in the component the returned properties is an empty set.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the application configuration represented as a set of properties.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public Properties getApplicationConfiguration(String componentName,
            String name, String targetName) throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        Map<String, Properties> configMap = getApplicationConfigurations(
                componentName, targetName);
        Properties appConfigProps = configMap.get(name);
        
        if ( appConfigProps == null )
        {
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.missing.app.config.error", args, null);
                throw new ManagementRemoteException(mgmtEx);
        }
        return appConfigProps;
        
    }
    
    /**
     * Get all the application configurations set on a component.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a map of all the application configurations keyed by the
     *         configuration name.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getApplicationConfigurations(java.lang.String,
     *      java.lang.String)
     */
    public Map<String, Properties> getApplicationConfigurations(
            String componentName, String targetName)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        TabularData td = null;
        Map<String, Properties> appConfigMap = new HashMap<String, Properties>();
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("getApplicationConfigurations(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                Object result = this.getAttributeValue(configFacadeMBean,
                        "ApplicationConfigurations");
                if (result != null) {
                    td = (TabularData) result;
                    
                    // Convert the application configuration Tabular Data to a
                    // Map
                    appConfigMap = getApplicationConfigurationsMap(td);
                }
                return appConfigMap;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("getApplicationConfigurations caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("getApplicationConfigurations  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.get.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        
    }
    
    /**
     * Get all the application variables set on a component.
     * 
     * @return all the application variables et on the component. The return
     *         proerties set has the name="[type]value" pairs for the
     *         application variables.
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getApplicationVariables(java.lang.String,
     *      java.lang.String)
     */
    public Properties getApplicationVariables(String componentName,
            String targetName) throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.vars.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        Properties appVarProps = new Properties();
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("getApplicationVariables(" + componentName + "," + targetName
                + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                TabularData appVarTable = (TabularData) this.getAttributeValue(
                        configFacadeMBean, "ApplicationVariables");
                
                appVarProps = this.componentConfigurationHelper
                        .convertToApplicationVariablesProperties(appVarTable);
                logDebug("getApplicationVariables(): result = " + appVarProps);
            } catch (ManagementRemoteException jbiRE) {
                // exception msg is a management message already; just keep it
                // as it is
                logDebug("caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[]{componentName, targetName};
                Exception exception = this.createManagementException(
                        "ui.mbean.component.get.app.vars.error",
                        args, ex);
                ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
                throw remoteException;
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return appVarProps;
    }
    
    /**
     * Retrieve component configuration
     * 
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as properties
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentConfiguration(java.lang.String,
     *      java.lang.String)
     */
    public Properties getComponentConfiguration(String componentName,
            String targetName) throws ManagementRemoteException {
        Properties result = null;
        TargetType targetType = checkTargetType(targetName);
        Exception exception = null;
        String[] args = { targetName };
        
        switch (targetType) {
            case STANDALONE_SERVER:
            case CLUSTER:
            case CLUSTERED_SERVER:
                // use facade mbean code below
                break;
            
            case DOMAIN:
            case INVALID_TARGET:
            default:
                logDebug("getComponentConfiguration(): target " + targetName
                        + " type not supported.");
                exception = this.createManagementException(
                        "ui.mbean.schemaorg_apache_xmlbeans.system.config.target.type.not.supported",
                        args, null);
                throw new ManagementRemoteException(exception);
        }
        
        targetUpCheck(targetName);
        componentInstalledOnTargetCheck(componentName, targetName);
        componentStartedOnTargetCheck(componentName, targetName);
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        
        logDebug("getComponentConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                result = this
                        .getConfigurationAttributeValues(configFacadeMBean);
                logDebug("getComponentConfiguration: result properties = "
                        + result);
            } catch (ManagementRemoteException jbiRE) {
                // exception msg is a management message already; just keep it
                // as it is
                logDebug("caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("caught non-ManagementRemoteException:");
                logDebug(ex);
                exception = this.createManagementException(
                        "ui.mbean.install.config.mbean.error.get.attrs.error",
                        null, ex);
                ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
                throw remoteException;
            }
            
        }
        return result;
    }
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentExtensionMBeanObjectNames(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, ObjectName[]> getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName)
            throws ManagementRemoteException {
        return super.getComponentExtensionMBeanObjectNames(componentName, extensionName, targetName);
    }
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return an array of ObjectName(s)
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentExtensionMBeanObjectNames(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public ObjectName[] getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName,
            String targetInstanceName) throws ManagementRemoteException {
        return super.getComponentExtensionMBeanObjectNames(componentName, extensionName, targetName, targetInstanceName);
    }
    
    /**
     * Gets the component custom loggers and their levels
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerName to their log levels
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentLoggerLevels(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, Level> getComponentLoggerLevels(String componentName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        Map<String, Level> resultObject = new TreeMap<String, Level>();
        
        logDebug("Get Component Logger Levels ");
        
        boolean isTargetCluserInstance = false;
        
        if ( targetInstanceName != null )
        {
            isTargetCluserInstance = getEnvironmentContext().
                    getPlatformContext().isClusteredServer(targetInstanceName);
        }
        
        logDebug("Get Component Configuration MBean Name for component " + componentName);
        
        ObjectName extensionMBeanObjectName = null;

        if ( isTargetCluserInstance )
        {
            String cluster = getEnvironmentContext().
                getPlatformContext().getTargetName(targetInstanceName);
            extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                    componentName, cluster);
            this.checkForValidTarget(extensionMBeanObjectName, 
                    JBIAdminCommands.DOMAIN_TARGET_KEY);
        }
        else
        {
            extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                    componentName, targetName);
            this.checkForValidTarget(extensionMBeanObjectName, 
                    JBIAdminCommands.DOMAIN_TARGET_KEY);
        }
        
        logDebug("Calling getLoggerMBeanNames on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        // The following returns the Logger MBean registered by the Component on
        // the Target. This is the MBean which matches the ObjectName pattern :
        // com.sun.jbi:ComponentName=<component-name>,ControlType=Logger
        // 
        // getLoggerMBeanNames() : Map[String, ObjectName[]]
        // 
        Map<String, ObjectName[]> loggerMBeansMap = (Map<String, ObjectName[]>) this
                .invokeMBeanOperation(extensionMBeanObjectName,
                        "getLoggerMBeanNames");
        
        if ((loggerMBeansMap != null)
                && ((targetName != null) || (targetInstanceName != null))) 
        {
            // You now have the map of targetInstances to logger MBean names
            // For each of these get the log levels
            ObjectName[] filteredLoggerObjectNames = null;
            if (isTargetCluserInstance)
            {
                filteredLoggerObjectNames = loggerMBeansMap.get(targetInstanceName);
            }
            else
            {
                
                if ( this.getPlatformContext().isCluster(targetName) )
                {

                    // Target is a cluster, so getting the logger MBean for
                    // any instance in the map will do
                    Iterator  clusterItr = loggerMBeansMap.keySet().iterator();
                    String clusterInstance = (String) clusterItr.next();

                    filteredLoggerObjectNames = loggerMBeansMap.get(clusterInstance);
                }
                else
                {
                    filteredLoggerObjectNames = loggerMBeansMap.get(targetName);
                }
            }
            
            // for each logger object name, get the loggers for that component
            if ((filteredLoggerObjectNames != null)
                    && (filteredLoggerObjectNames.length > 0)) {
                
                for (int index = 0; index < filteredLoggerObjectNames.length; index++) {
                    try {
                        this.checkForValidTarget(
                                filteredLoggerObjectNames[index],
                                JBIAdminCommands.DOMAIN_TARGET_KEY);
                        String[] loggerNames;
                        String logLevel;
                        loggerNames = (String[]) this.invokeMBeanOperation(
                                filteredLoggerObjectNames[index],
                                "getLoggerNames");
                        for (int i = 0; i < loggerNames.length; i++) {
                            try {
                                Object[] args = new Object[1];
                                args[0] = loggerNames[i];
                                String[] signature = new String[1];
                                signature[0] = "java.lang.String";
                                logLevel = (String) this.invokeMBeanOperation(
                                        filteredLoggerObjectNames[index],
                                        "getLevel", args, signature);
                                resultObject.put(loggerNames[i], Level
                                        .parse(logLevel));
                            } catch (ManagementRemoteException exception) {
                                logDebug(exception);
                            } catch (RuntimeException exception) {
                                logDebug(exception);
                            }
                        }
                    } catch (ManagementRemoteException exception) {
                        logDebug(exception);
                    } catch (RuntimeException exception) {
                        logDebug(exception);
                    }
                }
            }
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.extension.getComponentLoggerLevels.error", null,
                    null);
            throw new ManagementRemoteException(exception);
        }
        
        return resultObject;
    }
    
    /**
     * Gets the component custom loggers and their display names.
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerName to their display names
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentLoggerDisplayNames(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> getComponentLoggerDisplayNames(String componentName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        Map<String, String> resultObject = new TreeMap<String, String>();
        
        logDebug("Get Component Logger Display Names ");
        
        boolean isTargetCluserInstance = false;
        
        if ( targetInstanceName != null )
        {
            isTargetCluserInstance = getEnvironmentContext().
                    getPlatformContext().isClusteredServer(targetInstanceName);
        }
        
        logDebug("Get Component Configuration MBean Name for component " + componentName);
        
        ObjectName extensionMBeanObjectName = null;

        if ( isTargetCluserInstance )
        {
            String cluster = getEnvironmentContext().
                getPlatformContext().getTargetName(targetInstanceName);
                extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                        componentName, cluster);
                this.checkForValidTarget(extensionMBeanObjectName, cluster);
        }
        else
        {
                extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                        componentName, targetName);
                this.checkForValidTarget(extensionMBeanObjectName, targetName);
        }
        
        logDebug("Calling getLoggerMBeanNames on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        // The following returns the Logger MBean registered by the Component on
        // the Target. This is the MBean which matches the ObjectName pattern :
        // com.sun.jbi:ComponentName=<component-name>,ControlType=Logger
        // 
        // getLoggerMBeanNames() : Map[String, ObjectName[]]
        // 
        Map<String, ObjectName[]> loggerMBeansMap = (Map<String, ObjectName[]>) this
                .invokeMBeanOperation(extensionMBeanObjectName,
                        "getLoggerMBeanNames");
        
        if ((loggerMBeansMap != null)
                && ((targetName != null) || (targetInstanceName != null))) {
            // You now have the map of targetInstances to logger MBean names
            // For each of these get the display names
            ObjectName[] filteredLoggerObjectNames = null;
            if (isTargetCluserInstance) 
            {
                filteredLoggerObjectNames = loggerMBeansMap
                        .get(targetInstanceName);
            } else {
                if ( this.getPlatformContext().isCluster(targetName) )
                {
                    // Target is a cluster, so getting the logger MBean for
                    // any instance in the map will do
                    Iterator  clusterItr = loggerMBeansMap.keySet().iterator();
                    String clusterInstance = (String) clusterItr.next();
                    
                    filteredLoggerObjectNames = loggerMBeansMap.get(clusterInstance);
                }
                else
                {
                    filteredLoggerObjectNames = loggerMBeansMap.get(targetName);
                }
            }
            
            // for each logger object name, get the loggers for that component
            if ((filteredLoggerObjectNames != null)
                    && (filteredLoggerObjectNames.length > 0)) {
                
                for (int index = 0; index < filteredLoggerObjectNames.length; index++) {
                    try {
                        this.checkForValidTarget(
                                filteredLoggerObjectNames[index],
                                JBIAdminCommands.DOMAIN_TARGET_KEY);
                        String[] loggerNames;
                        String displayName;
                        loggerNames = (String[]) this.invokeMBeanOperation(
                                filteredLoggerObjectNames[index],
                                "getLoggerNames");
                        for (int i = 0; i < loggerNames.length; i++) {
                            try {
                                Object[] args = new Object[1];
                                args[0] = loggerNames[i];
                                String[] signature = new String[1];
                                signature[0] = "java.lang.String";
                                displayName = (String) this.invokeMBeanOperation(
                                        filteredLoggerObjectNames[index],
                                        "getDisplayName", args, signature);
                                resultObject.put(loggerNames[i], displayName);
                            } catch (ManagementRemoteException exception) {
                                logDebug(exception);
                            } catch (RuntimeException exception) {
                                logDebug(exception);
                            }
                        }
                    } catch (ManagementRemoteException exception) {
                        logDebug(exception);
                    } catch (RuntimeException exception) {
                        logDebug(exception);
                    }
                }
            }
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.extension.getComponentLoggerDisplayNames.error",
                    null, null);
            throw new ManagementRemoteException(exception);
        }
        
        return resultObject;
    }
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Properties that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getDefaultRuntimeConfiguration()
     */
    public Properties getDefaultRuntimeConfiguration()
            throws ManagementRemoteException {
        return getDefaultRuntimeConfigurationInternal(JBIAdminCommands.DOMAIN_TARGET_KEY);
    }
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Properties that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeConfiguration(java.lang.String)
     */
    public Properties getRuntimeConfiguration(String targetName)
            throws ManagementRemoteException {
        return getRuntimeConfigurationInternal(targetName);
    }
    
    /**
     * This method returns the runtime configuration metadata associated with
     * the specified property. The metadata contain name-value pairs like:
     * default, descriptionID, descriptorType, displayName, displayNameId,
     * isStatic, name, resourceBundleName, tooltip, tooltipId, etc.
     * 
     * @param propertyKeyName
     * @return Properties that represent runtime configuration metadata
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeConfigurationMetaData(java.lang.String)
     */
    public Properties getRuntimeConfigurationMetaData(String propertyKeyName)
            throws ManagementRemoteException {
        Map<String /* propertyKeyName */, Properties> metadata = null;
        metadata = getRuntimeConfigurationMetadataInternal(JBIAdminCommands.DOMAIN_TARGET_KEY);
        return metadata.get(propertyKeyName);
    }
    
    /**
     * Lookup the level of one runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return the matching Log Level.
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeLoggerLevel(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public Level getRuntimeLoggerLevel(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        return getRuntimeLoggerLevel(runtimeLoggerName, targetName);
    }
    
    /**
     * Gets all the runtime loggers and their levels
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of runtimeLoggerName to their log levels
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeLoggerLevels(java.lang.String,
     *      java.lang.String)
     */
    public Map<String /* runtimeLoggerName */, Level /* logLevel */> getRuntimeLoggerLevels(
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        return getRuntimeLoggerLevels(targetName);
    }
    
    /**
     * Gets all the runtime loggers and their display names
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of display names to their logger MBeans
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        return getRuntimeLoggerNames(targetName);
    }
    
    /**
     * Gets all the runtime loggers and their levels.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of runtimeLoggerName to their log levels
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    public Map<String /* runtimeLoggerName */, Level /* logLevel */> getRuntimeLoggerLevels(
            String targetName)
            throws ManagementRemoteException 
    {
        Map<String, Level> resultObject = new TreeMap<String, Level>();
        
        logDebug("getRuntimeLoggerLevels: targetName: " + targetName);
        
        TargetType type = checkTargetType(targetName);
        String[] args = { targetName };
        
        switch (type) 
        {
            case CLUSTERED_SERVER:
            case STANDALONE_SERVER:
                try {
                    if ( !this.getPlatformContext().isInstanceUp(targetName) )
                    {
                        Exception exception = this.createManagementException(
                                "ui.mbean.list.rt.logger.levels.instance.down.error",
                                args, null);
                        throw new ManagementRemoteException(exception);
                    }
                    
                    // call getSystemLoggerMBeans to get list of all schemaorg_apache_xmlbeans.system loggers
                    ObjectName[] loggers = getSystemLoggerMBeans(targetName);
                    
                    logDebug("found the following " + loggers.length + " loggers: " + loggers);
                    for (int index = 0; index < loggers.length; index++) {
                        String loggerName = null;
                        String logLevelString = null;
                        try {
                            logDebug("loggers[" + index + "]: ObjectName = " + loggers[index]);
                            loggerName = (String) this.invokeMBeanOperation(loggers[index],
                                "getLoggerName");
                            logDebug("loggers[" + index + "]: name = " + loggerName);
                            
                            logLevelString = (String) this.invokeMBeanOperation(loggers[index],
                                            "getLogLevel");
                            logDebug("level = " + logLevelString);
                            this.checkForValidTarget(loggers[index],
                                    JBIAdminCommands.DOMAIN_TARGET_KEY);
                            resultObject.put(loggerName, Level.parse(logLevelString));
                        } catch (ManagementRemoteException exception) {
                            logDebug("ManagementRemoteException");
                            logDebug(exception);
                        } catch (RuntimeException exception) {
                            logDebug("RuntimeException");
                            logDebug(exception);
                        }
                    }
                } catch (Exception ex) {
                    throw ManagementRemoteException.filterJmxExceptions(ex);
                }
                
                break;
            
            case INVALID_TARGET:

                logDebug ("getRuntimeLoggerLevels(): target " + targetName +
                          " type not supported.");
                Exception exception = this.createManagementException(
                                "ui.mbean.invalid.target.error",
                                args, null);
                throw new ManagementRemoteException(exception);
                
            default:
                // Get all the attributes on the config facade MBean
                ObjectName loggerCfgFacade = 
                            getRuntimeLoggerConfigFacadeMBeanName(targetName);
                
                Properties attribs = getConfigurationAttributeValues(loggerCfgFacade);
                
                for ( java.util.Iterator itr = attribs.keySet().iterator(); itr.hasNext(); )
                {
                    String loggerName = (String) itr.next();
                    String level = (String) attribs.get(loggerName);
                    Level loggerValue = null;
                    if ( level == null || 
                         "null".equalsIgnoreCase(level) || 
                         LOG_LEVEL_DEFAULT.equalsIgnoreCase(level))
                    { 
                        loggerValue = null;
                    }
                    else
                    {
                        loggerValue = Level.parse(level);
                    }
                    resultObject.put(loggerName, loggerValue);
                }
        }
        
        return resultObject;
    }
    
    /**
     * Lookup the level of one runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return the matching Log Level.
     * @throws ManagementRemoteException
     *             on error
     */
    public Level getRuntimeLoggerLevel(
            String runtimeLoggerName,
            String targetName)
            throws ManagementRemoteException {

        
        Map<String, Level> loggers = this.getRuntimeLoggerLevels(targetName);
        
        if ( loggers.containsKey(runtimeLoggerName) )
        {
            Level returnLevel = (Level) loggers.get(runtimeLoggerName);
            return returnLevel;
        }
        Exception exception = this.createManagementException(
                "ui.mbean.logger.id.does.not.exist", new String[]{runtimeLoggerName}, null);
        throw new ManagementRemoteException(exception);
    }
    
    /**
     * Gets all the runtime loggers and their display names
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of display names to their logger MBeans
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
             String targetName)
            throws ManagementRemoteException {
        Map<String, String> resultObject = new TreeMap<String, String>();
        logDebug("getRuntimeLoggerNames: targetName: " + targetName);
        
        TargetType type = checkTargetType(targetName);
        String[] args = { targetName };
        
        switch (type) 
        {
            case CLUSTERED_SERVER:
                try 
                {
                    if ( !this.getPlatformContext().isInstanceUp(targetName) )
                    {
                        Exception exception = this.createManagementException(
                                "ui.mbean.target.down",
                                args, null);
                        throw new ManagementRemoteException(exception);
                    }
                    

                    // call getSystemLoggerMBeans to get list of all schemaorg_apache_xmlbeans.system loggers
                    ObjectName[] loggers = getSystemLoggerMBeans(targetName);
                    
                    logDebug("found the following " + loggers.length + " loggers: " + loggers);
                    for (int index = 0; index < loggers.length; index++) {
                        String loggerDisplayName = null;
                        String loggerName = null;
                        try {
                            logDebug("loggers[" + index + "]: ObjectName = " + loggers[index]);
                            loggerDisplayName = (String) this.invokeMBeanOperation(loggers[index],
                                            "getDisplayName");
                            loggerName = (String) this.invokeMBeanOperation(loggers[index],
                                "getLoggerName");
                            logDebug("loggers[" + index + "]: name = " + loggerName
                                     + ", display name = " + loggerDisplayName);
                            this.checkForValidTarget(loggers[index],
                                    JBIAdminCommands.DOMAIN_TARGET_KEY);
                            resultObject.put(loggerDisplayName, loggerName);
                        } catch (ManagementRemoteException exception) {
                            logDebug("ManagementRemoteException");
                            logDebug(exception);
                        } catch (RuntimeException exception) {
                            logDebug("RuntimeException");
                            logDebug(exception);
                        }
                    }
                } catch (Exception ex) {
                    throw ManagementRemoteException.filterJmxExceptions(ex);
                }
                break;
            
            case INVALID_TARGET:
                logDebug ("getRuntimeLoggerNames(): target " + targetName +
                          " type not supported.");
                Exception exception = this.createManagementException(
                                "ui.mbean.invalid.target.error",
                                args, null);
                throw new ManagementRemoteException(exception);
                
            default:
                // Get all the attributes on the config facade MBean
                ObjectName loggerCfgFacade = 
                            getRuntimeLoggerConfigFacadeMBeanName(targetName);
                
                Map<String, Descriptor> descrMap = getConfigurationDescriptors(loggerCfgFacade);
                
                for ( java.util.Iterator itr = descrMap.keySet().iterator(); itr.hasNext(); )
                {
                    String loggerName = (String) itr.next();
                    Descriptor descr = descrMap.get(loggerName);
                    String loggerDisplayName = 
                        (String) descr.getFieldValue("displayName");
                    resultObject.put(loggerDisplayName, loggerName);
                }
        }
        
        return resultObject;
    }
    
    /**
     * Return the display name for a runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return the display name for the given logger
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName)
            throws ManagementRemoteException {
        String resultString = null;
        logDebug ("getRuntimeLoggerDisplayName: loggerName: " + runtimeLoggerName
                 + " , targetName: " + targetName);
        
        TargetType type = checkTargetType(targetName);
        String[] args = { targetName };
        
        switch (type) 
        {
            case CLUSTERED_SERVER:
                try 
                {
                    if ( !this.getPlatformContext().isInstanceUp(targetName) )
                    {
                        Exception exception = this.createManagementException(
                                "ui.mbean.target.down",
                                args, null);
                        throw new ManagementRemoteException(exception);
                    }
                    
                    // call getSystemLoggerMBeans to get list of all schemaorg_apache_xmlbeans.system loggers
                    ObjectName[] loggers = getSystemLoggerMBeans(targetName);
                    
                    for (int index = 0; index < loggers.length; index++) {
                        String loggerName = null;
                        String loggerDisplayName = null;
                        try {
                            logDebug("loggers[" + index + "]: ObjectName = " + loggers[index]);
                            loggerDisplayName = (String) this.invokeMBeanOperation(loggers[index],
                                            "getDisplayName");
                            loggerName = (String) this.invokeMBeanOperation(loggers[index],
                                "getLoggerName");
                            logDebug("loggers[" + index + "]: " + loggerName + ", "
                                     + loggerDisplayName);
                            if (loggerName.equals(runtimeLoggerName))
                            {
                                logDebug("Found the matching logger.");
                                resultString = loggerDisplayName;
                            }
                        } catch (ManagementRemoteException exception) {
                            logDebug("ManagementRemoteException");
                            logDebug(exception);
                        } catch (RuntimeException exception) {
                            logDebug("RuntimeException");
                            logDebug(exception);
                        }
                    }
                    
                } catch (Exception ex) {
                    throw ManagementRemoteException.filterJmxExceptions(ex);
                }
                break;
            
            case INVALID_TARGET:
                logDebug ("getRuntimeLoggerName(): target " + targetName +
                          " type not supported.");
                Exception exception = this.createManagementException(
                                "ui.mbean.invalid.target.error",
                                args, null);
                throw new ManagementRemoteException(exception);
                
            default:
                // Get all the attributes on the config facade MBean
                ObjectName loggerCfgFacade = 
                            getRuntimeLoggerConfigFacadeMBeanName(targetName);
                
                Map<String, Descriptor> descrMap = getConfigurationDescriptors(loggerCfgFacade);
                
                for ( java.util.Iterator itr = descrMap.keySet().iterator(); itr.hasNext(); )
                {
                    
                    String loggerName = (String) itr.next();
                    if ( loggerName.equals(runtimeLoggerName))
                    {
                        Descriptor descr = descrMap.get(loggerName);
                        resultString = 
                            (String) descr.getFieldValue("displayName");
                    }
                }
        }
        
        if ( resultString == null )
        {
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.logger.id.does.not.exist",
                            new String[]{runtimeLoggerName}, null);
            throw new ManagementRemoteException(exception);
        }
        return resultString;
        
    }
    
    /**
     * checks if the server need to be restarted to apply the changes made to
     * some of the configuration parameters.
     * 
     * @return true if server need to be restarted for updated configuration to
     *         take effect. false if no server restart is needed.
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#isServerRestartRequired()
     */
    public boolean isServerRestartRequired() throws ManagementRemoteException {
        return false;
    }
    
    /**
     * List all the application configurations in a component.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return an array of names of all the application configurations.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#listApplicationConfigurationNames(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String[] listApplicationConfigurationNames(String componentName,
            String targetName) throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        Map<String, Properties> configMap = getApplicationConfigurations(
                componentName, targetName);
        
        Set configNamesSet = configMap.keySet();
        String[] configNames = new String[configNamesSet.size()];
        configNames = (String[]) configNamesSet.toArray(configNames);
        
        return configNames;
    }
    
    /**
     * Update a named application configuration in a component installed on a
     * given target.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            application configuration name
     * @param config
     *            application configuration represented as a set of properties.
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.util.Properties, java.lang.String)
     */
    public String setApplicationConfiguration(String componentName,
            String name, Properties config, String targetName)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("setApplicationConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "setApplicationConfiguration",
                        new Object[] { name, config }, new String[] {
                                "java.lang.String", "java.util.Properties" });
                
                logDebug("setApplicationConfiguration(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("setApplicationConfiguration caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("setApplicationConfiguration  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * Set application variables on a component installed on a given target. If
     * even a variable from the set has not been added to the component, this
     * operation fails.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariables -
     *            set of application variables to update. The values of the
     *            application variables have the application variable type and
     *            value and the format is "[type]value"
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not set the management message has a ERROR
     *         task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setApplicationVariables(java.lang.String,
     *      java.lang.String, java.util.Properties)
     */
    @SuppressWarnings("unchecked")
    public String setApplicationVariables(String componentName,
            Properties appVariables, String targetName)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("setApplicationVariables(" + componentName + "," + targetName
                + "): configMBean = " + configFacadeMBean);
        
        Map<String/* appVarName */, String /* JBI MgmtMsg result */> results = new HashMap<String/* appVarName */, String /*
                                                                                                                             * JBI
                                                                                                                             * MgmtMsg
                                                                                                                             * result
                                                                                                                             */>();
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            
            componentStartedOnTargetCheck(componentName, targetName);
            
            // For each application variable invoke setApplicationVariable() on
            // the
            // facade MBean. If the operation is not successful, get the
            // instance
            // details : the error task status msg and the exception info.
            //
            // Build a composite message which has all the details re. which
            // attribute failed to be set and why ( in case of cluster need the
            // instance details )
            Set appVarNames = appVariables.keySet();
            
            for (Iterator itr = appVarNames.iterator(); itr.hasNext();) 
            {
                String name = (String) itr.next();
                String typeAndValueStr = (String) appVariables.get(name);
                String result = null;
                try {
                    
                    String type = this.componentConfigurationHelper.
                            getAppVarType(typeAndValueStr, null);
                    String value = this.componentConfigurationHelper.
                            getAppVarValue(typeAndValueStr);
                    if (  type == null )
                    {
                        type = getExistingAppVarType(componentName,
                                    targetName, name); 
                    }
                 
                    CompositeData appVarCD = this.componentConfigurationHelper
                            .createApplicationVariableComposite(name,
                                    value, type);
                    result = (String) this
                            .invokeMBeanOperation(
                                    configFacadeMBean,
                                    "setApplicationVariable",
                                    new Object[] { name, appVarCD },
                                    new String[] { "java.lang.String",
                                            "javax.management.openmbean.CompositeData" });
                    
                    logDebug("setApplicationVariable(): result = " + result);
                } catch (ManagementRemoteException jbiRE) {
                    // exception msg is a management message already; just keep
                    // it as it is
                    logDebug("caught ManagementRemoteException:");
                    logDebug(jbiRE);
                    result = jbiRE.getMessage();
                    
                } catch (Exception ex) {
                    logDebug("caught non-ManagementRemoteException:");
                    logDebug(ex);
                    result = ex.getMessage();
                }
                results.put(name, result);
            }
            
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        JBIManagementMessage msg = JBIManagementMessage
                .createJBIManagementMessage("setApplicationVariables", results,
                        false);
        
        String strMsg = com.sun.jbi.ui.common.JBIResultXmlBuilder.getInstance()
                .createJbiResultXml(msg);
        
        return com.sun.jbi.ui.common.JBIResultXmlBuilder.getInstance()
                .createJbiResultXml(msg);
    }
    
    /**
     * Set the component static configuration. This operation uses the facade
     * component configuration MBean to set the component properties.
     * 
     * @param componentName
     * @param targetName
     * @param configurationValues
     * @return a management message indicating the status of the operation. If
     *         the operation fails a JBIException ( encapsulated in the
     *         ManagementRemoteException ) is thrown, the exception message is a
     *         management XML message and gives the failure details.
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setComponentConfiguration(java.lang.String,
     *      java.util.Properties, java.lang.String)
     */
    public String setComponentConfiguration(String componentName,
            Properties configurationValues, String targetName)
            throws ManagementRemoteException {
        String result = null;
        AttributeList list = null;
        TargetType targetType = checkTargetType(targetName);
        Exception exception = null;
        String[] args = { targetName };
        
        switch (targetType) {
            case STANDALONE_SERVER:
            case CLUSTER:
            case CLUSTERED_SERVER:
                // use facade mbean code below
                break;
            case DOMAIN:
            case INVALID_TARGET:
            default:
                logDebug("setComponentConfiguration(): target " + targetName
                        + " type not supported.");
                exception = this.createManagementException(
                        "ui.mbean.schemaorg_apache_xmlbeans.system.config.target.type.not.supported",
                        args, null);
                throw new ManagementRemoteException(exception);
        }
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("setComponentConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                list = this.constructMBeanAttributes(configFacadeMBean,
                        configurationValues);
                result = this.setMBeanConfigAttributes(configFacadeMBean, list);
                logDebug("setComponentConfiguration(): result = " + result);
            } catch (ManagementRemoteException jbiRE) {
                // exception msg is a management message already; just keep it
                // as it is
                logDebug("caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("caught non-ManagementRemoteException:");
                logDebug(ex);
                exception = this.createManagementException(
                        "ui.mbean.component.config.mbean.error.set.attrs.error",
                        null, ex);
                ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
                throw remoteException;
            }
        }
        else
        {
            logDebug("Component " + componentName + " does not exist.");
            exception = this.createManagementException(
                    "ui.mbean.component.id.does.not.exist",
                    new String [] { componentName }, null);
            ManagementRemoteException remoteException = new ManagementRemoteException(
                    exception);
            throw remoteException;
        }
    
        return result;
    }
    
    
    
    /**
     * Sets the component log level for a given logger
     * 
     * @param componentName
     *            name of the component
     * @param loggerName
     *            the component logger name
     * @param logLevel
     *            the level to set the logger; when null, reverts the logger to
     *            inherit its level from its parent logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setComponentLoggerLevel(java.lang.String,
     *      java.lang.String, java.util.logging.Level, java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public void setComponentLoggerLevel(String componentName,
            String loggerCustomName, Level logLevel, String targetName,
            String targetInstanceName) throws ManagementRemoteException {
        logDebug("Set Component Logger Level");
        
        boolean isTargetCluserInstance = false;
        
        if ( targetInstanceName != null )
        {
            isTargetCluserInstance = getEnvironmentContext().
                    getPlatformContext().isClusteredServer(targetInstanceName);
        }
        
        logDebug("Get Component Configuration MBean Name for component " + componentName);
        
        ObjectName extensionMBeanObjectName = null;

        if ( isTargetCluserInstance )
        {
            targetName = getEnvironmentContext().
                getPlatformContext().getTargetName(targetInstanceName);
        }
        componentInstalledOnTargetCheck(componentName, targetName);
        extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                    componentName, targetName);
        
        this.checkForValidTarget(extensionMBeanObjectName, 
                    JBIAdminCommands.DOMAIN_TARGET_KEY);
        
       
        
        logDebug("Calling getLoggerMBeanNames on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        // The following filters the Logger MBeans registered by the Component
        // on the target based on the log name. These are the MBeans which match
        // the ObjectName pattern:
        // com.sun.jbi:ComponentName=<component-name>,ControlType=Logger
        Map<String, ObjectName[]> loggerMBeansMap = (Map<String, ObjectName[]>) this
                .invokeMBeanOperation(extensionMBeanObjectName,
                        "getLoggerMBeanNames");
        
        if ((loggerMBeansMap != null)
                && ((targetName != null) || (targetInstanceName != null))) {
            // You now have the map of targetInstances to logger MBean names
            // For each of these get the log level for the logger
            ObjectName[] filteredLoggerObjectNames = null;
            if (isTargetCluserInstance) 
            {
                filteredLoggerObjectNames = loggerMBeansMap
                        .get(targetInstanceName);
                setLoggerLevelsForInstance(filteredLoggerObjectNames, loggerCustomName, logLevel);
            } 
            else 
            {
                if ( this.getPlatformContext().isCluster(targetName) )
                {

                    // Target is a cluster, so getting the logger MBean for
                    // any instance in the map will do
                    Iterator  clusterItr = loggerMBeansMap.keySet().iterator();
                    
                    while ( clusterItr.hasNext() )
                    {
                        String clusterInstance = (String) clusterItr.next();

                        filteredLoggerObjectNames = loggerMBeansMap.get(clusterInstance);
                        setLoggerLevelsForInstance(filteredLoggerObjectNames, loggerCustomName, logLevel);
                    }
                }
                else
                {
                    // Target = Standalone server
                    filteredLoggerObjectNames = loggerMBeansMap.get(targetName);
                    setLoggerLevelsForInstance(filteredLoggerObjectNames, loggerCustomName, logLevel);
                }
            }
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.extension.setComponentLoggerLevel.error", null,
                    null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /**
     * Invoke setLevel on each logger MBean in the passed array
     *
     * @param loggerObjectNames - Logger MBean name array
     * @param loggerCustomName - logger name
     * @param logLevel - log level to set
     */
    private void setLoggerLevelsForInstance(ObjectName[] loggerObjectNames, 
            String loggerCustomName, Level logLevel) 
            throws ManagementRemoteException
    {
        // for each logger name, set the log level
        if ((loggerObjectNames != null)
                && (loggerObjectNames.length > 0)) 
        {
            for (int index = 0; index < loggerObjectNames.length; index++) 
            {

                this.checkForValidTarget(
                        loggerObjectNames[index],
                        JBIAdminCommands.DOMAIN_TARGET_KEY);
                Object[] args = new Object[1];
                args[0] = loggerCustomName;
                String[] signature = new String[1];
                signature[0] = "java.lang.String";

                this.invokeMBeanOperation(
                        loggerObjectNames[index],
                        setLevel(logLevel), args, signature);
            }
        }
    }
    
    /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     * 
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     * 
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     * 
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     * 
     * @param Properties
     *            params Properties object that contains name/value pairs
     *            corresponding to the configuration parameters to be set on the
     *            runtime.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return true if server restart is required, false if not
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setRuntimeConfiguration(java.util.Properties,
     *      java.lang.String)
     */
    public boolean setRuntimeConfiguration(Properties parameters,
            String targetName) throws ManagementRemoteException {
        return setRuntimeConfigurationInternal(parameters, targetName);
    }
    
    /**
     * Sets the log level for a given runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setRuntimeLoggerLevel(java.lang.String,
     *      java.util.logging.Level, java.lang.String, java.lang.String)
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName, Level logLevel,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        setRuntimeLoggerLevel(runtimeLoggerName, logLevel, targetName);
    }
    
    /**
     * Sets the log level for a given runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    public void setRuntimeLoggerLevel(String runtimeLoggerName,
            Level logLevel, String targetName)
            throws ManagementRemoteException {
         
        logDebug("setRuntimeLoggerLevel(" + runtimeLoggerName + ")");
        TargetType type = checkTargetType(targetName);
        String[] args = { targetName };
        boolean updated = false;

        switch (type) 
        {
            case CLUSTERED_SERVER:
                try 
                {
                    if ( !this.getPlatformContext().isInstanceUp(targetName) )
                    {
                        Exception exception = this.createManagementException(
                                "ui.mbean.target.down",
                                args, null);
                    }
                    
                    // call getSystemLoggerMBeans to get list of all schemaorg_apache_xmlbeans.system loggers
                    ObjectName[] loggers = getSystemLoggerMBeans(targetName);
                    
                    for (int index = 0; index < loggers.length; index++) {
                        String loggerName = null;
                        try {
                            this.checkForValidTarget(loggers[index],
                                    JBIAdminCommands.DOMAIN_TARGET_KEY);
                            loggerName = (String) this.invokeMBeanOperation(loggers[index],
                                "getLoggerName");
                            logDebug("loggers[" + index + "]: name = " + loggerName);
                            if (loggerName.equals(runtimeLoggerName))
                            {
                                logDebug("Found the matching logger.");
                               this.invokeMBeanOperation(loggers[index], setLevel(logLevel));
                                updated = true;
                            }
                        } catch (ManagementRemoteException exception) {
                            logDebug("ManagementRemoteException");
                            logDebug(exception);
                        } catch (RuntimeException exception) {
                            logDebug("RuntimeException");
                            logDebug(exception);
                        }
                    }
                    
                } 
                catch (Exception ex) 
                {
                    throw ManagementRemoteException.filterJmxExceptions(ex);
                }
                break;
            
            case INVALID_TARGET:
                logDebug ("setRuntimeLoggerLevels(): target " + targetName +
                          " type not supported.");
                Exception exception = this.createManagementException(
                                "ui.mbean.invalid.target.error",
                                args, null);
                throw new ManagementRemoteException(exception);
                
            default:

                ObjectName loggerCfgFacade = 
                            getRuntimeLoggerConfigFacadeMBeanName(targetName);
                
                AttributeList attribList = new AttributeList();
                Attribute attrib = new Attribute(runtimeLoggerName, 
                        (null == logLevel ? "DEFAULT" : logLevel.getName()));                
                attribList.add(attrib);
                AttributeList updatedAttribs = 
                        (AttributeList) setMBeanAttributes(
                                            environmentContext.getMBeanServer(),
                                            loggerCfgFacade, 
                        attribList);
                if(updatedAttribs != null) {
                    for ( Object updatedObj : updatedAttribs )
                    {
                        Attribute updatedAttrib = (Attribute) updatedObj;
                        if ( updatedAttrib.getName().equals(attrib.getName()))
                        {
                            updated = true;
                        }
                    }
                }
                
        }
        if ( !updated )
        {
                Exception exception = this
                    .createManagementException(
                            "ui.mbean.logger.id.does.not.exist",
                            new String[]{runtimeLoggerName}, null);
            throw new ManagementRemoteException(exception);
        }
        
    }
    
    /**
     * Return the display name for a runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return the display name for the given logger
     * @throws ManagementRemoteException
     *             on error
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        return getRuntimeLoggerDisplayName(runtimeLoggerName, targetName);
    }
    
    /**
     * This method is used to verify if the application variables and
     * application configuration objects used in the given application are
     * available in JBI runtime in the specified target. Also this method
     * verifies if all necessary components are installed. If generateTemplates
     * is true templates for missing application variables and application
     * configurations are generated. A command script that uses the template
     * files to set configuration objects is generated.
     * 
     * @param applicationURL
     *            the URL for the application zip file
     * @param generateTemplates
     *            true if templates have to be generated
     * @param templateDir
     *            the dir to store the generated templates
     * @param includeDeployCommand
     *            true if the generated script should include deploy command
     * @param targetName
     *            the target on which the application has to be verified
     * @param clientSAFilePath
     *            the path to the SA in the client file schemaorg_apache_xmlbeans.system
     * 
     * @returns XML string corresponding to the verification report
     * 
     * CompositeType of verification report String - "ServiceAssemblyName",
     * String - "ServiceAssemblyDescription", Integer - "NumServiceUnits",
     * Boolean - "AllComponentsInstalled", String[] - "MissingComponentsList",
     * CompositeData[] - "EndpointInfo", String - "TemplateZIPID"
     * 
     * CompositeType of each EndpointInfo String - "EndpointName", String -
     * "ServiceUnitName", String - "ComponentName", String - "Status"
     * 
     * @throws ManagementRemoteException
     *             if the application could not be verified
     * 
     * Note: param templateDir is used between ant/cli and common client client
     * TemplateZIPID is used between common client server and common client
     * client
     * 
     * @see com.sun.esb.management.api.configuration.ConfigurationService#verifyApplication(java.lang.String,
     *      java.lang.String, boolean, java.lang.String, boolean)
     */
    public String verifyApplication(String applicationURL,
            boolean generateTemplates, String templateDir,
            boolean includeDeployCommand, String targetName,
            String clientSAFilePath)
            throws ManagementRemoteException {
        String result = null;
        JBIApplicationVerifier verifier = new JBIApplicationVerifier(
                this.environmentContext);
        CompositeData data = null;
        try {
            domainTargetCheck(targetName);
            data = verifier.verifyApplication(applicationURL, targetName,
                    generateTemplates, templateDir, includeDeployCommand,
                    clientSAFilePath);
        } catch (Exception ManagementRemoteException) {
            throw new ManagementRemoteException(ManagementRemoteException);
        }
        if (data != null) {
            ApplicationVerificationReport report = null;
            report = new ApplicationVerificationReport(data);
            try {
                result = ApplicationVerificationReportWriter.serialize(report);
            } catch (ParserConfigurationException e) {
                throw new ManagementRemoteException(e);
            } catch (TransformerException e) {
                throw new ManagementRemoteException(e);
            }
        }
        return result;
    }
    
    /**
     * Get the component configuration facade MBean name for the target. If the
     * component is not installed on the target, this returns a null.
     * 
     * @param componentName -
     *            component id
     * @param targetName -
     *            target name
     * @return the ObjectName of the components facade Configuration MBean for a
     *         target.
     */
    protected ObjectName getComponentConfigurationFacadeMBeanName(
            String componentName, String targetName)
            throws ManagementRemoteException 
    {
        domainTargetCheck(targetName);
        ObjectName objName = null;
        
        // if the target is a cluster instance get the Component Extension
        // MBean for the cluster and then get the config MBean for the clustered
        // instance
        boolean isTargetCluserInstance = getEnvironmentContext().
                    getPlatformContext().isClusteredServer(targetName);
        
        logDebug("Get Component Configuration MBean Name for component " + componentName);
        
        ObjectName extensionMBeanObjectName = null;

        if ( isTargetCluserInstance )
        {
            String cluster = getEnvironmentContext().
                getPlatformContext().getTargetName(targetName);
                extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                        componentName, cluster);
        }
        else
        {
                extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                        componentName, targetName);
            }
        
        if (!(isMBeanRegistered(extensionMBeanObjectName)) ) 
        {
            return objName;
        }
        
        logDebug("Calling getComponentConfigurationFacadeMBeanName on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        objName = (ObjectName) this.invokeMBeanOperation(
                extensionMBeanObjectName, "getComponentConfigurationFacadeMBeanName", targetName);
        
        return objName;
    }
    
    /**
     * Retrieve component configuration for a clustered server (that is up)
     * 
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as properties
     */
    protected Properties getComponentConfigurationForClusteredServer(
            String componentName, String targetName)
            throws ManagementRemoteException {
        Properties result = null;
        
        logDebug("Entering getComponentConfigurationForClusteredServer("
                + componentName + ", " + targetName + ")...");
        
        try {
            ObjectName configMBeanName = this.getConfigMBeanName(componentName,
                    targetName);
            
            javax.management.MBeanServerConnection mbns = this
                    .getPlatformContext().getMBeanServerConnection(targetName);
            
            result = this.getMBeanAttributeValues(mbns, configMBeanName);
            logDebug("getComponentConfigurationForClusteredServer: result properties = "
                    + result);
        } catch (ManagementRemoteException jbiRE) {
            // exception msg is a management message already; just keep it
            // as it is
            logDebug("caught ManagementRemoteException:");
            logDebug(jbiRE);
            throw jbiRE;
        } catch (Exception ex) {
            Exception exception = this.createManagementException(
                    "ui.mbean.install.config.mbean.error.get.attrs.error",
                    null, ex);
            ManagementRemoteException remoteException = new ManagementRemoteException(
                    exception);
            throw remoteException;
        }
        return result;
    }
    
    /**
     * Find the actual object config MBean Name -- used by both g/setting
     * component configuations for clustered servers
     * 
     * @param componentName
     * @param targetName
     * @return the actual component's config MBean name
     */
    @SuppressWarnings("unchecked")
    protected ObjectName getConfigMBeanName(String componentName,
            String targetName) throws ManagementRemoteException {
        
        logDebug("getConfigMBeanName(" + componentName + ", " + targetName
                + ")...");
        
        String clusterName = this.getPlatformContext()
                .getTargetName(targetName);
        ObjectName result = null;
        
        // Check if the component is installed on the cluster, if not then
        // its an error.
        if (false == this.isExistingComponent(componentName, clusterName)) {
            String[] args = { componentName };
            Exception exception = this.createManagementException(
                    "ui.mbean.component.id.does.not.exist", args, null);
            logDebug(exception);
            throw new ManagementRemoteException(exception);
        }
        
        // Check if the component is in the Started state on the cluster,
        // if not then you cannot configure it.
        String state = JBIComponentInfo.UNKNOWN_STATE;
        try {
            ObjectName lifeCycleMBeanObjectName = null;
            lifeCycleMBeanObjectName = this
                    .getComponentLifeCycleMBeanObjectName(componentName,
                            clusterName);
            state = (String) this.getMBeanAttribute(lifeCycleMBeanObjectName,
                    "CurrentState");
        } catch (ManagementRemoteException exception) {
            // do not rethrow it. The state is already defaulted to
            // JBIComponentInfo.UNKNOWN_STATE
            state = JBIComponentInfo.UNKNOWN_STATE;
        }
        if (true == RUNNING_STATE.equals(state)) {
            state = JBIComponentInfo.STARTED_STATE;
        }
        logDebug("state of component " + componentName + " on target "
                + targetName + " = " + state);
        if (!JBIComponentInfo.STARTED_STATE.equals(state)) {
            String[] args = { componentName };
            Exception exception = this.createManagementException(
                    "ui.mbean.component.id.not.started.state", args, null);
            logDebug(exception);
            throw new ManagementRemoteException(exception);
        }
        
        try {
            javax.management.MBeanServerConnection mbns = this
                    .getPlatformContext().getMBeanServerConnection(targetName);
            
            // first, look for "standard naming convention" MBean
            // com.sun.jbi:ControlType=Configuration,ComponentName=___
            Map<String, ObjectName[]> clusterMBeans = new HashMap<String, ObjectName[]>();
            try {
                String target = this.getPlatformContext().getTargetName(
                        targetName);
                clusterMBeans = getComponentExtensionMBeanObjectNames(
                        componentName, "Configuration", target);
                
            } catch (Exception ex) {
                logDebug(ex);
            }
            
            ObjectName[] clusteredInstanceMBeans = clusterMBeans
                    .get(targetName);
            
            if (clusteredInstanceMBeans.length > 0) {
                if (clusteredInstanceMBeans.length > 1) {
                    logDebug("More than one component configuration MBean registered for component "
                            + componentName);
                }
                result = clusteredInstanceMBeans[0];
            } else {
                // search for the ebi name (existing jbi components) MBean
                // com.sun.ebi:ServiceType=Configuration,IdentificationName=___
                ObjectName ebiPattern = null;
                try {
                    ebiPattern = new ObjectName(
                            "com.sun.ebi:ServiceType=Configuration,IdentificationName="
                                    + componentName + ",*");
                } catch (Exception ex) {
                    logDebug(ex);
                }
                
                Set<ObjectName> ebiNames = mbns.queryNames(ebiPattern, null);
                
                if (!ebiNames.isEmpty()) {
                    if (ebiNames.size() > 1) {
                        logDebug("More than one MBean matches ebi pattern "
                                + ebiPattern + ": " + convertToString(ebiNames));
                    }
                    result = (ObjectName) ebiNames.iterator().next();
                }
            }
            if (result == null) {
                String[] args = { componentName, targetName };
                Exception exception = this.createManagementException(
                        "ui.mbean.component.cannot.find.config.mbean", args,
                        null);
                logDebug(exception);
                throw new ManagementRemoteException(exception);
            }
        } catch (ManagementRemoteException jbiRE) {
            // exception msg is a management message already; just keep it
            // as it is
            logDebug("caught ManagementRemoteException:");
            logDebug(jbiRE);
            throw jbiRE;
        } catch (Exception ex) {
            Exception exception = this.createManagementException(
                    "ui.mbean.install.config.mbean.error.get.attrs.error",
                    null, ex);
            ManagementRemoteException remoteException = new ManagementRemoteException(
                    exception);
            throw remoteException;
        }
        logDebug("getConfigMBeanName() returning " + result);
        return result;
    }
    
    /**
     * Convert the objects in the collection to a printable string.
     * 
     * @param set
     *            a non-empty set
     */
    @SuppressWarnings("unchecked")
    protected String convertToString(Collection colxn) {
        StringBuffer strBuf = new StringBuffer("[ ");
        if (!colxn.isEmpty()) {
            java.util.Iterator itr = colxn.iterator();
            while (itr.hasNext()) {
                strBuf.append(itr.next().toString());
                if (itr.hasNext()) {
                    strBuf.append(",  ");
                } else {
                    strBuf.append("  ");
                }
            }
        }
        strBuf.append(" ]");
        return strBuf.toString();
    }
    
    /**
     * returns the ObjectName for the Extension Mbean of this component.
     * 
     * @param componentName
     * @param targetName
     * 
     * @return the ObjectName of the Extension MBean or null.
     */
    @SuppressWarnings("unchecked")
    protected ObjectName getExtensionMBeanObjectName(String componentName,
            String targetName) throws ManagementRemoteException {
        return super.getExtensionMBeanObjectName(componentName, targetName);
    }
    
    /**
     * Get the config extension MBean attribute values
     * 
     * @param objectName
     * @return
     * @throws ManagementRemoteException
     */
    protected Properties getConfigurationAttributeValues(ObjectName objectName)
            throws ManagementRemoteException {
        Properties properties = new Properties();
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        try {
            properties = this.getMBeanAttributeValues(mbeanServer, objectName);
        } catch (ManagementRemoteException exception) {
            properties = new Properties();
            JBIManagementMessage mgmtMsg = null;
            mgmtMsg = exception.extractJBIManagementMessage();
            properties.setProperty(COMPONENT_CONFIG_INSTANCE_ERROR_KEY, mgmtMsg
                    .getMessage());
            
        }
        
        return properties;
    }

    /**
     * Get the config extension MBean attribute values
     * 
     * @param objectName
     * @return
     * @throws ManagementRemoteException
     */
    protected Map<String /*attributeName*/, Object /*attributeValue*/> getConfigurationAttributeValuesAsMap(ObjectName objectName)
            throws ManagementRemoteException {
        Map<String /*attributeName*/, Object /*attributeValue*/> properties = null;
        properties = new HashMap<String /*attributeName*/, Object /*attributeValue*/>();
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        try {
            properties = this.getMBeanAttributeValuesAsMap(mbeanServer, objectName);
        } catch (ManagementRemoteException exception) {
            properties = new HashMap<String /*attributeName*/, Object /*attributeValue*/>();
            JBIManagementMessage mgmtMsg = null;
            mgmtMsg = exception.extractJBIManagementMessage();
            properties.put(COMPONENT_CONFIG_INSTANCE_ERROR_KEY, mgmtMsg
                    .getMessage());
            
        }
        
        return properties;
    }
    
    /**
     * Get the default JBI runtime configuration
     * 
     * @param targetName
     * @return
     * @throws ManagementRemoteException
     */
    protected Properties getDefaultRuntimeConfigurationInternal(
            String targetName) throws ManagementRemoteException {
        final String DEFAULT_VALUE_KEY = "default";
        Properties properties = new Properties();
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    if(fields != null) {
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                    && (value != null)) {
                                properties.setProperty(attributeName, value + "");
                            }
                        }
                    }
                }
            }
        }
        
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    if(fields != null) {
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                    && (value != null)) {
                                properties.setProperty(attributeName, value + "");
                            }
                        }
                    }
                }
            }
        }
        
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    if(fields != null) {
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                    && (value != null)) {
                                properties.setProperty(attributeName, value + "");
                            }
                        }
                    }
                }
            }
        }
        
        return properties;
    }
    
    /**
     * Get the JBI Runtime Configuration Service MBean Object Name
     * 
     * @param targetName
     * @param configurationType
     * @return
     * @throws ManagementRemoteException
     */
    public ObjectName getConfigurationMBeanObjectName(String targetName,
            String configurationType) throws ManagementRemoteException {
        ObjectName configurationMBeanObjectName = null;
        String objectNameString = JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON
                + JBIJMXObjectNames.TARGET_KEY + EQUAL + targetName + COMMA
                + JBIJMXObjectNames.SERVICE_NAME_KEY + EQUAL
                + JBIJMXObjectNames.CONFIGURATION_SERVICE + COMMA
                + JBIJMXObjectNames.SERVICE_TYPE_KEY + EQUAL
                + configurationType;
        
        try {
            configurationMBeanObjectName = new ObjectName(objectNameString);
        } catch (MalformedObjectNameException exception) {
            throw new ManagementRemoteException(exception);
        } catch (NullPointerException exception) {
            throw new ManagementRemoteException(exception);
        }
        return configurationMBeanObjectName;
    }
    
    /**
     * Get the JBI Runtime Configuration
     * 
     * @param targetName
     * @return
     * @throws ManagementRemoteException
     */
    Properties getRuntimeConfigurationInternal(String targetName)
            throws ManagementRemoteException {
        Properties properties = new Properties();
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";

        validateTargetForRuntimeConfigurationSpport(targetName);
        
        if (((TargetType) checkTargetType(targetName)) == TargetType.INVALID_TARGET)
        {
            logDebug("getRuntimeConfigurationInternal(): target " + targetName
                        + " type not supported.");
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                        "ui.mbean.schemaorg_apache_xmlbeans.system.config.target.type.not.supported",
                        args, null);
            throw new ManagementRemoteException(exception);
        }

        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(systemObjectName,
                        attributeName);
                properties.setProperty(attributeName, attributeValue + "");
            }
        }
        
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(
                        installationObjectName, attributeName);
                properties.setProperty(attributeName, attributeValue + "");
            }
        }
        
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(deploymentObjectName,
                        attributeName);
                properties.setProperty(attributeName, attributeValue + "");
            }
        }
        
        return properties;
    }
    
    /**
     * Get JBI Runtime Configuration Metadata
     * 
     * @param targetName
     * @return
     * @throws ManagementRemoteException
     */
    Map<String /* propertyKeyName */, Properties> getRuntimeConfigurationMetadataInternal(
            String targetName) throws ManagementRemoteException {
        Properties properties = null;
        Map<String /* propertyKeyName */, Properties> result = null;
        result = new HashMap<String /* propertyKeyName */, Properties>();
        
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeName != null) {
                    properties = new Properties();
                    if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                        Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                                .getDescriptor();
                        String[] fields = descriptor.getFieldNames();
                        if(fields != null) {
                            for (String fieldName : fields) {
                                Object value = descriptor.getFieldValue(fieldName);
                                if ((fieldName != null) && (value != null)) {
                                    properties.setProperty(fieldName, value + "");
                                }
                            }
                        }
                    }
                    result.put(attributeName, properties);
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeName != null) {
                    properties = new Properties();
                    if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                        Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                                .getDescriptor();
                        String[] fields = descriptor.getFieldNames();
                        if(fields != null) {
                            for (String fieldName : fields) {
                                Object value = descriptor.getFieldValue(fieldName);
                                if ((fieldName != null) && (value != null)) {
                                    properties.setProperty(fieldName, value + "");
                                }
                            }
                        }
                    }
                    result.put(attributeName, properties);
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeName != null) {
                    properties = new Properties();
                    if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                        Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                                .getDescriptor();
                        String[] fields = descriptor.getFieldNames();
                        if(fields != null) {
                            for (String fieldName : fields) {
                                Object value = descriptor.getFieldValue(fieldName);
                                if ((fieldName != null) && (value != null)) {
                                    properties.setProperty(fieldName, value + "");
                                }
                            }
                        }
                    }
                    result.put(attributeName, properties);
                }
            }
        }
        
        return result;
    }
    
    /**
     * returns the ObjectName for the LoggingService Mbean of this target.
     * 
     * @param targetName
     * 
     * @return the ObjectName of the LoggingService MBean or null.
     */
    public ObjectName getLoggingServiceMBeanObjectName(String targetName) {
        ObjectName loggingMBeanObjectName = null;
        String loggingString = JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON
                + MBeanNames.INSTANCE_NAME_KEY + EQUAL + targetName + COMMA
                + JBIJMXObjectNames.SERVICE_NAME_KEY + EQUAL
                + MBeanNames.SERVICE_NAME_LOGGING_SERVICE + COMMA
                + JBIJMXObjectNames.CONTROL_TYPE_KEY + EQUAL
                + MBeanNames.CONTROL_TYPE_LOGGING_SERVICE + COMMA
                + JBIJMXObjectNames.COMPONENT_TYPE_KEY + EQUAL
                + JBIJMXObjectNames.SYSTEM_COMPONENT_TYPE_VALUE;
        
        try {
            loggingMBeanObjectName = new ObjectName(loggingString);
        } catch (Exception exception) {
            logDebug(exception);
        }
        return loggingMBeanObjectName;
    }
    
    /**
     * Set component configuration for a clustered server (that is up)
     * 
     * @param componentName
     * @param targetName
     * @param configurationValues
     * @return a management message indicating the status of the operation. If
     *         the operation fails a JBIException ( encapsulated in the
     *         ManagementRemoteException ) is thrown, the exception message is a
     *         management XML message and gives the failure details.
     */
    String setComponentConfigurationForClusteredServer(String componentName,
            String targetName, Properties configurationValues)
            throws ManagementRemoteException {
        String result = null;
        AttributeList list = null;
        
        logDebug("Entering setComponentConfigurationForClusteredServer("
                + componentName + ", " + targetName + ")...");
        
        try {
            ObjectName configMBeanName = this.getConfigMBeanName(componentName,
                    targetName);
            
            javax.management.MBeanServerConnection mbns = this
                    .getPlatformContext().getMBeanServerConnection(targetName);
            
            list = this.constructMBeanAttributes(mbns, configMBeanName,
                    configurationValues);
            this.setMBeanAttributes(mbns, configMBeanName, list);
            
            // create success management message
            result = createManagementMessage("setComponentConfiguration", true,
                    "INFO", "ui.mbean.component.set.attribute.success",
                    new Object[] { componentName });
            logDebug("setComponentConfigurationForClusteredServer(): result = "
                    + result);
        } catch (ManagementRemoteException jbiRE) {
            logDebug("caught ManagementRemoteException:");
            logDebug(jbiRE);
            Exception jbiMgmtEx = createManagementException(
                    "ui.mbean.component.set.attribute.error", new String[] {
                            componentName, jbiRE.toString() }, jbiRE);
            
            throw new ManagementRemoteException(jbiMgmtEx);
        } catch (Exception ex) {
            Exception exception = this.createManagementException(
                    "ui.mbean.component.set.attribute.error", null, ex);
            ManagementRemoteException remoteException = new ManagementRemoteException(
                    exception);
            throw remoteException;
        }
        return result;
    }
    
    /**
     * Return the "verb" (the setXXX MBean operation) for the given log level
     * 
     * @param logLevel
     *            log level to set logger to
     * @return a MBean operation to set the logger to that level
     */
    protected String setLevel(Level logLevel) {
        if (null == logLevel) {
            return "setDefault";
        }
        if (Level.OFF.equals(logLevel)) {
            return "setOff";
        }
        if (Level.SEVERE.equals(logLevel)) {
            return "setSevere";
        }
        if (Level.WARNING.equals(logLevel)) {
            return "setWarning";
        }
        if (Level.INFO.equals(logLevel)) {
            return "setInfo";
        }
        if (Level.CONFIG.equals(logLevel)) {
            return "setConfig";
        }
        if (Level.FINE.equals(logLevel)) {
            return "setFine";
        }
        if (Level.FINER.equals(logLevel)) {
            return "setFiner";
        }
        if (Level.FINEST.equals(logLevel)) {
            return "setFinest";
        }
        if (Level.ALL.equals(logLevel)) {
            return "setAll";
        }
        // should never reach this statement
        return "setDefault";
    }
    
    /**
     * Set JBI Runtime Configuration
     * 
     * @param properties
     * @param targetName
     * @return
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    protected boolean setRuntimeConfigurationInternal(Properties properties,
            String targetName) throws ManagementRemoteException {
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        final String SYSTEM_TYPE = "System";
        final String INSTALLATION_TYPE = "Installation";
        final String DEPLOYMENT_TYPE = "Deployment";

        validateTargetForRuntimeConfigurationSpport(targetName);
        
        if (((TargetType) checkTargetType(targetName)) == TargetType.INVALID_TARGET)
        {
            logDebug("setRuntimeConfigurationInternal(): target " + targetName
                        + " type not supported.");
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                        "ui.mbean.schemaorg_apache_xmlbeans.system.config.target.type.not.supported",
                        args, null);
            throw new ManagementRemoteException(exception);
        }
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        Object attributeValueObject = null;
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        
        Map<String /* Name */, Object /* Type */> attributeNameTypeMap = new HashMap<String /* Name */, Object /* Type */>();
        
        Properties metaDataProperties = null;
        // Validate to ensure there is no poison data
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
            }
        }
        
        // Make sure the properties does not have bogus key values
        // passed in.
        Set keySet = properties.keySet();
        Set attributeSet = attributeNameTypeMap.keySet();
        
        if(keySet != null) {
            for (Object keyObject : keySet) {
                String key = (String) keyObject;
                if (attributeSet.contains(key) == false) {
                    // throw an exception
                    String[] args = { key };
                    Exception exception = this
                            .createManagementException(
                                    "ui.mbean.runtime.config.mbean.attrib.key.invalid.error",
                                    args, null);
                    throw new ManagementRemoteException(exception);
                }
            }
        }
        
        // Retrieve runtime configuration metadata
        final String IS_STATIC_KEY = "isStatic";
        boolean isRestartRequired = false;
        Map<String /* propertyKeyName */, Properties> metadata = null;
        metadata = getRuntimeConfigurationMetadataInternal(targetName);
        
        // Set Values
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                    this.setAttributeValue(systemObjectName, attributeName,
                            attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                    this.setAttributeValue(installationObjectName,
                            attributeName, attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                String stringValue = properties.getProperty(attributeName);
                if (stringValue != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeValueObject = Util.newInstance(type,
                                stringValue);
                    } catch (Exception ex) {
                        String[] args = { stringValue, type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                    this.setAttributeValue(deploymentObjectName, attributeName,
                            attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        return isRestartRequired;
    }
    
    /**
     * Convert a application configuration TabularData to a Map keyed by the
     * "configurationName". The value is the application configuration
     * represented as properties.
     * 
     * @return a Map of application configuration properties
     */
    @SuppressWarnings("unchecked")
    protected Map<String, Properties> getApplicationConfigurationsMap(
            TabularData td) {
        Map<String, Properties> configMap = new HashMap();
        Set configKeys = td.keySet();
        
        if(configKeys != null) {
            for (Object configKey : configKeys) {
                List keyList = (List) configKey;
                
                String[] index = new String[keyList.size()];
                index = (String[]) keyList.toArray(index);
                CompositeData cd = td.get(index);
                
                Properties configProps = this.componentConfigurationHelper
                        .convertCompositeDataToProperties(cd);
                configMap.put(index[0], configProps);
            }
        }
        return configMap;
    }
    
    /**
     * Retrieves the component specific configuration schema.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a String containing the configuration schema.
     * @throws ManagementRemoteException
     *             on errors.
     */
    public String retrieveConfigurationDisplaySchema(String componentName,
            String targetName) throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        String schema = "";
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("retrieveConfigurationDisplaySchema(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                Object[] params = new Object[] { componentName, targetName };
                
                String[] signature = new String[] { "java.lang.String",
                        "java.lang.String" };
                
                schema = (String) this
                        .invokeMBeanOperation(configFacadeMBean,
                                "retrieveConfigurationDisplaySchema", params,
                                signature);
            } catch (ManagementRemoteException jbiRE) {
                logDebug("retrieveConfigurationDisplaySchema caught ManagementRemoteException:");
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("retrieveConfigurationDisplaySchema caught non-ManagementRemoteException:");
                
                throw new ManagementRemoteException(ex);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return schema;
    }
    
    /**
     * Retrieves the component configuration metadata. The XML data conforms to
     * the component configuration schema.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a String containing the configuration metadata.
     * @throws ManagementRemoteException
     *             on errors
     */
    public String retrieveConfigurationDisplayData(String componentName,
            String targetName) throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        String data = "";
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("retrieveConfigurationDisplayData(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                Object[] params = new Object[] { componentName, targetName };
                
                String[] signature = new String[] { "java.lang.String",
                        "java.lang.String" };
                
                data = (String) this.invokeMBeanOperation(configFacadeMBean,
                        "retrieveConfigurationDisplayData", params, signature);
            } catch (ManagementRemoteException jbiRE) {
                logDebug("retrieveConfigurationDisplayData caught ManagementRemoteException:");
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("retrieveConfigurationDisplayData caught non-ManagementRemoteException:");
                
                throw new ManagementRemoteException(ex);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return data;
    }
    
    /**
     * Add an application configuration. The configuration name is a part of the
     * CompositeData. The itemName for the configuration name is
     * "configurationName" and the type is SimpleType.STRING
     * 
     * @param componentName
     * @param targetName
     * @param name -
     *            configuration name, must match the value of the field "name"
     *            in the namedConfig
     * @param appConfig -
     *            application configuration composite
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if the application configuration cannot be added.
     */
    public String addApplicationConfiguration(String componentName,
            String targetName, String name, CompositeData appConfig)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("addApplicationConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "addApplicationConfiguration",
                        new Object[] { name, appConfig }, new String[] {
                                "java.lang.String",
                                "javax.management.openmbean.CompositeData" });
                
                logDebug("addApplicationConfiguration(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("addApplicationConfiguration caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("addApplicationConfiguration  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.add.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * Update a application configuration. The configuration name is a part of
     * the CompositeData. The itemName for the configuration name is
     * "configurationName" and the type is SimpleType.STRING
     * 
     * @param componentName
     * @param targetName
     * @param name -
     *            configuration name, must match the value of the field
     *            "configurationName" in the appConfig
     * @param appConfig -
     *            application configuration composite
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if there are errors encountered when updating the
     *             configuration.
     */
    public String setApplicationConfiguration(String componentName,
            String targetName, String name, CompositeData appConfig)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("setApplicationConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "setApplicationConfiguration",
                        new Object[] { name, appConfig }, new String[] {
                                "java.lang.String",
                                "javax.management.openmbean.CompositeData" });
                
                logDebug("setApplicationConfiguration(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("setApplicationConfiguration caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("setApplicationConfiguration  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * Get a Map of all application configurations for the component.
     * 
     * @param componentName
     * @param targetName
     * @return a TabularData of all the application configurations for a
     *         component keyed by the configuration name.
     * @throws ManagementRemoteException
     *             if there are errors encountered when updating the
     *             configuration.
     */
    public TabularData getApplicationConfigurationsAsTabularData(
            String componentName, String targetName)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppConfigSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.config.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        TabularData td = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("getApplicationConfigurations(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                Object result = this.getAttributeValue(configFacadeMBean,
                        "ApplicationConfigurations");
                if (result != null) {
                    td = (TabularData) result;
                }
                return td;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("getApplicationConfigurations caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("getApplicationConfigurations  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.get.app.config.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * Get the Application Variable set for a component.
     * 
     * @param componentName
     * @param targetName
     * @return a TabularData which has all the application variables set on the
     *         component.
     * @throws ManagementRemoteException
     */
    public TabularData getApplicationVariablesAsTabularData(
            String componentName, String targetName)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        TabularData appVarTable = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("getApplicationVariables(" + componentName + "," + targetName
                + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                appVarTable = (TabularData) this.getAttributeValue(
                        configFacadeMBean, "ApplicationVariables");
                
                logDebug("getApplicationVariables(): result = " + appVarTable);
            } catch (ManagementRemoteException jbiRE) {
                // exception msg is a management message already; just keep it
                // as it is
                logDebug("caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[]{componentName, targetName};
                Exception exception = this.createManagementException(
                        "ui.mbean.component.get.app.vars.error",
                        null, ex);
                ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
                throw remoteException;
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
        return appVarTable;
    }
    
    /**
     * This operation adds a new application variable. If a variable already
     * exists with the same name as that specified then the operation fails.
     * 
     * @param componentName
     * @param targetName
     * @param name -
     *            name of the application variable
     * @param appVar -
     *            this is the application variable composite
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if an error occurs in adding the application variables to the
     *             component.
     */
    public String addApplicationVariable(String componentName,
            String targetName, String name, CompositeData appVar)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("addApplicationVariable(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "addApplicationVariable",
                        new Object[] { name, appVar }, new String[] {
                                "java.lang.String",
                                "javax.management.openmbean.CompositeData" });
                
                logDebug("addApplicationVariable(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("addApplicationVariable caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("addApplicationVariable  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.add.app.vars.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param componentName
     * @param targetName
     * @param name - name of the application variable
     * @param appVar - this is the application variable composite to be updated.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     * @throws ManagementRemoteException if one or more application variables cannot be deleted
     */
    public String setApplicationVariable(String componentName,
            String targetName, String name, CompositeData appVar)
            throws ManagementRemoteException {
        domainTargetCheck(targetName);
        
        if (!isAppVarsSupported(componentName, targetName))
        {
            Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.not.supported",
                        new String [] { componentName,  targetName}, null);
            logDebug(mgmtEx.getMessage());
            throw new ManagementRemoteException(mgmtEx);
        }
        
        String jbiMgmtMsgResult = null;
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("setApplicationVariable(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                jbiMgmtMsgResult = (String) this.invokeMBeanOperation(
                        configFacadeMBean, "setApplicationVariable",
                        new Object[] { name, appVar }, new String[] {
                                "java.lang.String",
                                "javax.management.openmbean.CompositeData" });
                
                logDebug("setApplicationVariable(): result = "
                        + jbiMgmtMsgResult);
                return jbiMgmtMsgResult;
            } catch (ManagementRemoteException jbiRE) {
                logDebug("setApplicationVariable caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("setApplicationVariable  caught non-ManagementRemoteException:");
                logDebug(ex);
                String[] args = new String[] { name, componentName, targetName };
                Exception mgmtEx = this.createManagementException(
                        "ui.mbean.component.set.app.vars.error", args, ex);
                throw new ManagementRemoteException(mgmtEx);
            }
        } else {
            // -- Component Configuration Facade MBean not found -- error
            // component not
            // installed on target
            String[] args = new String[] { componentName, targetName };
            Exception mgmtEx = this.createManagementException(
                    "ui.mbean.component.configuration.mbean.not.found.error",
                    args, null);
            throw new ManagementRemoteException(mgmtEx);
        }
    }
    /////////////////
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getDefaultRuntimeConfigurationAsMap()
            throws ManagementRemoteException {
        return getDefaultRuntimeConfigurationAsMapInternal(JBIAdminCommands.DOMAIN_TARGET_KEY);
    }

    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getDefaultRuntimeConfigurationAsMapInternal(String targetName)
            throws ManagementRemoteException {
        final String DEFAULT_VALUE_KEY = "default";
        Map<String /*attributeName*/, Object /*attributeValue*/> properties = null;
        properties = new HashMap<String /*attributeName*/, Object /*attributeValue*/>();
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    if(fields != null) {
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                    && (value != null)) {
                                properties.put(attributeName, value);
                            }
                        }
                    }
                }
            }
        }
        
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    if(fields != null) {
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                    && (value != null)) {
                                properties.put(attributeName, value);
                            }
                        }
                    }
                }
            }
        }
        
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                if (attributeInfo instanceof ModelMBeanAttributeInfo) {
                    Descriptor descriptor = ((ModelMBeanAttributeInfo) attributeInfo)
                            .getDescriptor();
                    String[] fields = descriptor.getFieldNames();
                    if(fields != null) {
                        for (String fieldName : fields) {
                            Object value = descriptor.getFieldValue(fieldName);
                            if ((DEFAULT_VALUE_KEY.equals(fieldName) == true)
                                    && (value != null)) {
                                properties.put(attributeName, value);
                            }
                        }
                    }
                }
            }
        }
        
        return properties;
    }
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getRuntimeConfigurationAsMap(String targetName)
            throws ManagementRemoteException {
        Map<String /*attributeName*/, Object /*attributeValue*/> properties = null;
        properties = new HashMap<String /*attributeName*/, Object /*attributeValue*/>();
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        String SYSTEM_TYPE = "System";
        String INSTALLATION_TYPE = "Installation";
        String DEPLOYMENT_TYPE = "Deployment";

        validateTargetForRuntimeConfigurationSpport(targetName);
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(systemObjectName,
                        attributeName);
                properties.put(attributeName, attributeValue);
            }
        }
        
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(
                        installationObjectName, attributeName);
                properties.put(attributeName, attributeValue);
            }
        }
        
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                Object attributeValue = getAttributeValue(deploymentObjectName,
                        attributeName);
                properties.put(attributeName, attributeValue);
            }
        }
        
        return properties;
    }
            
   /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     * 
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     * 
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     * 
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     * 
     * @param params
     *            Map object that contains name/value pairs corresponding
     *            to the configuration parameters to be set on the runtime.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return true if server restart is required, false if not
     * 
     * @throws ManagementRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     */
    @SuppressWarnings("unchecked")
    public boolean setRuntimeConfiguration(Map<String /*attributeName*/, Object /*attributeValue*/> properties,
            String targetName) throws ManagementRemoteException {
        ObjectName systemObjectName = null;
        ObjectName installationObjectName = null;
        ObjectName deploymentObjectName = null;
        
        final String SYSTEM_TYPE = "System";
        final String INSTALLATION_TYPE = "Installation";
        final String DEPLOYMENT_TYPE = "Deployment";

        validateTargetForRuntimeConfigurationSpport(targetName);
        
        systemObjectName = this.getConfigurationMBeanObjectName(targetName,
                SYSTEM_TYPE);
        installationObjectName = this.getConfigurationMBeanObjectName(
                targetName, INSTALLATION_TYPE);
        deploymentObjectName = this.getConfigurationMBeanObjectName(targetName,
                DEPLOYMENT_TYPE);
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        MBeanInfo mbeanSystemInfo = null;
        MBeanInfo mbeanInstallationInfo = null;
        MBeanInfo mbeanDeploymentInfo = null;
        
        try {
            mbeanSystemInfo = mbeanServer.getMBeanInfo(systemObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanInstallationInfo = mbeanServer
                    .getMBeanInfo(installationObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        try {
            mbeanDeploymentInfo = mbeanServer
                    .getMBeanInfo(deploymentObjectName);
        } catch (InstanceNotFoundException exception) {
            throw new ManagementRemoteException(exception);
        } catch (IntrospectionException exception) {
            throw new ManagementRemoteException(exception);
        } catch (ReflectionException exception) {
            throw new ManagementRemoteException(exception);
        }
        
        Object attributeValueObject = null;
        MBeanAttributeInfo[] systemAttributes = mbeanSystemInfo.getAttributes();
        MBeanAttributeInfo[] installationAttributes = mbeanInstallationInfo
                .getAttributes();
        MBeanAttributeInfo[] deploymentAttributes = mbeanDeploymentInfo
                .getAttributes();
        
        Map<String /* Name */, Object /* Type */> attributeNameTypeMap = new HashMap<String /* Name */, Object /* Type */>();
        
        Properties metaDataProperties = null;
        // Validate to ensure there is no poison data
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                attributeValueObject = properties.get(attributeName);
                if (attributeValueObject != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { attributeValueObject+"", type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                attributeValueObject = properties.get(attributeName);
                if (attributeValueObject != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { attributeValueObject+"", type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                attributeValueObject = properties.get(attributeName);
                if (attributeValueObject != null) {
                    String type = attributeInfo.getType();
                    try {
                        // construct the value object using reflection.
                        attributeNameTypeMap.put(attributeName,
                                attributeValueObject);
                    } catch (Exception ex) {
                        String[] args = { attributeValueObject+"", type, attributeName };
                        Exception exception = this
                                .createManagementException(
                                        "ui.mbean.install.config.mbean.attrib.type.convertion.error",
                                        args, ex);
                        throw new ManagementRemoteException(exception);
                        
                    }
                }
            }
        }
        
        // Make sure the properties does not have bogus key values
        // passed in.
        Set keySet = properties.keySet();
        Set attributeSet = attributeNameTypeMap.keySet();
        
        if(keySet != null) {
            for (Object keyObject : keySet) {
                String key = (String) keyObject;
                if (attributeSet.contains(key) == false) {
                    // throw an exception
                    String[] args = { key };
                    Exception exception = this
                            .createManagementException(
                                    "ui.mbean.runtime.config.mbean.attrib.key.invalid.error",
                                    args, null);
                    throw new ManagementRemoteException(exception);
                }
            }
        }
        
        // Retrieve runtime configuration metadata
        final String IS_STATIC_KEY = "isStatic";
        boolean isRestartRequired = false;
        Map<String /* propertyKeyName */, Properties> metadata = null;
        metadata = getRuntimeConfigurationMetadataInternal(targetName);
        
        // Set Values
        if (systemAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : systemAttributes) {
                String attributeName = attributeInfo.getName();
                attributeValueObject = properties.get(attributeName);
                if (attributeValueObject != null) {
                    this.setAttributeValue(systemObjectName, attributeName,
                            attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        if (installationAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : installationAttributes) {
                String attributeName = attributeInfo.getName();
                attributeValueObject = properties.get(attributeName);
                if (attributeValueObject != null) {
                    this.setAttributeValue(installationObjectName,
                            attributeName, attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        if (deploymentAttributes != null) {
            for (MBeanAttributeInfo attributeInfo : deploymentAttributes) {
                String attributeName = attributeInfo.getName();
                attributeValueObject = properties.get(attributeName);
                if (attributeValueObject != null) {
                    this.setAttributeValue(deploymentObjectName, attributeName,
                            attributeValueObject);
                    
                    metaDataProperties = metadata.get(attributeName);
                    if (true == Boolean.valueOf(metaDataProperties
                            .getProperty(IS_STATIC_KEY))) {
                        isRestartRequired = true;
                    }
                    
                }
            }
        }
        
        return isRestartRequired;
    }
            
            
    /**
     * Retrieve component configuration
     * 
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as Map
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /*attributeName*/, Object /*attributeValue*/> getComponentConfigurationAsMap(String componentName,
            String targetName) throws ManagementRemoteException {     
        Map<String /*attributeName*/, Object /*attributeValue*/> result = null;
        TargetType targetType = checkTargetType(targetName);
        Exception exception = null;
        String[] args = { targetName };
        
        switch (targetType) {
            case STANDALONE_SERVER:
            case CLUSTER:
            case CLUSTERED_SERVER:
                // use facade mbean code below
                break;
            
            case DOMAIN:
            case INVALID_TARGET:
            default:
                logDebug("getComponentConfiguration(): target " + targetName
                        + " type not supported.");
                exception = this.createManagementException(
                        "ui.mbean.schemaorg_apache_xmlbeans.system.config.target.type.not.supported",
                        args, null);
                throw new ManagementRemoteException(exception);
        }
        
        targetUpCheck(targetName);
        componentInstalledOnTargetCheck(componentName, targetName);
        componentStartedOnTargetCheck(componentName, targetName);
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        
        logDebug("getComponentConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                result = this
                        .getConfigurationAttributeValuesAsMap(configFacadeMBean);
                logDebug("getComponentConfiguration: result properties = "
                        + result);
            } catch (ManagementRemoteException jbiRE) {
                // exception msg is a management message already; just keep it
                // as it is
                logDebug("caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("caught non-ManagementRemoteException:");
                logDebug(ex);
                exception = this.createManagementException(
                        "ui.mbean.install.config.mbean.error.get.attrs.error",
                        null, ex);
                ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
                throw remoteException;
            }
            
        }
        return result;
    }
    
    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The entry per instance will have value as part of the
     * management message (XML) String.
     * 
     * @param componentName
     * @param configurationValues
     * @param targetName
     * @return value as part of the management message (XML) String.
     * @throws ManagementRemoteException
     *             on error
     */
    public String setComponentConfiguration(String componentName,
            Map<String /*attributeName*/, Object /*attributeValue*/> configurationValues, String targetName)
            throws ManagementRemoteException {
        String result = null;
        AttributeList list = null;
        TargetType targetType = checkTargetType(targetName);
        Exception exception = null;
        String[] args = { targetName };
        
        switch (targetType) {
            case STANDALONE_SERVER:
            case CLUSTER:
            case CLUSTERED_SERVER:
                // use facade mbean code below
                break;
            case DOMAIN:
            case INVALID_TARGET:
            default:
                logDebug("setComponentConfiguration(): target " + targetName
                        + " type not supported.");
                exception = this.createManagementException(
                        "ui.mbean.schemaorg_apache_xmlbeans.system.config.target.type.not.supported",
                        args, null);
                throw new ManagementRemoteException(exception);
        }
        
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName,
                        targetName);
        logDebug("setComponentConfiguration(" + componentName + ","
                + targetName + "): configMBean = " + configFacadeMBean);
        
        if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) {
            try {
                list = this.constructMBeanAttributes(configFacadeMBean,
                        configurationValues);
                result = this.setMBeanConfigAttributes(configFacadeMBean, list);
                logDebug("setComponentConfiguration(): result = " + result);
            } catch (ManagementRemoteException jbiRE) {
                // exception msg is a management message already; just keep it
                // as it is
                logDebug("caught ManagementRemoteException:");
                logDebug(jbiRE);
                throw jbiRE;
            } catch (Exception ex) {
                logDebug("caught non-ManagementRemoteException:");
                logDebug(ex);
                exception = this.createManagementException(
                        "ui.mbean.component.config.mbean.error.set.attrs.error",
                        null, ex);
                ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
                throw remoteException;
            }   
        }
        else
        {
             logDebug("Component " + componentName + " does not exist.");
             exception = this.createManagementException(
                        "ui.mbean.component.id.does.not.exist",
                        new String [] { componentName }, null);
             ManagementRemoteException remoteException = new ManagementRemoteException(
                        exception);
             throw remoteException;
        }
        return result;
    }
    

    /////////////////
    
    /**
     * Invokes an operation on an Extension MBean.
     * @param componentName
     * @param extensionName the name of the extension (e.g., Configuration, Logger, etc.)
     * @param operationName The name of the operation to be invoked.
     * @param parameters An array containing the parameters to be set when the operation is invoked
     * @param signature An array containing the signature of the operation. The class objects will be loaded using the same class loader as the one used for loading the MBean on which the operation was invoked.
     * @param targetName name of the target (e.g., server, Cluster1, StandloneServer2, etc.)
     * @param targetInstanceName name of the target instance (e.g., Cluster1-Instance1, Cluster2-Instance10, etc.)
     * @return The object returned by the operation, which represents the result of invoking the operation on the Extension MBean specified.
     * @throws ManagementRemoteException
     */
    public Object invokeExtensionMBeanOperation(String componentName, String extensionName, String operationName, Object[] parameters, String[] signature, String targetName, String targetInstanceName) throws ManagementRemoteException {
        Object result = null;
        ObjectName[] objectNames = null;
        boolean isClusteredServer = false;
        objectNames = this.getComponentExtensionMBeanObjectNames(componentName, extensionName, targetName, targetInstanceName);
        if(targetName != null) {
            isClusteredServer = this.getPlatformContext().isCluster(targetName);
        }
        if((isClusteredServer == true) && (targetInstanceName == null)) {
            // perform operation on all target instances
            if(objectNames != null) {
                for(ObjectName objectName : objectNames) {
                    result = this.invokeMBeanOperation(objectName, operationName, parameters, signature);
                }
            }
        } else {
            // perform operation on specified target or targetInstance
            String target = targetName;
            if(targetInstanceName != null) {
                target = targetInstanceName;
            }
            if(objectNames != null) {
                for(ObjectName objectName : objectNames) {
                    String currentTarget = objectName.getKeyProperty("JbiName");
                    if((currentTarget != null) && (currentTarget.equals(target) == true)) {
                        result = this.invokeMBeanOperation(objectName, operationName, parameters, signature);
                        break;
                    }
                }
            }
        }
        return result;
    }
    
    
    /**
     * @return the runtime logger configuration facade MBean Name for the 
     *         specific target.
     */
    protected ObjectName getRuntimeLoggerConfigFacadeMBeanName(String target)
    {       
        return environmentContext.getMBeanNames().
                    getSystemServiceMBeanName(
                MBeanNames.ServiceName.ConfigurationService,
                        MBeanNames.ServiceType.Logger,
                        target);
    }
    
    /**
     * @return the type of the actual application variable
     *
     */
    protected String getExistingAppVarType(String compName, String targetName,
                String appVarName)
    {
        String type = null;
        try
        {
            Properties appVars = getApplicationVariables(compName, targetName);
            
            if ( appVars.containsKey(appVarName) )
            {
                String typeAndValueStr = appVars.getProperty(appVarName);
                type = this.
                        componentConfigurationHelper.getAppVarType(typeAndValueStr, null);
            }
        }
        catch ( Exception ex ){}
        return type;
    }

    /**
     * validates targetName and throws exception if the target is a clustered instance.
     * For clustered instances, cluster name should be used for runtime configuration.
     * The supported targets are target=server, target=standalone-instance target=cluster
     * @param targetName name of the target intance.
     * @throws com.sun.esb.management.common.ManagementRemoteException
     */
    protected void validateTargetForRuntimeConfigurationSpport(String targetName) throws ManagementRemoteException {

        if (targetName != null &&
                !this.getPlatformContext().isCluster(targetName) &&
                this.getPlatformContext().isInstanceClustered(targetName)) {
            logDebug("Runtime configuration is managed at the cluster level. The target " + targetName
                        + " is a clustered instance. For clustered instances, use cluster name as target for runtime configuration.");
            String cluster = "";
            try {
                cluster = this.getPlatformContext().getTargetName(targetName);
            } catch (Exception ex) {
                //Ignore
                logDebug(ex);
            }
            String i18nKey = "ui.mbean.system.config.runtime.target.type.not.supported";
            String[] args = { targetName, cluster };
            Exception exception = this.createManagementException(i18nKey,args, null);
            throw new ManagementRemoteException(exception);
        }
    }
}
