/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIStatisticsMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.mbeans;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

import javax.management.ObjectName;
import javax.management.openmbean.ArrayType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.OpenDataException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;

import com.sun.jbi.ComponentType;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.common.JBIStatisticsItemNames;
import com.sun.jbi.ui.common.ToolsLogManager;
import com.sun.jbi.EnvironmentContext;


/**
 * This class is used to provide JBI statistics statisticsinformation to clients.
 * @author Sun Microsystems, Inc.
 */
   
public class JBIStatisticsMBeanImpl 
extends AbstractUIMBeanImpl 
implements JBIStatisticsMBean 
{    

    /** comma */
    public static String COMMA =",";    
 
    /** colon */
    public static String COLON =":";
    
    /** equal */
    public static String EQUAL = "=";
    
    /** statistics service type */
    public static String SERVICE_TYPE_KEY ="CustomControlName";    
    
    /** ojc components specific marker for provisioning endpoints */
    static String PROVIDER_MARKER = "Provider";
    
    /** ojc components specific marker for consuming endpoints */
    static String CONSUMER_MARKER = "Consumer";
    
    /**
     * method name in message service statistics MBean to query for endpoints in a
     * delivery channel 
     */
    static String QUERY_ENDPOINT_LIST = "getEndpointsForDeliveryChannel";
    
    /**
     * method name in message service statistics MBean to query for endpoints in a
     * delivery channel 
     */
    static String QUERY_CONSUMING_ENDPOINT_LIST = "getConsumingEndpointsForDeliveryChannel";
    
    
    
    /**
     * mbean names
     */
    MBeanNames mBeanNames = null;
    
    /**
     * resource bundle
     */
    I18NBundle mResourceBundle = null;

    /**
     * constructor
     */
    public JBIStatisticsMBeanImpl(EnvironmentContext anEnvContext) 
    {
        super(anEnvContext);
        this.mBeanNames = anEnvContext.getMBeanNames();
        this.mResourceBundle = getI18NBundle();
    }
    

    /**
     * This method is used to provide JBIFramework statistics in the
     * given target.
     * @param target target name.
     * @return TabularData table of framework statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getFrameworkStats(String targetName)
    throws JBIRemoteException
    {

        if (!isValidTarget(targetName))
        {
            logDebug ("getFrameworkStats(): target " + targetName + " not valid target.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_TARGET,
                            new String[] { targetName}, null);
            throw new JBIRemoteException(exception);
        }

        try
        {

            Map<String, ObjectName>  frameworkMBeans  = 
                    getFrameworkStatsMBeans(targetName);
            Set<String> instances = frameworkMBeans.keySet();
            
            CompositeType frameworkStatsEntriesType =
                new CompositeType(
                    JBIStatisticsItemNames.FRAMEWORK_STATISTICS_NAME,
                    JBIStatisticsItemNames.FRAMEWORK_STATISTICS_DESCRIPTION,
                    FRAMEWORK_STATS_ITEM_NAMES, 
                    FRAMEWORK_STATS_ITEM_DESCRIPTIONS, 
                    FRAMEWORK_STATS_ITEM_TYPES);            
            
            TabularType frameworkStatsType = 
                    new TabularType(
                        JBIStatisticsItemNames.FRAMEWORK_STATISTICS_TABLE_ITEM_NAME,
                        JBIStatisticsItemNames.FRAMEWORK_STATISTICS_TABLE_ITEM_DESCRIPTION,
                        frameworkStatsEntriesType,
                        JBIStatisticsItemNames.STATS_TABLE_INDEX);
            
            TabularData frameworkStatsTable = new TabularDataSupport(frameworkStatsType);
                    
            for (String instance:instances)
            {
                ObjectName mbean = frameworkMBeans.get(instance);
                if (mbean != null)
                {
                    logDebug("Getting framework statistics for " + instance);                               
                    frameworkStatsTable.put( 
                        getFrameworkStats(
                            frameworkStatsEntriesType,
                            instance, 
                            mbean));
                }
                
            }
            
            return frameworkStatsTable;
        }
        catch (OpenDataException ex)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { targetName }, ex);
            logDebug(ex.getMessage());
            throw new JBIRemoteException(exception);                   

        }    
                
    }
    
    /**
     * This method is used to provide statistics for the given component
     * in the given target
     * @param targetName target name
     * @param componentName component name
     * @return TabularData table of component statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getComponentStats(String componentName, String targetName)
    throws JBIRemoteException
    {
        
        if (!isValidTarget(targetName))
        {
            logDebug ("getFrameworkStats(): target " + targetName + " not valid target.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_TARGET,
                            new String[] { targetName}, null);
            throw new JBIRemoteException(exception);
        }

        try
        {
            //check if the component is up and throw an exception if it is not
            if (!isComponentUp(componentName, targetName))
            {
                Exception exception = this.createManagementException(
                      LocalStringKeys.STATS_COMP_NOT_STARTED,
                      new String[] { componentName, targetName }, null);
                logDebug(exception.getMessage());
                throw new JBIRemoteException(exception);
            }
            
            
            Map<String, ObjectName>  compStatsFrameworkMBeans  = 
                    getCompStatsFrameworkMBeans(targetName, componentName);
            logDebug("Got statistics MBeans for components registered by framework");            
            
            Map<String, ObjectName>  compStatsExtensionMBeans  = 
                    getCompStatsExtensionMBeans(targetName, componentName);
            logDebug("Got statistics MBeans for components registered by components");            
            
            Map<String, ObjectName>  messageServiceStatsMBeans  = 
                    getNMRStatsMBeans(targetName);
            logDebug("Got statistics MBeans for message service");      
            
            Set<String> instances = compStatsFrameworkMBeans.keySet();
            
            TabularType componentStatsTableType = null;
            TabularData componentStatsTable = null;
            CompositeData[] componentStats = new CompositeData[instances.size()];
            int i=0;
            for (String instance:instances)
            {
                ObjectName compFrameworkMBean = compStatsFrameworkMBeans.get(instance);
                if (compFrameworkMBean != null)
                {
                    logDebug("Getting component stats for " + instance);                                                

                    componentStats[i++] = 
                        getComponentStats(
                            instance, 
                            componentName,
                            compFrameworkMBean,
                            messageServiceStatsMBeans.get(instance),
                            compStatsExtensionMBeans.get(instance));
                }
            }
            //the tabular type could be constructed only after we have the component stats composite type
            //we need atleast one entry
            if ( i > 0 && componentStats[0] != null)
            {
                componentStatsTableType = 
                    new TabularType(
                        "ComponentStats",
                        "Component Statistic Information",
                        componentStats[0].getCompositeType(),
                        JBIStatisticsItemNames.STATS_TABLE_INDEX);                
                componentStatsTable = new TabularDataSupport(componentStatsTableType);
                
                for(int j=0; j<i; j++)
                {
                    componentStatsTable.put(componentStats[j]);
                }
            }
            return componentStatsTable;
        }
        catch (OpenDataException ex)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { componentName }, ex);
            logDebug(ex.getMessage());
            throw new JBIRemoteException(exception);                   

        }         
    }
    
                
    /**
     * This method is used to provide statistic information about the given 
     * endpoint in the given target
     * @param targetName target name
     * @param endpointName the endpoint Name
     * @return TabularData table of endpoint statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getEndpointStats(String endpointName, String targetName)
    throws JBIRemoteException
    {
       if (!isValidTarget(targetName))
       {
            logDebug ("getFrameworkStats(): target " + targetName + " not valid target.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_TARGET,
                            new String[] { targetName}, null);
            throw new JBIRemoteException(exception);
       }

       try
       {
            logDebug("Entered getEndpointStats");            
           
            Map<String, ObjectName>  messageServiceStatsMBeans  = 
                    getNMRStatsMBeans(targetName);
            logDebug("Obtained statistics MBeans for message service");      
            
            Set<String> instances = messageServiceStatsMBeans.keySet();
            
            TabularType endpointStatsTableType = null;
            TabularData endpointsStatsTable = null;
            CompositeData[] endpointStats = new CompositeData[instances.size()];
            int i=0;
            for (String instance:instances)
            {
                ObjectName messageServiceMBean = messageServiceStatsMBeans.get(instance);
                if (messageServiceMBean != null)
                {
                    logDebug("Obtaining endpoint stats for " + instance);                                                

                    //pass in the targetName to handle cluster cases, need this to get componentquery
                    endpointStats[i++] = 
                        getEndpointStats(
                            targetName,
                            instance, 
                            endpointName,
                            messageServiceStatsMBeans.get(instance));
                }
            }
            //the tabular type could be constructed only after we have the component stats composite type
            //so we need atleast one entry
            if ( i > 0 && endpointStats[0] != null)
            {
                endpointStatsTableType = 
                    new TabularType(
                        "EndpointStats",
                        "Endpoint Statistic Information",
                        endpointStats[0].getCompositeType(),
                        JBIStatisticsItemNames.STATS_TABLE_INDEX);                
                endpointsStatsTable = new TabularDataSupport(endpointStatsTableType);
                
                for(int j=0; j<i; j++)
                {
                    endpointsStatsTable.put(endpointStats[j]);
                }
            }
            return endpointsStatsTable;
        }
        catch (OpenDataException ex)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { endpointName }, ex);
            logDebug(ex.getMessage());
            throw new JBIRemoteException(exception);                   

        }
    }
    
    /**
     * This method is used to provide statistics about the message service in the
     * given target.
     * @param target target name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getNMRStats(String targetName)
    throws JBIRemoteException
    {
        if (!isValidTarget(targetName))
        {
            logDebug ("getFrameworkStats(): target " + targetName + " not valid target.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_TARGET,
                            new String[] { targetName}, null);
            throw new JBIRemoteException(exception);
        }
 
        try
        {
             
            Map<String, ObjectName>  nmrMBeans  = 
                    getNMRStatsMBeans(targetName);
            Set<String> instances = nmrMBeans.keySet();
            

            OpenType[] nmrStatsItemTypes =
            {
                SimpleType.STRING,
                new ArrayType(1, SimpleType.STRING),
                new ArrayType(1, SimpleType.STRING)
            };             
            
            CompositeType NMRStatsEntriesType =
                new CompositeType(
                    JBIStatisticsItemNames.NMR_STATISTICS_NAME,
                    JBIStatisticsItemNames.NMR_STATISTICS_DESCRIPTION,
                    NMR_STATS_ITEM_NAMES, 
                    NMR_STATS_ITEM_DESCRIPTIONS, 
                    nmrStatsItemTypes);            
            
            TabularType nmrStatsType = 
                    new TabularType(
                        JBIStatisticsItemNames.NMR_STATISTICS_TABLE_ITEM_NAME,
                        JBIStatisticsItemNames.NMR_STATISTICS_TABLE_ITEM_DESCRIPTION,
                        NMRStatsEntriesType,
                        JBIStatisticsItemNames.STATS_TABLE_INDEX);
            
            TabularData nmrStatsTable = new TabularDataSupport(nmrStatsType);
                    
            for (String instance:instances)
            {
                ObjectName mbean = nmrMBeans.get(instance);
                if ( mbean != null)
                {
                    logDebug("Getting NMR statistics for " + instance);                            
                    nmrStatsTable.put( 
                        getNMRStats(
                            NMRStatsEntriesType,
                            instance, 
                            mbean));
                }
                
            }
            
            return nmrStatsTable;
        }
        catch (OpenDataException ex)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { targetName }, ex);
            logDebug(ex.getMessage());
            throw new JBIRemoteException(exception);                   

        }
    }
    
    
    /**
     * This method is used to provide statistics about a Service Assembly
     * in the given target.
     * @param target target name.
     * @param saName the service assembly name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getServiceAssemblyStats(String saName, String targetName)
    throws JBIRemoteException
    {
        if (!isValidTarget(targetName))
        {
            logDebug ("getFrameworkStats(): target " + targetName + " not valid target.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_TARGET,
                            new String[] { targetName}, null);
            throw new JBIRemoteException(exception);
        }

        try
        {
            Map<String, ObjectName>  deploymentServiceStatsMBeans  = 
                    getDeploymentServiceStatsMBeans(targetName);
            
            Set<String> instances = deploymentServiceStatsMBeans.keySet();
            
            TabularType saStatsTableType = null;
            TabularData saStatsTable = null;
            CompositeData[] saStats = new CompositeData[instances.size()];
            
            int i=0;
            for (String instance:instances)
            {
                ObjectName deploymentServiceMBean = deploymentServiceStatsMBeans.get(instance);
                if (deploymentServiceMBean != null)
                {
                    logDebug("Getting service assembly statistics for " + instance);                                                
                    saStats[i++] = 
                        getSAStats(
                            instance, 
                            saName,
                            deploymentServiceStatsMBeans.get(instance));
                }
            }

            //the tabular type could be constructed only after we have the component stats composite type
            //so we need atleast one entry
            if ( i > 0 && saStats[0] != null)
            {
                saStatsTableType = 
                    new TabularType(
                        JBIStatisticsItemNames.SA_STATISTICS_NAME,
                        JBIStatisticsItemNames.SA_STATISTICS_DESCRIPTION,
                        saStats[0].getCompositeType(),
                        JBIStatisticsItemNames.STATS_TABLE_INDEX);                
                saStatsTable = new TabularDataSupport(saStatsTableType);
                
                for(int j=0; j<i; j++)
                {
                    saStatsTable.put(saStats[j]);
                }
            }
            return saStatsTable;
        }
        catch (JBIRemoteException remoteException)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { saName }, remoteException);
            logDebug(remoteException.getMessage());
            throw new JBIRemoteException(remoteException);                
        }        
        catch (OpenDataException ex)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { saName }, ex);
            logDebug(ex.getMessage());
            throw new JBIRemoteException(ex);                   

        }

    }

    
    /**
     * This method is used to return the list of framework stats 
     * MBeans that contain statistic information for the given target
     * @param targetName the target
     * @return map of instance names and corresponding framework MBeans
     */
    private Map<String, ObjectName> getFrameworkStatsMBeans(String targetName)
    throws JBIRemoteException
    {
        return getStatsMBeans(targetName, mBeanNames.SERVICE_NAME_FRAMEWORK);
    }
    
    
    /**
     * This method is used to return the list of deploymentservice stats 
     * MBeans that contain statistic information for the given target
     * @param targetName the target
     * @return map of instance names and corresponding deployment service MBeans
     */
    private Map<String, ObjectName> getDeploymentServiceStatsMBeans(String targetName)
    throws JBIRemoteException
    {
        return getStatsMBeans(targetName, mBeanNames.SERVICE_NAME_DEPLOY_SERVICE);
    }
    
    /**
     * This method is used to return the list of nmr stats 
     * MBeans that contain statistic information for the given target
     * @param targetName the target
     * @return map of instance names and corresponding framework MBeans
     */
    private Map<String, ObjectName> getNMRStatsMBeans(String targetName)
    throws JBIRemoteException
    {
        return getStatsMBeans(targetName, mBeanNames.SERVICE_NAME_MESSAGE_SERVICE);
    }
    
    /**
     * This method is used to return the list of component stats Mbeans registered by framework 
     * MBeans that contain statistic information for the given target
     * @param targetName the target
     * @return map of instance names and corresponding component stats MBeans
     */
    private Map<String, ObjectName> getCompStatsFrameworkMBeans(
            String targetName,
            String componentName)
    throws JBIRemoteException
    {
        try
        {
            Map<String, ObjectName> mbeanList = new HashMap<String, ObjectName>();
            if ("domain".equals(targetName))
            {
                logDebug ("getCompStatsFrameworkMBeans(): target " + targetName + " type not supported.");
                Exception exception = this.createManagementException(
                                LocalStringKeys.STATS_TARGET_NOT_SUPPORTED,
                                new String[] { targetName}, null);
                throw new JBIRemoteException(exception);                
            }
        
            //find out if this is a binding or engine
            ComponentType compType;
            try
            {

                ComponentQuery compQuery = mEnvContext.getComponentQuery(targetName);
                ComponentInfo compInfo = compQuery.getComponentInfo(componentName);
                compType = compInfo.getComponentType();
                logDebug("ComponentQuery in target " + targetName + " returned component type " + compType);
            }
            catch (Exception ex)
            {
                logDebug ("getCompStatsFrameworkMBeans(): component " + componentName + " type not known.");
                Exception exception = this.createManagementException(
                                LocalStringKeys.STATS_COMP_TYPE_NOT_KNOWN,
                                new String[] { componentName }, null);
                throw new JBIRemoteException(exception);                  
            }
                

                    
            if (getPlatformContext().isStandaloneServer(targetName))
            {
                mbeanList.put(targetName, 
                        getCompStatsFrameworkMBeans(
                            targetName, 
                            componentName,
                            compType));
            }
            else if (getPlatformContext().isClusteredServer(targetName))
            {
                mbeanList.put(targetName,
                        getCompStatsFrameworkMBeans(
                            targetName, 
                            componentName,
                            compType));                        
            }            
            else if (this.getPlatformContext().isCluster(targetName))
            {
                Set<String> servers = getPlatformContext().getServersInCluster(targetName);
                for(String server: servers)
                {
                    mbeanList.put(server,  
                        getCompStatsFrameworkMBeans(
                            server, 
                            componentName,
                            compType));                            
                }
            }
            return mbeanList;
        }
        catch (Exception ex)
        {
            throw new JBIRemoteException(ex);            
        }
    }    
    
    
    /**
     * This method is used to return the list of component stats Mbeans registered by framework 
     * MBeans that contain statistic information for the given target
     * @param targetName the target
     * @return map of instance names and corresponding component stats MBeans
     */
    private Map<String, ObjectName> getCompStatsExtensionMBeans(
            String targetName,
            String componentName)
    throws JBIRemoteException
    {
        try
        {
            Map<String, ObjectName> mbeanList = new HashMap<String, ObjectName>();
            if ("domain".equals(targetName))
            {
                logDebug ("getCompStatsExtensionMBeans(): target " + targetName + " type not supported.");
                Exception exception = this.createManagementException(
                                LocalStringKeys.STATS_TARGET_NOT_SUPPORTED,
                                new String[] { targetName}, null);
                throw new JBIRemoteException(exception);                
            }
        
            //find out if this is a binding or engine
            ComponentType compType;
            try
            {
                ComponentQuery compQuery = mEnvContext.getComponentQuery(targetName);
                ComponentInfo compInfo = compQuery.getComponentInfo(componentName);
                compType = compInfo.getComponentType();
            }
            catch (Exception ex)
            {
                logDebug ("getCompStatsExtensionMBeans(): component " + componentName + " type not known.");
                Exception exception = this.createManagementException(
                                LocalStringKeys.STATS_COMP_TYPE_NOT_KNOWN,
                                new String[] { componentName }, null);
                throw new JBIRemoteException(exception);                  
            }
                

                    
            if (getPlatformContext().isStandaloneServer(targetName))
            {
                mbeanList.put(targetName, 
                        getCompStatsExtensionMBeans(
                            targetName, 
                            componentName,
                            compType));
            }
            else if (getPlatformContext().isClusteredServer(targetName))
            {
                mbeanList.put(targetName,
                        getCompStatsExtensionMBeans(
                            targetName, 
                            componentName,
                            compType));                        
            }            
            else if (this.getPlatformContext().isCluster(targetName))
            {
                Set<String> servers = getPlatformContext().getServersInCluster(targetName);
                for(String server: servers)
                {
                    mbeanList.put(server,  
                        getCompStatsExtensionMBeans(
                            server, 
                            componentName,
                            compType));                            
                }
            }
            return mbeanList;
        }
        catch (Exception ex)
        {
            throw new JBIRemoteException(ex);            
        }
    }            
    
    /**
     * This method is used to get the name of the stats mbeans
     */
    
    private ObjectName getCompStatsExtensionMBeans(
            String instance,
            String componentName,
            ComponentType compType)
    throws JBIRemoteException
    {
    
        boolean isUp = getPlatformContext().isInstanceUp(instance);           
       
        if (!isUp)
        {
            logDebug ("getCompStatsExtensionMBeans(): instance " + instance + " is down.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_SERVER_DOWN,
                            new String[] { instance }, null);
            throw new JBIRemoteException(exception);
        }    
        
        String componentType;
        if (compType.equals(ComponentType.BINDING))
        {
            componentType = mBeanNames.INSTALLED_TYPE_BINDING;
        }
        else if (compType.equals(ComponentType.ENGINE))
        {
            componentType = mBeanNames.INSTALLED_TYPE_ENGINE;
        }
        else
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_COMP_TYPE,
                            new String[] { componentName }, null);
            throw new JBIRemoteException(exception);            
        }
        
        String statsMBeanName = 
                JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON  +
                mBeanNames.INSTANCE_NAME_KEY + EQUAL + instance + COMMA +
                mBeanNames.COMPONENT_ID_KEY + EQUAL + componentName + COMMA +
                mBeanNames.CONTROL_TYPE_KEY + EQUAL + mBeanNames.CONTROL_TYPE_CUSTOM + COMMA +
                mBeanNames.COMPONENT_TYPE_KEY + EQUAL + mBeanNames.COMPONENT_TYPE_INSTALLED + COMMA +
                mBeanNames.INSTALLED_TYPE_KEY + EQUAL + componentType + COMMA +
                SERVICE_TYPE_KEY + EQUAL + mBeanNames.CONTROL_TYPE_STATISTICS;
        try 
        {
            return new ObjectName(statsMBeanName);
        } 
        catch (Exception e) 
        {
            ToolsLogManager.getRuntimeLogger().warning(
                mResourceBundle.getMessage(
                    LocalStringKeys.STATS_MBEAN_NOT_PRESENT,
                    new Object[] {statsMBeanName}));
            logWarning(e);
            return null;
        }
    }    
        
    /**
     * This method is used to get the name of the stats mbeans
     */
    
    private ObjectName getCompStatsFrameworkMBeans(
            String instance,
            String componentName,
            ComponentType compType)
    throws JBIRemoteException
    {
    
        boolean isUp = getPlatformContext().isInstanceUp(instance);           
       
        if (!isUp)
        {
            logDebug ("getCompStatsFrameworkMBeans(): instance " + instance + " is down.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_SERVER_DOWN,
                            new String[] { instance }, null);
            throw new JBIRemoteException(exception);
        }    
        
        String componentType;
        if (compType.equals(ComponentType.BINDING))
        {
            componentType = mBeanNames.INSTALLED_TYPE_BINDING;
        }
        else if (compType.equals(ComponentType.ENGINE))
        {
            componentType = mBeanNames.INSTALLED_TYPE_ENGINE;
        }
        else
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_INVALID_COMP_TYPE,
                            new String[] { componentName }, null);
            throw new JBIRemoteException(exception);            
        }
        
        String statsMBeanName = 
                JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON  +
                mBeanNames.INSTANCE_NAME_KEY + EQUAL + instance + COMMA +
                mBeanNames.COMPONENT_ID_KEY + EQUAL + componentName + COMMA +
                mBeanNames.CONTROL_TYPE_KEY + EQUAL + mBeanNames.CONTROL_TYPE_STATISTICS + COMMA +
                mBeanNames.COMPONENT_TYPE_KEY + EQUAL + mBeanNames.COMPONENT_TYPE_INSTALLED + COMMA +
                mBeanNames.INSTALLED_TYPE_KEY + EQUAL + componentType;
        try 
        {
            return new ObjectName(statsMBeanName);
        } 
        catch (Exception e) 
        {
            ToolsLogManager.getRuntimeLogger().warning(
                mResourceBundle.getMessage(
                    LocalStringKeys.STATS_MBEAN_NOT_PRESENT,
                    new Object[] {statsMBeanName}));
            logWarning(e);
            return null;
        }
    }

    
    /**
     * This method is used to get a list of statitics MBeans with the following 
     * pattern
     * @param targetName the target
     * @param servicename the mbean pattern
     * @return Map instance name to MBeans
     */
    private Map<String, ObjectName> getStatsMBeans(String targetName, String serviceName)
    throws JBIRemoteException
    {

        Map<String, ObjectName> mbeanList = new HashMap<String, ObjectName>();
        if ("domain".equals(targetName))
        {
            logDebug ("getStatsMBeans(): target " + targetName + " type not supported.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_TARGET_NOT_SUPPORTED,
                            new String[] { targetName}, null);
            throw new JBIRemoteException(exception);                
        }
        if (getPlatformContext().isStandaloneServer(targetName))
        {
            mbeanList.put(targetName, getStatsMBean(targetName, serviceName));
        }
        else if (getPlatformContext().isClusteredServer(targetName))
        {
            mbeanList.put(targetName, getStatsMBean(targetName, serviceName));
        }            
        else if (this.getPlatformContext().isCluster(targetName))
        {
            Set<String> servers = getPlatformContext().getServersInCluster(targetName);
            for(String server: servers)
            {
                mbeanList.put(server, getStatsMBean(server, serviceName));                    
            }
        }
        return mbeanList;
    }    

    
    /**
     * This method is used to get the name of the stats mbeans
     * @param serviceName service name
     * @param instance instance name
     * @return ObjectName object name of the mbean
     */
    
    private ObjectName getStatsMBean(String instance,String serviceName)
    throws JBIRemoteException
    {
    
        boolean isUp = getPlatformContext().isInstanceUp(instance);                
        if (!isUp)
        {
            logDebug ("getStatsMBeans(): instance " + instance + " is down.");
            Exception exception = this.createManagementException(
                            LocalStringKeys.STATS_SERVER_DOWN,
                            new String[] { instance }, null);
            throw new JBIRemoteException(exception);
        }        
        
        String statsMBeanName = 
                JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON  +
                mBeanNames.INSTANCE_NAME_KEY + EQUAL + instance + COMMA +
                mBeanNames.SERVICE_NAME_KEY + EQUAL + serviceName + COMMA +
                mBeanNames.CONTROL_TYPE_KEY + EQUAL + mBeanNames.CONTROL_TYPE_STATISTICS + COMMA +
                mBeanNames.COMPONENT_TYPE_KEY + EQUAL + mBeanNames.CONTROL_TYPE_SYSTEM;
        try 
        {
            return new ObjectName(statsMBeanName);
        } 
        catch (Exception ex) 
        {
            //catch all possible exceptions because if stats mbean is not available for
            //one instance we should be able to report for the rest of the instances.
            ToolsLogManager.getRuntimeLogger().warning(
                mResourceBundle.getMessage(
                    LocalStringKeys.STATS_MBEAN_NOT_PRESENT,
                    new Object[] {statsMBeanName}));            
            logWarning(ex);
            return null;
        }
    }
    
    /**
     * This method is used to provide query framework statistics from the
     * given mbean
     */
    private CompositeData getFrameworkStats(
            CompositeType frameworkStatsEntriesType, 
            String instanceName, 
            ObjectName mbean)
    throws JBIRemoteException
    {
        try
        {
            //get specific attributes and process them
            Long startTime = (Long) getMBeanAttribute(
                    mbean, 
                    FRAMEWORK_MBEAN_STARTUP_TIME_ATTR);

            Date lastRestart = (Date) getMBeanAttribute(
                    mbean, 
                    FRAMEWORK_MBEAN_LAST_RESTART_TIME_ATTR);
            long upTime = 0;
                    
            if (lastRestart != null)
            {
                upTime =  System.currentTimeMillis() - lastRestart.getTime();
            }
            
            Object[] values = { 
                instanceName,
                startTime,
                upTime
            };
         
            return new CompositeDataSupport(
                    frameworkStatsEntriesType, 
                    FRAMEWORK_STATS_ITEM_NAMES,
                    values);            
            
    
        }
        catch (OpenDataException ex)
        {
            throw new JBIRemoteException(ex);
        }
    }
    
    /**
     * This method is used to provide query framework statistics from the
     * given mbean
     */
    private CompositeData getNMRStats(
            CompositeType NMRStatsEntriesType, 
            String instanceName, 
            ObjectName mbean)
    throws JBIRemoteException
    {
        try
        {

            String[] channels = (String[]) getMBeanAttribute(
                    mbean, 
                    NMR_MBEAN_ACTIVE_CHANNELS_ATTR);
            String[] endpoints = (String[]) getMBeanAttribute(
                    mbean, 
                    NMR_MBEAN_ACTIVE_ENDPOINTS_ATTR);
           
            Object[] values = 
            { 
                instanceName,
                channels,
                endpoints
            };
            
            return new CompositeDataSupport(
                    NMRStatsEntriesType,
                    NMR_STATS_ITEM_NAMES,
                    values);            
            
        }
        catch (OpenDataException ex)
        {
            throw new JBIRemoteException(ex);
        }
    }
    
    /**
     * This method is used to obtain statistics for a SA by talking to the
     * DeploymentServiceStatistics MBean
     * @param instanceName instance name
     * @param saName sa Name
     * @param deploymentServiceMBean deployment service mbean object name
     */
    private CompositeData getSAStats(
            String instanceName, 
            String saName, 
            ObjectName deploymentServiceMBean)
            throws JBIRemoteException
    {

        try
        {
            return
              (CompositeData)invokeMBeanOperation(
                    deploymentServiceMBean,
                    "getServiceAssemblyStatistics",
                    new Object[] { saName },
                    new String[] { "java.lang.String"});
        }
        catch (JBIRemoteException remoteException)
        {
            throw new JBIRemoteException(remoteException); 
        }
    }

    
    /**
     * This method is used to provide component stats collected from framework 
     * component mbean and component extension stats mbean if one is registered
     * @param instance instance name
     * @param componentName comopnent name
     * @param frameworkMBean mbean registered by framework for this component's stat
     * @param messageServiceStatsMBean nmr stats mbean
     * @param compExtnMBean mbean registered by the component itself
     * @return CompositeData all stats composed from all the mbeans
     * @throws JBIRemoteException if stats could not be collected
     */
    private CompositeData getComponentStats(
            String instanceName,
            String componentName,
            ObjectName frameworkMBean,
            ObjectName messageServiceStatsMBean,
            ObjectName compExtnMBean)
    throws JBIRemoteException
    {
        
        long upTime = 0;
        Date lastRestartTime = (Date) getMBeanAttribute(
                frameworkMBean, 
                COMPONENT_LAST_RESTART_TIME_ATTR);            
        if (lastRestartTime != null)
        {
            upTime = System.currentTimeMillis() - lastRestartTime.getTime();
        }

        CompositeData allDeliveryChannelStats = 
                (CompositeData)invokeMBeanOperation(
                    messageServiceStatsMBean,
                    "getDeliveryChannelStatistics",
                    new Object[] { componentName },
                    new String[] { "java.lang.String"});

        if (allDeliveryChannelStats == null)
        {
            Exception exception = this.createManagementException(
                     LocalStringKeys.STATS_COMP_NOT_EXISTS,
                     new String [] { componentName }, null);
            logDebug(exception.getMessage());
            throw new JBIRemoteException(exception);
        }

        //compute completedexchanges = sendDONE + receiveDONE + sendERROR + receiveERROR
        long completedExchanges = 0;
        long errorExchanges = 0;
        Long numSendDone = (Long)allDeliveryChannelStats.get(COMPONENT_SEND_DONE);
        Long numReceiveDone = (Long)allDeliveryChannelStats.get(COMPONENT_RECEIVE_DONE);            
        Long numSendError = (Long)allDeliveryChannelStats.get(COMPONENT_SEND_ERROR);            
        Long numReceiveError = (Long)allDeliveryChannelStats.get(COMPONENT_RECEIVE_ERROR);            
        if (numSendDone != null && numReceiveDone != null && numSendError != null 
                && numReceiveError != null)
        {
            completedExchanges = 
                    numSendDone.longValue() + 
                    numReceiveDone.longValue() + 
                    numSendError.longValue() + 
                    numReceiveError.longValue();

            errorExchanges = 
                    numSendError.longValue() + 
                    numReceiveError.longValue();
        }

        CompositeData componentExtnStats = null;

        return composeComponentStats(
                instanceName,
                upTime,
                allDeliveryChannelStats,
                completedExchanges,
                errorExchanges,
                getCompStatsAttributes(compExtnMBean));
            
        
                
    }
    

    
    /**
     * This method is used compose component stats composite data
     * @param instanceName instance name
     * @param upTime component uptime
     * @param allDeliveryChannelStats stats from messageservice 
     * @param completedExchanges number of completed exchanges - (computed value)
     * @param errorExchanges number of error exchanges - (computed value)
     * @param componentExtnStats attributes exposed by component stats mbean
     * @throws JBIRemoteException if the data could not be composed into CompositeData
     */
    private CompositeData composeComponentStats(
            String instanceName,
            long upTime,
            CompositeData deliveryChannelStats,
            long completedExchanges,
            long errorExchanges,
            CompositeData componentExtnStats)
    throws JBIRemoteException
    {
        try
        {
            ArrayList<String> itemNames = new ArrayList<String>();
            itemNames.add(JBIStatisticsItemNames.INSTANCE_NAME);
            itemNames.add(JBIStatisticsItemNames.COMPONENT_UPTIME);      
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_ACTIVATED_ENDPOINTS);      
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS);     
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS);                              
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_COMPLETED_EXCHANGES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES_MAX);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_QUEUED_EXCHANGES);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_QUEUED_EXCHANGES_MAX);
            itemNames.add(JBIStatisticsItemNames.NUMBER_OF_ERROR_EXCHANGES);
            
            ArrayList<String> itemDescriptions = new ArrayList<String>();
            itemDescriptions.add("Instance Name");
            itemDescriptions.add("Component Uptime");
            itemDescriptions.add("Number of activated endpoints");
            itemDescriptions.add("Number of received requests");
            itemDescriptions.add("Number of sent requests");
            itemDescriptions.add("Number of received replies");
            itemDescriptions.add("Number of sent replies");
            itemDescriptions.add("Number of received DONEs");
            itemDescriptions.add("Number of sent DONEs");
            itemDescriptions.add("Number of received faults");
            itemDescriptions.add("Number of sent faults");
            itemDescriptions.add("Number of received errors");
            itemDescriptions.add("Number of sent errors");
            itemDescriptions.add("Number of completed exchanges");
            itemDescriptions.add("Number of active exchanges");
            itemDescriptions.add("Number of active exchanges max");
            itemDescriptions.add("Number of queued exchanges");
            itemDescriptions.add("Number of queued exchanges max");
            itemDescriptions.add("Number of error exchanges");

          
            ArrayList<OpenType> itemTypes = new ArrayList<OpenType>();
            itemTypes.add(SimpleType.STRING);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG);
            itemTypes.add(SimpleType.LONG); 

           
            ArrayList<Object> itemValues = new ArrayList<Object>();
            itemValues.add(instanceName);
            itemValues.add(new Long(upTime));
            itemValues.add(deliveryChannelStats.get(COMPONENT_ACTIVE_ENDPOINTS));
            itemValues.add(deliveryChannelStats.get(COMPONENT_RECEIVE_REQUEST));
            itemValues.add(deliveryChannelStats.get(COMPONENT_SEND_REQUEST));
            itemValues.add(deliveryChannelStats.get(COMPONENT_RECEIVE_REPLY));
            itemValues.add(deliveryChannelStats.get(COMPONENT_SEND_REPLY));
            itemValues.add(deliveryChannelStats.get(COMPONENT_RECEIVE_DONE));
            itemValues.add(deliveryChannelStats.get(COMPONENT_SEND_DONE));
            itemValues.add(deliveryChannelStats.get(COMPONENT_RECEIVE_FAULT));
            itemValues.add(deliveryChannelStats.get(COMPONENT_SEND_FAULT));
            itemValues.add(deliveryChannelStats.get(COMPONENT_RECEIVE_ERROR));
            itemValues.add(deliveryChannelStats.get(COMPONENT_SEND_ERROR));
            itemValues.add(new Long(completedExchanges));
            itemValues.add(deliveryChannelStats.get(COMPONENT_ACTIVE_EXCHANGE));            
            itemValues.add(deliveryChannelStats.get(COMPONENT_ACTIVE_EXCHANGE_MAX));            
            itemValues.add(deliveryChannelStats.get(COMPONENT_QUEUED_EXCHANGE));            
            itemValues.add(deliveryChannelStats.get(COMPONENT_QUEUED_EXCHANGE_MAX));            
            itemValues.add(new Long(errorExchanges));

                               
            if (deliveryChannelStats.containsKey(COMPONENT_RESPONSE_TIME))
            {
                itemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME);
                itemDescriptions.add("Avg. response time for message exchange");        
                itemTypes.add(SimpleType.LONG); 
                itemValues.add(deliveryChannelStats.get(COMPONENT_RESPONSE_TIME));                
            }
            if (deliveryChannelStats.containsKey(COMPONENT_COMPONENT_TIME))
            {
                itemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);
                itemDescriptions.add("Avg. time taken in component by message exchange");  
                itemTypes.add(SimpleType.LONG);  
                itemValues.add(deliveryChannelStats.get(COMPONENT_COMPONENT_TIME));                
            }
            if(deliveryChannelStats.containsKey(COMPONENT_CHANNEL_TIME))
            {
                itemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);
                itemDescriptions.add("Avg. time taken in delivery channel by message exchange");
                itemTypes.add(SimpleType.LONG);  
                itemValues.add(deliveryChannelStats.get(COMPONENT_CHANNEL_TIME));                
            }
            if (deliveryChannelStats.containsKey(COMPONENT_NMR_TIME))
            {
                itemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);
                itemDescriptions.add("Avg. time taken in message service by message exchange");                
                itemTypes.add(SimpleType.LONG);   
                itemValues.add(deliveryChannelStats.get(COMPONENT_NMR_TIME));                
            }
            if (deliveryChannelStats.containsKey(COMPONENT_STATUS_TIME))
            {
                itemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_STATUS_TIME);
                itemDescriptions.add("Avg. status time taken by message exchange");                
                itemTypes.add(SimpleType.LONG);   
                itemValues.add(deliveryChannelStats.get(COMPONENT_STATUS_TIME));                
            }            
            if (componentExtnStats != null)
            {
                itemNames.add("ComponentExtensionStats");
                itemDescriptions.add("Statistics reported by component statistics MBeans");    
                itemTypes.add(componentExtnStats.getCompositeType());     
                itemValues.add(componentExtnStats);                
            }
            
            return new CompositeDataSupport(
                    new CompositeType(
                       JBIStatisticsItemNames.COMPONENT_STATISTICS_TABLE_ITEM_NAME,
                       JBIStatisticsItemNames.COMPONENT_STATISTICS_TABLE_ITEM_DESCRIPTION,
                       (String[])itemNames.toArray(new String[]{}),
                       (String[])itemDescriptions.toArray(new String[]{}),
                       (OpenType[])itemTypes.toArray(new OpenType[]{})),
                    (String[])itemNames.toArray(new String[]{}),
                    (Object[])itemValues.toArray(new Object[]{}));
        }
        catch (OpenDataException ex)
        {
            throw new JBIRemoteException(ex);
        }

    }
    
    /**
     * This method is used to obtain the statistics attributes exposed 
     * by component registered statistics mbeans
     * @param mbeanName stats mbean object name
     * @return CompositeData containing the stats mbean's atributes
     * @throws JBIRemoteException if the attributes could not be obtained
     */
    private CompositeData getCompStatsAttributes(ObjectName mbeanName)
    throws JBIRemoteException
    {
        try
        {
            if (!isValidTarget(mbeanName))
            {
                //for components that did not register custom stats mbean
                return null;
            }
            MBeanInfo mbeanInfo = getMBeanInfo(mbeanName);
            MBeanAttributeInfo[] attributes = mbeanInfo.getAttributes();


            String[] compExtnItems = new String[attributes.length];
            String[] compExtnDesc = new String[attributes.length];
            OpenType[] compExtnTypes = new OpenType[attributes.length];
            Object[] compAttrValues = new Object[attributes.length];

            for (int i = 0; i < attributes.length; i++)
            {
                String attrName = attributes[i].getName();
                String attrType = attributes[i].getType();
                Object attrValue = getMBeanAttribute(mbeanName, attrName);

                //create the composite type
                compExtnItems[i] = attrName;
                compExtnDesc[i] = attrName;
                compExtnTypes[i] = getOpenType(attrType, attrValue);
                compAttrValues[i] = attrValue;

                //logDebug("OpenType for " + attrType + " is " + compExtnTypes[i] );

            }
            return new CompositeDataSupport(
                    new CompositeType(
                        "ComponentExtensionStats",
                        "Component extension stats",
                        compExtnItems,
                        compExtnDesc,
                        compExtnTypes),
                   compExtnItems,
                   compAttrValues);

        }
        catch (OpenDataException ex)
        {
            throw new JBIRemoteException(ex);
        }
        catch (JBIRemoteException jbiRemoteEx)
        {
            logDebug("Component custom stats are not available: "+ jbiRemoteEx.getMessage());        
            return null;
        }        
        catch (Exception exception)
        {
            logDebug(exception);        
            return null;
        }             
    }
    
    /**
     * This method is used to get the stats for an endpoint
     * @param targetName the target name
     * @param instanceName instance name
     * @param endpointName endpoint name
     * @param messageServiceMBean object name
     * @return CompositeData statistic info about the endpoint
     * @throws JBIRemoteException if statistics could not be obtained
     */
    private CompositeData getEndpointStats(
            String targetName,
            String instanceName,
            String endpointName,
            ObjectName messageServiceStatsMBean)
    throws JBIRemoteException
    {
        boolean isProvider = true;

        CompositeData allEndpointStats = 
                (CompositeData)invokeMBeanOperation(
                    messageServiceStatsMBean,
                    "getEndpointStatistics",
                    new Object[] { endpointName },
                    new String[] { "java.lang.String"});
        
       
        if (allEndpointStats == null)
        {
            Exception exception = this.createManagementException(
                     LocalStringKeys.ERROR_ENDPOINT_NOT_EXISTS,
                     new String [] { endpointName }, null);
            logDebug(exception.getMessage());
            throw new JBIRemoteException(exception); 
        }

        String owningComponent = null;
        try
        {
            Object[] exposedEndpointStatsProvider = 
                allEndpointStats.getAll(ENDPOINT_STATS_PROVIDER_ITEM_NAMES);   
            owningComponent = (String)
                allEndpointStats.get(OWNING_COMPONENT);
        }
        catch (javax.management.openmbean.InvalidKeyException invalidKeyException)
        {
            logDebug ("All provider items are not present, could be consuming endpoint");                    
            isProvider = false;
        }
 
        TabularData ojcPerfTable = null;
        ObjectName compStatsExtensionMBean = null;
        Map<String, ObjectName> compStatsExtensionMBeans = null;
        
        //check if we have a good component name before trying to locate component stats mbeans
        if (owningComponent != null && owningComponent.length() > 0)
        {
            compStatsExtensionMBeans = getCompStatsExtensionMBeans(targetName, owningComponent);            
        }

        if (compStatsExtensionMBeans  != null)
        {
            compStatsExtensionMBean = compStatsExtensionMBeans.get(instanceName);
        }
        if (compStatsExtensionMBean != null)
        {
            //OJC components mark the endpoints as follows distinguish Provider/Consumer
            if (!endpointName.endsWith(COMMA))
            {
                endpointName = endpointName + COMMA;
            }
            if (isProvider)
            {
                endpointName = endpointName + PROVIDER_MARKER;
            }
            else
            {
                endpointName = endpointName + CONSUMER_MARKER;
            }
            logDebug ("Getting extension stats from component");                                
            ojcPerfTable = 
                    getOJCPerformanceMeasurement(
                    compStatsExtensionMBean,
                    endpointName);
        }
        return composeEndpointStats(
                instanceName,
                allEndpointStats,
                isProvider,
                ojcPerfTable);
    }
    
    /**
     * This method is used to obtain a list of performance measurements 
     * recorded by OJC components
     * @param compMbeanName component stats object name
     * @return TabularData performance measurement categories
     */
    private TabularData getOJCPerformanceMeasurement(
            ObjectName compMbeanName,
            String endpointName)
    {
        try
        {
            TabularData allPerformanceStats = 
                (TabularData)invokeMBeanOperation(
                    compMbeanName,
                    "getPerformanceInstrumentationMeasurement",
                    new Object[] { endpointName },
                    new String[] { "java.lang.String"});
            
            CompositeType performanceStatsType = 
                   new CompositeType(
                        "PerformanceInstrumentationStats",
                        "Performance Instrumentation Stats",
                        OJC_HULP_STATS_ITEMS,
                        OJC_HULP_STATS_DESCRIPTIONS,
                        OJC_HULP_STATS_TYPES);
            
            TabularType performanceStatsTableType =
                    new TabularType(
                        "PerformanceInstrumentationStats",
                        "Performance Instrumentation Stats",
                        performanceStatsType,
                        OJC_STATS_TABLE_INDEX);
                        
            TabularData perfStats = new TabularDataSupport(performanceStatsTableType);
                    
            Iterator iter = allPerformanceStats.values().iterator();

            while(iter.hasNext()) 
            {
                CompositeData compositeData = (CompositeData) iter.next();
                CompositeType compositeType = compositeData.getCompositeType();
                Iterator perfStatsIter = compositeType.keySet().iterator();
                Object[] values =  new Object[OJC_HULP_STATS_ITEMS.length];
                
                while(perfStatsIter.hasNext())
                {
                    String item = (String) perfStatsIter.next();                
                    if (true == item.equals(AVERAGE_KEY)) 
                    {
                        values[0] = (Double) compositeData.get(item);
                    }                
                    if (true == item.equals(AVERAGEWITHOUTFIRSTMEASUREMENT_KEY)) 
                    {
                        values[1] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(FIRSTMEASUREMENTTIME_KEY)) 
                    {
                        values[2] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(LOAD_KEY)) 
                    {
                        values[3] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(NUMBEROFMEASUREMENTOBJECTS_KEY)) 
                    {
                        values[4] = (Integer) compositeData.get(item);
                    }
                    if (true == item.equals(NUMBEROFMEASUREMENTS_KEY)) 
                    {
                        values[5] = (Integer) compositeData.get(item);
                    }
                    if (true == item.equals(THROUGHPUT_KEY)) 
                    {
                        values[6] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(TIMETAKEN_KEY)) 
                    {
                        values[7] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(TOTALTIME_KEY)) 
                    {
                        values[8] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(MEDIAN_KEY)) 
                    {
                        values[9] = (Double) compositeData.get(item);
                    }
                    if (true == item.equals(SOURCE_KEY)) 
                    {
                        values[10] = (String) compositeData.get(item);
                    }
                    if (true == item.equals(SUB_TOPIC_KEY)) 
                    {
                        values[11] = (String) compositeData.get(item);
                    }
                    if (true == item.equals(TOPIC_KEY)) 
                    {
                        values[12] = (String) compositeData.get(item);
                    }
                }
                perfStats.put(new CompositeDataSupport(
                        performanceStatsType,
                        OJC_HULP_STATS_ITEMS,
                        values));

               
            }
            return perfStats;
        }
        catch (OpenDataException openDataEx)
        {
            //issues in composing the perf stats
            logWarning(openDataEx);
            return null;
        }
        catch (JBIRemoteException jbiRemoteEx)
        {
            //may not be an OJC component. do not log warning
            logDebug("Perfomance Instrumentation details are not available: "+ jbiRemoteEx.getMessage());        
            return null;
        }        
        catch (Exception ex)
        {
            logWarning(ex);
            return null;
        }             

    }
            
  
    /**
     * This method is used to compose endpoint stats
     * @param instanceName
     * @param endpointStats endpoint stats
     * @param isProvider true if this is a provider endpoint
     * @param ojcStats table of performance data
     * @return CompositeData composite data
     *
     */
    private CompositeData composeEndpointStats(
            String instanceName,
            CompositeData endpointStats,
            boolean isProvider,
            TabularData ojcStats)
    {
        try 
        {
            if (isProvider)
            {
                return composeProviderEndpointStats(instanceName, endpointStats, ojcStats);
            }
            else
            {
                return composeConsumerEndpointStats(instanceName, endpointStats, ojcStats);
            }
        }
        catch (Exception ex)
        {
            logWarning(ex);
            return null;
        }
    }    
    
    /**
     * This method is used to compose provider endpoint stats
     * @param instanceName
     * @param endpointStats endpoint stats
     * @param ojcStats table of performance data
     * @return CompositeData composite data
     *
     */    
     private CompositeData composeProviderEndpointStats(
            String instanceName,
            CompositeData endpointStats,
            TabularData ojcStats)
     {
    
        TabularType ojcStatsType = null;
        if (ojcStats !=  null)
        {
           ojcStatsType = ojcStats.getTabularType();
        }
           
        ArrayList<String> providerItemNames = new ArrayList<String>();
        providerItemNames.add(JBIStatisticsItemNames.INSTANCE_NAME);
        providerItemNames.add(JBIStatisticsItemNames.PROVIDER_ENDPOINT_ACTIVATION_TIME);
        providerItemNames.add(JBIStatisticsItemNames.PROVIDER_ENDPOINT_UPTIME);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REQUESTS);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_REPLIES);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS);
        providerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS);
        providerItemNames.add(JBIStatisticsItemNames.ENDPOINT_COMPONENT_NAME);
        
        
        ArrayList<String> providerItemDescriptions = new ArrayList<String>();
        providerItemDescriptions.add("Instance Name");
        providerItemDescriptions.add("Time of activation");
        providerItemDescriptions.add("Endpoint upTime");
        providerItemDescriptions.add("Number of Active Exchanges");
        providerItemDescriptions.add("Number of Received Requests");
        providerItemDescriptions.add("Number of Sent Replies");
        providerItemDescriptions.add("Number of Received DONEs");
        providerItemDescriptions.add("Number of Sent DONEs");
        providerItemDescriptions.add("Number of Received Faults");
        providerItemDescriptions.add("Number of Sent Faults");
        providerItemDescriptions.add("Number of Received Errors");
        providerItemDescriptions.add("Number of Sent Errors");
        providerItemDescriptions.add("Component Name");

      
        ArrayList<OpenType> providerItemTypes = new ArrayList<OpenType>();
        providerItemTypes.add(SimpleType.STRING);
        providerItemTypes.add(SimpleType.DATE);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);                   
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.STRING);

        //convert uptime from long to date
        //calculate uptime from activation time        
        Date activationTime = null;
        long upTime = 0;
        Long activationTimeStamp = (Long) endpointStats.get(PROVIDER_ACTIVATION_TIMESTAMP);
        if (activationTimeStamp != null)
        {
            activationTime = new Date(activationTimeStamp.longValue());
            upTime = System.currentTimeMillis() - activationTime.getTime();
        }
        
        ArrayList<Object> providerItemValues = new ArrayList<Object>();
        providerItemValues.add(instanceName);
        providerItemValues.add(activationTime);
        providerItemValues.add(new Long(upTime)); //uptime is calculated here
        providerItemValues.add(endpointStats.get(COMPONENT_ACTIVE_EXCHANGE));    
        providerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_REQUEST));   
        providerItemValues.add(endpointStats.get(COMPONENT_SEND_REPLY));  
        providerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_DONE));
        providerItemValues.add(endpointStats.get(COMPONENT_SEND_DONE));
        providerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_FAULT));
        providerItemValues.add(endpointStats.get(COMPONENT_SEND_FAULT));
        providerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_ERROR));                     
        providerItemValues.add(endpointStats.get(COMPONENT_SEND_ERROR));
        providerItemValues.add(endpointStats.get(OWNING_COMPONENT));  
        
        
        if (endpointStats.containsKey(COMPONENT_RESPONSE_TIME))
        {
            providerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME);            
            providerItemDescriptions.add("Message Exchange ResponseTime Avg in ns");            
            providerItemTypes.add(SimpleType.LONG);
            providerItemValues.add(endpointStats.get(COMPONENT_RESPONSE_TIME));      
                    
        }
        if (endpointStats.containsKey(COMPONENT_CHANNEL_TIME))
        {
            providerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);                        
            providerItemDescriptions.add("Message Exchange DeliveryChannelTime Avg in ns");            
            providerItemTypes.add(SimpleType.LONG);            
            providerItemValues.add(endpointStats.get(COMPONENT_CHANNEL_TIME));       
        }
        if (endpointStats.containsKey(COMPONENT_COMPONENT_TIME))
        {
            providerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);              
            providerItemDescriptions.add("Message Exchange ComponentTime Avg in ns");            
            providerItemTypes.add(SimpleType.LONG);            
            providerItemValues.add(endpointStats.get(COMPONENT_COMPONENT_TIME));       
        }
        if (endpointStats.containsKey(COMPONENT_NMR_TIME))
        {
            providerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);
            providerItemTypes.add(SimpleType.LONG);  
            providerItemDescriptions.add("Message Exchange MessageServiceTime  Avg in ns");                       
            providerItemValues.add(endpointStats.get(COMPONENT_NMR_TIME));       
        }
        if ( ojcStatsType != null)
        {
            providerItemNames.add(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS);
            providerItemDescriptions.add("Performance Measurements recorded by OJC Components");            
            providerItemTypes.add(ojcStatsType);            
            providerItemValues.add(ojcStats);
        }
               
        try
        {
            return new CompositeDataSupport(
                        new CompositeType(
                        JBIStatisticsItemNames.PROVIDER_STATS_NAME,
                        JBIStatisticsItemNames.PROVIDER_STATS_DESCRIPTION,
                        (String[])providerItemNames.toArray(new String[]{}),
                        (String[])providerItemDescriptions.toArray(new String[]{}),
                        (OpenType[])providerItemTypes.toArray(new OpenType[]{})),
                    (String[])providerItemNames.toArray(new String[]{}),
                    (Object[])providerItemValues.toArray(new Object[]{}));
        }
        catch (OpenDataException ode)
        {
            //if stats could not be composed return null and proceed with other instances
            logWarning(ode);
            return null;
        }
    }
        
        
        
    /**
     * This method is used to compose consumer endpoint stats
     * @param instanceName
     * @param endpointStats endpoint stats
     * @param ojcStats table of performance data
     * @return CompositeData composite data
     *
     */    
     private CompositeData composeConsumerEndpointStats(
            String instanceName,
            CompositeData endpointStats,
            TabularData ojcStats)
     {        
         
         
        TabularType ojcStatsType = null;
        if (ojcStats !=  null)
        {
           ojcStatsType = ojcStats.getTabularType();
        }
        
        ArrayList<String> consumerItemNames = new ArrayList<String>();
        consumerItemNames.add(JBIStatisticsItemNames.INSTANCE_NAME);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_REQUESTS);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_REPLIES);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_DONES);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_DONES);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_FAULTS);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_FAULTS);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_RECEIVED_ERRORS);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_SENT_ERRORS);
        consumerItemNames.add(JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES);
        consumerItemNames.add(JBIStatisticsItemNames.ENDPOINT_COMPONENT_NAME);        

          
        ArrayList<String> consumerItemDescriptions = new ArrayList<String>();
        consumerItemDescriptions.add("Instance Name");
        consumerItemDescriptions.add("Number of Sent Requests");
        consumerItemDescriptions.add("Number of Received Replies");
        consumerItemDescriptions.add("Number of Received DONEs");
        consumerItemDescriptions.add("Number of Sent DONEs");
        consumerItemDescriptions.add("Number of Received Faults");
        consumerItemDescriptions.add("Number of Sent Faults");
        consumerItemDescriptions.add("Number of Received Errors");
        consumerItemDescriptions.add("Number of Sent Errors");
        consumerItemDescriptions.add("Number of active exchanges");
        consumerItemDescriptions.add("Name of the owning component");
            
        ArrayList<OpenType> consumerItemTypes = new ArrayList<OpenType>();
        consumerItemTypes.add(SimpleType.STRING);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);    
        consumerItemTypes.add(SimpleType.LONG); 
        consumerItemTypes.add(SimpleType.STRING); 

        ArrayList<Object> consumerItemValues = new ArrayList<Object>();
        consumerItemValues.add(instanceName);
        consumerItemValues.add(endpointStats.get(COMPONENT_SEND_REQUEST));      
        consumerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_REPLY));  
        consumerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_DONE));         
        consumerItemValues.add(endpointStats.get(COMPONENT_SEND_DONE));
        consumerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_FAULT)); 
        consumerItemValues.add(endpointStats.get(COMPONENT_SEND_FAULT));
        consumerItemValues.add(endpointStats.get(COMPONENT_RECEIVE_ERROR));
        consumerItemValues.add(endpointStats.get(COMPONENT_SEND_ERROR));
        consumerItemValues.add(endpointStats.get(COMPONENT_ACTIVE_EXCHANGE));    
        consumerItemValues.add(endpointStats.get(OWNING_COMPONENT));  

        if (endpointStats.containsKey(COMPONENT_STATUS_TIME))
        {
            consumerItemNames.add(JBIStatisticsItemNames.CONSUMING_ENDPOINT_STATUS_TIME);   
            consumerItemDescriptions.add("Message Exchange Status Time Avg in ns");        
            consumerItemTypes.add(SimpleType.LONG);        
            consumerItemValues.add(endpointStats.get(COMPONENT_STATUS_TIME));            
        }
        if (endpointStats.containsKey(COMPONENT_COMPONENT_TIME))
        {
            consumerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);            
            consumerItemDescriptions.add("Message Exchange ComponentTime Avg in ns");        
            consumerItemTypes.add(SimpleType.LONG);                
            consumerItemValues.add(endpointStats.get(COMPONENT_COMPONENT_TIME));                 
        }
        if (endpointStats.containsKey(COMPONENT_CHANNEL_TIME))
        {
            consumerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);      
            consumerItemDescriptions.add("Message Exchange DeliveryChannelTime Avg in ns");            
            consumerItemTypes.add(SimpleType.LONG);            
            consumerItemValues.add(endpointStats.get(COMPONENT_CHANNEL_TIME));                           
        }
        if (endpointStats.containsKey(COMPONENT_NMR_TIME))
        {
            consumerItemNames.add(JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);                                     
            consumerItemDescriptions.add("Message Exchange MessageServiceTime  Avg in ns");   
            consumerItemTypes.add(SimpleType.LONG);
            consumerItemValues.add(endpointStats.get(COMPONENT_NMR_TIME));                         
        }
        if ( ojcStatsType != null)
        {        
            consumerItemNames.add(JBIStatisticsItemNames.OJC_PERFORMANCE_STATS);       
            consumerItemDescriptions.add("Performance Measurements recorded by OJC Components");              
            consumerItemTypes.add(ojcStatsType);            
            consumerItemValues.add(ojcStats);
        }
        
        try
        {
            return 
                    new CompositeDataSupport(
                        new CompositeType(
                        JBIStatisticsItemNames.CONSUMING_ENDPOINT_STATS_NAME,
                        JBIStatisticsItemNames.CONSUMING_ENDPOINT_STATS_DESCRIPTION,
                        (String[])consumerItemNames.toArray(new String[]{}),
                        (String[])consumerItemDescriptions.toArray(new String[]{}),
                        (SimpleType[])consumerItemTypes.toArray(new SimpleType[]{})),
                    (String[])consumerItemNames.toArray(new String[]{}),
                    (Object[])consumerItemValues.toArray(new Object[]{}));            
        }
        catch(OpenDataException ode)
        {
            logWarning(ode);
            return null;
        }
       
    }
    
    /**
     * This method is used to construct an OpenType for the given java class
     * @param javaType the java class name
     * @param value the object 
     * @return OpenType OpenType corresponding to the given javaType
     * @throws JBIRemoteException if there are issues in getting the OpenType
     */
    private OpenType getOpenType(String javaType, Object value)
    throws JBIRemoteException
    {

        try
        {
            if (javaType.equals("javax.management.openmbean.CompositeType"))
            {
                //handle compositetype
                return ((CompositeData)value).getCompositeType();
            }
            else if (javaType.equals("javax.management.openmbean.TabularType"))
            {
                //handle tabulartype
                return ((TabularData)value).getTabularType();
            }
            else if (javaType.startsWith("["))
            {
                //handle arraytype
                String className = javaType.substring(javaType.indexOf("java"), javaType.length()-1);
                //TODO find out how many times [ comes to decide the dimension of the array            
                return new ArrayType(1, JAVATYPE_TO_OPENTYPE.get(className));
            }
            else
            {
                return JAVATYPE_TO_OPENTYPE.get(javaType);
            }
        }
        catch (OpenDataException ex)
        {
            throw new JBIRemoteException(ex);
        }
       
    }
    
    /**
     * This method is used to find out if a component is started in a given target
     * @param componentName componentName
     * @param targetName target name
     * @return true if the component is started
     */
    private boolean isComponentUp(String componentName, String targetName)
        throws JBIRemoteException
    {
            ComponentQuery compQuery = mEnvContext.getComponentQuery(targetName);
            ComponentInfo compInfo = compQuery.getComponentInfo(componentName);
            if (compInfo == null)
            {
                Exception exception = this.createManagementException(
                      LocalStringKeys.STATS_COMP_NOT_EXISTS,
                      new String[] { componentName }, null);
                logDebug(exception.getMessage());
                throw new JBIRemoteException(exception);
            }
            logDebug("Component status in target " + targetName + " is " + compInfo.getStatus());        
            if (compInfo.getStatus() == ComponentState.STARTED)
            {
                return true;
            }
            else
            {
                return false;
            }
    }
        
    /**
     * This method is used to find out if a component is installed in a given target
     * @param componentName componentName
     * @param targetName target name
     * @return true if the component is started
     */
    private boolean isComponentInstalled(String componentName, String targetName)
    {
        ComponentQuery compQuery = mEnvContext.getComponentQuery(targetName);
        if (compQuery.getComponentInfo(componentName) == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    
    /**
     * This method is used to provide a list of consuming endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of consuming endpoints 
     * @throws JBIRemoteException if the list of endpoints could not be obtained.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getConsumingEndpointsForComponent(String componentName, String targetName)
    throws JBIRemoteException
    {
        return getEndpointListForComponent(componentName, targetName, true);
    }
    
    /**
     * This method is used to provide a list of provisioning endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of provisioning endpoints 
     * @throws JBIRemoteException if the list of endpoints could not be obtained.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getProvidingEndpointsForComponent(String componentName, String targetName)
    throws JBIRemoteException
    {
        return getEndpointListForComponent(componentName, targetName, false);
    }          
        
    /**
     * This method is used to provide a list of consuming endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @param listConsuming true if you need a list of consuming endpoints
     * @return TabularData list of consuming endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getEndpointListForComponent(
            String componentName, 
            String targetName,
            boolean listConsuming)
    throws JBIRemoteException
    {        
        try
        {
             
            if (!isComponentInstalled(componentName, targetName))
            {
                Exception exception = this.createManagementException(
                      LocalStringKeys.STATS_COMP_NOT_INSTALLED,
                      new String[] { componentName, targetName }, null);
                logDebug(exception.getMessage());
                throw new JBIRemoteException(exception);
            }
            
            Map<String, ObjectName>  nmrMBeans  = 
                    getNMRStatsMBeans(targetName);
            Set<String> instances = nmrMBeans.keySet();
            

            OpenType[] endpointListItemTypes =
            {
                SimpleType.STRING,
                new ArrayType(1, SimpleType.STRING),
            };             
            
            CompositeType endpointListCompositeType =
                new CompositeType(
                    JBIStatisticsItemNames.ENDPOINT_LIST_STATISTICS_NAME,
                    JBIStatisticsItemNames.ENDPOINT_LIST_STATISTICS_DESCRIPTION,
                    ENDPOINT_LIST_STATS_ITEM_NAMES, 
                    ENDPOINT_LIST_STATS_ITEM_DESCRIPTIONS, 
                    endpointListItemTypes);            
            
            TabularType endpointListTabularType = 
                    new TabularType(
                        JBIStatisticsItemNames.ENDPOINT_LIST_STATISTICS_TABLE_NAME,
                        JBIStatisticsItemNames.ENDPOINT_LIST_STATISTICS_TABLE_DESCRIPTION,
                        endpointListCompositeType,
                        JBIStatisticsItemNames.STATS_TABLE_INDEX);
            
            TabularData endpointListTable = new TabularDataSupport(endpointListTabularType);
                    
            for (String instance:instances)
            {
                ObjectName mbean = nmrMBeans.get(instance);
                if ( mbean != null)
                {
                    logDebug("Getting endpoint list statistics for " + instance);                            
                    endpointListTable.put( 
                        getEndpointsList(
                            endpointListCompositeType,
                            instance, 
                            componentName,
                            mbean,
                            listConsuming));
                }
                
            }
            
            return endpointListTable;
        }
        catch (OpenDataException ex)
        {
            Exception exception = this.createManagementException(
                            LocalStringKeys.ERROR_IN_STATS_COMPOSING,
                            new String[] { targetName }, ex);
            logDebug(ex.getMessage());
            throw new JBIRemoteException(exception);                   

        }
    }    
    

    /**
     * This method is used to query the list of endpoints in a component 
     * in the given service assembly.
     * @param endpointListStatsEntriesType composite type of the endpoint list data
     * @param instanceName instance name 
     * @param componentName the component name
     * @param listConsuming true if consuming endpoints have to be queried
     * @param mbean name of the NMR stats MBean
     */
    private CompositeData getEndpointsList(
            CompositeType endpointListStatsEntriesType, 
            String instanceName, 
            String componentName,
            ObjectName mbean,
            boolean listConsuming)
    throws JBIRemoteException
    {
        try
        {
            String endpointQueryMethodName;
            if (listConsuming)
            {
                endpointQueryMethodName = QUERY_CONSUMING_ENDPOINT_LIST;
            }
            else
            {
                endpointQueryMethodName = QUERY_ENDPOINT_LIST;
            }

            String[] endpointsList = 
                (String[])invokeMBeanOperation(
                    mbean,
                    endpointQueryMethodName,
                    new Object[] { componentName },
                    new String[] { "java.lang.String"});
           
            Object[] values = 
            { 
                instanceName,
                endpointsList
            };
            
            return new CompositeDataSupport(
                    endpointListStatsEntriesType,
                    ENDPOINT_LIST_STATS_ITEM_NAMES,
                    values);            
            
        }
        catch (OpenDataException ex)
        {
            throw new JBIRemoteException(ex);
        }
    }    

   /**  
    * table index for OJC hulp statistics
    */        
    static String[] STATS_TABLE_INDEX = new String[]{ "InstanceName" };          
    
    /** 
     * attr. name for startup time in framework stats. mbean
     */        
    static String FRAMEWORK_MBEAN_STARTUP_TIME_ATTR = "StartupTime";
    
    /** 
     * attr. name for last restart time in framework stats, mbean
     */
    static String FRAMEWORK_MBEAN_LAST_RESTART_TIME_ATTR = "LastRestartTime";
        
            
   /** 
    * FrameworkStats CompositeType item names   
    */
    static String[] FRAMEWORK_STATS_ITEM_NAMES = 
    {
        JBIStatisticsItemNames.INSTANCE_NAME,
        JBIStatisticsItemNames.FRAMEWORK_STARTUP_TIME,
        JBIStatisticsItemNames.FRAMEWORK_UPTIME
    };    
    
   /** 
    * FrameworkStats CompositeType item descriptions   
    */    
    static String[] FRAMEWORK_STATS_ITEM_DESCRIPTIONS =
    {
        "Instance Name",
        "Time taken to startup the framework (ms)",
        "Time elapsed since framework has been started (ms)"
    };

   /** 
    * FrameworkStats CompositeType item types   
    */        
    static OpenType[] FRAMEWORK_STATS_ITEM_TYPES =
    {
        SimpleType.STRING,
        SimpleType.LONG,
        SimpleType.LONG
    };
    
    /** 
     * attr. name  for active channels in NMR stats MBean.
     */
    static String NMR_MBEAN_ACTIVE_CHANNELS_ATTR = "ActiveChannels";
    
    /** 
     * attr. name for active endpoints in NMR stats MBean.
     */
    static String NMR_MBEAN_ACTIVE_ENDPOINTS_ATTR = "ActiveEndpoints";
    
   /** 
    * nmr CompositeType item names   
    */        
    static String[] NMR_STATS_ITEM_NAMES = 
    {
        JBIStatisticsItemNames.INSTANCE_NAME,
        JBIStatisticsItemNames.NMR_STATS_ACTIVE_CHANNELS,
        JBIStatisticsItemNames.NMR_STATS_ACTIVE_ENDPOINTS
    };    
    
   /** 
    * nmr CompositeType item descriptions   
    */            
    static String[] NMR_STATS_ITEM_DESCRIPTIONS =
    {
        "Instance Name",
        "List of active delivery channels",
        "List of active endpoints"
    };
    
     
     /** 
      * attr. name for last restart time in 
      * component statistics mbean
      */
     static String COMPONENT_LAST_RESTART_TIME_ATTR = "LastRestartTime";
    
     /**
      * item name for active endpoints in component statistics data 
      */
     static String COMPONENT_ACTIVE_ENDPOINTS = "ActiveEndpoints";

     /**
      * item name for number of received requests in component statistics data 
      */
     static String COMPONENT_RECEIVE_REQUEST = "ReceiveRequest";
     
     /**
      * item name for number of sent requests in component statistics data 
      */     
     static String COMPONENT_SEND_REQUEST = "SendRequest";
     
     /**
      * item name for number of received replies in component statistics data 
      */     
     static String COMPONENT_RECEIVE_REPLY = "ReceiveReply";
     
     /**
      * item name for number of sent replies in component statistics data 
      */     
     static String COMPONENT_SEND_REPLY = "SendReply";
     
     /**
      * item name for number of received DONEs in component statistics data 
      */     
     static String COMPONENT_RECEIVE_DONE = "ReceiveDONE";
     
     /**
      * item name for number of sent DONEs requests in component statistics data 
      */     
     static String COMPONENT_SEND_DONE = "SendDONE";
     
     /**
      * item name for number of received faults in component statistics data 
      */     
     static String COMPONENT_RECEIVE_FAULT = "ReceiveFault";
     
     /**
      * item name for number of sent faults in component statistics data 
      */     
     static String COMPONENT_SEND_FAULT = "SendFault";
     
     /**
      * item name for number of received errors in component statistics data 
      */     
     static String COMPONENT_RECEIVE_ERROR = "ReceiveERROR";
     
     /**
      * item name for number of sent errors in component statistics data 
      */     
     static String COMPONENT_SEND_ERROR = "SendERROR";
     
     /**
      * item name for number of active exchangesin component statistics data 
      */     
     static String COMPONENT_ACTIVE_EXCHANGE = "ActiveExchanges";
     
     /**
      * item name for number of active exchangesin component statistics data 
      */     
     static String COMPONENT_ACTIVE_EXCHANGE_MAX = "MaxActiveExchanges";
     
     /**
      * item name for number of active exchangesin component statistics data 
      */     
     static String COMPONENT_QUEUED_EXCHANGE = "QueuedExchanges";
     
     /**
      * item name for number of active exchangesin component statistics data 
      */     
     static String COMPONENT_QUEUED_EXCHANGE_MAX = "MaxQueuedExchanges";
     
     /**
      * item name for response time in component statistics data 
      */     
     static String COMPONENT_RESPONSE_TIME = "ResponseTimeAvg (ns)";
     
     /**
      * item name for component time in component statistics data 
      */     
     static String COMPONENT_COMPONENT_TIME = "ComponentTimeAvg (ns)";
     
     /**
      * item name for channel time in component statistics data 
      */     
     static String COMPONENT_CHANNEL_TIME = "ChannelTimeAvg (ns)";
     
     /**
      * item name for nmr time in component statistics data 
      */     
     static String COMPONENT_NMR_TIME = "NMRTimeAvg (ns)";
    
     /**
      * item name for owning component in endpoint statistics data 
      */    
     static String OWNING_COMPONENT = "OwningChannel";
     
     /**
      * item name for activation time stamp in endpoint statistics data
      */     
     static String PROVIDER_ACTIVATION_TIMESTAMP = "ActivationTimestamp";

     /**
      * item name for status time in endpoint statistics data 
      */
     static String COMPONENT_STATUS_TIME = "StatusTimeAvg (ns)";

    /** 
     * endpoint stats items - provider specific 
     *  used to find out if the endpoint is a provider
     */    
    static String[] ENDPOINT_STATS_PROVIDER_ITEM_NAMES =
    {
        PROVIDER_ACTIVATION_TIMESTAMP,
        COMPONENT_RECEIVE_REQUEST,
        COMPONENT_SEND_REPLY,
    };    

   
    /**
     * names of items in the composite data for endpoints list
     */
    static String[] ENDPOINT_LIST_STATS_ITEM_NAMES =
    {
        JBIStatisticsItemNames.INSTANCE_NAME,
        JBIStatisticsItemNames.ENDPOINTS_LIST_ITEM_NAME
    };
    
    /**
     * descriptions of items in the composite data for endpoints list
     */
    static String[] ENDPOINT_LIST_STATS_ITEM_DESCRIPTIONS =
    {
        "Instance Name",
        "List of endpoints"
    };
        
   
    /** map used to convery a java type to OpenType */
    static Map<String, OpenType> JAVATYPE_TO_OPENTYPE = new HashMap<String, OpenType>();
    
    /* populate the map */
    static 
    {
        JAVATYPE_TO_OPENTYPE.put("java.lang.Long", SimpleType.LONG);
        JAVATYPE_TO_OPENTYPE.put("java.lang.String", SimpleType.STRING);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Boolean", SimpleType.BOOLEAN);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Character", SimpleType.CHARACTER);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Byte", SimpleType.BYTE);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Short", SimpleType.SHORT);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Integer", SimpleType.INTEGER);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Float", SimpleType.FLOAT);
        JAVATYPE_TO_OPENTYPE.put("java.lang.Double", SimpleType.DOUBLE);
        JAVATYPE_TO_OPENTYPE.put("java.lang.BigDecimal", SimpleType.BIGDECIMAL);
        JAVATYPE_TO_OPENTYPE.put("java.lang.BigInteger", SimpleType.BIGINTEGER);        
        JAVATYPE_TO_OPENTYPE.put("java.lang.Date", SimpleType.DATE);      
    }
           
    
    /* ###########################
     * OJC hulp statistics
     * ###########################
     */        
    // following fields are for OJC hulp statistics
    // taken from com.sun.esb.management.common.PerformanceData
    
    /**
     * Number of measurements (or N), i.e. the number of dt-s, i.e. the number
     * of times that Measurement.begin() - end() was called.
     */
    public static final String NUMBEROFMEASUREMENTS_KEY           = "n";
    
    /** total time (ms) the sum of all dt-s */
    public static final String TOTALTIME_KEY                      = "total time (ms)";
    
    /**
     * average' (ms) (the sum of all dt-s minus the first dt) divided by N. The
     * first measurement is discounted because it typically includes
     * classloading times and distorts the results considerably. If there's only
     * one measurement, the first measurement is not discounted and the value
     * should be equal to total time.
     */
    public static final String AVERAGEWITHOUTFIRSTMEASUREMENT_KEY = "average' (ms)";
    
    /**
     * act the number of measurement objects on which begin() was called but not
     * end(). This indicates the number of active measurements. Caveat: there's
     * a problem in the this accounting when the subtopic of the measurement is
     * changed.
     */
    public static final String NUMBEROFMEASUREMENTOBJECTS_KEY     = "act";
    
    /** first the first dt */
    public static final String FIRSTMEASUREMENTTIME_KEY           = "first (ms)";
    
    /**
     * average sum of all dt-s divided by N; this does not discount the first
     * measurement
     */
    public static final String AVERAGE_KEY                        = "average (ms)";
    
    /**
     * throughput N divided by (tlast - tfirst); this is the average throughput.
     * This number is meaningful if there were no long pauses in processing.
     */
    public static final String THROUGHPUT_KEY                     = "throughput (s-1)";
    
    /**
     * tlast - tfirst the wallclock time of the first measurement's begin()
     * method is tracked as tfirst and the wallclock time of the last
     * measurement's end() method is tracked as tlast
     */
    public static final String TIMETAKEN_KEY                      = "last-first (ms)";
    
    /**
     * Load The sum of all dt-s divided by (tlast - tfirst). This is a measure
     * of concurrency: the higher the number, the greater the concurrency. In a
     * single threaded scenario this number can never exceed 1.
     */
    public static final String LOAD_KEY                           = "load";
    
    public static final String MEDIAN_KEY                         = "median (ms)";
    
    public static final String SOURCE_KEY                         = "source";
    
    /**
     * sub topic the name of the measurement specified in the second argument of
     * begin() or in setSubTopic().
     */
    public static final String SUB_TOPIC_KEY                      = "sub topic";
    
    /**
     * topic the name of the measurement specified in the first argument of
     * begin() or in setSubTopic().
     */
    public static final String TOPIC_KEY                          = "topic";    
    
    /** OJC components hulp stats */        
    static String[] OJC_HULP_STATS_ITEMS =
    {
        AVERAGE_KEY,
        AVERAGEWITHOUTFIRSTMEASUREMENT_KEY,
        FIRSTMEASUREMENTTIME_KEY,
        LOAD_KEY,
        NUMBEROFMEASUREMENTOBJECTS_KEY,
        NUMBEROFMEASUREMENTS_KEY,
        THROUGHPUT_KEY,
        TIMETAKEN_KEY,
        TOTALTIME_KEY,
        MEDIAN_KEY,
        SOURCE_KEY,
        SUB_TOPIC_KEY,
        TOPIC_KEY
    };
    
    /** OJC components hulp stats item descriptions */        
    static String[] OJC_HULP_STATS_DESCRIPTIONS =
    {
        "average sum of all dt-s divided by N",
        "average' (ms) (the sum of all dt-s minus the first dt) divided by N",
        "first dt",
        "Load The sum of all dt-s divided by (tlast - tfirst)",
        "the number of measurement objects on which begin() was called but not * end()",         
        "Number of measurements (or N)",
        "throughput N divided by (tlast - tfirst)",
        "tlast - tfirst the wallclock time of the first measurement's begin()method",
        "total time (ms) the sum of all dt-s",
        "median key",
        "source key",
        "sub topic the name of the measurement",
        "topic the name of the measurement"
    };
    
    /** OJC components hulp stats item types */        
    static OpenType[] OJC_HULP_STATS_TYPES =
    {
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,        
        SimpleType.INTEGER,
        SimpleType.INTEGER,
        SimpleType.DOUBLE,        
        SimpleType.DOUBLE,        
        SimpleType.DOUBLE,        
        SimpleType.DOUBLE,        
        SimpleType.STRING,        
        SimpleType.STRING,        
        SimpleType.STRING,        
    };
    
    /**  table index  for OJC perf stats */        
    static String[] OJC_STATS_TABLE_INDEX = new String[]{ SUB_TOPIC_KEY };   
}
