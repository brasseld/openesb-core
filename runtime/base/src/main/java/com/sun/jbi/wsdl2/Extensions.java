/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Extensions.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/**
 * API for  Extension elements and attributes for a WSDL component.
 *
 * @author ApiGen AX.00
 */
public interface Extensions
{
    /**
     * Get the number of Element items in elements.
     *
     * @return The number of Element items in elements
     */
    int getElementsLength();

    /**
     * Get extension elements by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Extension elements at given <code>index</code> position.
     */
    Element getElement(int index);

    /**
     * Set extension elements by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theElement Item to add at position <code>index</code>.
     */
    void setElement(int index, Element theElement);

    /**
     * Append an item to extension elements.
     *
     * @param theElement Item to append to elements
     */
    void appendElement(Element theElement);

    /**
     * Remove extension elements by index position.
     *
     * @param index The index position of the element to remove
     * @return The Element removed, if any.
     */
    Element removeElement(int index);

    /**
     * Get the number of Attr items in attributes.
     *
     * @return The number of Attr items in attributes
     */
    int getAttributesLength();

    /**
     * Get extension attributes by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Extension attributes at given <code>index</code> position.
     */
    Attr getAttribute(int index);

    /**
     * Set extension attributes by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theAttribute Item to add at position <code>index</code>.
     */
    void setAttribute(int index, Attr theAttribute);

    /**
     * Append an item to extension attributes.
     *
     * @param theAttribute Item to append to attributes
     */
    void appendAttribute(Attr theAttribute);

    /**
     * Remove extension attributes by index position.
     *
     * @param index The index position of the attribute to remove
     * @return The Attr removed, if any.
     */
    Attr removeAttribute(int index);

}

// End-of-file: Extensions.java
