/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Service.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;
import org.w3c.dom.DocumentFragment;

/**
 * API for  WSDL 2.0 Service component.
 *
 * @author ApiGen AX.00
 */
public interface Service extends ExtensibleDocumentedComponent
{
    /**
     * Get target namespace of this binding.
     *
     * @return Target namespace of this binding
     */
    String getTargetNamespace();

    /**
     * Get local name of this service component.
     *
     * @return Local name of this service component
     */
    String getName();

    /**
     * Set local name of this service component.
     *
     * @param theName Local name of this service component
     */
    void setName(String theName);

    /**
     * Get qualified name of this component.
     *
     * @return Qualified name of this component
     */
    QName getQName();

    /**
     * Get interface provided by this service.
     *
     * @return Interface provided by this service
     */
    Interface getInterface();

    /**
     * Set interface provided by this service.
     *
     * @param theInterface Interface provided by this service
     */
    void setInterface(Interface theInterface);

    /**
     * Get the number of Endpoint items in endpoints.
     *
     * @return The number of Endpoint items in endpoints
     */
    int getEndpointsLength();

    /**
     * Get endpoints for this service by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Endpoints for this service at given <code>index</code>
     * position.
     */
    Endpoint getEndpoint(int index);

    /**
     * Set endpoints for this service by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theEndpoint Item to add at position <code>index</code>.
     */
    void setEndpoint(int index, Endpoint theEndpoint);

    /**
     * Append an item to endpoints for this service.
     *
     * @param theEndpoint Item to append to endpoints
     */
    void appendEndpoint(Endpoint theEndpoint);

    /**
     * Remove endpoints for this service by index position.
     *
     * @param index The index position of the endpoint to remove
     * @return The Endpoint removed, if any.
     */
    Endpoint removeEndpoint(int index);

    /**
     * Create a new end point component, appending it to this service's
     * endpoint list.
     *
     * @param name NC name for the new endpoint.
     * @param binding Binding to which the endpoint refers.
     * @return The newly created endpoint, appended to this service's
     * endpoint list.
     */
    Endpoint addNewEndpoint(
        String name,
        Binding binding);

    /**
     * Return this WSDL service as an XML string.
     *
     * @return This service, serialized as an XML string.
     */
    String toXmlString();

    /**
     * Return this service as a DOM document fragment. The DOM subtree is a
     * copy; altering it will not affect this service.
     *
     * @return This service, as a DOM document fragment.
     */
    DocumentFragment toXmlDocumentFragment();

}

// End-of-file: Service.java
