/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.security;

/**
 * The MessageContext interface provides a handle to the message.
 *
 * @author Sun Microsystems, Inc.
 */
public interface MessageContext
    extends Context
{
   
    /* ------------------------------------------------------------------------------- *\
     *                                Message Ctx                                      *
    \* ------------------------------------------------------------------------------- */
    
    /**
     * Get the SOAP Message associated with this Context.
     *
     * @return the Message in the Context
     */
    Object getMessage();
    
    /**
     * Set the  Message associated with this Context.
     *
     * @param msg is the Message in the Context
     */
    void setMessage(Object msg);
    
    /**
     * Get the ResponseMessage, a resonse message is present in the Context only when 
     * validation of an incoming request message, results in a respponse message to
     * be sent to the sender of the request
     *
     * @return the Response Message in the Context
     */
    Object getResponseMessage();
    
    /**
     * Set the ResponseMessage, a response message is present in the Context only when 
     * validation of an incoming request message, results in a respponse message to
     * be sent to the sender of the request
     *
     * @param respMsg is the Error Message in the Context
     */
    void setResponseMessage(Object respMsg);
}
