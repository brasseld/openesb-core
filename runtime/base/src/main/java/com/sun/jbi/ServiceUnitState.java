/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitState.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import javax.jbi.management.DeploymentServiceMBean;

import java.util.List;

/**
 * This interface provides information on Service Assemblys.
 *
 * @author Sun Microsystems, Inc.
 */
public enum ServiceUnitState
{
    SHUTDOWN("Shutdown"),
    STOPPED("Stopped"),
    STARTED("Started"),
    UNKNOWN("Unknown");

    /** The String value */
    private String mString;
    
    ServiceUnitState(String strValue)
    {
        mString = strValue;
    }
    


    /**
     * Compute the effective state of a Service Unit given a list of  
     * Service Unit States.
     *
     * If one or more service units is in a STARTED state the state is STARTED
     * If one or more service units is in the STOPPED state and no servuce unit is in the 
     * STARTED state the state is STOPPED
     * If no service unit is in the STOPPED or STARTED state and one or more 
     * service units is in the SHUTDOWN state, the state is SHUTDOWN.
     * If none of the above are true the state is UNKNOWN
     *
     * @return the effective service unit state
     */
    public static ServiceUnitState 
    getEffectiveServiceAssemblyState(List<ServiceUnitState> suStates)
    {
        if ( suStates.contains(ServiceUnitState.STARTED))
        {
            return ServiceUnitState.STARTED;
        }
        else if ( suStates.contains(ServiceUnitState.STOPPED))
        {
            return ServiceUnitState.STOPPED;
        }
        else if ( suStates.contains(ServiceUnitState.SHUTDOWN))
        {
            return ServiceUnitState.SHUTDOWN;
        }   
        else
        {
            return ServiceUnitState.UNKNOWN;
        }
    }
    
    /**
     * Converts the service unit state to the equivalent 
     * DeploymentService state.
     */
    public static String convertState(ServiceUnitState state)
    {
        String dsState;
        
        if (state == ServiceUnitState.STARTED)
        {
            dsState = DeploymentServiceMBean.STARTED;
        }
        else if (state == ServiceUnitState.STOPPED)
        {
            dsState = DeploymentServiceMBean.STOPPED;
        }
        else
        {
            dsState = DeploymentServiceMBean.SHUTDOWN;
        }
        
        return dsState;
    }
    
    /**
     * Get the equivalent ServiceUnitState from DeploymentServiceMBean state.
     */
    public static ServiceUnitState valueOfDeploymentServiceState(String state)
    {
        ServiceUnitState saState;
        
        if (DeploymentServiceMBean.STARTED.equals(state))
        {
            saState = ServiceUnitState.STARTED;
        }
        else if (DeploymentServiceMBean.STOPPED.equals(state))
        {
            saState = ServiceUnitState.STOPPED;
        }
        else
        {
            saState = ServiceUnitState.SHUTDOWN;
        }
        
        return saState;
    }

    /**
     * @return the String value for the ServiceUnitType
     */
    public String toString()
    {
        return mString;
    }
    
    /**
     * @return a ServiceUnitState based on the String value. 
     * @param valStr - the string whose equivalent ServiceUnitState
     *                 instance is required.  This operation ignores
     *                 the case, i.e. Stopped or STOPPED or sToPped
     *                 would return ServiceUnitState.STOPPED
     */
    public static ServiceUnitState valueOfString(String valStr)
    {
        return ServiceUnitState.valueOf(valStr.toUpperCase());
    }
    
    public static void main(String[] args)
    {
        System.out.println(ServiceUnitState.STOPPED.toString());
        System.out.println(ServiceUnitState.valueOfString("Stopped").toString());
        System.out.println(ServiceUnitState.valueOfString("StoPPed").toString());
        System.out.println(ServiceUnitState.valueOfString("sTaRted").toString());
    }
}
