/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProxyBindingStatisticsMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;
import java.util.Date;

/**
 * This interface defines the MBean for collection of global statistics for 
 * the Proxy Binding in a single JBI instance. 
 * All statistics are since the last JBI startup; they are all reset 
 * when JBI is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ProxyBindingStatisticsMBean extends
                 ProxyBindingStatisticsBaseMBean
{
    /**
     * Get the total number of Events received by the PB since the
     * last NMR startup.
     * @return The number of Events received.
     */
    long getReceivedEvents();

    /**
     * Get the total number of Events sent by the PB since the
     * last NMR startup.
     * @return The number of Events sent.
     */
    long getSentEvents();

    /**
     * Get the last time the NMR in the local JBI instance restarted.
     * @return The time of last restart.
     */
    Date getLastNodeRestartTime();
}
