/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingFault.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;

/**
 * API for WSDL 2.0 Binding Fault container.
 *
 * @author ApiGen AX.00
 */
public interface BindingFault extends ExtensibleDocumentedComponent
{
    /**
     * Get QName of Interface Fault that is bound to this binding fault.
     *
     * @return QName of Interface Fault that is bound to this binding fault
     */
    QName getRef();

    /**
     * Set QName of Interface Fault that is bound to this binding fault.
     *
     * @param theRef QName of Interface Fault that is bound to this binding
     * fault
     */
    void setRef(QName theRef);

}

// End-of-file: BindingFault.java
