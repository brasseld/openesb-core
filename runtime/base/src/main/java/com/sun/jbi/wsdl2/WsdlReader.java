/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;
import java.net.MalformedURLException;
import java.io.IOException;

/*
 * This interface is based on the JSR 110 RI. The original
 * author was:
 * 
 * @author Matthew J. Duftler
 */

/**
 * This interface defines a set of methods for reading a WSDL 2.0 document
 * from various XML sources.
 *
 * @author Sun Microsystems, Inc.
 */
public interface WsdlReader 
{
    /**
     * Read the WSDL document accessible via the specified
     * URI into a WSDL definition.
     *
     * @param wsdlURI The URI (can be a filename or URL) pointing to a
     *                WSDL XML definition.
     * @return The WSDL document as a Description component.
     * @exception WsdlException         if a WSDL 2.0 validity problem is found
     *                                  with the given document.
     * @exception MalformedURLException if the given URL is invalid.
     * @exception IOException           if an I/O problem occurs while reading
     *                                  the document.
     */
    Description readDescription(String wsdlURI) 
        throws WsdlException, MalformedURLException, IOException;

    /**
     * @deprecated - replaced by readDescription
     */
    Definitions readWsdl(String wsdlURI) 
        throws WsdlException, MalformedURLException, IOException;

    /**
     * Read the WSDL document accessible via the specified
     * URI into a WSDL definition.
     *
     * @param contextURI The context in which to resolve the wsdlURI, if the 
     *                   wsdlURI is relative. Can be null, in which case it 
     *                   will be ignored.
     * @param wsdlURI    The URI (can be a filename or URL) pointing to a
     *                   WSDL XML definition.
     * @return The WSDL document as a Description component.
     * @exception WsdlException         if a WSDL 2.0 validity problem is found
     *                                  with the given document.
     * @exception MalformedURLException if a given URI is invalid.
     * @exception IOException           if an I/O problem occurs while reading
     *                                  the document.
     */
    Description readDescription(String contextURI, String wsdlURI)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * @deprecated use readDescription
     */
    Definitions readWsdl(String contextURI, String wsdlURI)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * Read the specified &lt;wsdl:definitions&gt; element into a WSDL
     * definition.
     *
     * @param documentBaseURI The document base URI of the WSDL definition
     *                        described by the element. Will be set as the 
     *                        documentBaseURI of the returned Definition. 
     *                        Can be null, in which case it will be ignored.
     * @param definitionsElement The &lt;wsdl:definitions&gt; element to read.
     * @return The WSDL document as a Description component.
     * @exception WsdlException         if a WSDL 2.0 validity problem is found
     *                                  with the given document.
     * @exception MalformedURLException if a given URI is invalid.
     * @exception IOException           if an I/O problem occurs while reading
     *                                  the document.
     */
    Description readDescription(String documentBaseURI, Element definitionsElement)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * @deprecated use readDescription
     */
    Definitions readWsdl(String documentBaseURI, Element definitionsElement)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * Read the specified WSDL document into a WSDL definition.
     *
     * @param documentBaseURI The document base URI of the WSDL definition
     *                        described by the document. Will be set as the
     *                        documentBaseURI of the returned Definition. 
     *                        Can be null, in which case it will be ignored.
     * @param wsdlDocument The WSDL document, an XML document obeying the WSDL 
     *                     schema.
     * @return The WSDL document as a Description component.
     * @exception WsdlException         if a WSDL 2.0 validity problem is found
     *                                  with the given document.
     * @exception MalformedURLException if a given URI is invalid.
     * @exception IOException           if an I/O problem occurs while reading
     *                                  the document.
     */
    Description readDescription(String documentBaseURI, Document wsdlDocument)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * @deprecated use readDescription
     */
    Definitions readWsdl(String documentBaseURI, Document wsdlDocument)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * Read a WSDL document into a WSDL definition.
     *
     * @param documentBaseURI The document base URI of the WSDL definition
     *                        described by the document. Will be set as the 
     *                        documentBaseURI of the returned Definition. 
     *                        Can be null, in which case it will be ignored.
     * @param inputSource     An InputSource pointing to the WSDL document, an 
     *                        XML document obeying the WSDL schema.
     * @return The WSDL document pointed to by the <code>inputSource</code>, 
     *         as a Description component.
     * @exception WsdlException         if a WSDL 2.0 validity problem is found
     *                                  with the given document.
     * @exception MalformedURLException if a given URI is invalid.
     * @exception IOException           if an I/O problem occurs while reading
     *                                  the document.
     */
    Description readDescription(String documentBaseURI, InputSource inputSource)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * @deprecated use readDescription
     */
    Definitions readWsdl(String documentBaseURI, InputSource inputSource)
        throws WsdlException, MalformedURLException, IOException;

    /**
     * Read a WSDL document into a WSDL definition.
     *
     * @param locator A WSDLLocator object used to provide InputSources
     *                pointing to the wsdl file.
     * @return The WSDL document as a Description component.
     */
    //  public Definition readWsdl(WSDLLocator locator) throws WsdlException;
}
