/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * This interface defines a factory API that enables applications to obtain 
 * instances of WSDL 2.0 API objects needed to begin using the API. The objects
 * that can be created by this factory are:
 * <ul>
 *   <li><b>Description.</b> An empty WSDL definitions component. This is useful for 
 *       programatic creation of WSDL components.</li>
 *   <li><b>WsdlReader.</b> An object that can be used to read existing WSDL 
 *       documents from a variety of sources, producing a Description component.</li>
 *   <li><b>WsdlWriter.</b> An object that can used to write a Description component
 *       to a variety of destinations. This is useful for persisting Description
 *       that were programatically created.</li>
 * </ul>
 * 
 * @author Sun Microsystems, Inc.
 */
public interface WsdlFactory
{
    /**
     * Create a new instance of a WSDL reader. A reader is useful for creating WSDL
     * Description components from existing XML sources (that is, it can deserialize
     * the XML presentation of WSDL).
     * 
     * @return A new instance of a WSDL reader.
     */
    WsdlReader newWsdlReader();

    /**
     * Create a new instance of a WSDL writer. A writer is useful for converting WSDL
     * Description components in memory into a proper XML serialization.
     * 
     * @return A new instance of a WSDL writer.
     */
    WsdlWriter newWsdlWriter();

    /**
     * Create a new instance of a WSDL Description component. This is useful for
     * programmatic creation of service descriptions.
     * 
     * @param targetNamespace Target namespace for the new component.
     * @return A new, empty WSDL Description component for the given target namespace.
     */
    Description newDescription(String targetNamespace);

    /**
     * Create a new instance of a WSDL Definitions . This is useful for
     * programmatic creation of service definitions.  This method is for
     * backward compatibility.
     * 
     * @deprecated - replaced by newDescritption(String targetNamespace)
     * @param targetNamespace Target namespace for the new component.
     * @return A new, empty WSDL Definitions component for the given target namespace.
     */
    Definitions newDefinitions(String targetNamespace);

}
