/*
 * XML Type:  BindingOperationType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.BindingOperationType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl;


/**
 * An XML BindingOperationType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public interface BindingOperationType extends org.w3.ns.wsdl.ExtensibleDocumentedType
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(BindingOperationType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s2EAECD9BB08C57F25B7B261051DD8E7E").resolveHandle("bindingoperationtypeeafftype");
    
    /**
     * Gets array of all "input" elements
     */
    org.w3.ns.wsdl.BindingOperationMessageType[] getInputArray();
    
    /**
     * Gets ith "input" element
     */
    org.w3.ns.wsdl.BindingOperationMessageType getInputArray(int i);
    
    /**
     * Returns number of "input" element
     */
    int sizeOfInputArray();
    
    /**
     * Sets array of all "input" element
     */
    void setInputArray(org.w3.ns.wsdl.BindingOperationMessageType[] inputArray);
    
    /**
     * Sets ith "input" element
     */
    void setInputArray(int i, org.w3.ns.wsdl.BindingOperationMessageType input);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "input" element
     */
    org.w3.ns.wsdl.BindingOperationMessageType insertNewInput(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "input" element
     */
    org.w3.ns.wsdl.BindingOperationMessageType addNewInput();
    
    /**
     * Removes the ith "input" element
     */
    void removeInput(int i);
    
    /**
     * Gets array of all "output" elements
     */
    org.w3.ns.wsdl.BindingOperationMessageType[] getOutputArray();
    
    /**
     * Gets ith "output" element
     */
    org.w3.ns.wsdl.BindingOperationMessageType getOutputArray(int i);
    
    /**
     * Returns number of "output" element
     */
    int sizeOfOutputArray();
    
    /**
     * Sets array of all "output" element
     */
    void setOutputArray(org.w3.ns.wsdl.BindingOperationMessageType[] outputArray);
    
    /**
     * Sets ith "output" element
     */
    void setOutputArray(int i, org.w3.ns.wsdl.BindingOperationMessageType output);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "output" element
     */
    org.w3.ns.wsdl.BindingOperationMessageType insertNewOutput(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "output" element
     */
    org.w3.ns.wsdl.BindingOperationMessageType addNewOutput();
    
    /**
     * Removes the ith "output" element
     */
    void removeOutput(int i);
    
    /**
     * Gets array of all "infault" elements
     */
    org.w3.ns.wsdl.BindingOperationFaultType[] getInfaultArray();
    
    /**
     * Gets ith "infault" element
     */
    org.w3.ns.wsdl.BindingOperationFaultType getInfaultArray(int i);
    
    /**
     * Returns number of "infault" element
     */
    int sizeOfInfaultArray();
    
    /**
     * Sets array of all "infault" element
     */
    void setInfaultArray(org.w3.ns.wsdl.BindingOperationFaultType[] infaultArray);
    
    /**
     * Sets ith "infault" element
     */
    void setInfaultArray(int i, org.w3.ns.wsdl.BindingOperationFaultType infault);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "infault" element
     */
    org.w3.ns.wsdl.BindingOperationFaultType insertNewInfault(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "infault" element
     */
    org.w3.ns.wsdl.BindingOperationFaultType addNewInfault();
    
    /**
     * Removes the ith "infault" element
     */
    void removeInfault(int i);
    
    /**
     * Gets array of all "outfault" elements
     */
    org.w3.ns.wsdl.BindingOperationFaultType[] getOutfaultArray();
    
    /**
     * Gets ith "outfault" element
     */
    org.w3.ns.wsdl.BindingOperationFaultType getOutfaultArray(int i);
    
    /**
     * Returns number of "outfault" element
     */
    int sizeOfOutfaultArray();
    
    /**
     * Sets array of all "outfault" element
     */
    void setOutfaultArray(org.w3.ns.wsdl.BindingOperationFaultType[] outfaultArray);
    
    /**
     * Sets ith "outfault" element
     */
    void setOutfaultArray(int i, org.w3.ns.wsdl.BindingOperationFaultType outfault);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "outfault" element
     */
    org.w3.ns.wsdl.BindingOperationFaultType insertNewOutfault(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "outfault" element
     */
    org.w3.ns.wsdl.BindingOperationFaultType addNewOutfault();
    
    /**
     * Removes the ith "outfault" element
     */
    void removeOutfault(int i);
    
    /**
     * Gets the "ref" attribute
     */
    javax.xml.namespace.QName getRef();
    
    /**
     * Gets (as xml) the "ref" attribute
     */
    org.apache.xmlbeans.XmlQName xgetRef();
    
    /**
     * Sets the "ref" attribute
     */
    void setRef(javax.xml.namespace.QName ref);
    
    /**
     * Sets (as xml) the "ref" attribute
     */
    void xsetRef(org.apache.xmlbeans.XmlQName ref);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.w3.ns.wsdl.BindingOperationType newInstance() {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.w3.ns.wsdl.BindingOperationType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.w3.ns.wsdl.BindingOperationType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.w3.ns.wsdl.BindingOperationType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.w3.ns.wsdl.BindingOperationType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.w3.ns.wsdl.BindingOperationType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.w3.ns.wsdl.BindingOperationType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
