/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessagingStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.Date;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;

/**
 * This class provides methods for creation and retrieval of messaging
 * statistics in the message service.
 *
 * @author Sun Microsystems, Inc.
 */
public class MessagingStatistics
{
    /**
     * Count of the current number of active Message Exchanges.
     */
    private int mActiveExchanges;

    /**
     * Count of the total number of InOnly Message Exchanges created since the
     * last component or message service startup.
     */
    private long mInOnlyExchanges;

    /**
     * Count of the total number of RobustInOnly Message Exchanges created since
     * the last component or message service startup.
     */
    private long mRobustInOnlyExchanges;

    /**
     * Count of the total number of InOptionalOut Message Exchanges created
     * since the last component or message service startup.
     */
    private long mInOptionalOutExchanges;

    /**
     * Count of the total number of InOut Message Exchanges created since the
     * last component or message service startup.
     */
    private long mInOutExchanges;

    /**
     * Running total of times from the creation to the closing of all Message
     * Exchanges. This is used to compute the mean time to close an ME.
     */
    private long mTotalExchangeTimeToClose;

    /**
     * Count of the total number of failed Message Exchanges since the last
     * component or message service startup.
     */
    private long mFailedExchanges;

    /**
     * Count of the total number of transacted Message Exchanges since the last
     * component or message service startup.
     */
    private long mTransactedExchanges;

    /**
     * Count of the Message Exchange send's since the last
     * component or message service startup.
     */
    private long    mSends;

    /**
     * Count of the Message Exchange sendSync's since the last
     * component or message service startup.
     */
    private long    mSendSyncs;
        
    /**
     * Count of the Message Exchange accept's since the last
     * component or message service startup.
     */
    private long    mAccepts;

    /**
     * Count of the Message Exchange acceptTimeout's since the last
     * component or message service startup.
     */
    private long    mAcceptTimeouts;

    /**
     * Count of the Message Exchange completed with a Fault since the last
     * component or message service startup.
     */
    private long    mFaultedExchanges;
    
    /**
     * Time of last restart of either the component or the message service.
     */
    private Date mLastRestartTime;

    /**
     * Constant used to compute percentages.
     */
    private static final int ONE_HUNDRED = 100;

    /**
     * Constant used to compute rates.
     */
    private static final int MILLISECONDS_PER_HOUR = 3600000;

    /**
     * Constant used to compute rates.
     */
    private static final int MILLISECONDS_PER_MINUTE = 60000;

    /**
     * List of item names for CompositeData construction.
     */
    private static final String[] ITEM_NAMES = {
        "ActiveExchanges",
        "ActiveExchangeRate",
        "InOnlyExchanges",
        "RobustInOnlyExchanges",
        "InOptionalOutExchanges",
        "InOutExchanges",
        "ExchangeMeanTimeToClose",
        "ExchangeSuccessRate",
        "FailedExchanges",
        "TransactedExchanges",
        "FaultedExchanges",
        "SentExchanges",
        "SynchronousSentExchanges",
        "AcceptExchanges",
        "AcceptExchangesTimeout"
    };

    /**
     * List of descriptions of items for ComponsiteData construction.
     */
    private static final String ITEM_DESCRIPTIONS[] = {
        "Number of message exchanges currently active",
        "Current rate of active exchanges per hour",
        "Total number of InOnly message exchanges processed",
        "Total number of RobustInOnly message exchanges processed",
        "Total number of InOptionalOut message exchanges processed",
        "Total number of InOut message exchanges  processed",
        "Mean time to close a message exchange",
        "Message exchange success rate",
        "Total number of failed message exchanges",
        "Total number of transacted message exchanges",
        "Total number of faulted message exchanges",
        "Total number of message exchange send operations",
        "Total number of message exchange sendSync operations",
        "Total number of message exchange accept operations",
        "Total number of message exchange accept timeouts"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType ITEM_TYPES[] = {
         SimpleType.INTEGER,
         SimpleType.DOUBLE,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.FLOAT,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG         
    };

    /**
     * CompositeType used to describe CompositeData representing messaging
     * statistics.
     */
    private static CompositeType sMessagingStatisticsType;

    /**
     * Static initializer.
     */
    {
        try
        {
            sMessagingStatisticsType = new CompositeType(
                "MessagingStatistics",
                "Message exchange statistics",
                ITEM_NAMES, ITEM_DESCRIPTIONS, ITEM_TYPES);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            ; // ignore this for now
        }
    }

    //
    // Methods to retrieve statistics.
    //

    /**
     * Get the time of the last restart of the associated entity.
     * @return The last restart time.
     */
    public Date getLastRestartTime()
    {
        return mLastRestartTime;
    }

    /**
     * Get the current number of active message exchanges.
     * @return The number of active message exchanges.
     */
    public Integer getActiveExchanges()
    {
        return new Integer(mActiveExchanges);
    }

    /**
     * Get the current active message exchange rate per hour.
     * @return The number of active message exchanges per hour.
     */
    public Double getActiveExchangeRate()
    {
        // Compute the total number of MessageExchanges
        double totalExchanges = mInOnlyExchanges + mRobustInOnlyExchanges +
            mInOptionalOutExchanges + mInOutExchanges;

        // Compute the number of hours since the component started
        long totalTime = new Date().getTime() - mLastRestartTime.getTime();
        double totalHours = totalTime / MILLISECONDS_PER_HOUR;
        
        // Compute exchange rate.
        double exchangeRate = 0;
        if ( 0 < totalExchanges && 0 < totalHours )
        {
            exchangeRate = totalExchanges / totalHours;
        }
         
        return new Double(exchangeRate);
    }

    /**
     * Get the total number of InOnly Message Exchanges created since the
     * last message service startup.
     * @return The number of InOnly MEs.
     */
    public Long getInOnlyExchanges()
    {
        return new Long(mInOnlyExchanges);
    }

    /**
     * Get the total number of RobustInOnly Message Exchanges created since the
     * last message service startup.
     * @return The number of RobustInOnly MEs.
     */
    public Long getRobustInOnlyExchanges()
    {
        return new Long(mRobustInOnlyExchanges);
    }

    /**
     * Get the total number of InOptionalOut Message Exchanges created since the
     * last message service startup.
     * @return The number of InOptionalOut MEs.
     */
    public Long getInOptionalOutExchanges()
    {
        return new Long(mInOptionalOutExchanges);
    }

    /**
     * Get the total number of InOut Message Exchanges created since the
     * last message service startup.
     * @return The number of InOut MEs.
     */
    public Long getInOutExchanges()
    {
        return new Long(mInOutExchanges);
    }

    /**
     * Get the total number of Message Exchanges of all types created since the 
     * last message service startup.
     * @return The total number of MEs.
     */
    public Long getTotalExchanges()
    {
        return new Long(mInOnlyExchanges + mRobustInOnlyExchanges +
            mInOptionalOutExchanges + mInOutExchanges);
    }

    /**
     * Get the total time from the creation to the closing of all Message
     * Exchanges.
     * @return The total time to close for all MEs.
     */
    public long getTotalExchangeTimeToClose()
    {
        return mTotalExchangeTimeToClose;
    }

    /**
     * Get the average time from the creation to the closing of a Message
     * Exchange.
     * @return The mean time to close an ME.
     */
    public Long getExchangeMeanTimeToClose()
    {
        // Compute the total number of MessageExchanges
        long totalTime = mTotalExchangeTimeToClose;
        long totalExchanges = mInOnlyExchanges + mRobustInOnlyExchanges +
            mInOptionalOutExchanges + mInOutExchanges;
        long meanTime = 0;
        if ( 0 < totalExchanges )
        {
            meanTime = totalTime / totalExchanges;
        }
        return new Long(meanTime);
    }

    /**
     * Get the total number of failed Message Exchanges since the last message
     * service startup.
     * @return The number of failed MEs.
     */
    public Long getFailedExchanges()
    {
        return new Long(mFailedExchanges);
    }

    /**
     * Get the total number of faulted Message Exchanges since the last message
     * service startup.
     * @return The number of failed MEs.
     */
    public Long getFaultedExchanges()
    {
        return new Long(mFaultedExchanges);
    }
    
    /**
     * Get the total number of  Message Exchange send operations since the last message
     * service startup.
     * @return The number of failed MEs.
     */
    public Long getSendExchanges()
    {
        return new Long(mSends);
    }
    
    /**
     * Get the total number of Message Exchange sendSync operations since the last message
     * service startup.
     * @return The number of failed MEs.
     */
    public Long getSendSyncExchanges()
    {
        return new Long(mSendSyncs);
    }
    
    /**
     * Get the total number of Message Exchange accept operations since the last message
     * service startup.
     * @return The number of failed MEs.
     */
    public Long getAcceptExchanges()
    {
        return new Long(mAccepts);
    }
    /**
     * Get the total number of Message Exchange accept timeout operations since the last message
     * service startup.
     * @return The number of failed MEs.
     */
    public Long getAcceptTimeoutExchanges()
    {
        return new Long(mAcceptTimeouts);
    }
    
    /**
     * Get the total number of transacted Message Exchanges since the last 
     * message service startup.
     * @return The number of transacted MEs.
     */
    public Long getTransactedExchanges()
    {
        return new Long(mTransactedExchanges);
    }

    /**
     * Get the success rate for Message Exchanges, as a percentage.
     * @return The percentage of MEs that were successful.
     */
    public Float getExchangeSuccessRate()
    {
        // Compute the total number of MessageExchanges
        if ( 0 == mFailedExchanges )
        {
            return new Float(ONE_HUNDRED);
        }
        else
        {
            long failedExchanges = mFailedExchanges;
            long totalExchanges = mInOnlyExchanges + mRobustInOnlyExchanges +
                mInOptionalOutExchanges + mInOutExchanges;
            float successRate =
                ((totalExchanges - failedExchanges) / totalExchanges) * ONE_HUNDRED;
            return new Float(successRate);
        }
    }

    //
    // Methods to update statistics.
    //

    /**
     * Decrement the current number of active message exchanges.
     */
    public synchronized void decrementActiveExchanges()
    {
        --mActiveExchanges;
    }

    /**
     * Increment the current number of active message exchanges.
     */
    public synchronized void incrementActiveExchanges()
    {
        ++mActiveExchanges;
    }

    /**
     * Increment the total number of InOnly Message Exchanges created since the
     * last message service startup.
     */
    public synchronized void incrementInOnlyExchanges()
    {
        ++mInOnlyExchanges;
    }

    /**
     * Increment the total number of RobustInOnly Message Exchanges created
     * since the last message service startup.
     */
    public synchronized void incrementRobustInOnlyExchanges()
    {
        ++mRobustInOnlyExchanges;
    }

    /**
     * Increment the total number of InOptionalOut Message Exchanges created
     * since the last message service startup.
     */
    public synchronized void incrementInOptionalOutExchanges()
    {
        ++mInOptionalOutExchanges;
    }

    /**
     * Increment the total number of InOut Message Exchanges created since the
     * last message service startup.
     */
    public synchronized void incrementInOutExchanges()
    {
        ++mInOutExchanges;
    }

    /**
     * Add the time to close a Message Exchange to the running total used to
     * compute the average.
     * @param timeToClose The time to close an ME in milliseconds.
     */
    public synchronized void addTotalExchangeTimeToClose(long timeToClose)
    {
            mTotalExchangeTimeToClose += timeToClose;
    }

    /**
     * Increment the total number of failed Message Exchanges since the last
     * message service startup.
     */
    public synchronized void incrementFailedExchanges()
    {
        ++mFailedExchanges;
    }

    /**
     * Increment the total number of transacted Message Exchanges since the last
     * message service startup.
     */
    public synchronized void incrementTransactedExchanges()
    {
        ++mTransactedExchanges;
    }

    /**
     * Set the time of the last restart of the associated entity (component or
     * message service);
     */
    public synchronized void setLastRestartTime(Date restartTime)
    {
        mLastRestartTime = restartTime;
    }

    /**
     * Increment the total number of Message Exchange send operations since the last
     * message service startup.
     */
    public synchronized void incrementSends()
    {
        mSends++;
    }

    /**
     * Increment the total number of Message Exchange sendSync operations since the last
     * message service startup.
     */
    public synchronized void incrementSendSyncs()
    {
        mSendSyncs++;
    }
    
    /**
     * Increment the total number of Message Exchange accept operations since the last
     * message service startup.
     */
    public synchronized void incrementAccepts()
    {
        mAccepts++;
    }
    
    /**
     * Increment the total number of Message Exchange accept timeout operations since the last
     * message service startup.
     */
    public synchronized void incrementAcceptTimeouts()
    {
        mAcceptTimeouts++;
    }
    
    /**
     * Increment the total number of faulted Message Exchanges since the last
     * message service startup.
     */
    public synchronized void incrementFaults()
    {
        mFaultedExchanges++;
    }

    /**
     * Reset all statistics.
     */
    public synchronized void resetStatistics()
    {
        mActiveExchanges = 0;
        mInOnlyExchanges = 0;
        mRobustInOnlyExchanges = 0;
        mInOptionalOutExchanges = 0;
        mInOutExchanges = 0;
        mTotalExchangeTimeToClose = 0;
        mFailedExchanges = 0;
        mTransactedExchanges = 0;
        mSends = 0;
        mSendSyncs = 0;
        mAccepts = 0;
        mAcceptTimeouts = 0;
        mFaultedExchanges = 0;
        mLastRestartTime = new Date();
    }

    /**
     * Convert this instance to a CompositeData type.
     */
    public CompositeData toCompositeData()
        throws javax.management.openmbean.OpenDataException
    {
        Object values[] = {
            getActiveExchanges(),
            getActiveExchangeRate(),
            getInOnlyExchanges(),
            getRobustInOnlyExchanges(),
            getInOptionalOutExchanges(),
            getInOutExchanges(),
            getExchangeMeanTimeToClose(),
            getExchangeSuccessRate(),
            getFailedExchanges(),
            getTransactedExchanges(),
            getFaultedExchanges(),
            getSendExchanges(),
            getSendSyncExchanges(),
            getAcceptExchanges(),
            getAcceptTimeoutExchanges()
        };

        return new CompositeDataSupport(
            sMessagingStatisticsType,
            ITEM_NAMES,
            values);
    }
}
