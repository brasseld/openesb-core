/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.stats.METimestamps;

import com.sun.jbi.messaging.util.Translator;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.transaction.Transaction;

import javax.xml.namespace.QName;

/** This serves as the container about all of the state of a message exchange.
 *  The objects presented to the binding and engine are instances of
 *  MessageExchangeProxy. The MessageExchangeProxy forwards all get/set
 *	requests to here. This class maintains a reference to each.
 * @author Sun Microsystems, Inc.
 */
public class MessageExchangeImpl
    implements javax.jbi.messaging.MessageExchange, Cloneable 
{
    /** NMR sendSync spec value. Note: no name was given in the spec. */
    private static final String         SENDSYNC_PROPERTY_NAME = "javax.jbi.messaging.sendSync";

    /** Default factory for normalized messages. */
    private static MessageFactory       sMsgFac;
    
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
    /** Service reference for this exchange. */
    private QName                       mService;
    /** Interface name for this exchange. */
    private QName                       mInterfaceName;
    /** Endpoint reference for this exchange. */
    private ServiceEndpoint             mEndpoint;
    /** Operation name for this exchange. */
    private QName                       mOperation;
    /** WSDL Fault. */
    private Fault                       mFault;
    /** Stores messages for this exchange. */
    private HashMap                     mMessages;
    /** Stores exchange properties. */
    private HashMap                     mProperties;
    private HashMap                     mDeltaProperties;
    
    /** Unique Id for exchange. */
    private String                      mExchangeId;
    /** Status of the exchange. */
    private ExchangeStatus              mStatus;
    /** Error information. */
    private Exception                   mError;
    
    /** Transaction for this exchange.  Since the javax.transaction is not 
     *  part of Java SE, we need to store the Transaction reference as an object.
     */
    private Object                      mTransaction;
    
    private boolean                     mAutoSuspendResume;
    /** Source MessageExchange.	*/
    private MessageExchangeProxy        mSource;
    /** MessageService          */
    private MessageService              mMsgSvc;
    /** Created Remotely    */
    private boolean                    mRemote;
    /** SendSync used on last send */
    private boolean                    mSendSync;
    /** MessageExchange statistics. */
    private METimestamps                mStamps;
    
    /** Static init of random number generator and message factory. */
    static 
    {
         sMsgFac = MessageFactory.getInstance();
    }
    
    /**
     * Create a new message exchange for the given proxy.
     * @param proxy MessageExchangeProxy for pattern
     */
    MessageExchangeImpl(MessageExchangeProxy proxy, MessageService msgSvc) 
    {   
        boolean         statisticsEnabled = msgSvc.areStatisticsEnabled();
        
        mMsgSvc = msgSvc;
        mSource = proxy;
        mAutoSuspendResume = false;
        proxy.setMessageExchange(this, statisticsEnabled);
        if (statisticsEnabled)
        {
            mStamps = new METimestamps();
        }
        init();
    }
    
    /**
     * Create a new message exchange for the given proxy and exchange.
     * USed by the Observer mechanism to create the observed at that point in time.
     * @param proxy MessageExchangeProxy for pattern
     */
    MessageExchangeImpl(MessageExchangeProxy proxy, MessageExchangeImpl source, MessageService msgSvc) 
    {   
        mMsgSvc = msgSvc;
        mSource = proxy;
        mAutoSuspendResume = false;
        proxy.setMessageExchange(this, false);
        mExchangeId = source.mExchangeId;
        mProperties = (HashMap)source.mProperties.clone();
        mDeltaProperties = (HashMap)source.mDeltaProperties.clone();
        mStatus = source.mStatus;
        mFault = source.mFault;
        mInterfaceName = source.mInterfaceName;
        mOperation = source.mOperation;
        mService = source.mService;
        mEndpoint = source.mEndpoint;
        mError = source.mError;
        mMessages   = new HashMap();
    }
    
    /** Initialize instance vars and set state to 0. */
    private void init() 
    {
        mExchangeId = mMsgSvc.generateNextId();
        mProperties = new HashMap();
        mDeltaProperties = new HashMap();
        mMessages   = new HashMap();
        mStatus = ExchangeStatus.ACTIVE;
        mRemote = false;
    }
    
    /** Deferred to message factory. */
    public Fault createFault()
        throws MessagingException 
    {
        return sMsgFac.createFault();
    }
    
    /** Deferred to message factory. */
    public NormalizedMessage createMessage()
        throws MessagingException 
    {
        return sMsgFac.createMessage();
    }
    
    /** Obvious. */
    public ServiceEndpoint getEndpoint() 
    {
        return mEndpoint;
    }
    
    /** Obvious. */
    public Exception getError() 
    {
        return mError;
    }
    
    /** Obvious. */
    public String getExchangeId() 
    {
        return mExchangeId;
    }
    
    /** Obvious. */
    public Fault getFault() 
    {
        return mFault;
    }
    
    /** Return the message associated with the given reference id. */
    public NormalizedMessage getMessage(String name) 
    {
        return (NormalizedMessage)mMessages.get(name);
    }
    
    /** Obvious. */
    public QName getOperation() 
    {
        return mOperation;
    }
    
    /** Obvious. */
    public URI getPattern() 
    {
        return (mSource == null ? null : mSource.getPattern());
    }
    
    /** Obvious. */
    public Object getProperty(String name) 
    {
        Object prop = mDeltaProperties.get(name);
        if (prop == null)
        {
            prop = mProperties.get(name);
        }
        if (prop == null && mSendSync && name.equals(SENDSYNC_PROPERTY_NAME))
	{
            prop = Boolean.valueOf(true);
        }
        return (prop);
    }
    
    /** Obvious. */
    public Set getPropertyNames()
    {
        Set names = new HashSet();

        names.addAll(mProperties.keySet());
        names.addAll(mDeltaProperties.keySet()); 
        if (mSendSync)
        {
            names.add(SENDSYNC_PROPERTY_NAME);
        }
        return (names);
    }
    
    public MessageExchange.Role getRole()
    {
        return (null);
    }
    
    /** Obvious. */
    public QName getService() 
    {
        return mService;
    }
    
    /** Obvious. */
    public ExchangeStatus getStatus() 
    {
        return mStatus;
    }
  
    /** Special for serialization.  */
    Set getDeltaProperties()
    {
        return (mDeltaProperties.entrySet());
    }
    
    /** Used for deserialization support. */
    void setExchangeId(String id)
    {
        mExchangeId = id;
        mRemote = true;
    }
    
    /** Only allowed on a freshly created exchange. */
    public void setEndpoint(ServiceEndpoint endpoint) 
    {
        mEndpoint = endpoint;
    }
    
    /** Sets the exception and automatically advances status to Error. */
    public void setError(Exception error) 
    {
        mError  = error;
    }
    
    /** Set a fault on the exchange. */
    public void setFault(Fault fault)
    {
        mFault = fault;
    }
    
    /** Set a message on the exchange. */
    public void setMessage(NormalizedMessage msg, String name)
    {
        if (msg == null)
        {
            mMessages.remove(name);
        }
        else
        {
            mMessages.put(name, msg);
        }
    }
    
    /** Only allowed on a freshly created exchange. */
    public void setOperation(QName name) 
    {
        mOperation = name;
    }
    
    /** Obvious. */
    public void setProperty(String name, Object obj) 
    {
        if (name.equals(JTA_TRANSACTION_PROPERTY_NAME))
        {
            try
            {
                setTransactionContext((Transaction)obj);
            }
            catch (javax.jbi.messaging.MessagingException msgEx)
            {
                mLog.warning(msgEx.toString());
            }
        }
        if (!name.equals(SENDSYNC_PROPERTY_NAME))
        {
            mDeltaProperties.put(name, obj);
        }
    }
    
    public void mergeProperties()
    {
        mProperties.putAll(mDeltaProperties);
        mDeltaProperties.clear();
    }
    
    void setSyncProperty(boolean isSync)
    {
	mSendSync = isSync;
    }

    boolean isRemote()
    {
        return (mRemote);
    }
    
    /** Only allowed on a freshly created exchange. */
    public void setService(QName service) 
    {
        mService = service;
    }
    
    /** Set status of the exchange.  ERROR status is allowed at any point. */
    public void setStatus(ExchangeStatus status) 
    {
        mStatus = status;
    }    
    
    public QName getInterfaceName()
    {
        return mInterfaceName;
    }
    
    public void setInterfaceName(QName interfaceName)
    {
        mInterfaceName = interfaceName;
    }
    
    /** Only allowed on a freshly created exchange. */
    void setTransactionContext(Transaction xact)
        throws javax.jbi.messaging.MessagingException 
    {
        if (xact == null)
        {
            try 
            {
                xact = mMsgSvc.getTransactionManager().getTransaction();
            }
            //catch (javax.transaction.SystemException se) 
            catch (Exception ex)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_GET_DEFAULT_TRANSACTION));
            }
        }
        mTransaction = xact;
        
    }
    
    /** Only allowed on a freshly created exchange. */
    public Transaction getTransactionContext()
    {
        return (Transaction)mTransaction;
    }
    
    /** Only allowed on a freshly created exchange. */
    public boolean isTransacted() 
    {
        return (mTransaction != null);
    }
    
    MessageExchangeProxy getSource()
    {
        return (mSource);
    }

    /** Suspend the current transaction if defined. */
    void suspendTX()
        throws javax.jbi.messaging.MessagingException 
    {
        if (mTransaction != null) 
        {
            try 
            {
                if (mAutoSuspendResume)
                {
                    if (mTransaction == mMsgSvc.getTransactionManager().getTransaction()) 
                    {
                        if (mLog.isLoggable(Level.FINE))
                        {
                            mLog.finer("Suspending transaction on exchange " + mExchangeId);
                        }
                        mMsgSvc.getTransactionManager().suspend();
                    }
                }
                else
                {
                    if (mMsgSvc.getTransactionManager().getTransaction() == mTransaction)
                    {
                        throw new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.MUST_SUSPEND));
                    }
                }
            }
            //catch (javax.transaction.SystemException se) 
            catch (Exception ex) 
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_SUSPEND));
            }
        }
    }
    
    /** Resume the current transaction if defined. */
    void resumeTX()
        throws javax.jbi.messaging.MessagingException 
    {
        if (mTransaction != null && mAutoSuspendResume) 
        {
            try 
            {
                if (mLog.isLoggable(Level.FINE))
                {
                    mLog.finer("Resuming transaction on exchange " + mExchangeId);
                }
                mMsgSvc.getTransactionManager().resume((Transaction)mTransaction);
            }
            catch (javax.transaction.InvalidTransactionException ite) 
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_RESUME_INVALID));
            }
            //catch (javax.transaction.SystemException se) 
            catch (Exception ex) 
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_RESUME));
            }
        }
    }
    
    //
    //  Capture a timestamp associated with the given tag.
    //
    void capture(byte tag)
    {
        if (mStamps != null)
        {
            mStamps.capture(tag);
        }
    }
    
    METimestamps getTimestamps()
    {
        return (mStamps);
    }
    
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        sb.append("\n        Status: ");
        sb.append(mStatus.equals(ExchangeStatus.ACTIVE) ? "ACTIVE" :
            (mStatus.equals(ExchangeStatus.DONE) ? "DONE" : "ERROR"));
        sb.append("  Location: ");
        sb.append(mRemote ? "REMOTE" : "LOCAL");
        sb.append("  Transaction:   ");
        sb.append(mTransaction == null ? "None" : mTransaction.toString());
        sb.append("\n        Service:   " );
        sb.append(mService);
        sb.append("\n        Endpoint:  ");
        sb.append(mEndpoint != null ? ((RegisteredEndpoint)mEndpoint).toExternalName() : "null");
        sb.append("\n        Operation: ");
        sb.append(mOperation == null ? "null" : mOperation);
        sb.append("\n        Interface: ");   
        sb.append(mInterfaceName == null ? "null" : mInterfaceName);
        sb.append("\n        Properties Count: ");
        sb.append(mProperties.size());
        sb.append("\n");
        if (mSendSync)
        {
            sb.append("          Name: " + SENDSYNC_PROPERTY_NAME);
            sb.append("\n          Value: true\n");
        }
        for (Iterator p = mProperties.entrySet().iterator(); p.hasNext(); )
        {
            Map.Entry   me = (Map.Entry)p.next();
            sb.append("          Name: ");
            sb.append((String)me.getKey());
            if (me.getValue() instanceof String)
            {
                sb.append("\n            Value: ");
                sb.append((String)me.getValue());
            }
            else if (me.getValue() != null)
            {  
                sb.append("\n          Value(Type): ");
                sb.append(me.getValue() == null ? "null" : me.getValue().getClass().getName());
            }
            sb.append("\n");
        }
        sb.append("        DeltaProperties Count: ");
        sb.append(mDeltaProperties.size());
        sb.append("\n");
        for (Iterator p = mDeltaProperties.entrySet().iterator(); p.hasNext(); )
        {
            Map.Entry   me = (Map.Entry)p.next();
            sb.append("          Name: ");
            sb.append((String)me.getKey());
            if (me.getValue() instanceof String)
            {
                sb.append("\n          Value: ");
                sb.append((String)me.getValue());
            }
            else if (me.getValue() != null)
            {  
                sb.append("\n          Value(Type): ");
                sb.append(me.getValue() == null ? "null" : me.getValue().getClass().getName());
            }
            sb.append("\n");
        }
        sb.append("        Message Count:   ");
        sb.append(mMessages.size());
        sb.append("\n");
        for (Iterator m = mMessages.entrySet().iterator(); m.hasNext(); )
        {
            Map.Entry   me = (Map.Entry)m.next();
            
            sb.append("          Message Name: ");
            sb.append((String)me.getKey());
            sb.append("\n");
            sb.append(me.getKey() == null ? "null" : me.getValue().toString());
        }
        if (mFault != null)
        {
            sb.append("        Fault: \n");
            sb.append(mFault.toString());
        }
        if (mStamps != null)
        {
            sb.append("        " + mStamps.toString());
        }
       return (sb.toString());
    }
}
