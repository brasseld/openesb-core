/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.Translator;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import java.util.concurrent.ConcurrentHashMap;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.xml.namespace.QName;

import javax.jbi.servicedesc.ServiceEndpoint;

/** In-memory store of all endpoints that have been registered with the NMS.
 * @author Sun Microsystems, Inc.
 */
public class EndpointRegistry implements ConnectionManager
{
    /** Me. */
    private static EndpointRegistry                             mMe;
    private long                                                mTotalEndpoints;
    
    /** List of internal endpoints. */
    private ConcurrentHashMap<Endpoint, RegisteredEndpoint>     mInternalEndpoints;
    private ConcurrentHashMap<QName, VectorArray>               mIEbyService;
    private ConcurrentHashMap<QName, VectorArray>               mIEbyInterface;
    private ConcurrentHashMap<String, RegisteredEndpoint>       mInternalEndpointNames;
    private LinkedList<RegisteredEndpoint>                      mPendingInternalInterfaces;
    private long                                                mTotalInternalEndpoints;
    
    /** List of external endpoints. */
    private ConcurrentHashMap<Endpoint, RegisteredEndpoint>     mExternalEndpoints;
    private ConcurrentHashMap<QName, VectorArray>               mEEbyService;
    private ConcurrentHashMap<QName, VectorArray>               mEEbyInterface;
    private LinkedList<RegisteredEndpoint>                      mPendingExternalInterfaces;
    private long                                                mTotalExternalEndpoints;
    
    /** List of mapped endpoints. */
    private ConcurrentHashMap<Endpoint, LinkedEndpoint>         mLinkedEndpoints;
    private ConcurrentHashMap<String, RegisteredEndpoint>       mLinkedEndpointNames;
    private long                                                mTotalLinkedEndpoints;
      
    /** Map of interface (QName) to endpoint (Endpoint) connections. */
    private ConcurrentHashMap<QName, Endpoint>                  mInterfaceConnections;
    
    private MessageService                                      mMsgSvc;
    
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
    /** No public access. */
    private EndpointRegistry()
    {
        mInternalEndpoints      = new ConcurrentHashMap();
        mIEbyService            = new ConcurrentHashMap();
        mIEbyInterface          = new ConcurrentHashMap();
        mInternalEndpointNames  = new ConcurrentHashMap();
        mPendingInternalInterfaces = new LinkedList();
        mExternalEndpoints      = new ConcurrentHashMap();
        mEEbyService            = new ConcurrentHashMap();
        mEEbyInterface          = new ConcurrentHashMap();
        mPendingExternalInterfaces = new LinkedList();
        mLinkedEndpoints        = new ConcurrentHashMap();
        mLinkedEndpointNames  = new ConcurrentHashMap();
        mInterfaceConnections   = new ConcurrentHashMap();
    }
    
    /** Get an instance of the registry. */
    static final synchronized EndpointRegistry getInstance()
    {
        if (mMe == null)
        {
            mMe = new EndpointRegistry();
        }        
        return mMe;
    }
    
    synchronized void setMessageService(MessageService ms)
    {
        mMsgSvc = ms;
    }
    
    boolean statisticsEnabled()
    {
        return (mMsgSvc.areStatisticsEnabled());
    }
    
    void zeroStatistics()
    {
        for (RegisteredEndpoint re : mInternalEndpoints.values())
        {
            re.zeroStatistics();
        }
        for (LinkedEndpoint le : mLinkedEndpoints.values())
        {
            le.zeroStatistics();
        }
    }
    
    /**######################################################################
     * ####################### EXTERNAL ENDPOINTS ###########################
     * ####################################################################*/    
    
    public synchronized RegisteredEndpoint registerExternalEndpoint(ServiceEndpoint endpoint, String ownerId)
        throws javax.jbi.messaging.MessagingException
    {        
        RegisteredEndpoint re;
        Endpoint           ep = new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName());
        
        re = (RegisteredEndpoint)mExternalEndpoints.get(ep);        
        if (re != null)
        {
            // Duplicate registrations are not permitted for external endpoints
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.DUPLICATE_ENDPOINT,
                        new Object[] {re.toExternalName(), re.getOwnerId()}));
        }
        
        re = new ExternalEndpoint(endpoint, ownerId);
        mEEbyService.put(endpoint.getServiceName(), new VectorArray(mEEbyService.get(endpoint.getServiceName()), re));
        mExternalEndpoints.put(ep, re);
        mPendingExternalInterfaces.add(re);
        mTotalEndpoints++;

        if (mLog.isLoggable(Level.FINE))
        {
            mLog.fine("Registered External endpoint: " + re.toExternalName());
        }
        return re;
    }
    
    public RegisteredEndpoint getExternalEndpoint(QName service, String endpoint)
    {
        return ((RegisteredEndpoint)mExternalEndpoints.get(new Endpoint(service, endpoint)));        
    }
    
    public RegisteredEndpoint[] getExternalEndpointsForService(QName service)
    {
        VectorArray va = mEEbyService.get(service);
        return (va == null ? new RegisteredEndpoint[0] : va.mArray);
    }
    
    public RegisteredEndpoint[] getExternalEndpointsForInterface(QName interfaceName)
    {
        RegisteredEndpoint[]    endpoints;
        Endpoint                link;
        
        //
        //  Check if the caller wants all of the endpoints of just ones that implement the named interface.
        //
        if (interfaceName != null)
        {
            if ((link = (Endpoint)mInterfaceConnections.get(interfaceName)) != null)
            {
                RegisteredEndpoint  ep;

                ep = (RegisteredEndpoint)mExternalEndpoints.get(new Endpoint(link.mServiceName, link.mEndpointName));        

                if (ep == null)
                {
                     endpoints = new RegisteredEndpoint[0];
                }
                else
                {            
                    endpoints = new RegisteredEndpoint[] {ep};
                }
            }
            else
            {
                // no connection, search through all of the available endpoints
                endpoints = getEndpointsForInterface(
                    interfaceName, RegisteredEndpoint.EXTERNAL);
            }
        }
        else
        {
            //
            //  Don't allow updates so that the size() doesn't change concurrently.
            //'
            synchronized (this)
            {
                endpoints = new RegisteredEndpoint[mExternalEndpoints.size()];
                mExternalEndpoints.values().toArray(endpoints);
            }
        }
       
        return endpoints;
    }
        
    /**######################################################################
     * ####################### INTERNAL ENDPOINTS ###########################
     * ####################################################################*/
    
    public synchronized RegisteredEndpoint registerInternalEndpoint(
        QName service, String endpoint, String ownerId)
        throws javax.jbi.messaging.MessagingException
    {        
        RegisteredEndpoint re;
        Endpoint           ep = new Endpoint(service, endpoint);
        
        // check to see if it already exists
        re = (RegisteredEndpoint)mInternalEndpoints.get(ep);        
        
        if (re != null)
        {
            // Duplicate registrations are permitted if it's the same component.
            if (!re.getOwnerId().equals(ownerId))
            {
                throw new javax.jbi.messaging.MessagingException(
                     Translator.translate(LocalStringKeys.DUPLICATE_ENDPOINT,
                        new Object[] {re.toExternalName(), re.getOwnerId()}));
            }
            return re;
        }
        
        re = new InternalEndpoint(service, endpoint, ownerId);
        mIEbyService.put(service, new VectorArray(mIEbyService.get(service), re));
        mInternalEndpoints.put(ep, re);
        mPendingInternalInterfaces.add(re);
        mInternalEndpointNames.put(re.toExternalName(), re);
        mTotalEndpoints++;
        
        if (mLog.isLoggable(Level.FINE))
        {
            mLog.fine("Registered Internal endpoint: " + re.toExternalName());
        }
        return re;
    }    
    
    /** Return a registered endpoint with the specified name, if one exists. */
    public RegisteredEndpoint getInternalEndpoint(QName service, String endpoint)
    {
        RegisteredEndpoint re;
        Endpoint           e = new Endpoint(service, endpoint);
        
        // check for a service connection first
        re = (RegisteredEndpoint)mLinkedEndpoints.get(e);
        
        if (re == null)
        {
            // no mapping, see if an internal endpoint exists
            re = (RegisteredEndpoint)mInternalEndpoints.get(e);
        }
        
        return re;
    } 
    
    /** Return a registered endpoint with the specified name, if one exists. */
    public RegisteredEndpoint getInternalEndpointByName(String epName)
    {
        return ((RegisteredEndpoint)mInternalEndpointNames.get(epName));
    }   
    
    /** Return a linked endpoint with the specified name, if one exists. */
    public RegisteredEndpoint getLinkedEndpointByName(String epName)
    {
        return ((RegisteredEndpoint)mLinkedEndpointNames.get(epName));
    }    
        
        
    /** Return a list of linked endpoint owned by the specified channel. */
    public String[]  getLinkedEndpointsByChannel(String dcName)
    {
        LinkedEndpoint[]        linkedEndpoints;
        LinkedList<String>      ll = new LinkedList();
        
        
        synchronized (this)
        {
            linkedEndpoints = new LinkedEndpoint[mLinkedEndpoints.size()];
            mLinkedEndpoints.values().toArray(linkedEndpoints);
        }
        for (int i = 0;i < linkedEndpoints.length; i++ )
        {
            if (linkedEndpoints[i].getOwnerId().equals(dcName))
            {
                ll.add(linkedEndpoints[i].toExternalName());
            }
        }
        return (ll.toArray(new String[ll.size()]));
    }    
        
    /** Retrieves all internal endpoints, including linked endpoints established
     *  through service connections.
     *  @param service the service QName
     *  @param convertLinks true if a service connection link should be converted to 
     *   its corresponding internal (e.g. hard) endpoint.  False if the linked
     *   endpoint should be returned directly.
     */
    RegisteredEndpoint[] getInternalEndpointsForService(QName service, boolean convertLinks)
    {
        RegisteredEndpoint  endpoints[];
        boolean            cloned = false;
        
        //
        //  Check if the called wants all endpoints or just then ones that implemented the named service.
        //
        if (service != null)
        {
            VectorArray va = mIEbyService.get(service);
            endpoints =  va == null ? new RegisteredEndpoint[0] : va.mArray;
        }
        else
        {
            //
            //  Don't allow updates so that the size() doesn't change concurrently.
            //
            synchronized (this)
            {
                endpoints = new RegisteredEndpoint[mInternalEndpoints.size()];
                mInternalEndpoints.values().toArray(endpoints);
            }
        }
        
        if (convertLinks)
        {          
            for (int i = 0; i < endpoints.length; i++)
            {    
                LinkedEndpoint      le;
                RegisteredEndpoint  re = endpoints[i];
                
                //
                //  All linked endpoints appear at the front of the list.
                //
                if (re instanceof LinkedEndpoint)
                {
                    //
                    //  If we find a Linked endpoint, clone the array since we need to
                    //  change the contents.
                    //
                    if (!cloned)
                    {
                        endpoints = endpoints.clone();
                        cloned = true;
                    }

                    le = (LinkedEndpoint)re;
                    re = (RegisteredEndpoint)mInternalEndpoints.get(new Endpoint(le.getServiceLink(), le.getEndpointLink()));
                    if (re == null)
                    {
                         continue;
                    }
                    endpoints[i] = re;
                    continue;
                }
                break;
            }
        }
        
        return (endpoints);
    }
    
    /** Find internal endpoints that implement the specified interface.  If
     *  a service connection exists for the interface, that connection is 
     *  used exclusive of any other services provided in JBI.
     */
    public RegisteredEndpoint[] getInternalEndpointsForInterface(
        QName interfaceName)
    {
        RegisteredEndpoint[]    endpoints;
        Endpoint                link;
        
        //
        //  Check if the caller wants all of the endpoints of just ones that implement the named interface.
        //
        if (interfaceName != null)
        {
            if ((link = (Endpoint)mInterfaceConnections.get(interfaceName)) != null)
            {
                RegisteredEndpoint  ep;

                ep = (RegisteredEndpoint)mInternalEndpoints.get(new Endpoint(link.mServiceName, link.mEndpointName));        

                if (ep == null)
                {
                     endpoints = new RegisteredEndpoint[0];
                }
                else
                {            
                    endpoints = new RegisteredEndpoint[] {ep};
                }
            }
            else
            {
                // no connection, search through all of the available endpoints
                endpoints = getEndpointsForInterface(
                    interfaceName, RegisteredEndpoint.INTERNAL);
            }
        }
        else
        {
            //
            //  Don't allow updates so that the size() doesn't change concurrently.
            //'
            synchronized (this)
            {
                endpoints = new RegisteredEndpoint[mInternalEndpoints.size()];
                mInternalEndpoints.values().toArray(endpoints);
            }
        }
       
        return endpoints;
    }
    
    /**######################################################################
     * ########################## ALL ENDPOINTS #############################
     * ####################################################################*/
    
    /** Remove an endpoint reference from the registry.  Invoking this method
     *  will also set the reference to inactive status.
     */
    public synchronized void removeEndpoint(ServiceEndpoint ref)
    {
       RegisteredEndpoint re;

       if (ref instanceof RegisteredEndpoint)
       {
           re = (RegisteredEndpoint)ref;
       }
       else
       {
           re = (RegisteredEndpoint)mExternalEndpoints.get(
                   new Endpoint(ref.getServiceName(), ref.getEndpointName()));        
       }
       
       if (re != null && re.isActive())
       {
            re.setActive(false);

            if (re.isInternal())
            {
                removeInternalEndpoint(re);           
            }
            else if (re.isExternal())
            {
                removeExternalEndpoint(re);           
            }
            else if (re.isLinked())
            {
                removeLinkedEndpoint(re);           
            }
            if (mLog.isLoggable(Level.FINE))
            {
                mLog.fine("Removed endpoint: " + re.toExternalName());
            }
        }
    }    
    
    
    /**######################################################################
     * ##################### CONNECTION MANAGEMENT ##########################
     * ####################################################################*/    
    
    public void addEndpointConnection(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint, Link linkType) 
        throws javax.jbi.messaging.MessagingException
    {
        if (addEndpointConnectionInternal(fromService, fromEndpoint,
                toService, toEndpoint, linkType))
        {
	    EndpointListener	el = mMsgSvc.getEndpointListener();

	    if (el != null)
	    {
	        el.addServiceConnection(fromService, fromEndpoint, toService, toEndpoint, linkType.toString());
	    }
        }
    }
    
    public boolean addEndpointConnectionInternal(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint, Link linkType) 
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint  re;
        LinkedEndpoint      link;
        Endpoint            ep = new Endpoint(fromService, fromEndpoint);
        VectorArray         va;
        boolean             added = true;

        mLog.fine("Add service connection for endpoint " + fromService + " "+ fromEndpoint);
        re = (LinkedEndpoint)mLinkedEndpoints.get(ep);
        link = new LinkedEndpoint(fromService, fromEndpoint, toService, toEndpoint, linkType);
        
        // If we find an existing link, see if it's identical to the one we
        // are trying to add.  If it's identical, we consider the operation a
        // NOP, otherwise it's an error.
        if (re != null)
        {
            if (!re.equals(link))
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.ENDPOINT_CONNECTION_EXISTS,
                    new Object[] {fromService, fromEndpoint, re.getOwnerId()}));
            }
            added = false;
        }
        else
        {
            va = new VectorArray(mIEbyService.get(fromService), link);   
            mIEbyService.put(fromService, va);
            mLinkedEndpoints.put(ep, link);
            mLinkedEndpointNames.put(link.toExternalName(), link);
            mTotalLinkedEndpoints++;
        }
        return (added);
    }
    
    public  boolean removeEndpointConnection(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint) 
    {
        boolean removed;
        
       if (removed = removeEndpointConnectionInternal(fromService, fromEndpoint,
            toService, toEndpoint))
       {
	    EndpointListener	el = mMsgSvc.getEndpointListener();

	    if (el != null)
	    {
               el.removeServiceConnection(fromService, fromEndpoint, toService, toEndpoint);
	    }
       }
        return (removed);
    }
    
    public synchronized boolean removeEndpointConnectionInternal(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint) 
    {
        boolean         isRemoved = false;
        LinkedEndpoint  link;
        
        mLog.fine("Remove service connection for endpoint " + fromService + " "+ fromEndpoint);
            
        // look for the linked endpoint
        link = (LinkedEndpoint)mLinkedEndpoints.get(new Endpoint(fromService, fromEndpoint));
        
        if (link != null)
        {
            removeEndpoint(link);
            isRemoved = true;
            mTotalLinkedEndpoints--;
        }
        
        return isRemoved;
    }
        
    public synchronized void addInterfaceConnection(
        QName fromInterface, QName toService, String toEndpoint)
        throws javax.jbi.messaging.MessagingException 
    {
        Endpoint        ep = new Endpoint(toService, toEndpoint);
        
        if (mInterfaceConnections.containsKey(fromInterface) &&
 	             !ep.equals(mInterfaceConnections.get(fromInterface)))
        {            
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.INTERFACE_CONNECTION_EXISTS,
                new Object[] {fromInterface}));
        }

        mLog.fine("Adding service connection for interface " + fromInterface);
        mInterfaceConnections.put(fromInterface, ep);
    }
    
    public synchronized boolean removeInterfaceConnection(
        QName fromInterface, QName toService, String toEndpoint)
    {
        mLog.fine("Removing service connection for interface " + fromInterface);
        return (mInterfaceConnections.remove(fromInterface) != null);
    }
    
    /** Translates a linked endpoint to its internal counterpart based on 
     *  a service connection specification.
     */
    public RegisteredEndpoint resolveLinkedEndpoint(LinkedEndpoint link)
    {
        return ((RegisteredEndpoint)mInternalEndpoints.get(
            new Endpoint(link.getServiceLink(), link.getEndpointLink())));
    }
    
    /**######################################################################
     * #################### IMPLEMENTATION PRIVATE ##########################
     * ####################################################################*/
    
    /** List all the endpoints for a given interface.
     */
   private RegisteredEndpoint[] getEndpointsForInterface(
        QName interfaceName, int type)
    {
        LinkedList<RegisteredEndpoint>          interfaces = type == RegisteredEndpoint.INTERNAL ?
                                                    mPendingInternalInterfaces : mPendingExternalInterfaces;
        ConcurrentHashMap<QName, VectorArray>   interfaceMap = type == RegisteredEndpoint.INTERNAL ?
                                                    mIEbyInterface : mEEbyInterface;
    
        for (;;)
        {
            RegisteredEndpoint  ep;
           
            //
            //  Drain the pending queue of endpoints to query for interface information.
            //
            synchronized (this)
            {
                ep = interfaces.poll();
            }
            if (ep == null)
            {
                break;
            }
            getInterfacesForEndpoint(ep);
        }
        VectorArray va = interfaceMap.get(interfaceName);
        return (va == null ? new RegisteredEndpoint[0] : va.mArray);
    }
    
    void getInterfacesForEndpoint(RegisteredEndpoint re)
    {
        LinkedList<RegisteredEndpoint>          interfaces = re.getType() == RegisteredEndpoint.INTERNAL ?
                                                    mPendingInternalInterfaces : mPendingExternalInterfaces;
        ConcurrentHashMap<QName, VectorArray>   interfaceMap = re.getType() == RegisteredEndpoint.INTERNAL ?
                                                    mIEbyInterface : mEEbyInterface;

        synchronized (this)
        {
            //
            //  Remove from the work list if present.
            //
            interfaces.remove(re);
            
            //
            //  Query component for WSDL and extract supported interfaces.
            //
            try
            {
                if (re.getInterfacesInternal() == null)
                {
                    re.parseDescriptor(mMsgSvc.queryDescriptor(re));
                }
                for (QName in : re.getInterfaces())
                {
                    interfaceMap.put(in, new VectorArray(interfaceMap.get(in), re));                           
                }
            }
            catch (javax.jbi.messaging.MessagingException msgEx)
            {
                mLog.warning(msgEx.toString());
            }
        }

    }

    private void removeInternalEndpoint(RegisteredEndpoint endpoint)
    {
        VectorArray va;
        
        va = mIEbyService.get(endpoint.getServiceName());
        if (va != null)
        {
            if (va.remove(endpoint))
            {
                mIEbyService.remove(endpoint.getServiceName());
            }
            else
            {
                mIEbyService.put(endpoint.getServiceName(), va);
            }
            QName[] interfaceNames = endpoint.getInterfacesInternal();
            if (interfaceNames != null)
            {
                for (QName i : interfaceNames)
                {
                    va = mIEbyInterface.get(i);
                    if (va != null)
                    {
                        if (va.remove(endpoint))
                        {
                            mIEbyInterface.remove(i);
                        }
                        else
                        {
                            mIEbyInterface.put(i, va);
                        }
                    }
                }
            }
            mInternalEndpoints.remove(new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName()));        
            mInternalEndpointNames.remove(endpoint.toExternalName());
            mPendingInternalInterfaces.remove(endpoint);
	}
    }
    
    private void removeExternalEndpoint(RegisteredEndpoint endpoint)
    {
        VectorArray         va;
        
        va = mEEbyService.get(endpoint.getServiceName());
	if (va != null)
        {
            if (va.remove(endpoint))
            {
                mEEbyService.remove(endpoint.getServiceName());
            }
            else
            {
                mEEbyService.put(endpoint.getServiceName(), va);
            }
            QName[] interfaceNames = endpoint.getInterfacesInternal();
            if (interfaceNames != null)
            {
                for (QName i : interfaceNames)
                {
                    va = mEEbyInterface.get(i);
                    if (va != null)
                    {
                        if (va.remove(endpoint))
                        {
                            mEEbyInterface.remove(i);
                        }
                        else
                        {
                            mEEbyInterface.put(i, va);
                        }
                    }
                }
            }
            mExternalEndpoints.remove(new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName()));            
            mPendingExternalInterfaces.remove(endpoint);
        }
    }

    private void removeLinkedEndpoint(RegisteredEndpoint endpoint)
    {
        VectorArray         va;
        
        va = mIEbyService.get(endpoint.getServiceName());
	if (va != null)
        {
            if (va.remove(endpoint))
            {
                mIEbyService.remove(endpoint.getServiceName());
            }
            else
            {
                mIEbyService.put(endpoint.getServiceName(), va);
            }
            mLinkedEndpoints.remove(new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName()));
            mLinkedEndpointNames.remove(endpoint.toExternalName());        
        }
    }
            
    int countEndpoints(int type)
    { 
        int     count = 0;
        
        if (type == RegisteredEndpoint.INTERNAL)
        {
            count = mInternalEndpoints.size();
        }
        else if (type == RegisteredEndpoint.EXTERNAL)
        {
            count = mExternalEndpoints.size();
        }
        else
        {
            count = mLinkedEndpoints.size();
        }
        return (count);
    }
    
    ServiceEndpoint[] listEndpoints(int type)
    {
        ServiceEndpoint[]   endpoints;
        Collection          values;
        
        if (type == RegisteredEndpoint.INTERNAL)
        {
            values = mInternalEndpoints.values();
        }
        else if (type == RegisteredEndpoint.EXTERNAL)
        {
            values = mExternalEndpoints.values();
        }
        else
        {
            values = mLinkedEndpoints.values();
        }
        
        endpoints = new ServiceEndpoint[values.size()];
        values.toArray(endpoints);
        return endpoints;
    }
        
    void clear()
    {
        mLinkedEndpoints.clear();
        mInternalEndpoints.clear();
        mExternalEndpoints.clear();
        mInterfaceConnections.clear();
        mIEbyService.clear();
        mEEbyService.clear();
        mIEbyInterface.clear();
        mEEbyInterface.clear();
        mPendingInternalInterfaces.clear();
        mPendingExternalInterfaces.clear();
     }
        
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("  EndpointRegistry\n");
        sb.append("    InternalEndpoints Count: ");
        sb.append(mInternalEndpoints.size());
        sb.append("\n");
        for (Iterator i = mInternalEndpoints.entrySet().iterator(); i.hasNext(); )
        {
            sb.append(((Map.Entry<Endpoint, RegisteredEndpoint>)i.next()).getValue().toString());
        }
        sb.append("    InternalEndpointsByService Count: ");
        sb.append(mIEbyService.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mIEbyService.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Service: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
        }
        sb.append("    InternalEndpointsByInterface Count: ");
        sb.append(mIEbyInterface.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mIEbyInterface.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Interface: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
        }
        sb.append("    ExternalEndpoints Count: ");
        sb.append(mExternalEndpoints.size());
        sb.append("\n");
        for (Iterator i = mExternalEndpoints.entrySet().iterator(); i.hasNext(); )
        {
            sb.append(((Map.Entry<Endpoint, RegisteredEndpoint>)i.next()).getValue().toString());
        }
        sb.append("    ExternalEndpointsByService Count: ");
        sb.append(mEEbyService.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mEEbyService.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Service: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
        }
        sb.append("    ExternalEndpointsByInterface Count: ");
        sb.append(mEEbyInterface.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mEEbyInterface.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Interface: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
	}
        sb.append("    LinkedEndpoints Count: ");
        sb.append(mLinkedEndpoints.size());
        sb.append("\n");
        for (Iterator i = mLinkedEndpoints.entrySet().iterator(); i.hasNext(); )
        {
            sb.append(((Map.Entry<Endpoint, LinkedEndpoint>)i.next()).getValue().toString());
        }
        sb.append("    InterfaceConnections Count: ");
        sb.append(mInterfaceConnections.size());
        sb.append("\n");
        for (Iterator i = mInterfaceConnections.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry<QName, Endpoint>    e = (Map.Entry<QName, Endpoint>)i.next();
            sb.append("      InterfaceName: " + e.getKey());
            sb.append("\n");
            sb.append(e.getValue().toString());
        }
        return (sb.toString());
    }
    
    // Simple struct to hold fully-qualified endpoint name
    class Endpoint
    {
        private QName   mServiceName;
        private String  mEndpointName;
        
        Endpoint(QName serviceName, String endpointName)
        {
            mServiceName    = serviceName;
            mEndpointName   = endpointName;
        }
        
        public boolean equals(Object obj)
        {
            boolean isEqual = false;
            
            if (obj instanceof Endpoint &&
                ((Endpoint)obj).mServiceName.equals(mServiceName) &&
                ((Endpoint)obj).mEndpointName.equals(mEndpointName))
            {
                isEqual = true;
            }
            
            return isEqual;
        }
        
        public String toString()
        {
            StringBuilder   sb = new StringBuilder();

            sb.append("        ServiceName: " + mServiceName);
            sb.append("\n        EndpointName: " + mEndpointName);
            sb.append("\n");
            return (sb.toString());

        }
        public int hashCode()
        {
           return (mServiceName.hashCode() ^ mEndpointName.hashCode());
        }
    }
    
    private class VectorArray
    {
        Vector                  mVector;
        RegisteredEndpoint      mArray[];
        
        
        VectorArray(VectorArray va, RegisteredEndpoint o)
        {
            if (va == null)
            {
                mVector = new Vector();
            }
            else
            {
                mVector = va.mVector;
            }
            if (o instanceof LinkedEndpoint)
            {
                mVector.add(0, o);
            }
            else
            {
                mVector.add(o);
            }
            mArray = new RegisteredEndpoint[mVector.size()];
            mVector.toArray(mArray);
        }
        
        boolean remove(Object o)
        {
            mVector.remove(o);
            if (mVector.size() != 0)
            {
                mArray = new RegisteredEndpoint[mVector.size()];
                mVector.toArray(mArray);
                return (false);
            }
            return (true);
        }

        public String toString()
        {
            StringBuilder   sb = new StringBuilder();

            for (RegisteredEndpoint re : mArray)
            {
                sb.append(re.toString());
            }

            return (sb.toString());
        }
    }
    
}
