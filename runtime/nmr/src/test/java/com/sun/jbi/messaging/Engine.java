/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.wsdl2.Binding;
import com.sun.jbi.wsdl2.BindingOperation;
import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Endpoint;
import com.sun.jbi.wsdl2.Interface;
import com.sun.jbi.wsdl2.InterfaceOperation;
import com.sun.jbi.wsdl2.Service;

import com.sun.jbi.wsdl2.impl.WsdlFactory;

import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;

/** Generic engine skeleton for use with NMS junit/regress tests.
 * @author Sun Microsystems, Inc.
 */
public abstract class Engine 
        implements Runnable, Component
{
    private   Exception             mError;
    private   String                mFailTxt;
    protected boolean               mIsProvider;
    protected DeliveryChannelImpl   mChannel;
    protected ServiceEndpoint       mEndpoint;
    protected Document              mDocument;
    protected String                mPattern;
    protected String                mEndpointName;
    protected QName                 mServiceName;
    protected Sequencer             mSequencer;
    
    protected Engine(DeliveryChannelImpl channel)
    {
        mChannel = channel;
    }
    
    /** Used for provider cases. */
    public void init(QName service, String endpoint, String pattern)
        throws Exception
    {
        mPattern        = pattern;
        mEndpointName   = endpoint;
        mServiceName    = service;
        mEndpoint = mChannel.activateEndpoint(service, endpoint);
        mIsProvider = true;
    }    
    
    /** Used in consumer cases. */
    public void init(QName service)
        throws Exception
    {
        ServiceEndpoint[] refs;
        
        refs = mChannel.getEndpointsForService(service);
        if (refs.length == 0)
        {
            throw new Exception("No endpoints found for service: " + service.getLocalPart());
        }
        
        mIsProvider = false;
        mEndpoint   = refs[0];
    }

    public void setSequencer(Sequencer sequencer)
    {
        mSequencer = sequencer;
    }
    
    public Sequencer getSequencer()
    {
        return (mSequencer);
    }
     
    public void run()
    {
        try
        {
            start();
        }
        catch (Exception ex)
        {
            mError = ex;
        }
    }
    
    public void stop()
        throws Exception
    {
        if (mIsProvider)
        {
            mChannel.deactivateEndpoint(mEndpoint);
        }
        
        mChannel.close();
    }
    
    public abstract void start()
        throws Exception;
    
    public void checkError()
        throws Exception
    {
        if (mError != null)
        {
            throw mError;
        }
        else if (mFailTxt != null)
        {
            throw new Exception(mFailTxt);
        }
    }
    
    public void setFailure(String txt)
    {
        mFailTxt = txt;
    }
    
    public ServiceEndpoint getEndpoint()
    {
        return (mEndpoint);
    }
     
    public Document getDescription()
    {
        try
        {
            WsdlFactory         wf = WsdlFactory.newInstance();
            Description         d = wf.newDescription("");
            Interface           i = d.addNewInterface("if1");
            InterfaceOperation  io = i.addNewOperation();
            Binding             b = d.addNewBinding("binding1");
            Service             s = d.addNewService(mServiceName.getLocalPart());
            Endpoint            e = s.addNewEndpoint(mEndpointName, b);
            BindingOperation    bo = b.addNewOperation();

            io.setName("foobar");
            io.setPattern(mPattern);
            bo.setInterfaceOperation(io.getQualifiedName());
            e.setBinding(b);
            s.setInterface(i);
            b.setType("type1");
            b.setInterface(i);
            return (d.toXmlDocument());
        }
        catch (com.sun.jbi.wsdl2.WsdlException wEx)
        {
           System.out.println("Problem creating WSDL: " + wEx.toString());
           return (null);
        }
    }
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle()
    {
        return null;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        return null;        
    }
    
    public Document getServiceDescription(ServiceEndpoint endpoint)
    {
        return getDescription();
    }    
    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        return mEndpoint;
    }
}
