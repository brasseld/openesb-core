/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageProperties.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

/**
 * Message properties.
 *
 * @author Sun Microsystems Inc.
  */
interface MessageProperties
{
    /**
     *  Value of byte message.
     */
    String BYTE_MESSAGE = "ByteMessage";
    /**
     *  Persistence.
     */
    String DELIVERY_MODE = "PERSISTENCE";
    /**
     *  Error.
     */
    String ERROR = "ERROR";
    /**
     *  Fault.
     */
    String FAULT = "FAULT";
    /**
     * Endpoint name.
     */
    String JBI_ENDPOINT_NAME = "JBI_ENDPOINT_NAME";
    /**
     * Operation.
     */
    String JBI_OPERATION_NAME = "JBI_OPERATION";
    /**
     * Operation namespace.
     */
    String JBI_OPERATION_NAMESPACE = "JBI_OPERATION_NAMESPACE";
    /**
     *  Service name.s
     */
    String JBI_SERVICE_NAME = "JBI_SERVICE_NAME";
    /**
     *  Jbi status.
     */
    String JBI_STATUS = "JBI_STATUS";
    /**
     *  Correlation ID.
     */
    String JMS_CORRELATION_ID = "JMS_CORRELATION_ID";
    /**
     * Message Id.
     */
    String JMS_MESSAGE_ID = "JMS_MESSAGE_ID";
    /**
     *  Operation.
     */
    String JMS_OPERATION = "OPERATION";
   /**
     *    Map message.
     */
    String MAP_MESSAGE = "MapMessage";
    /**
     *    MEp.
     */
    String MEP = "JBI_MEP";
    /**
     *    Status.
     */
    String MESSAGE_STATUS = "MESSAGE_STATUS";
    /**
     *    Non persistent.
     */
    String NON_PERSISTENT_MODE = "NON-PERSISTENT";
    /**
     *    Object message.
     */
    String OBJECT_MESSAGE = "ObjectMessage";
    /**
     *    persistent.
     */
    String PERSISTENT_MODE = "PERSISTENT";
    /**
     * reply to.
     */
    String REPLY_TO = "REPLY_TO";
    /**
     * Stream message.
     */
    String STREAM_MESSAGE = "StreamMessage";
    /**
     *    Success.
     */
    String SUCCESS = "SUCCESS";
    /**
     *    Text message.
     */
    String TEXT_MESSAGE = "TextMessage";
    /**
     *    Unknown.
     */
    String UNKNOWN = "UNKNOWN";
}
