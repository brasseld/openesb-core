/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointStatus.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

/**
 * Typesafe enumeration containing status values for a endpoint.
 *
 * @author Sun Microsystems inc.
 */
public final class EndpointStatus
{    
    /**
     * Indicates that an endpoint has been deployed.
     */
    public static final EndpointStatus DEPLOYED = new EndpointStatus("DEPLOYED");
    /**
     * Indicates that an endpoint has been initialised.
     */
    public static final EndpointStatus INIT = new EndpointStatus("INIT");

    /**
     * Indicates that an endpoint has been started.
     */
    public static final EndpointStatus STARTED = new EndpointStatus("STARTED");

    /**
     * Indicates that an endpoint has been stopped.
     */
    public static final EndpointStatus STOPPED = new EndpointStatus("STOPPED");

    /**
     * Indicates that an endpoint has been shutdown.   
     */
    public static final EndpointStatus SHUTDOWN = new EndpointStatus("SHUTDOWN");

    /**
     * String representation of status.
     */
    private String mStatus;

    /**
     * Private constructor used to create a new EndpointStatus type.
     *
     * @param status value
     */
    private EndpointStatus(String status)
    {
        mStatus = status;
    }

    /**
     * Equality test.
     *
     * @param status status object.
     *
     * @return boolean result of test.
     */
    public boolean equals(EndpointStatus status)
    {
        return (mStatus.equals(status.mStatus));
    }

    /**
     * Returns string value of enumerated type.
     *
     * @return String representation of status value.
     */
    public String toString()
    {
        return mStatus;
    }

    /**
     * Returns instance of EndpointStatus that corresponds to given string.
     *
     * @param status status string.
     *
     * @return EndpointStatus status object.
     */
    public static EndpointStatus valueOf(String status)
    {
        EndpointStatus instance;

        //
        //  Convert symbolic name to object reference.
        //
        if (status.equals(SHUTDOWN.toString()))
        {
            instance = SHUTDOWN;
        }
        else if (status.equals(INIT.toString()))
        {
            instance = INIT;
        }
        else if (status.equals(STARTED.toString()))
        {
            instance = STARTED;
        }
        else if (status.equals(STOPPED.toString()))
        {
            instance = STOPPED;
        }        
        else if (status.equals(DEPLOYED.toString()))
        {
            instance = DEPLOYED;
        }
        else
        {
            //
            //  Someone has a problem.
            //
            throw new java.lang.IllegalArgumentException(status);
        }

        return (instance);
    }
}
