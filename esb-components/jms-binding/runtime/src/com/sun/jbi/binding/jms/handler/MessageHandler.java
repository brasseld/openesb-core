/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import com.sun.jbi.binding.jms.EndpointBean;

import com.sun.jbi.binding.jms.framework.Command;

import javax.jbi.messaging.MessageExchange;

import javax.jms.Message;

/**
 * Message Handler interface.
 *
 * @author Sun Microsystems Inc.
  */
public interface MessageHandler extends Command
{
    /**
     * Sets bean.
     *
     * @param eb endpoint bean.
     */
    void setBean(EndpointBean eb);

    /**
     * Gets bean.
     *
     * @return endpoint bean.
     */
    EndpointBean getBean();

    /**
     * Sets error.
     *
     * @param err error string.
     */
    void setError(String err);

    /**
     * Gets error. 
     *
     * @return error string.
     */
    String getError();

    /**
     *   Sets exception.
     *
     * @param ex exception.
     */
    void setException(Exception ex);

    /**
     * Returns the exception.
     *
     * @return exception.
     */
    Exception getException();

    /**
     * Set JMS message.
     *
     * @param msg JMS message. 
     */
    void setJMSMessage(Message msg);

    /**
     * Gets the JMS message.
     *
     * @return JMS message.
     */
    Message getJMSMessage();

    /**
     * Sets NMR exchange.
     *
     * @param exch NMR exchange.
     */
    void setNMSMessage(MessageExchange exch);

    /**
     * Gets the NMR exchange.
     *
     * @return NMR exchange.
     */
    MessageExchange getNMSMessage();

    /**
     * Clear.
     */
    void clear();

    /**
     * Sends the NMR exchange.
     *
     * @param exch NMR exhcange.
     *
     * @return true if sent.
     */
    boolean send(MessageExchange exch);

    /**
     * Send the JMS message.
     *
     * @return true if sent.
     */
    boolean sendJMSMessage();

    /**
     * Send NMR message.
     *
     * @return true if sent.
     */
    boolean sendNMSMessage();
}
