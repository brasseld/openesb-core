/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.binding.jms.mq.MQConnection;
import com.sun.jbi.binding.jms.mq.MQDestination;
import com.sun.jbi.binding.jms.mq.MQSession;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.util.ArrayList;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.jbi.servicedesc.ServiceEndpoint;


import javax.xml.namespace.QName;

/**
 * This Bean object holds all the attributes of an endpoint.
 *
 * @author Sun Microsystems Inc.
 */
public final class EndpointBean
{

    
    private Object mWsdlDefinition;
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private ServiceEndpoint mServiceEndpoint;

    /**
     * Table for storing the xml tags and values  present in the config file.
     * This table stores  throws jmsbinding sepcific configuration information
     * correspondint to each endpoint.
     */
    private Hashtable mConfigTable = null;

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private List mOperations = null;

    /**
     *
     */

    /**
     *    
     */
    private MQConnection mConnection;

    /**
     *
     */

    /**
     *    
     */
    private MQDestination mDestination;

    /**
     *
     */

    /**
     *    
     */
    private MQSession mReceiverSession;

    /**
     * Deployment Id.
     */
    private String mDeploymentId;

    /**
     *
     */

    /**
     *    
     */
    private Thread mReceiverThread;

    /**
     *    
     */
    private int mStyle;
    
    /**
     * Role.
     */
    private int mRole;
    
    /**
     * Status.
     */
    private EndpointStatus mStatus;
    
    private String mDeploymentType;
    

    /**
     * Constructor. Creates a new Table.
     */
    public EndpointBean()
    {
        mConfigTable = new Hashtable();
        mOperations = new ArrayList();
    }

    /**
     * 
     *
     * @return operation list.
     */
    public List getAllOperations()
    {
        return mOperations;
    }

    /**
     * 
     *
     * @param con conneciton.
     */
    public void setConnection(MQConnection con)
    {
        mConnection = con;
    }

    /**
     * 
     *
     * @return connection.
     */
    public MQConnection getConnection()
    {
        return mConnection;
    }

    /**
     * 
     *
     * @return qname of operation.
     */
    public QName getDefaultOperation()
    {
        QName operation = null;

        try
        {
            OperationBean op = (OperationBean) mOperations.get(0);
            operation = new QName(op.getNamespace(), op.getName());
        }
        catch (Exception e)
        {
            ;
        }

        return operation;
    }

    
    public void setWsdlDefinition(Object def)
    {
        mWsdlDefinition = def;
    }
    
    public Object getWsdlDefinition()
    {
        return mWsdlDefinition;
    }
    
    /**
     * Sets the deployment Id corresponding to this endpoint Bean.
     *
     * @param asId Application Sub assembly ID.
     */
    public void setDeploymentId(String asId)
    {
        mDeploymentId = asId;
    }

    /**
     * Fetches the deployment Id coresponding to this bean.
     *
     * @return Application Sub assembly ID
     */
    public String getDeploymentId()
    {
        return mDeploymentId;
    }

    /**
     * 
     *
     * @param dest mq destination.
     */
    public void setDestination(MQDestination dest)
    {
        mDestination = dest;
    }

    /**
     * 
     *
     * @return destination.
     */
    public MQDestination getDestination()
    {
        return mDestination;
    }

    /**
     * 
     *
     * @param ref service reference.
     */
    public void setServiceEndpoint(ServiceEndpoint ref)
    {
        mServiceEndpoint = ref;
    }

    /**
     * 
     *
     * @return endpoint reference.
     */
    public ServiceEndpoint getServiceEndpoint()
    {
        return mServiceEndpoint;
    }

    /**
     * 
     *
     * @param operation operation.
     *
     * @return input type for the operation.
     */
    public String getInputType(String operation)
    {
        String inputtype = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                inputtype = op.getInputType();

                break;
            }
        }

        return inputtype;
    }

    /**
     * 
     *
     * @param operation operation.
     *
     * @return String representing the operation , if not present then null
     */
    public String getMEP(String operation)
    {
        String mep = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                mep = op.getMep();

                break;
            }
        }

        return mep;
    }

    /**
     * 
     *
     * @param operation operation name.
     *
     * @return output typr for this operation.
     */
    public String getOutputType(String operation)
    {
        String outputtype = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                outputtype = op.getOutputType();

                break;
            }
        }

        return outputtype;
    }

    /**
     * 
     *
     * @param session session.
     */
    public void setReceiverSession(MQSession session)
    {
        mReceiverSession = session;
    }

    /**
     * 
     *
     * @return session.
     */
    public MQSession getReceiverSession()
    {
        return mReceiverSession;
    }

    /**
     * Setter for property mReceiverThread.
     *
     * @param mReceiverThread New value of property mReceiverThread.
     */
    public void setReceiverThread(java.lang.Thread mReceiverThread)
    {
        this.mReceiverThread = mReceiverThread;
    }

    /**
     * Getter for property mReceiverThread.
     *
     * @return Value of property mReceiverThread.
     */
    public java.lang.Thread getReceiverThread()
    {
        return mReceiverThread;
    }

    /**
     * 
     *
     * @param session session.
     */
    public void setSendSession(MQSession session)
    {
    }

    /**
     * 
     *
     * @param style style.
     */
    public void setStyle(int style)
    {
        mStyle = style;
    }

    /**
     * 
     *
     * @return style.
     */
    public int getStyle()
    {
        return mStyle;
    }

    /**
     * Returns a unique name which is a combination of service name and
     * endpoint name.
     *
     * @return unique name of this endpoint. Its a combination of service  name
     *         and endpoint name.
     */
    public String getUniqueName()
    {
        String servicenamespace = getValue(ConfigConstants.SERVICE_NAMESPACE);

        String servicename = getValue(ConfigConstants.SERVICENAME);
        String endpointname = getValue(ConfigConstants.ENDPOINTNAME);
        QName ser = null;
        try
        {
            ser = new QName(servicenamespace, servicename);
        }
        catch (Exception e)
        {
            ;
        }
        
        if (ser == null)
        {
            return endpointname;
        }
        if (mRole == ConfigConstants.PROVIDER)
        {
            return ser.toString() + endpointname;
        }
        else
        {
            return ser.toString();
        } 

    }

    /**
     * Sets the value for the key in the table.
     *
     * @param key for which value has to be retrieved.
     * @param value corresponding to the key
     */
    public void setValue(
        String key,
        String value)
    {
        if (key == null)
        {
            return;
        }

        if (value == null)
        {
            value = "";
        }

        mConfigTable.put(key, value);
    }

    /**
     * Returns the value associated with the key from the table.
     *
     * @param key the tag name for wcich value is required.
     *
     * @return value corresponding to the tag as in config file.
     */
    public String getValue(String key)
    {
        if (key == null)
        {
            return null;
        }

        return (String) mConfigTable.get(key);
    }

 
    /**
     *
     *
     * @param role role.
     */
    public void setRole(int role)
    {
        if ((role != ConfigConstants.PROVIDER) && (role != ConfigConstants.CONSUMER))
        {
            mRole = ConfigConstants.CONSUMER;
        }
        else
        {
            mRole = role;
        }
    }

    /**
     *
     *
     * @return  role.
     */
    public int getRole()
    {
        return mRole;
    }

    /**
     * 
     *
     * @param name name.
     * @param mep mep.
     * @param input input type.
     * @param output output type.
     */
    public void addOperation(
        String namespace,
        String name,
        String mep,
        String input,
        String output)
    {
        OperationBean op = new OperationBean(namespace, name, mep, input, output);
        /*System.out.println("Adding operation " + namespace + "|" + 
                        name + mep + "|" + input + "|"
            + output);
         */
        mOperations.add(op);
    }
    
    /** 
     * Maintains the status of the endpoint bean.
     *
     * @param status endpoint status.
     */
    public void setStatus(EndpointStatus status)
    {
        mStatus = status; 
    }
    
    /**
     * Returns the state of the endpoint.
     *
     * @return endpoint status.
     */    
    public EndpointStatus getStatus()
    {
        return mStatus;
    }
    
    /**
     * Getter for property mDeploymentType.
     * @return Value of property mDeploymentType.
     */
    public java.lang.String getDeploymentType() 
    {
        return mDeploymentType;
    }
    
    /**
     * Setter for property mDeploymentType.
     * @param mDeploymentType New value of property mDeploymentType.
     */
    public void setDeploymentType(java.lang.String mDeploymentType)
    {
        this.mDeploymentType = mDeploymentType;
    }
    
}
