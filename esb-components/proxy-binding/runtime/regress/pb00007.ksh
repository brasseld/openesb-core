#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)pb00007.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "pb00007 : proxy NMS performance test"

. ./regress_defs.ksh

echo Stop the Proxy Binding component
asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT SunProxyBinding

echo Shutdown the Proxy Binding component
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT SunProxyBinding

echo Uninstall tbe Proxy Binding component
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT SunProxyBinding

echo Stop ESBTest domain
asadmin stop-domain -t ESBTest

exit

