#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)buildenv.csh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

###################
# INPUT PARAMETERS:
###################

# IIS_TOOLROOT    - override the default TOOLROOT setting.
# IIS_CVSROOT     - override the default CVSROOT setting.
# IIS_BRANCH_NAME - override the default CVS_BRANCH_NAME setting
# IIS_CODELINE    - override the default CODELINE setting
# IIS_AS8BASE     - override the default AS8BASE setting
#
# WARNING:  the above variables are PRIVATE and are RESERVED for
#           this definition file.  They are unexported after use.

setenv PRODUCT openesb

set pwd = "`pwd`"

### set up source "base" variable:
if ( ! $?PROJECT ) then
    setenv PROJECT ""
endif
if ( ! $?SRCROOT ) then
    setenv SRCROOT ""
endif

#cannonicalize $SRCROOT if it is set:
if ( "$SRCROOT" != "" ) then
    set srcroot=`sh -c "cd $SRCROOT; pwd"`
endif

if ( "$SRCROOT" == "$cwd" || "$SRCROOT" == "$pwd" ) then
    setenv SRCROOT "$cwd"
else if ( "$PROJECT" != "" && ("$SRCROOT" == "$PROJECT" || "$SRCROOT" == "" )) then
    ## ASSUME we are using VSPMS
    setenv SRCROOT $PROJECT
else
   cat << 'EOF'
PLEASE SET $SRCROOT BEFORE SOURCING buildenv.csh
SETUP ABORTED
'EOF'
  goto HALT
endif


######
#allow user to override codeline var, which determines cvs branch names
#for main repository, and determines the placement of log and kit directories.
######
if ($?IIS_CODELINE) then
    setenv CODELINE $IIS_CODELINE
else
    setenv CODELINE main
endif

#have to unset or set it to zero this if you are building in a release environment
setenv DEVELOPER_BUILD 1

####
#CVS BRANCH NAMES.  Use IIS_BRANCH_NAME to override "main" when bootstraping a branch.
####
if ($?IIS_BRANCH_NAME) then
    setenv JBI_BRANCH_NAME "$IIS_BRANCH_NAME"
else
    setenv JBI_BRANCH_NAME main
endif

setenv CVS_BRANCH_NAME "$JBI_BRANCH_NAME"

#################
# CVS_BRANCH_NAME is used in the following scripts to denote the
# toolsBuild    - checkout tools src
# makedrv.pl    - checkout tools src
# buildenv.csh  - setup file
# buildenv.ksh  - setup file
# bldcmn.sh     - assert
# fortepj.rc    - cosmetic (sets $REV VSPMS var)
# fortepj.ksh   - cosmetic (sets $REV VSPMS var)
#################

#### CVS DEFS
setenv CVSREAD 1

####### TOOLS SETUP

if ($?IIS_TOOLROOT) then
    setenv TOOLROOT $IIS_TOOLROOT
else
    foreach tr ($SRCROOT/tools)
        setenv TOOLROOT $tr
        if (-x $TOOLROOT/boot/whatport ) then
            break
        endif
    end
endif

if (-x $TOOLROOT/boot/whatport ) then
    setenv FORTE_PORT `$TOOLROOT/boot/whatport`
else
    echo ERROR:  could not find tools - please check your IIS_TOOLROOT setting or create a copy in $SRCROOT/tools.
    goto HALT
endif

set path = ($TOOLROOT/bin/$FORTE_PORT $TOOLROOT/bin/cmn $path)

if ($?PERL5_HOME) then
    if ( -d $PERL5_HOME ) then
        #perl installations on solaris and linux differ - solaris has {bin,lib} subdirs:
        if ( -d $PERL5_HOME/lib ) then
            setenv PERL_LIBPATH ".;$PERL5_HOME/lib;$TOOLROOT/lib/cmn;$TOOLROOT/lib/cmn/perl5"
        else
            setenv PERL_LIBPATH ".;$PERL5_HOME;$TOOLROOT/lib/cmn;$TOOLROOT/lib/cmn/perl5"
        endif
        if ( -d $PERL5_HOME/bin ) then
            set path = ( $PERL5_HOME/bin $path )
            #otherwise, we assume perl is already in the path
        endif
    else
        echo "WARNING: not a directory, PERL5_HOME='$PERL5_HOME'. Please fix."
    endif
else
    #use port-specific perl libs in $TOOLROOT; this is for old solaris
    #and mks perl installs:
    setenv PERL_LIBPATH ".;$TOOLROOT/lib/cmn;$TOOLROOT/lib/$FORTE_PORT/perl5;$TOOLROOT/lib/cmn/perl5"
endif

#used by makemf utility:
setenv MAKEMF_LIB $TOOLROOT/lib/cmn

#used by codegen utility:
setenv CG_TEMPLATE_PATH ".;$TOOLROOT/lib/cmn/templates;$TOOLROOT/lib/cmn/templates/java"

####### END TOOLS SETUP

#### set up env required for release and tools builds:
setenv PATHNAME `basename $SRCROOT`
setenv PATHREF $SRCROOT
setenv RELEASE_ROOT  $SRCROOT/release
setenv RELEASE_DISTROOT  $SRCROOT/release
setenv HOST_NAME "`uname -n`"

if !($?FORTE_LINKROOT) setenv FORTE_LINKROOT    $SRCROOT/$CODELINE/$FORTE_PORT
if !($?DISTROOT) setenv DISTROOT    $FORTE_LINKROOT/dist/tools
if !($?KITROOT) setenv KITROOT  $FORTE_LINKROOT/kits
if !($?KIT_DISTROOT) setenv KIT_DISTROOT    $FORTE_LINKROOT/kits/$PRODUCT
if !($?KIT_REV) setenv KIT_REV  $CODELINE

if !($?REGRESS_DISPLAY) then
    if ($?DISPLAY) then
        setenv REGRESS_DISPLAY "$DISPLAY"
    else
        setenv REGRESS_DISPLAY NULL
    endif
endif

##### JAVA SDK SETUP
set path = ($JAVA_HOME/bin $path)

##### JAVA TOOLS
#ant
if !($?ANT_OPTS)    setenv ANT_OPTS -Xmx200m

##### JREGRESS
if !($?JREGRESS_TIMEOUT)    setenv JREGRESS_TIMEOUT 650

set path = ( $AS8BASE/bin $path )

##### Default JBIROOT:
#this can be set in the login env, as it is usually invariant
if !($?JBI_USER_NAME) setenv JBI_USER_NAME "$user"

if !($?JNET_USER) then
    setenv JNET_USER "$JBI_USER_NAME"
    echo "WARNING: your java.net user name (JNET_USER) was defaulted to '$JNET_USER'."
    echo "To remove this warning, please export JNET_USER in the environment."
endif

if !($?JBIROOT_BASE) setenv JBIROOT_BASE    $AS8BASE
if !($?JBIROOT) setenv JBIROOT    $JBIROOT_BASE/jbi
##### Default JBISE_BASE:
if !($?JBISE_BASE) setenv JBISE_BASE $SRCROOT/install/jbise

### useful aliases:
alias mkregresslink 'ln -s $FORTE_LINKROOT/regress $SRCROOT/regress'
alias gettools '(mkdir -p tools.new; cd $SRCROOT/tools.new;  cvs -f -d $TOOLS_CVSROOT co ${FORTE_PORT}tools; echo new tools are in tools.new)'
alias cvstools 'cvs -d $TOOLS_CVSROOT'

### checkstyle helpers:
#note - the \| makes '|' the delimiter in the address range.
#it is necessary because the filename expression could contain (/) delimiters.
alias cserrs 'sed -n -e "\|^<file .*\!$|,\|<\/file>|p" $SRCROOT/\!^/bld/checkstyle_src_report.xml | grep -v "</file>" |            sed -e "s|$SRCROOT/||g; s|<file name=.||; s/.>//" | fixcserrs | xml2ascii | nodoublequotes '
alias cserrsu 'echo Usage: cserr service java_classname'

alias fixcserrs 'sed -e "s|<error line=.| |; s|. column=.|/|; s|. severity=.error. message=.|	|; s|source=.*||" '

alias nodoublequotes 'tr -d \\042'

#this is obviously incomplete.  RT 10/6/04
alias xml2ascii sed -e '"' "s|&apos;|'|g; s|&lt;|<|g;; s|&gt;|>|g;"'"'
###

#this is a hack to bootstrap the boms.  RT 9/26/05
setenv JBI_DOT_VERSION "1.1"

#######
#cygwin: set java versions of SRCROOT, TOOLROOT (used in ant scripts):
#######
if ( "$FORTE_PORT" == "cygwin" ) then
    #warning - run cygpath on each, to convert paths of the /cygdrive form.  RT 9/13/06
    setenv JV_SRCROOT  `cygpath -wm "$SRCROOT"`
    setenv JV_TOOLROOT `cygpath -wm "$TOOLROOT"`
    setenv JV_AS8BASE  `cygpath -wm "$AS8BASE"`
else
    setenv JV_SRCROOT "$SRCROOT"
    setenv JV_TOOLROOT "$TOOLROOT"
    setenv JV_AS8BASE "$AS8BASE"
endif

HALT:

#dangerous to leave csh variables laying about:
unset pwd
unset srcroot
unset opt
unset fortepj

#these variables are PRIVATE and RESERVED for this definition file:
unsetenv IIS_TOOLROOT
unsetenv IIS_CVSROOT
unsetenv IIS_BRANCH_NAME
unsetenv IIS_CODELINE
unsetenv IIS_AS8BASE

#these variables are needed to update the local project tool sources during toolsBuild
setenv CVS_SRCROOT_PREFIX "open-esb"
setenv CVS_CO_ROOT "${SRCROOT}/.."

#finally, set the current release tag:
setenv REV "SM06"
