#!/usr/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)create_jnet_bundles.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "Setting the environment"
cd $SRCROOT 

JNET_TOOLS_CVSROOT_TEMP=":pserver:anoncvs@iis.sfbay:/tooldist/openesb/jnetmain"
CVSROOT_TEMP=":pserver:anoncvs@jwscvs.sfbay:/cvs/jws/java-net"


if [ x$JNET_TOOLS_CVSROOT = "x" ]; then
   JNET_TOOLS_CVSROOT=${JNET_TOOLS_CVSROOT_TEMP}
fi
if [ x$CVSROOT = "x" ]; then
   CVSROOT=${CVSROOT_TEMP}
fi

echo Using $JNET_TOOLS_CVSROOT as JNET_TOOLS_CVSROOT
echo Using $CVSROOT as CVSROOT
echo ""


echo "Removing old jnet_bundles dir" 
rm -rf $SRCROOT/jnet_bundles

echo "Creating new jnet_bundles dirs" 
mkdir -p $SRCROOT/jnet_bundles/logs
mkdir -p $SRCROOT/jnet_bundles/solsparc/tools
mkdir -p $SRCROOT/jnet_bundles/solx86/tools
mkdir -p $SRCROOT/jnet_bundles/linux/tools
mkdir -p $SRCROOT/jnet_bundles/cygwin/tools
mkdir -p $SRCROOT/jnet_bundles/nt/tools
mkdir -p $SRCROOT/jnet_bundles/macosx/tools

echo "Finished setting the environment"
echo ""

cd $SRCROOT/jnet_bundles/solsparc/tools
echo "Checking out jnet solsparctools... log is ${SRCROOT}/jnet_bundles/logs/co_solsparctools.log"
cvs -d $JNET_TOOLS_CVSROOT co solsparctools > ${SRCROOT}/jnet_bundles/logs/co_solsparctools.log
mkdir -p $SRCROOT/jnet_bundles/solsparc/tools/java/maven/plugins
cp $TOOLROOT/java/maven/plugins/jregress-1.0.jar $SRCROOT/jnet_bundles/solsparc/tools/java/maven/plugins
mkdir -p $SRCROOT/jnet_bundles/solsparc/tools/java/ant/lib
cp $TOOLROOT/java/ant/lib/ant-junit.jar $SRCROOT/jnet_bundles/solsparc/tools/java/ant/lib
cd $SRCROOT/jnet_bundles/solsparc
echo "tarring up the bundle"
tar cvf solsparctools.tar `walkdir -nocvs -f tools` > ${SRCROOT}/jnet_bundles/logs/tar_solsparctools.log


cd $SRCROOT/jnet_bundles/solx86/tools
echo "Checking out jnet solx86tools... log is ${SRCROOT}/jnet_bundles/logs/co_solx86tools.log"
cvs -d $JNET_TOOLS_CVSROOT co solx86tools > ${SRCROOT}/jnet_bundles/logs/co_solx86tools.log
mkdir -p $SRCROOT/jnet_bundles/solx86/tools/java/maven/plugins
cp $TOOLROOT/java/maven/plugins/jregress-1.0.jar $SRCROOT/jnet_bundles/solx86/tools/java/maven/plugins
mkdir -p $SRCROOT/jnet_bundles/solx86/tools/java/ant/lib
cp $TOOLROOT/java/ant/lib/ant-junit.jar $SRCROOT/jnet_bundles/solx86/tools/java/ant/lib
cd $SRCROOT/jnet_bundles/solx86
echo "tarring up the bundle"
tar cvf solx86tools.tar `walkdir -nocvs -f tools` > ${SRCROOT}/jnet_bundles/logs/tar_solx86tools.log

cd $SRCROOT/jnet_bundles/linux/tools
echo "Checking out jnet linuxtools... log is ${SRCROOT}/jnet_bundles/logs/co_linuxtools.log"
cvs -d $JNET_TOOLS_CVSROOT co linuxtools > ${SRCROOT}/jnet_bundles/logs/co_linuxtools.log
mkdir -p $SRCROOT/jnet_bundles/linux/tools/java/maven/plugins
cp $TOOLROOT/java/maven/plugins/jregress-1.0.jar $SRCROOT/jnet_bundles/linux/tools/java/maven/plugins
mkdir -p $SRCROOT/jnet_bundles/linux/tools/java/ant/lib
cp $TOOLROOT/java/ant/lib/ant-junit.jar $SRCROOT/jnet_bundles/linux/tools/java/ant/lib
cd $SRCROOT/jnet_bundles/linux
echo "tarring up the bundle"
tar cvf linuxtools.tar `walkdir -nocvs -f tools` > ${SRCROOT}/jnet_bundles/logs/tar_linuxtools.log

cd $SRCROOT/jnet_bundles/cygwin/tools
echo "Checking out jnet cygwin tools... log is ${SRCROOT}/jnet_bundles/logs/co_cygwintools.log"
cvs -d $JNET_TOOLS_CVSROOT co cygwintools > ${SRCROOT}/jnet_bundles/logs/co_cygwintools.log
mkdir -p $SRCROOT/jnet_bundles/cygwin/tools/java/maven/plugins
cp $TOOLROOT/java/maven/plugins/jregress-1.0.jar $SRCROOT/jnet_bundles/cygwin/tools/java/maven/plugins
mkdir -p $SRCROOT/jnet_bundles/cygwin/tools/java/ant/lib
cp $TOOLROOT/java/ant/lib/ant-junit.jar $SRCROOT/jnet_bundles/cygwin/tools/java/ant/lib
cd $SRCROOT/jnet_bundles/cygwin
echo "tarring up the bundle"
tar cvf cygwintools.tar `walkdir -nocvs -f tools` > ${SRCROOT}/jnet_bundles/logs/tar_cygwintools.log

cd $SRCROOT/jnet_bundles/nt/tools
echo "Checking out jnet nttools... log is ${SRCROOT}/jnet_bundles/logs/co_nttools.log"
cvs -d $JNET_TOOLS_CVSROOT co nttools > ${SRCROOT}/jnet_bundles/logs/co_nttools.log
mkdir -p $SRCROOT/jnet_bundles/nt/tools/java/maven/plugins
cp $TOOLROOT/java/maven/plugins/jregress-1.0.jar $SRCROOT/jnet_bundles/nt/tools/java/maven/plugins
mkdir -p $SRCROOT/jnet_bundles/nt/tools/java/ant/lib
cp $TOOLROOT/java/ant/lib/ant-junit.jar $SRCROOT/jnet_bundles/nt/tools/java/ant/lib
cd $SRCROOT/jnet_bundles/nt
echo "tarring up the bundle"
tar cvf nttools.tar `walkdir -nocvs -f tools` > ${SRCROOT}/jnet_bundles/logs/tar_nttools.log

cd $SRCROOT/jnet_bundles/macosx/tools
echo "Checking out jnet macosx tools... log is ${SRCROOT}/jnet_bundles/logs/co_macosxtools.log"
cvs -d $JNET_TOOLS_CVSROOT co macosxtools > ${SRCROOT}/jnet_bundles/logs/co_macosxtools.log
mkdir -p $SRCROOT/jnet_bundles/macosx/tools/java/maven/plugins
cp $TOOLROOT/java/maven/plugins/jregress-1.0.jar $SRCROOT/jnet_bundles/macosx/tools/java/maven/plugins
mkdir -p $SRCROOT/jnet_bundles/macosx/tools/java/ant/lib
cp $TOOLROOT/java/ant/lib/ant-junit.jar $SRCROOT/jnet_bundles/macosx/tools/java/ant/lib
cd $SRCROOT/jnet_bundles/macosx
echo "tarring up the bundle"
tar cvf macosxtools.tar `walkdir -nocvs -f tools` > ${SRCROOT}/jnet_bundles/logs/tar_macosxtools.log
