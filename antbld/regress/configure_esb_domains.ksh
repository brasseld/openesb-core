#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)configure_esb_domains.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
# Script to create a jbiBootstrap.properties file for the CAS and ESB Member domains.

# Export the CAS Environment

exit_status=0

configureDomain()
# Usage:  configureDomain esb_instance cas_instance
# esb_instance <=> cas_instance => stand-alone CAS instance.
{
    if [ ! -d "$AS8BASE/domains/$1" -o ! -d "$AS8BASE/domains/$2" ]; then
        1>&2 bldmsg -warn -p configure_esb_domains.ksh One or more domains in $1, $2 are not installed, not configuring.
        return 1
    else
        1>&2 bldmsg -p configure_esb_domains.ksh configuring CAS instance $2 with member instance $1
    fi

    # Export the CAS environment
    my_test_domain=$2
    . $SRCROOT/antbld/regress/common_defs.ksh

    JMX_RMI_PORT=`grep 'jmx-connector accept' $JBI_DOMAIN_ROOT/config/domain.xml | head -1 | sed -e 's/^.* port="\([0-9][0-9]*\)".*/\1/'`
    CAS_JMX_REMOTE_URL="service:jmx:rmi:///jndi/rmi://localhost:${JMX_RMI_PORT}/management/rmi-jmx-connector"

    # Export the instance environment
    my_test_domain=$1
    . $SRCROOT/antbld/regress/common_defs.ksh
    
    if [ $1 = $2 ]; then
        # CAS instance
        createBootstrap true true
    else
        # Member instance
        createBootstrap true false
    fi

    return 0
}

createBootstrap()
{
    jbiBootStrap="$JBI_DOMAIN_ROOT/config/jbiBootStrap.properties"
    rm -f "$jbiBootStrap"
    cat > "$jbiBootStrap" << EOF
#####
# JBI framework bootstrap properties file.
# 
# WARNING:  changes to $jbiBootStrap are IGNORED after the first start.
# After the first start, all configuration changes to the JBI Framework
# must be made via the standard JMX interfaces (web console, CLI, or ant).
#####

JbiName=$my_test_domain
IsEsbMember=$1
IsCentralAdminServer=$2
CASUrl=$CAS_JMX_REMOTE_URL
CASAdminUser=$AS_ADMIN_USER
CASAdminPassword=$AS_ADMIN_PASSWD
EOF

    if [ $? -ne 0 ]; then
        bldmsg -p $0 -error FAILED to create: $jbiBootStrap
        exit_status=1
    fi
}


############################################################
## Configure CAS and ESBMember domains as an ESB schemaorg_apache_xmlbeans.system   ##
############################################################

configureDomain CAS CAS
#configureDomain ESBMember CAS

##############################################################
## Configure remaining domains as stand-alone CAS instances ##
##############################################################

configureDomain JBITest JBITest 
configureDomain ESBTest ESBTest
configureDomain domain1 domain1

exit $exit_status
