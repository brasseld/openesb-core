/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtPropertyGroup.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.impl;

import org.openesb.tools.extpropertysheet.IExtProperty;
import org.openesb.tools.extpropertysheet.IExtPropertyGroup;
import org.openesb.tools.extpropertysheet.IExtPropertyOptions;
import java.awt.Color;
import java.awt.Font;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author rdwivedi
 */
public class ExtPropertyGroup implements IExtPropertyGroup {
    
    private HashMap mPropsMap = new HashMap();
    private String mGroupName = null;
    private static Logger mLogger = Logger.getLogger(ExtPropertyGroup.class.getName());
    /** Creates a new instance of ExtPropertyGroup */
    public ExtPropertyGroup() {
    }

    public void add(IExtProperty prop) {
        mPropsMap.put(prop.getName(), prop);
    }

    public IExtProperty removeProperty(IExtProperty prop) {
       return (IExtProperty) mPropsMap.remove(prop.getName());
    }

    public IExtProperty removePoperty(String name) {
        return (IExtProperty) mPropsMap.remove(name);
    }

    public Collection getAllProperties() {
        return Collections.unmodifiableCollection(mPropsMap.values());
    }

    public void empty() {
        mPropsMap.clear();
    }

    public IExtProperty getProperty(String name) {
       return (IExtProperty) mPropsMap.get(name);
    }

    public int size() {
        return mPropsMap.size();
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String name) {
        mGroupName = name;
    }

    public void toXML(StringBuffer buffer) {
        buffer.append("<PROPGROUP ");
        buffer.append("name=").append(getGroupName()).append(" ");
        buffer.append(">");
        Iterator iter = getAllProperties().iterator();
        while(iter.hasNext()) {
            IExtProperty p = (IExtProperty)iter.next();
            p.toXML(buffer);
        }
        buffer.append("</PROPGROUP>//n");
    }
    
    public void setProperty(String name , Object value ) {
         
        if(value instanceof Integer) { 
            setProperty(name,value,IExtProperty.INPUT_INT_TYPE);
        } else if(value instanceof Number) {
            setProperty(name,value,IExtProperty.INPUT_FLOAT_TYPE);
        } else if(value instanceof Boolean) {
            setProperty(name,value,IExtProperty.BOOLEAN_RADIO_BUTTON);
        } else if(value instanceof Color) {
            setProperty(name,value,IExtProperty.COLOR_SELECTOR);
        } else if(value instanceof Font) {
            setProperty(name,value,IExtProperty.FONT_TYPE);
        } else {
            setProperty(name,value,IExtProperty.INPUT_TEXT);
        }
   }
     public void setProperty(String name , Object value , short type) {
       IExtProperty prop = new ExtProperty();
       prop.setName(name);
       prop.setValue(value);
       prop.setType(type);
       add(prop);
   }
   public void setProperty(String name , Object value , IExtPropertyOptions options) {
       IExtProperty prop = new ExtProperty();
       prop.setName(name);
       prop.setValue(value);
       if(value instanceof Integer) { 
            prop.setType(IExtProperty.INPUT_INT_LIST_TYPE);
        } else if(value instanceof Number) {
            prop.setType(IExtProperty.INPUT_FLOAT_LIST_TYPE);
        } else {
       
            prop.setType(IExtProperty.SELECTONE_MENU_COMPONENT);
        }
       prop.setOptions(options);
       add(prop);
   }
   
    
   
   public Object getPropertyValue(String name) {
       IExtProperty prop = getProperty(name);
       if(prop == null) {
           mLogger.info("The property with name " + name + " is not found.");
           return null;
       }
       return getProperty(name).getValue();
   }
    
}
