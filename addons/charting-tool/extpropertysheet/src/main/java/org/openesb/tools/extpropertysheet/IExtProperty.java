/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IExtProperty.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet;

import org.openesb.tools.extpropertysheet.validation.IExtPropertyValidator;


/**
 *
 * @author rdwivedi
 */
public interface IExtProperty {
    public static final short OUTPUT_TEXT = 0;
    public static final short INPUT_TEXT = 1;
    public static final short CALENDER_TYPE = 2;
    public static final short COLOR_SELECTOR = 3;
    public static final short RADIO_BUTTONS = 4;
    public static final short SELECTONE_MENU_COMPONENT = 5;
    public static final short SELECTMULTIPLE_MENU_COMPONENT = 6;
    public static final short HIDDEN_TEXT = 7;   
    public static final short BOOLEAN_RADIO_BUTTON = 8;
    public static final short FONT_TYPE = 9;
    public static final short INPUT_INT_TYPE = 10;
    public static final short INPUT_FLOAT_TYPE = 11;
    public static final short INPUT_INT_LIST_TYPE = 12;
    public static final short INPUT_FLOAT_LIST_TYPE = 13;
    
    
    String getName();
    void setName(String name);
    Object getValue();
    void setValue(Object value);
    IExtPropertyOptions getOptions();
    void setOptions(IExtPropertyOptions opts);
    short getType();
    void setType(short type);
    String getValidationRule();
    void setvalidationRule(String rule);
    void toXML(StringBuffer buffer);
    
    
    
    
}
