/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LAFServlet.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LAFServlet extends javax.servlet.http.HttpServlet
{

	public LAFServlet()
	{
	}

	public void init(javax.servlet.ServletConfig servletconfig)
		throws javax.servlet.ServletException
	{
		super.init(servletconfig);
	}

	public void destroy()
	{
	}

	protected void processRequest(javax.servlet.http.HttpServletRequest httpservletrequest, javax.servlet.http.HttpServletResponse httpservletresponse)
		throws javax.servlet.ServletException, java.io.IOException
	{
                httpservletresponse.setContentType("text/xml;charset=utf-8");
		Object obj = null;
		java.io.PrintWriter printwriter = httpservletresponse.getWriter();
		java.io.PrintWriter printwriter1 = null;
		java.lang.String s = getServletContext().getRealPath("/");
		try
		{
			try
			{
				java.lang.String s1 = s + "/myout";
				java.lang.String s2 = s1 + "/out_" + getDate() + ".xml";
				java.io.File file = new File(s + "/myout");
				file.mkdirs();
				java.io.FileWriter filewriter = new FileWriter(s2);
				printwriter1 = new PrintWriter(filewriter);
			}
			catch(java.lang.Exception exception)
			{
				exception.printStackTrace();
			}
			java.io.BufferedReader bufferedreader = httpservletrequest.getReader();
			httpservletresponse.setContentType("text/xml;charset=utf-8");
			do
			{
				if((s = bufferedreader.readLine()) == null)
					break;
				printwriter.write(s);
				printwriter.write("\n");
				if(printwriter1 != null)
				{
					printwriter1.write(s);
					printwriter1.write("\n");
				}
			} while(true);
		/*	printwriter.write(s);
			printwriter.write("\n");
			if(printwriter1 != null)
			{
				printwriter1.write(s);
				printwriter1.write("\n");
			}
                 */
		}
		catch(java.lang.Exception exception1)
		{
			exception1.printStackTrace();
		}
		try
		{
			printwriter.close();
			if(printwriter1 != null)
				printwriter1.close();
		}
		catch(java.lang.Exception exception2)
		{
			exception2.printStackTrace();
		}
	}

	protected void doGet(javax.servlet.http.HttpServletRequest httpservletrequest, javax.servlet.http.HttpServletResponse httpservletresponse)
		throws javax.servlet.ServletException, java.io.IOException
	{
		processRequest(httpservletrequest, httpservletresponse);
	}

	protected void doPost(javax.servlet.http.HttpServletRequest httpservletrequest, javax.servlet.http.HttpServletResponse httpservletresponse)
		throws javax.servlet.ServletException, java.io.IOException
	{
		processRequest(httpservletrequest, httpservletresponse);
	}

	public java.lang.String getServletInfo()
	{
		return "Short description";
	}

	private java.lang.String getDate()
	{
		java.util.GregorianCalendar gregoriancalendar = new GregorianCalendar();
		java.util.Date date = gregoriancalendar.getTime();
		java.text.SimpleDateFormat simpledateformat = new SimpleDateFormat("ddHHmmssSS");
		java.lang.String s = simpledateformat.format(date);
		return s;
	}
}
