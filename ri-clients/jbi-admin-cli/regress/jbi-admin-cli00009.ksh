#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
    
  #--------------------------------------------------------------------------------
  # Make sure http binding component is started. (output redirected to TEMP file)
  #--------------------------------------------------------------------------------
  start_component server sun-http-binding
      
  #--------------------------------------------------------------------------------
  # Test setting and showing the component configuration values
  #--------------------------------------------------------------------------------
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set the OutboundTread component value to 5 from the command line"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-configuration --component=sun-http-binding OutboundThreads=5"
  $AS8BASE/bin/asadmin set-jbi-component-configuration --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS OutboundThreads=5
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show just the OutboundThreads configuration value"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --configuration sun-http-binding | grep OutboundThreads"
  $AS8BASE/bin/asadmin show-jbi-binding-component --configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep OutboundThreads
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a both the OutboundThread and UseJVMProxySettings variables from the command line"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-configuration --component=sun-http-binding OutboundThreads=7,UseJVMProxySettings=true"
  $AS8BASE/bin/asadmin set-jbi-component-configuration --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS OutboundThreads=7,UseJVMProxySettings=true
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the sun-http-binding Component Configuration Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --configuration sun-http-binding | grep OutboundThreads"
  $AS8BASE/bin/asadmin show-jbi-binding-component --configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep OutboundThreads

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the sun-http-binding Component Configuration Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --configuration sun-http-binding | grep UseJVMProxySettings"
  $AS8BASE/bin/asadmin show-jbi-binding-component --configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep UseJVMProxySettings

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Using the property file, set the values"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-configuration --component=sun-http-binding componentConfig.property"
  $AS8BASE/bin/asadmin set-jbi-component-configuration --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/componentConfig.property
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the sun-http-binding Component Configuration Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --configuration sun-http-binding | grep OutboundThreads"
  $AS8BASE/bin/asadmin show-jbi-binding-component --configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep OutboundThreads
  
  
  #--------------------------------------------------------------------------------
  # Test setting and showing the component logger levels
  #--------------------------------------------------------------------------------
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Using the property file, set the logger levels"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-logger --component=sun-http-binding componentLoggers.property"
  $AS8BASE/bin/asadmin set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/componentLoggers.property
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show just the com.sun.jbi.httpsoapbc.HttpNormalizer logger value"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --loggers sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer"
  $AS8BASE/bin/asadmin show-jbi-binding-component --loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show com.sun.jbi.httpsoapbc.HttpNormalizer but use the --terse flag"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --loggers --terse sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer"
  $AS8BASE/bin/asadmin show-jbi-binding-component --loggers --terse --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set the logger com.sun.jbi.httpsoapbc.HttpNormalizer to FINE from the command line"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-logger --component=sun-http-binding com.sun.jbi.httpsoapbc.HttpNormalizer=FINE"
  $AS8BASE/bin/asadmin set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS com.sun.jbi.httpsoapbc.HttpNormalizer=FINE
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show all the sun-http-binding Logger Levels"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --loggers sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer"
  $AS8BASE/bin/asadmin show-jbi-binding-component --loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a both the com.sun.jbi.httpsoapbc.HttpNormalizer and the"
  echo " com.sun.jbi.httpsoapbc.Extension loggers to WARNING from the command line"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-logger --component=sun-http-binding com.sun.jbi.httpsoapbc.HttpNormalizer=WARNING,com.sun.jbi.httpsoapbc.Extension=WARNING"
  $AS8BASE/bin/asadmin set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS com.sun.jbi.httpsoapbc.HttpNormalizer=WARNING,com.sun.jbi.httpsoapbc.Extension=WARNING
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show all the sun-http-binding Logger Levels"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --loggers sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer"
  $AS8BASE/bin/asadmin show-jbi-binding-component --loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep com.sun.jbi.httpsoapbc.HttpNormalizer
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the com.sun.jbi.httpsoapbc.Extension Logger Levels"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --loggers sun-http-binding | grep com.sun.jbi.httpsoapbc.Extension"
  $AS8BASE/bin/asadmin show-jbi-binding-component --loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep com.sun.jbi.httpsoapbc.Extension
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Using the property file, set the logger levels"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-logger --component=sun-http-binding componentLoggers.property"
  $AS8BASE/bin/asadmin set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/componentLoggers.property
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the com.sun.jbi.httpsoapbc.Extension Logger Levels"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --loggers sun-http-binding | grep com.sun.jbi.httpsoapbc.Extension"
  $AS8BASE/bin/asadmin show-jbi-binding-component --loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding | grep com.sun.jbi.httpsoapbc.Extension
 
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00009"
TEST_DESCRIPTION="Test Show and Set for Component configuration and loggers"
. ./regress_defs.ksh
run_test

exit 0


