/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.ConsumingEndpointStatisticsDataCreator;
import com.sun.esb.management.common.data.helper.EndpointStatisticsDataWriter;
import com.sun.esb.management.common.data.helper.ProvisioningEndpointStatisticsDataCreator;

/**
 * Provides Endpoint Statistics
 * 
 * @author graj
 * 
 */
public abstract class EndpointStatisticsData implements
        IEndpointStatisticsData, Serializable {
    static final long                                      serialVersionUID                 = -1L;
    
    public static final String                             INSTANCE_NAME_KEY                = "InstanceName";
    
    public static final String                             COMPONENT_NAME_KEY               = "ComponentName";
    
    public static final String                             NUM_ACTIVE_EXCHANGES_KEY         = "NumActiveExchanges";
    
    public static final String                             NUM_RECEIVED_DONES_KEY           = "NumReceivedDONEs";
    
    public static final String                             NUM_SENT_DONES_KEY               = "NumSentDONEs";
    
    public static final String                             NUM_RECEIVED_FAULTS_KEY          = "NumReceivedFaults";
    
    public static final String                             NUM_SENT_FAULTS_KEY              = "NumSentFaults";
    
    public static final String                             NUM_RECEIVED_ERRORS_KEY          = "NumReceivedErrors";
    
    public static final String                             NUM_SENT_ERRORS_KEY              = "NumSentErrors";
    
    public static final String                             ME_COMPONENT_TIME_AVG_KEY        =  "MessageExchangeComponentTime Avg (ns)";
    
    public static final String                             ME_DELIVERY_CHANNEL_TIME_AVG_KEY = "MessageExchangeDeliveryTime Avg (ns)";
    
    public static final String                             ME_MESSAGE_SERVICE_TIME_AVG_KEY  = "MessageExchangeNMRTime Avg (ns)";
    
    public static final String                             PERFORMANCE_MEASUREMENTS_KEY     = "PerformanceMeasurements";
    
    public static final String                             EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY        = "ExtendedTimingStatisticsFlagEnabled";
    
    protected boolean                                      isProvisioningEndpoint;
    
    protected String                                       componentName;
    
    protected String                                       instanceName;
    
    protected long                                         numberOfActiveExchanges;
    
    protected long                                         numberOfReceivedDones;
    
    protected long                                         numberOfSentDones;
    
    protected long                                         numberOfReceivedFaults;
    
    protected long                                         numberOfReceivedErrors;
    
    protected long                                         numberOfSentFaults;
    
    protected long                                         numberOfSentErrors;
    
    protected long                                       messageExchangeComponentTimeAverage;
    
    protected long                                       messageExchangeDeliveryChannelTimeAverage;
    
    protected long                                       messageExchangeServiceTimeAverage;
    
    protected boolean                                    extendedTimingStatisticsFlagEnabled;
    
    protected Map<String /* category */, PerformanceData> categoryToPerformanceDataMap     = new HashMap<String /* category */, PerformanceData>();
    
    /** Constructor - creates an EndpointStatisticsData object */
    public EndpointStatisticsData() {
    }
    
    /**
     * Constructor - creates an EndpointStatisticsData object
     * 
     * @param data
     */
    public EndpointStatisticsData(EndpointStatisticsData data) {
        this.setCategoryToPerformanceDataMap(data
                .getCategoryToPerformanceDataMap());
        this.setInstanceName(data.getInstanceName());
        this.setComponentName(data.getComponentName());
        this.setNumberOfActiveExchanges(data.getNumberOfActiveExchanges());
        this.setNumberOfReceivedDones(data.getNumberOfReceivedDones());
        this.setNumberOfReceivedErrors(data.getNumberOfReceivedErrors());
        this.setNumberOfReceivedFaults(data.getNumberOfReceivedFaults());
        this.setNumberOfSentDones(data.getNumberOfSentDones());
        this.setNumberOfSentErrors(data.getNumberOfSentErrors());
        this.setNumberOfSentFaults(data.getNumberOfSentFaults());
        this.setExtendedTimingStatisticsFlagEnabled(data.isExtendedTimingStatisticsFlagEnabled());
        this.setMessageExchangeComponentTimeAverage(data
                .getMessageExchangeComponentTimeAverage());
        this.setMessageExchangeDeliveryChannelTimeAverage(data
                .getMessageExchangeDeliveryChannelTimeAverage());
        this.setMessageExchangeServiceTimeAverage(data
                .getMessageExchangeServiceTimeAverage());
        
    }
    
    /**
     * Generate Tabular Data for this object
     * @return tabular data of this object
     */
    static public TabularData generateTabularData(Map<String /* instanceName */, IEndpointStatisticsData> map) {
        TabularData tabularData = null;
        boolean isProvisioningEndpoint = false;
        Collection<IEndpointStatisticsData> collection = map.values(); 
        for(IEndpointStatisticsData data : collection) {
            if(data.isProvisioningEndpoint() == true) {
                isProvisioningEndpoint = true;
            }
            break;
        }
        try {
            if(isProvisioningEndpoint == true) {
                tabularData = ProvisioningEndpointStatisticsDataCreator.createTabularData(map);
            } else {
                tabularData = ConsumingEndpointStatisticsDataCreator.createTabularData(map);
            }
        } catch (ManagementRemoteException e) {
        }
        return tabularData;
    }    
    
    /**
     * Retrieves the Endpoint Statistics Data
     * 
     * @param tabularData
     * @return NMR Statistics Data
     */
    @SuppressWarnings("unchecked")
    static public Map<String /* instanceName */, IEndpointStatisticsData> retrieveDataMap(
            TabularData tabularData) {
        IEndpointStatisticsData data = null;
        ProvisioningEndpointStatisticsData provisioningData = null;
        ConsumingEndpointStatisticsData consumingData = null;
        Map<String /* instanceName */, IEndpointStatisticsData> map = null;
        map = new HashMap<String /* instanceName */, IEndpointStatisticsData>();
        for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                .hasNext();) {
            CompositeData compositeData = (CompositeData) dataIterator.next();
            CompositeType compositeType = compositeData.getCompositeType();
            if(compositeType.keySet().contains(ProvisioningEndpointStatisticsData.ACTIVATION_TIME_KEY) 
                    || compositeType.keySet().contains(ProvisioningEndpointStatisticsData.ME_RESPONSE_TIME_AVG_KEY) 
                    || compositeType.keySet().contains(ProvisioningEndpointStatisticsData.NUM_RECEIVED_REQUESTS_KEY) 
                    || compositeType.keySet().contains(ProvisioningEndpointStatisticsData.NUM_SENT_REPLIES_KEY) 
                    || compositeType.keySet().contains(ProvisioningEndpointStatisticsData.UPTIME_KEY)) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            } else {
                data = new ConsumingEndpointStatisticsData();
                consumingData = (ConsumingEndpointStatisticsData) data;
            }
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) {
                String item = (String) itemIterator.next();
                // 1. Read all the Provisioning Endpoints first
                if (true == item.equals(ProvisioningEndpointStatisticsData.ACTIVATION_TIME_KEY)) {
                    if(data == null) {
                        data = new ProvisioningEndpointStatisticsData();
                        provisioningData = (ProvisioningEndpointStatisticsData) data;
                    }
                    Date value = (Date) compositeData.get(item);
                    provisioningData.setActivationTime(value);
                }
                if (true == item.equals(ProvisioningEndpointStatisticsData.NUM_RECEIVED_REQUESTS_KEY)) {
                    if(data == null) {
                        data = new ProvisioningEndpointStatisticsData();
                        provisioningData = (ProvisioningEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        provisioningData.setNumberOfReceivedRequests(value.longValue());
                    }
                }
                if (true == item.equals(ProvisioningEndpointStatisticsData.NUM_SENT_REPLIES_KEY)) {
                    if(data == null) {
                        data = new ProvisioningEndpointStatisticsData();
                        provisioningData = (ProvisioningEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        provisioningData.setNumberOfSentReplies(value.longValue());
                    }
                }
                if (true == item.equals(ProvisioningEndpointStatisticsData.UPTIME_KEY)) {
                    if(data == null) {
                        data = new ProvisioningEndpointStatisticsData();
                        provisioningData = (ProvisioningEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        provisioningData.setUptime(value.longValue());
                    }
                }
                if (true == item.equals(ProvisioningEndpointStatisticsData.ME_RESPONSE_TIME_AVG_KEY)) {
                    if(data == null) {
                        data = new ProvisioningEndpointStatisticsData();
                        provisioningData = (ProvisioningEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        provisioningData.setMessageExchangeResponseTimeAverage(value.longValue());
                    }
                }
                // 2. Read all the Consuming Endpoints.
                if (true == item.equals(ConsumingEndpointStatisticsData.NUM_RECEIVED_REPLIES_KEY)) {
                    if(data == null) {
                        data = new ConsumingEndpointStatisticsData();
                        consumingData = (ConsumingEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        consumingData.setNumberOfReceivedReplies(value.longValue());
                    }
                }
                if (true == item.equals(ConsumingEndpointStatisticsData.NUM_SENT_REQUESTS_KEY)) {
                    if(data == null) {
                        data = new ConsumingEndpointStatisticsData();
                        consumingData = (ConsumingEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        consumingData.setNumberOfSentRequests(value.longValue());
                    }
                }
                if (true == item.equals(ConsumingEndpointStatisticsData.ME_STATUS_TIME_AVG_KEY)) {
                    if(data == null) {
                        data = new ConsumingEndpointStatisticsData();
                        consumingData = (ConsumingEndpointStatisticsData) data;
                    }
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        consumingData.setMessageExchangeStatusTimeAverage(value.longValue());
                    }
                }
                // 3. Read all the base Endpoint Statistics Data.
                if (true == item.equals(EndpointStatisticsData.INSTANCE_NAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    if(data != null) {
                        data.setInstanceName(value);
                    }
                }
                if (true == item.equals(EndpointStatisticsData.COMPONENT_NAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    if(data == null) {
                        data.setComponentName(value);
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_ACTIVE_EXCHANGES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(value != null) {
                        data.setNumberOfActiveExchanges(value.longValue());
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_RECEIVED_DONES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setNumberOfReceivedDones(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_RECEIVED_ERRORS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setNumberOfReceivedErrors(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_RECEIVED_FAULTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setNumberOfReceivedFaults(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_SENT_DONES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setNumberOfSentDones(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_SENT_ERRORS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setNumberOfSentErrors(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.NUM_SENT_FAULTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setNumberOfSentFaults(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY)) {
                    Boolean value = (Boolean) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setExtendedTimingStatisticsFlagEnabled(value.booleanValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.ME_COMPONENT_TIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setMessageExchangeComponentTimeAverage(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.ME_DELIVERY_CHANNEL_TIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setMessageExchangeDeliveryChannelTimeAverage(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.ME_MESSAGE_SERVICE_TIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        if(value != null) {
                            data.setMessageExchangeServiceTimeAverage(value.longValue());
                        }
                    }
                }
                if (true == item.equals(EndpointStatisticsData.PERFORMANCE_MEASUREMENTS_KEY)) {
                    Map<String /* Category */, PerformanceData> performanceMap = null;
                    TabularData value = (TabularData) compositeData.get(item);
                    if(value != null) {
                        performanceMap = PerformanceData.retrieveDataMap(value);
                    }
                    if(data != null) {
                        data.setCategoryToPerformanceDataMap(performanceMap);
                    }
                }
            }
            map.put(data.getInstanceName(), data);
        }
        return map;
    }
    
    /**
     * Converts a Component Statistics Data to an XML String
     * 
     * @param map
     * @return XML string representing a Component Statistics Datum
     * @throws ManagementRemoteException
     */
    static public String convertDataMapToXML(
            Map<String /* instanceName */, IEndpointStatisticsData> map)
            throws ManagementRemoteException {
        String xmlText = null;
        try {
            xmlText = EndpointStatisticsDataWriter.serialize(map);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (TransformerException e) {
            throw new ManagementRemoteException(e);
        }
        
        return xmlText;
        
    }
    
    /**
     * @return the instanceName
     */
    public String getInstanceName() {
        return this.instanceName;
    }
    
    /**
     * @return the isProvisioningEndpoint
     */
    public boolean isProvisioningEndpoint() {
        return this.isProvisioningEndpoint;
    }
    
    /**
     * @return the componentName
     */
    public String getComponentName() {
        return this.componentName;
    }

    /**
     * @param componentName the componentName to set
     */
    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }
    
    /**
     * @return the numberOfActiveExchanges
     */
    public long getNumberOfActiveExchanges() {
        return this.numberOfActiveExchanges;
    }
    
    /**
     * @param numberOfActiveExchanges
     *            the numberOfActiveExchanges to set
     */
    public void setNumberOfActiveExchanges(long numberOfActiveExchanges) {
        this.numberOfActiveExchanges = numberOfActiveExchanges;
    }
    
    

    /**
     * @param isProvisioningEndpoint
     *            the isProvisioningEndpoint to set
     */
    public void setProvisioningEndpoint(boolean isProvisioningEndpoint) {
        this.isProvisioningEndpoint = isProvisioningEndpoint;
    }
    
    /**
     * @param instanceName
     *            the instanceName to set
     */
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    
    /**
     * @return the numberOfReceivedDones
     */
    public long getNumberOfReceivedDones() {
        return this.numberOfReceivedDones;
    }
    
    /**
     * @param numberOfReceivedDones
     *            the numberOfReceivedDones to set
     */
    public void setNumberOfReceivedDones(long numberOfReceivedDones) {
        this.numberOfReceivedDones = numberOfReceivedDones;
    }
    
    /**
     * @return the numberOfSentDones
     */
    public long getNumberOfSentDones() {
        return this.numberOfSentDones;
    }
    
    /**
     * @param numberOfSentDones
     *            the numberOfSentDones to set
     */
    public void setNumberOfSentDones(long numberOfSentDones) {
        this.numberOfSentDones = numberOfSentDones;
    }
    
    /**
     * @return the numberOfReceivedFaults
     */
    public long getNumberOfReceivedFaults() {
        return this.numberOfReceivedFaults;
    }
    
    /**
     * @param numberOfReceivedFaults
     *            the numberOfReceivedFaults to set
     */
    public void setNumberOfReceivedFaults(long numberOfReceivedFaults) {
        this.numberOfReceivedFaults = numberOfReceivedFaults;
    }
    
    /**
     * @return the numberOfReceivedErrors
     */
    public long getNumberOfReceivedErrors() {
        return this.numberOfReceivedErrors;
    }
    
    /**
     * @param numberOfReceivedErrors
     *            the numberOfReceivedErrors to set
     */
    public void setNumberOfReceivedErrors(long numberOfReceivedErrors) {
        this.numberOfReceivedErrors = numberOfReceivedErrors;
    }
    
    /**
     * @return the numberOfSentFaults
     */
    public long getNumberOfSentFaults() {
        return this.numberOfSentFaults;
    }
    
    /**
     * @param numberOfSentFaults
     *            the numberOfSentFaults to set
     */
    public void setNumberOfSentFaults(long numberOfSentFaults) {
        this.numberOfSentFaults = numberOfSentFaults;
    }
    
    /**
     * @return the numberOfSentErrors
     */
    public long getNumberOfSentErrors() {
        return this.numberOfSentErrors;
    }
    
    /**
     * @param numberOfSentErrors
     *            the numberOfSentErrors to set
     */
    public void setNumberOfSentErrors(long numberOfSentErrors) {
        this.numberOfSentErrors = numberOfSentErrors;
    }
    
    /**
     * @return the extendedTimingStatisticsFlagEnabled
     */
    public boolean isExtendedTimingStatisticsFlagEnabled() {
        return this.extendedTimingStatisticsFlagEnabled;
    }

    /**
     * @param extendedTimingStatisticsFlagEnabled the extendedTimingStatisticsFlagEnabled to set
     */
    public void setExtendedTimingStatisticsFlagEnabled(
            boolean extendedTimingStatisticsFlagEnabled) {
        this.extendedTimingStatisticsFlagEnabled = extendedTimingStatisticsFlagEnabled;
    }

    /**
     * @return the messageExchangeComponentTimeAverage
     */
    public long getMessageExchangeComponentTimeAverage() {
        return this.messageExchangeComponentTimeAverage;
    }
    
    /**
     * @param messageExchangeComponentTimeAverage
     *            the messageExchangeComponentTimeAverage to set
     */
    public void setMessageExchangeComponentTimeAverage(
            long messageExchangeComponentTimeAverage) {
        this.messageExchangeComponentTimeAverage = messageExchangeComponentTimeAverage;
    }
    
    /**
     * @return the messageExchangeDeliveryChannelTimeAverage
     */
    public long getMessageExchangeDeliveryChannelTimeAverage() {
        return this.messageExchangeDeliveryChannelTimeAverage;
    }
    
    /**
     * @param messageExchangeDeliveryChannelTimeAverage
     *            the messageExchangeDeliveryChannelTimeAverage to set
     */
    public void setMessageExchangeDeliveryChannelTimeAverage(
            long messageExchangeDeliveryChannelTimeAverage) {
        this.messageExchangeDeliveryChannelTimeAverage = messageExchangeDeliveryChannelTimeAverage;
    }
    
    /**
     * @return the messageExchangeServiceTimeAverage
     */
    public long getMessageExchangeServiceTimeAverage() {
        return this.messageExchangeServiceTimeAverage;
    }
    
    /**
     * @param messageExchangeServiceTimeAverage
     *            the messageExchangeServiceTimeAverage to set
     */
    public void setMessageExchangeServiceTimeAverage(
            long messageExchangeServiceTimeAverage) {
        this.messageExchangeServiceTimeAverage = messageExchangeServiceTimeAverage;
    }
    
    /**
     * @return the categoryToPerformanceDataMap
     */
    public Map<String, PerformanceData> getCategoryToPerformanceDataMap() {
        return this.categoryToPerformanceDataMap;
    }
    
    /**
     * @param categoryToPerformanceDataMap
     *            the categoryToPerformanceDataMap to set
     */
    public void setCategoryToPerformanceDataMap(
            Map<String, PerformanceData> categoryToPerformanceDataMap) {
        this.categoryToPerformanceDataMap = categoryToPerformanceDataMap;
    }
    
    /**
     * Return a displayable string of values
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Instance Name" + "=" + this.getInstanceName());
        buffer.append("\n    Component Name" + "=" + this.getComponentName());
        buffer.append("\n    Number of Active Exchanges" + "=" + this.getNumberOfActiveExchanges());
        buffer.append("\n    Number Of Received Dones" + "="
                + this.getNumberOfReceivedDones());
        buffer.append("\n    Number Of Received Faults" + "="
                + this.getNumberOfReceivedFaults());
        buffer.append("\n    Number Of Received Errors" + "="
                + this.getNumberOfReceivedErrors());
        buffer.append("\n    Number Of Sent Dones" + "="
                + this.getNumberOfSentDones());
        buffer.append("\n    Number Of Sent Errors" + "="
                + this.getNumberOfSentErrors());
        buffer.append("\n    Number Of Sent Faults" + "="
                + this.getNumberOfSentFaults());
        buffer.append("\n    Extended Timing Statistics are "+(this.isExtendedTimingStatisticsFlagEnabled()?"":"NOT")+" Enabled.");
        buffer.append("\n\t    Message Exchange Component Time Average" + "="
                + this.getMessageExchangeComponentTimeAverage());
        buffer.append("\n\t    Message Exchange Delivery Channel Time Average"
                + "=" + this.getMessageExchangeDeliveryChannelTimeAverage());
        buffer.append("\n\t    Message Exchange Service Time Average" + "="
                + this.getMessageExchangeServiceTimeAverage());
        for (String category : this.getCategoryToPerformanceDataMap().keySet()) {
            System.out.println(this.getCategoryToPerformanceDataMap().get(
                    category).getDisplayString());
        }
        buffer.append("\n");
        return buffer.toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
