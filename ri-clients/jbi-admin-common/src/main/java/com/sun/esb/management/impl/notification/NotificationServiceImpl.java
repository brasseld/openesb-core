/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NotificationServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;

import com.sun.esb.management.api.notification.EventNotification;
import com.sun.esb.management.api.notification.EventNotificationListener;
import com.sun.esb.management.api.notification.NotificationService;
import com.sun.esb.management.base.services.BaseServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;

/**
 * @author graj
 *
 */
public class NotificationServiceImpl extends BaseServiceImpl implements NotificationListener,
        NotificationService {
    
    static final long serialVersionUID = -1L;
    

    /**
     * Store a list of listeners
     */
    private ArrayList<EventNotificationListener> listeners = new ArrayList<EventNotificationListener>();
    
    /**
     * Executor service to send notifications on threads from a pool.
     */
    private ExecutorService executorService;
    
    /** Constructor - Constructs a new instance of NotificationServiceImpl */
    public NotificationServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of NotificationServiceImpl
     * 
     * @param serverConnection
     */
    public NotificationServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of NotificationServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public NotificationServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
        // Create the thread pool to be used for sending notifications
        executorService = Executors.newCachedThreadPool();
        registerForNotifications();
    }
    
    /**
     * Register for notifications
     */
    protected void registerForNotifications() {
        try {
            ObjectName mbeanName = getNotificationServiceMBeanObjectName();
            this.remoteConnection.addNotificationListener(mbeanName, this, null, new Object());
        } catch (InstanceNotFoundException e) {
        } catch (ManagementRemoteException e) {
        } catch (IOException e) {
        }
    }

    /**
     * Handle the Event Notification
     * @param the notification
     * @param the handback object
     *  
     * @see javax.management.NotificationListener#handleNotification(javax.management.Notification, java.lang.Object)
     */
    public void handleNotification(Notification notification, Object handback) {
        EventNotificationImpl event = new EventNotificationImpl(notification.getSource());
        event.setNotification(notification);
        executorService.execute(new NotificationSender(this.listeners, event));
    }
    
    /**
     * Add notification listener
     * @param an EventNotificationListener object
     * @see com.sun.esb.management.api.notification.NotificationService#addNotificationEventListener(com.sun.esb.management.api.notification.EventNotificationListener)
     */
    public void addNotificationEventListener(EventNotificationListener aListener)
            throws ManagementRemoteException {
        listeners.add(aListener);
    }
    
    /**
     * Remove notification listener
     * @param an EventNotificationListener object
     * @see com.sun.esb.management.api.notification.NotificationService#removeNotificationEventListener(com.sun.esb.management.api.notification.EventNotificationListener)
     */
    public void removeNotificationEventListener(EventNotificationListener aListener) {
        listeners.remove(aListener);
    }
    
    /**
     * Class to send out notifications on a separate thread 
     * @author graj
     */
    class NotificationSender implements Runnable {
        private ArrayList<EventNotificationListener> listeners = new ArrayList<EventNotificationListener>();
        
        /**
         * Notification instance to be sent.
         */
        EventNotification eventNotification;     
        
        /**
         * Constructor.
         *
         * @param en the instance of EventNotifier that called this constructor.
         * @param notif the JMX Notification to be sent.
         */
        NotificationSender(ArrayList<EventNotificationListener> en, 
                EventNotification notif)
        {
            listeners = en;
            eventNotification = notif;
        }

        /**
         * Run the actual sending of the notification. This runs on a thread
         * from the pool created by Executors.newCachedThreadPool().
         */
        public void run()
        {
            ArrayList<EventNotificationListener> list = null;
            synchronized(this) {
                list = (ArrayList<EventNotificationListener>)listeners.clone();
            }
            for(EventNotificationListener aListener : list) {
                aListener.processNotification(eventNotification);
            }
        }        
    }
    
    
}
