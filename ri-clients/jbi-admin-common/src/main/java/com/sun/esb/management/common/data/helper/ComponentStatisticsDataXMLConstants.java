/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsDataXMLConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

/**
 * @author graj
 * 
 */
public interface ComponentStatisticsDataXMLConstants {
    // XML TAGS
    public static final String VERSION_KEY                                        = "version";
    
    public static final String VERSION_VALUE                                      = "1.0";
    
    public static final String NAMESPACE_KEY                                      = "xmlns";
    
    public static final String NAMESPACE_VALUE                                    = "http://java.sun.com/xml/ns/esb/management/ComponentStatisticsDataList";
    
    public static final String COMPONENT_STATISTICS_DATA_LIST_KEY                 = "ComponentStatisticsDataList";
    
    public static final String COMPONENT_STATISTICS_DATA_KEY                      = "ComponentStatisticsData";
    
    public static final String INSTANCE_NAME_KEY                                  = "InstanceName";
    
    public static final String COMPONENT_UPTIME_KEY                               = "ComponentUpTime";
    
    public static final String NUMBER_OF_ACTIVATED_ENDPOINTS_KEY                  = "NumberOfActivatedEndpoints";
    
    public static final String NUMBER_OF_RECEIVED_REQUESTS_KEY                    = "NumberOfReceivedRequests";
    
    public static final String NUMBER_OF_SENT_REQUESTS_KEY                        = "NumberOfSentRequests";
    
    public static final String NUMBER_OF_RECEIVED_REPLIES_KEY                     = "NumberOfReceivedReplies";
    
    public static final String NUMBER_OF_SENT_REPLIES_KEY                         = "NumberOfSentReplies";
    
    public static final String NUMBER_OF_RECEIVED_DONES_KEY                       = "NumberOfReceivedDones";
    
    public static final String NUMBER_OF_SENT_DONES_KEY                           = "NumberOfSentDones";
    
    public static final String NUMBER_OF_RECEIVED_FAULTS_KEY                      = "NumberOfReceivedFaults";
    
    public static final String NUMBER_OF_SENT_FAULTS_KEY                          = "NumberOfSentFaults";
    
    public static final String NUMBER_OF_RECEIVED_ERRORS_KEY                      = "NumberOfReceivedErrors";
    
    public static final String NUMBER_OF_SENT_ERRORS_KEY                          = "NumberOfSentErrors";
    
    public static final String NUMBER_OF_COMPLETED_EXCHANGES_KEY                  = "NumberOfCompletedExchanges";
    
    public static final String NUMBER_OF_ACTIVE_EXCHANGES_KEY                     = "NumberOfActiveExchanges";
    
    public static final String NUMBER_OF_ERROR_EXCHANGES_KEY                      = "NumberOfErrorExchanges";
    
    public static final String MESSAGE_EXCHANGE_RESPONSE_TIME_AVERAGE_KEY         = "MessageExchangeResponseTimeAverage";
    
    public static final String MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY        = "MessageExchangeComponentTimeAverage";
    
    public static final String MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY = "MessageExchangeDeliveryChannelTimeAverage";
    
    public static final String MESSAGE_EXCHANGE_MESSAGE_SERVICE_TIME_AVERAGE_KEY  = "MessageExchangeMessageServiceTimeAverage";
    
    public static final String COMPONENT_EXTENSION_STATUS_KEY                     = "ComponentExtensionStatus";
    
    public static final String EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY        = "ExtendedTimingStatisticsFlagEnabled";
    
}
