/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJMXConnectionProperties.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJMXConnectionProperties extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestJMXConnectionProperties(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testConnectionURL() throws Exception
    {
        String defURL = JMXConnectionProperties.getInstance().getConnectionURL();
        
        /*
        if ( "service:jmx:jmxmp://localhost:5555".equals(defURL) ) { 
            System.out.println("Default URL: " + defURL);
        } else {
            throw new Exception ("In correct Default URL " + defURL );
        }
        */
        if ( "service:jmx:rmi:///jndi/rmi://localhost:8686/management/rmi-jmx-connector"
        .equals(defURL) ) { 
            System.out.println("Default URL: " + defURL);
        } else {
            throw new Exception ("In correct Default URL " + defURL );
        }
        
        String customURL = 
        JMXConnectionProperties.getInstance().getConnectionURL("myhost", "7777");
        
        if ( "service:jmx:rmi:///jndi/rmi://myhost:7777/management/rmi-jmx-connector"
        .equals(customURL) ) { 
            System.out.println("Custom URL: " + customURL);
        } else {
            throw new Exception ("In Correct Custom URL " + customURL );
        }
        
        /*
        String customURL = 
        JMXConnectionProperties.getInstance().getConnectionURL("myhost", "7777");
        
        if ( "service:jmx:jmxmp://myhost:7777".equals(customURL) ) { 
            System.out.println("Custom URL: " + customURL);
        } else {
            throw new Exception ("In Correct Custom URL " + customURL );
        }
         */
        
        /*
        System.setProperty("com.sun.jbi.tools.remote.jmx.host","syshost");
        System.setProperty("com.sun.jbi.tools.remote.jmx.port","8888");
        System.setProperty("com.sun.jbi.tools.remote.jmx.url.property",
        "com.sun.jbi.tools.remote.jmx.url.jmxrmi");

        String sysHost = JMXConnectionProperties.getInstance().getHost();
        String sysPort = JMXConnectionProperties.getInstance().getPort();
        String sysURL = 
        JMXConnectionProperties.getInstance().getConnectionURL(sysHost, sysPort); 
        
        if ( "service:jmx:rmi:///jndi/rmi://syshost:8888/management/rmi-jmx-connector"
        .equals(sysURL) ) { 
            System.out.println("System URL: " + sysURL);
        } else {
            throw new Exception ("In Correct System URL " + sysURL );
        }
        */
        
    }
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestJMXConnectionProperties("test").testConnectionURL();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
