<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/addCompAppVar.jsf -->

<sun:page>

    <!beforeCreate 
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks")
setSessionAttribute(key="addCompAppVarTitleSuffixText", value='#{"binding-component"==sessionScope.sharedRequestType ? "$resource{i18n.jbi.add.comp.app.var.page.title.binding.suffix.text}"  : "$resource{i18n.jbi.add.comp.app.var.page.title.engine.suffix.text}" }')

if (!$session{sharedShowName}) {
    //setSessionAttribute(key="sharedShowName" value="$requestParameter{name}")
    getRequestValue(key="name", value=>$attribute{name});
    setSessionAttribute(key="sharedShowName" value="$attribute{name}")
}
if (!$session{sharedShowType}) {
     //setSessionAttribute(key="sharedShowType" value="$requestParameter{type}")
     getRequestValue(key="type", value=>$attribute{type});
     setSessionAttribute(key="sharedShowType" value="$attribute{type}")
}
if (!$session{jbiSelectedInstanceValue}) {
     //setSessionAttribute(key="jbiSelectedInstanceValue" value="$requestParameter{jbiSelectedInstanceValue}")
     getRequestValue(key="jbiSelectedInstanceValue", value=>$attribute{jbiSelectedInstanceValue});
     setSessionAttribute(key="jbiSelectedInstanceValue" value="$attribute{jbiSelectedInstanceValue}")
}
if (!$session{jbiAddCompAppVarType}) {
    setSessionAttribute(key="jbiAddCompAppVarType" value="[STRING]")
}
    />

    <sun:html>
	 <sun:head id="addCompAppVarHead" />

	 <sun:body>

	     <sun:form id="addCompAppVarBreadcrumbsForm" > 
                <sun:hidden id="helpKey" value="$resource{help.jbi.cluster.addCompAppVar}" />

#include treeBreadcrumbs.inc
#include "jbi/scripts/jbiscripts.js"

             </sun:form>

            <sun:form id="tabsForm">
#include "jbi/cluster/inc/showTabs.inc"
            </sun:form>

#include "jbi/cluster/inc/alertBox.inc"

      <sun:form id="addCompAppVarForm">

                <sun:legend id="legend2" text="$resource{i18n2.required.field.legend}" style="padding-right: 8pt;"/>
                "<br />
                <sun:title id="addCompAppVarPageTitle"
                    title="#{sessionScope.sharedShowName} - $resource{i18n.jbi.add.comp.app.var.title.suffix.text}"
                    helpText="$resource{i18n.jbi.add.comp.app.var.page.help.inline.text}"
                    >
                    <!-- Buttons  -->
                    <!facet pageButtonsTop>
                        <sun:panelGroup id="topButtons">

                            <sun:button 
                                id="topSaveButton" 
                                onClick='javascript: 
                                    if ( guiValidate("#{reqMsg}","#{reqInt}","#{reqPort}"))
                                        submitAndDisable(this, "$resource{i18n2.button.Processing}");
                                    return false; '
                                primary="#{true}"
                                text="$resource{i18n2.button.Save}" 
                                >
                                <!command
jbiAddCompAppVar (instanceName="#{jbiSelectedInstanceValue}",
                  componentName="#{sharedShowName}",
                  compAppVarName="#{sessionScope.jbiAddCompAppVarName}",
                  compAppVarType="#{sessionScope.jbiAddCompAppVarType}",
                  compAppVarValue="#{
'[BOOLEAN]'==sessionScope.jbiAddCompAppVarType 
? sessionScope.jbiAddCompAppVarBooleanValue 
: '[NUMBER]'==sessionScope.jbiAddCompAppVarType 
  ? requestScope.jbiAddCompAppVarNumberValue 
  : '[PASSWORD]'==sessionScope.jbiAddCompAppVarType 
    ? requestScope.jbiAddCompAppVarPasswordValue 
    : requestScope.jbiAddCompAppVarStringValue }",
	          redirectOnSuccess="showCompAppVars.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}",
	          redirectOnFailure="addCompAppVar.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}&tname=#{jbiSelectedInstanceValue}",
                  isAlertNeeded=>$session{isJbiAlertNeeded}, 
                  alertSummary=>$session{jbiAlertSummary}, 
                  alertDetails=>$session{jbiAlertDetails},
                  redirectTo=>$session{redirectTo});
setSessionAttribute(key="jbiAddCompAppVarName" value="")
jbiLogFine(where="addCompAppVar.jsf top save", diagnostic="redirectTo=#{sessionScope.redirectTo}");
redirect(page="#{sessionScope.redirectTo}");
                                />
                            </sun:button>

       	                    <sun:button 
                                id="topCancelButton" 
	                        immediate="#{true}"
                                primary="$boolean{false}" 
                                text="$resource{i18n.jbi.wizard.cancel.button}"
                                >
       			        <!command
setSessionAttribute(key="jbiAddCompAppVarName" value="")
redirect(page="showCompAppVars.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}");
                		/>
                            </sun:button>

                        </sun:panelGroup>
                    </facet>

                </sun:title>
	       
                 <sun:propertySheet id="newVarPs" 
	             rendered="#{true}" >
                     <sun:propertySheetSection id="targetPss"
                         label="$resource{i18n.jbi.add.comp.app.var.pss.target.label}"
                         >

                         <sun:property id="instanceNameProperty" 
                             label="$resource{i18n.jbi.add.comp.app.var.instance.label}"
                             >
                             <sun:staticText 
                                 id="instanceNameText" 
                                 value="#{jbiSelectedInstanceValue}"
                              />
                         </sun:property>

                         <sun:property id="componentNameProperty" 
                             label="$resource{i18n.jbi.add.comp.app.var.component.label}"
                             >
                             <sun:staticText 
                                 id="componentNameText" 
        		         value="#{sessionScope.sharedShowName}"
                              />
                         </sun:property>

                     </sun:propertySheetSection >

                     <sun:propertySheetSection id="varPss"
                         label="$resource{i18n.jbi.add.comp.app.var.pss.var.label}"
                         >

                         <sun:property id="nameProperty" 
                             label="$resource{i18n.jbi.add.comp.app.var.name.label}"
                             >
                             <sun:textField
                                 columns="$int{55}"
                                 id="compAppVarNameText" 
                                 required="$boolean{true}"
				 styleClass="required"
                                 text="#{sessionScope.jbiAddCompAppVarName}"
                                 />

                         </sun:property>

                         <sun:property id="typeProperty" 
                             label="$resource{i18n.jbi.add.comp.app.var.type.label}"
                             >
                               <sun:dropDown id="typeDropDown"
                                   disabled    = "#{false}" 
                                   forgetValue = "#{false}"
                                   labels      = {"$resource{i18n.jbi.show.comp.app.vars.typeIsString}" "$resource{i18n.jbi.show.comp.app.vars.typeIsNumber}" "$resource{i18n.jbi.show.comp.app.vars.typeIsBoolean}" "$resource{i18n.jbi.show.comp.app.vars.typeIsPassword}"}
                                   submitForm  = "#{true}"
                                   selected    = "#{sessionScope.jbiAddCompAppVarType}"
                                   values      = {"[STRING]" "[NUMBER]" "[BOOLEAN]" "[PASSWORD]"} 
                               > <!-- note: labels are I18n, values are not -->
                                   <!command
setAttribute(key="click" value="$this{component}");
setAttribute(key="typeVal" value="#{click.selected}")
setSessionAttribute(key="jbiAddCompAppVarType", value="$attribute{typeVal}" );
getUIComponent (clientId="addCompAppVarForm:newVarPs:varPss:typeProperty:typeDropDown", component=>$attribute{typeDropDown})
getUIComponentProperty (component="$attribute{typeDropDown}", name="value", value=>$attribute{typeDropDownValue})	
getUIComponentProperty (component="$attribute{typeDropDown}", name="values", value=>$attribute{typeDropDownValues})	
redirect(page="addCompAppVar.jsf?type=#{ShowBean.type}&name=#{ShowBean.name}")
                                   />
                               </sun:dropDown>

                         </sun:property>

                         <sun:property id="valueProperty" 
                             label="$resource{i18n.jbi.add.comp.app.var.value.label}"
                             >
                               <sun:dropDown id="valueDropDown"
                                   disabled    = "#{false}" 
                                   forgetValue = "#{false}"
                                   labels      = {"$resource{i18n.jbi.show.comp.app.vars.valueIsFalse}" "$resource{i18n.jbi.show.comp.app.vars.valueIsTrue}" }
                                   rendered="#{'[BOOLEAN]'==sessionScope.jbiAddCompAppVarType}"
                                   submitForm  = "#{false}"
                                   selected    = "#{sessionScope.jbiAddCompAppVarBooleanValue}"
                                   values      = {"FALSE" "TRUE" } 
                               > <!-- note: labels are I18n, values are not -->
                                   <!command
setAttribute(key="click2" value="$this{component}");
setAttribute(key="valueVal" value="#{click2.selected}")
setSessionAttribute(key="jbiAddCompAppVarBooleanValue", value="$attribute{valueVal}" );
getUIComponent (clientId="addCompAppVarForm:newVarPs:varPss:typeProperty:valueDropDown", component=>$attribute{valueDropDown})
getUIComponentProperty (component="$attribute{valueDropDown}", name="value", value=>$attribute{valueDropDownValue})	
getUIComponentProperty (component="$attribute{valueDropDown}", name="values", value=>$attribute{valueDropDownValues})	
                                   />
                             </sun:dropDown>

                             <sun:textField
                                 columns="$int{55}"
                                 id="compAppVarValueNumberText" 
	                         rendered="#{'[NUMBER]'==sessionScope.jbiAddCompAppVarType}"
                                 text="#{requestScope.jbiAddCompAppVarNumberValue}"
                                 />
                             <sun:textField
                                 columns="$int{55}"
                                 id="compAppVarValueStringText" 
	                         rendered="#{'[STRING]'==sessionScope.jbiAddCompAppVarType}"
                                 text="#{requestScope.jbiAddCompAppVarStringValue}"
                                 />
                             <sun:passwordField
                                 columns="$int{55}"
                                 id="compAppVarValuePasswordField" 
	                         rendered="#{'[PASSWORD]'==sessionScope.jbiAddCompAppVarType}"
                                 text="#{requestScope.jbiAddCompAppVarPasswordValue}"
                                 />

                         </sun:property>

                     </sun:propertySheetSection >

                 </sun:propertySheet>
                               
                <sun:title id="title2">
                    <!facet pageButtonsBottom>
                        <sun:panelGroup id="bottomButtons">

                            <sun:button 
                                id="bottomSaveButton" 
                                onClick='javascript: 
                                    if ( guiValidate("#{reqMsg}","#{reqInt}","#{reqPort}"))
                                        submitAndDisable(this, "$resource{i18n2.button.Processing}");
                                    return false; '
                                primary="#{true}"
                                text="$resource{i18n2.button.Save}" 
                                >
                                <!command
jbiAddCompAppVar (instanceName="#{jbiSelectedInstanceValue}",
                  componentName="#{sharedShowName}",
                  compAppVarName="#{sessionScope.jbiAddCompAppVarName}",
                  compAppVarType="#{sessionScope.jbiAddCompAppVarType}",
                  compAppVarValue="#{
'[BOOLEAN]'==sessionScope.jbiAddCompAppVarType 
? sessionScope.jbiAddCompAppVarBooleanValue 
: '[NUMBER]'==sessionScope.jbiAddCompAppVarType 
  ? requestScope.jbiAddCompAppVarNumberValue 
  : '[PASSWORD]'==sessionScope.jbiAddCompAppVarType 
    ? requestScope.jbiAddCompAppVarPasswordValue 
    : requestScope.jbiAddCompAppVarStringValue }",
	          redirectOnSuccess="showCompAppVars.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}",
	          redirectOnFailure="addCompAppVar.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}&tname=#{jbiSelectedInstanceValue}",
                  isAlertNeeded=>$session{isJbiAlertNeeded}, 
                  alertSummary=>$session{jbiAlertSummary}, 
                  alertDetails=>$session{jbiAlertDetails},
                  redirectTo=>$session{redirectTo});
setSessionAttribute(key="jbiAddCompAppVarName" value="")
jbiLogFine(where="addCompAppVar.jsf bottom save", diagnostic="redirectTo=#{sessionScope.redirectTo}");
redirect(page="#{sessionScope.redirectTo}");
                                />
                            </sun:button>
                            <sun:button 
                                id="bottomCancelButton" 
	                        immediate="#{true}"
                                primary="$boolean{false}" 
                                text="$resource{i18n.jbi.wizard.cancel.button}"
                                >
       			        <!command
setSessionAttribute(key="jbiAddCompAppVarName" value="")
redirect(page="showCompAppVars.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}");
                		/>
		                </sun:button>
                        </sun:panelGroup>
                    </facet>
                </sun:title>

             </sun:form>

	 </sun:body> 
     
#include "changeButtonsJS.inc"           

     </sun:html>  
 </sun:page>
