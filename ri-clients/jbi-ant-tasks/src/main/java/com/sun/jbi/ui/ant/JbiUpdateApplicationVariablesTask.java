/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiUpdateApplicationVariablesTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.Properties;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for updating service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiUpdateApplicationVariablesTask extends JbiTargetTask
{
    /**
     * appvariable success msg key
     */
    private static final String APPVARIABLE_SUCCESS_STATUS_KEY = "jbi.ui.ant.set.appvariable.successful";

    /**
     * appvariable failure msg key
     */
    private static final String APPVARIABLE_FAILED_STATUS_KEY = "jbi.ui.ant.set.appvariable.failed";
   
    /**
     * appvariable success msg key
     */
    private static final String APPVARIABLE_PARTIAL_SUCCESS_STATUS_KEY = "jbi.ui.ant.set.appvariable.partial.success";
    
    /** Holds &lt;appvariable> Nested elements */
    private List mAppVariableList = null;
    
    /** Holds appvariibles File **/
    private String mAppVariablesFile = null;

    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /** Getter for property appvariables.
     * @return Value of property appvariables.
     *
     */
    public String getAppVariables()
    {
        return this.mAppVariablesFile;
    }
    
    /**
     * Sets the params file location to the absolute filename of the
     * given file. If the value of this attribute is an absolute path, it
     * is left unchanged (with / and \ characters converted to the
     * current platforms conventions). Otherwise it is taken as a path
     * relative to the project's basedir and expanded.
     * @param appVariablesFile path to set
     */
    
    public void setAppVariables(String appVariablesFile)
    {
        this.mAppVariablesFile = appVariablesFile;
    }
    
    private void debugPrintParams(Properties params) {
        if( params == null ) {
            this.logDebug("Set Configuration params are NULL");
            return;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter out = new PrintWriter(stringWriter);
        params.list(out);
        out.close();
        this.logDebug(stringWriter.getBuffer().toString());
    }
    
    private String createFormatedSuccessJbiResultMessage(String i18nKey, Object[] args) {
        
        String msgCode = getI18NBundle().getMessage(i18nKey + ".ID");
        String msg = getI18NBundle().getMessage(i18nKey, args);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("JBI_ANT_TASK_SET_CONFIG",
            JBIResultXmlBuilder.SUCCESS_RESULT,
            JBIResultXmlBuilder.INFO_MSG_TYPE,
            msgCode, msg, args, null);
        
        JBIManagementMessage mgmtMsg = null;
        mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        return (mgmtMsg != null) ? mgmtMsg.getMessage() : msg ;
    }
    
    private void executeUpdateApplicationVariables(String componentName)
	throws BuildException
    {

        try
        {
	    // Go throught the static/constant configuration paramter elements
            
	    String	target		= getValidTarget();

            if ((componentName == null) || (componentName.compareTo("") == 0))
            {
                String errMsg = createFailedFormattedJbiAdminResult(
                                                "jbi.ui.ant.task.error.nullCompName",
                                                null);
                throw new BuildException(errMsg);
            }

            this.logDebug("Executing Update Application Variables ....");

            // Go throught the appvariable elements
            this.logDebug("Update  application variables, component name: " +
                                                        componentName +
                                                        " target: " +
                                                        target);
            List appVariableList = this.getAppVariableList();
            Properties varsProps = new Properties();

            Iterator itr = appVariableList.iterator();
            while (itr.hasNext())
            {
                AppVariable appVar = (AppVariable) itr.next();
                if ((""+appVar.getName()).compareTo("") != 0)
                {
                    String valueString = null;
                    if ( appVar.getName() != null && !("".equals(appVar.getName()) ) )
                    {
                        if ((appVar.getType() != null) && (appVar.getType().compareTo("") != 0))
                        {
                            valueString = "[" + appVar.getType() + "]" + appVar.getValue();
                        }
                        else
                        {
                            valueString = appVar.getValue();
                        }
                        varsProps.setProperty(appVar.getName(), valueString);
                    }
                }
            }

            String appVariablesFile = this.getAppVariables();
            if ((appVariablesFile != null)  && (appVariablesFile.compareTo("") != 0))
            {
                this.logDebug("the params file is " + appVariablesFile);

                Properties theProp = this.loadParamsFromFile(new File(appVariablesFile));
                // Check for the default type
                /* SortedSet keys = new TreeSet(theProp.keySet());
                for(Object key : keys) {
                    String name = (String)key;
                    String value = theProp.getProperty(name, "");
                    if ((value+"").indexOf("[") < (value+"").indexOf("]"))
                    {
                        // Do nothing
                    }
                    else
                    {
                        value = "[STRING]" + value;
                        theProp.setProperty(name, value);
                    }
                } */

                varsProps.putAll(theProp);
            }

            if (varsProps.size() == 0)
            {
                String msg =
                    createFailedFormattedJbiAdminResult(
                        "jbi.ui.ant.list.no.input.appvariable.data.found",
                        null);
                throw new BuildException(msg,getLocation());
            }

            String rtnXml = this.getJBIAdminCommands().setApplicationVariables(componentName,
                                        target,
                                        varsProps);

            JBIManagementMessage mgmtMsg = JBIManagementMessage.createJBIManagementMessage(rtnXml);
            if ( mgmtMsg.isFailedMsg() )
            {
                throw new Exception(rtnXml);
            }
            else
            {
                // print success message
                printTaskSuccess(mgmtMsg);
            }
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }                
    }

    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {        
        this.logDebug("Executing Set Configuration Task....");
        String compName = getComponentName();
        
       executeUpdateApplicationVariables(compName);
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return APPVARIABLE_FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return APPVARIABLE_SUCCESS_STATUS_KEY;
    }
    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return  APPVARIABLE_PARTIAL_SUCCESS_STATUS_KEY;
    }    
    /**
     * returns nested element list of &lt;appvariable>
     * @return Paramter List
     */
    protected List getAppVariableList()
    {
        if ( this.mAppVariableList == null )
        {
            this.mAppVariableList = new ArrayList();
        }
        return this.mAppVariableList;
    }

    /**
     * load properties from a file
     * @return the Loaded properties
     * @param file file to load
     * @throws BuildException on error
     */
    protected Properties loadParamsFromFile(File file) throws BuildException
    {
        String absFilePath = null;
        String fileName = null;
        if ( file != null )
        {
            absFilePath = file.getAbsolutePath();
            fileName = file.getName();
        }
        if ( file == null || !file.exists() )
        {
            String msg =
                createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.config.params.file.not.exist",
                new Object[] {fileName});
            throw new BuildException(msg,getLocation());
        }
        
        if ( file.isDirectory() )
        {
            String msg =
                createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.config.params.file.is.directory", null);
            throw new BuildException(msg,getLocation());
        }
        
        Properties props = new Properties();
        this.logDebug("Loading " + file.getAbsolutePath());
        try
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                props.load(fis);
            }
            finally
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            return props;
        }
        catch (IOException ex)
        {
            throw new BuildException(ex, getLocation());
        }
    }

    /**
     * factory method for creating the nested element &lt;param>
     * @return Param Object
     */
    public AppVariable createAppVariable()
    {
        AppVariable appVariable = new AppVariable();
        this.getAppVariableList().add(appVariable);
        return appVariable;
    }
}
